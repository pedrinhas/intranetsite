USE [cargos]
GO
/****** Object:  StoredProcedure [dbo].[spGetOrgaosAll]    Script Date: 01/02/2021 16:01:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spGetOrgaosAll]
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT o.id, o.idTipoOrgao, o.idOrgaoParent, tpo.codigo as codigoTipoOrgao, o.nome, o.contato, o.morada, o.codigo, o.grupos, o.codigoRH, o.extensao, o.ativo --just in case
	FROM Orgaos AS o
	LEFT JOIN TiposOrgaos AS tpo ON o.idTipoOrgao=tpo.id
	WHERE o.ativo=1