﻿using IntranetAssemblies.DataAcess;
using System.Collections.Generic;
using System.Web.Http;
using System.Configuration;
using IntranetAssemblies.Models.Intranet;

namespace IIntranet.Controllers
{
    public class IIntranetController : ApiController
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        [HttpGet]
        [Route("GetCategorias")]
        public IHttpActionResult GetCategorias()
        {
            List<Categoria> categoriasList = _intranetService.STP_Categoria_LS();

            if (categoriasList == null)
            {
                return InternalServerError();
            }

            return Ok(categoriasList);
        }

        [HttpGet]
        [Route("GetCategoria")]
        public IHttpActionResult GetCategoria(int id)
        {
            Categoria categoria = _intranetService.STP_Categorias_S(id);

            if (categoria == null)
            {
                return InternalServerError();
            }

            return Ok(categoria);
        }

        [HttpGet]
        [Route("GetCategoriaByCodigo")]
        public IHttpActionResult GetCategoriaByCodigo(string codigo)
        {
            Categoria categoria = _intranetService.STP_Categorias_S_ByCodigo(codigo);

            if (categoria == null)
            {
                return InternalServerError();
            }

            return Ok(categoria);
        }

    }
}