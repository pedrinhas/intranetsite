﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetSite.ViewModels;

using IntranetAssemblies.Models;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Aplicacoes;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using IntranetAssemblies.Models.QuoVadis.Dashboard;
using IntranetAssemblies.Models.QuoVadis.Mensagens;
using IntranetAssemblies.Models.QuoVadis.PortalBase;

namespace IntranetSite.Models
{
    public class Session
    {
        public bool IsInShibbolethLogin { get; set; }

        public int IdUser { get; set; }
        public string NMec { get; set; }
        public Guid IUPI { get; set; }
        public string Username { get; set; }
        public string Nome { get; set; }
        public string FotoHTMLSrc { get; set; }

        public string CodigoCurso { get; set; }
        public string Regime { get; set; }

        public List<Cargo> Cargos { get; set; }
        public List<SessionPerfil> Perfis { get; set; }
        public List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> GruposQuoVadis { get; set; }

        public List<Grupo> Grupos { get; set; }
        public List<Autorizacao> Autorizacoes { get; set; }

        public List<Linguagem> Linguagens { get; set; }
        public Linguagem SelectedLinguagem { get; set; }

        public AplicacaoPortalBase AplicacaoPortalBase { get; set; }

        public List<MenuItem> Contactos { get; set; }
        public List<MenuItem> TiposConteudoMenuLateral { get; set; }
        public List<MenuItem> CategoriasMenuLateral { get; set; }
        public List<MenuItem> MenuLateral { get; set; }
        public List<MenuItem> FavoritosMenuLateral { get; set; }
        public List<MenuItem> AplicacoesPortalBaseMenuLateral { get; set; }

        public List<MenuItem> NoticasMenuLateral { get; set; }
        public List<MenuItem> EventosMenuLateral { get; set; }
        public List<MenuItem> OrgaosMenuLateral { get; set; }
        public List<MenuItem> EstudarMenuLateral { get; set; }

        public Footer Footer { get; set; }

        public List<IndexViewModel> IndexViewModels { get; set; }
        public List<Favorito> Favoritos { get; set; }
        public List<Mensagem> Mensagens { get; set; }

        public List<ConteudoAnexoFile> ConteudoAnexosFotos { get; set; }
        public List<ConteudoAnexoFile> ConteudoAnexosFiles { get; set; }

        public List<Link> ConteudoLinksList { get; set; }

        public Session()
        {
            IsInShibbolethLogin = false;

            IdUser = 0;
            NMec = "";
            IUPI = new Guid();
            Username = "";
            Nome = "";
            FotoHTMLSrc = "";

            CodigoCurso = "";
            Regime = "";

            Cargos = new List<Cargo>();
            Perfis = new List<SessionPerfil>();
            GruposQuoVadis = new List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo>();

            Grupos = new List<Grupo>();
            Autorizacoes = new List<Autorizacao>();

            Linguagens = new List<Linguagem>();
            SelectedLinguagem = new Linguagem();

            AplicacaoPortalBase = new AplicacaoPortalBase();

            Contactos = new List<MenuItem>();
            TiposConteudoMenuLateral = new List<MenuItem>();
            CategoriasMenuLateral = new List<MenuItem>();
            MenuLateral = new List<MenuItem>();
            FavoritosMenuLateral = new List<MenuItem>();
            AplicacoesPortalBaseMenuLateral = new List<MenuItem>();

            NoticasMenuLateral = new List<MenuItem>();
            EventosMenuLateral = new List<MenuItem>();
            OrgaosMenuLateral = new List<MenuItem>();
            EstudarMenuLateral = new List<MenuItem>();

            Footer = new Footer();

            IndexViewModels = new List<IndexViewModel>();
            Favoritos = new List<Favorito>();
            Mensagens = new List<Mensagem>();

            ConteudoAnexosFiles = new List<ConteudoAnexoFile>();
            ConteudoAnexosFotos = new List<ConteudoAnexoFile>();

            ConteudoLinksList = new List<Link>();

        }
    }
}