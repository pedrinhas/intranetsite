using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;

namespace IntranetSite.Helpers
{
    public enum LogType
    {
        Info = 1,
        Error = 2
    }

    public class Logger
    {
        private string loggerDBString;

        public Logger(string _loggerDBString)
        {
            loggerDBString = _loggerDBString;
        }

        public bool LogInfo(Guid? iupiUtilizador, string message)
        {
            return AdicionarLog(LogType.Info, iupiUtilizador, message, null);
        }

        public bool LogError(Guid? iupiUtilizador, string message, string stackTrace)
        {
            return AdicionarLog(LogType.Error, iupiUtilizador, message, stackTrace);
        }

        private bool AdicionarLog(LogType type, Guid? iupiUtilizador, string message, string stackTrace)
        {
            bool success = false;

            try
            {
                if (string.IsNullOrEmpty(message) == false)
                {
                    using (SqlConnection dbConn = new SqlConnection(loggerDBString))
                    {
                        using (SqlCommand dbCommand = new SqlCommand())
                        {
                            dbCommand.CommandText = "nsi_stp_Log_I";
                            dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                            dbCommand.Parameters.Add(new SqlParameter("@_type", type));
                            dbCommand.Parameters.Add(new SqlParameter("@_message", message));
                            dbCommand.Parameters.Add(new SqlParameter("@_stackTrace", stackTrace));

                            if (iupiUtilizador.HasValue == true)
                            {
                                dbCommand.Parameters.Add(new SqlParameter("@_iupiUtilizador", iupiUtilizador));
                            }
                            else
                            {
                                dbCommand.Parameters.Add(new SqlParameter("@_iupiUtilizador", DBNull.Value));
                            }

                            dbCommand.Connection = dbConn;

                            dbConn.Open();

                            dbCommand.ExecuteNonQuery();

                            dbConn.Close();
                        }
                    }
                }
            }
            catch(Exception e)
            {

            }

            return success;
        }
    }
}