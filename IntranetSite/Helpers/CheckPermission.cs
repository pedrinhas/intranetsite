﻿using IntranetSite.App_Start;
using IntranetSite.Clients;
using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IntranetSite.Helpers
{
    public static class CheckPermission
    {
        private static readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        private static readonly CargosClient _cargosClient = new CargosClient();

        public static List<Conteudo> CheckPermissionConteudos(List<Conteudo> conteudos)
        {
            List<Conteudo> conteudosPermission = new List<Conteudo>();

            string username = IntranetSession.Session.Username;

            List<Cargo> CargosList = IntranetSession.Session.Cargos;
            List<SessionPerfil> PerfisList = IntranetSession.Session.Perfis;
            List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> GruposQuoVadisList = IntranetSession.Session.GruposQuoVadis;

            // Primeiro constrói a lista de órgãos aos quais o utilizador tem acesso para criar conteúdos na Intranet
            List<Orgao> orgaosList = new List<Orgao>();

            // Obtém os órgãos respectivos aos cargos do utilizador
            foreach (Cargo cargo in IntranetSession.Session.Cargos)
            {
                orgaosList.Add(_cargosClient.GetOrgao(cargo.IdOrgao));
            }

            // Depois obtém a lista de órgãos nos quais o utilizador faz parte do respectivo grupo de secretariado
            foreach (IntranetAssemblies.Models.QuoVadis.Grupos.Grupo grupoQuoVadis in IntranetSession.Session.GruposQuoVadis)
            {
                List<Orgao> grupoQuoVadisSecretariadoOrgaosList = _cargosClient.GetOrgaosByGrupoSecretariado(grupoQuoVadis.Nome);

                foreach (Orgao grupoQuoVadisSecretariadoOrgao in grupoQuoVadisSecretariadoOrgaosList)
                {
                    if (orgaosList.SingleOrDefault(o => o.Id == grupoQuoVadisSecretariadoOrgao.Id) == default(Orgao))
                    {
                        orgaosList.Add(grupoQuoVadisSecretariadoOrgao);
                    }
                }
            }

            // retornar os conteudos de acordo com as definições de visualiação de cada um
            Parallel.ForEach(conteudos, (conteudo) =>
            {
                bool permisson = false;

                if (conteudo.VisibilidadePublica == true)
                {
                    permisson = true;
                }

                while (!permisson)
                {
                    foreach (var perfil in PerfisList)
                    {
                        if (conteudo.PerfisVisibilidadeList.Contains(perfil.Tipo)) permisson = true;
                    }
                    foreach (var cargo in CargosList)
                    {
                        if (conteudo.TiposCargosVisibilidadeList.Contains(cargo.CodigoTipoCargo)) permisson = true;
                    }

                    foreach (var orgao in orgaosList)
                    {
                        if (conteudo.OrgaosVisibilidadeList.Contains(orgao.Codigo)) permisson = true;
                    }
                    foreach (var grupoQuovadis in GruposQuoVadisList)
                    {
                        if (conteudo.GruposQuoVadisVisibilidadeList.Contains(grupoQuovadis.Nome)) permisson = true;
                    }

                    if (conteudo.PessoasVisibilidadeList.Contains(username)) permisson = true;

                    break;
                }
                if (permisson == true)
                {
                    conteudosPermission.Add(conteudo);
                }
            });

            return conteudosPermission;
        }
    }
}