﻿using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using IntranetSite.Clients;
using IntranetSite.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetSite.Helpers
{
    public static class OrgaoContacto
    {
        private static readonly CargosClient _cargosClient = new CargosClient();

        public static ContactosOrgaosViewModel OrgaoContactoMenu (int id)
        {
            ContactosOrgaosViewModel contactosorgaos = new ContactosOrgaosViewModel();
            List<OrgaosViewModel> orgaosList = new List<OrgaosViewModel>();

            foreach (var item in _cargosClient.GetOrgaosByOrgaoParent(id))
            {
                OrgaosViewModel orgao = new OrgaosViewModel();
                orgao.Orgao = item;
      
                if (_cargosClient.GetOrgaosByOrgaoParent(item.Id).Count() > 0)
                {
                    orgao.HasChild = true;
                }
                orgaosList.Add(orgao);
            }
            contactosorgaos.OrgaoParent = id;
            contactosorgaos.Orgaos = orgaosList;

            int idParent = id;
            while (idParent != 0)
            {
                Orgao orgaoParente = _cargosClient.GetOrgao(idParent);
                var nomeParent = orgaoParente.Nome;
                var idAtual = orgaoParente.Id;
                idParent = orgaoParente.IdOrgaoParent;
                contactosorgaos.OrgaoParentList.Add(new ConteudoPropriedade(nomeParent, idAtual.ToString()));
            }
            contactosorgaos.OrgaoParentList = contactosorgaos.OrgaoParentList.OrderBy(x => x.Valor).ToList();
            double menuItemPadding = 0;
            if (id != 0)
            {
                menuItemPadding = menuItemPadding + (8 * (contactosorgaos.OrgaoParentList.Count()));
            }
            
            contactosorgaos.MenuItemPadding = menuItemPadding;

            return contactosorgaos;
        }
    }
}