using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

using IntranetAssemblies.Models;

namespace IntranetSite.Helpers
{
    public static class HTMLExtender
    {
        public static MvcHtmlString IntranetMenuItem(this HtmlHelper helper, MenuItem menuItem)
        {
            // Cria um novo elemento HTML <a>
            TagBuilder menuItemCreator = new TagBuilder("a");

            // Define em primeiro lugar as propriedades comuns para todos os items do menu lateral
            menuItemCreator.MergeAttribute("name", menuItem.Name);
            menuItemCreator.MergeAttribute("title", menuItem.Toggle);

            menuItemCreator.AddCssClass("list-group-item list-group-item-action");

            if (string.IsNullOrEmpty(menuItem.Icon) == false)
            {
                menuItemCreator.InnerHtml = menuItemCreator.InnerHtml + menuItem.Icon + Environment.NewLine;
            }

            menuItemCreator.InnerHtml = menuItemCreator.InnerHtml + menuItem.Descricao;

            if (menuItem.MenuItemChildList.Count > 0)
            {
                menuItemCreator.MergeAttribute("data-toggle", "collapse");
                menuItemCreator.MergeAttribute("href", "#collapseSidebar-" + menuItem.Id);
            }
            else
            {
                menuItemCreator.MergeAttribute("href", menuItem.Url);

                if (menuItem.UrlIsOpenedInNewTab == true)
                {
                    menuItemCreator.MergeAttribute("target", "_blank");
                }
            }

            if (menuItem.MenuLevel > 0)
            {
                menuItemCreator.AddCssClass("portalbase-sidebar-submenuitem");

                double menuItemPadding = 17.5 + (7.5 * menuItem.MenuLevel);

                menuItemCreator.MergeAttribute("style", "padding-left: " + menuItemPadding + "px;");
            }

            return MvcHtmlString.Create(menuItemCreator.ToString());
        }
    }
}