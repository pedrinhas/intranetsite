﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetSite.Helpers
{
    public static class Enums
    {
        public enum TipoGrupoPortalBase
        {
            NULL,
            GRUPO,
            CARGO,
            PERFIL,
            GRUPOQUOVADIS
        }

        public enum TipoGrupoQuoVadis
        {
            GRUPO,
            GRUPOSISTEMA
        }

        public enum IntranetAutorizacoesAttributeModo
        {
            DEFAULT,
            ALLOWGRUPOS,
            DENYGRUPOS
        }

        public enum ConteudoEstado
        {
            NULL,
            RASCUNHO,
            NAOPUBLICADO,
            PUBLICADO,
            ARQUIVO
        }

        public enum ConteudoAnexoTipo
        {
            NULL,
            FOTO,
            FICHEIRO
        }
    }
}