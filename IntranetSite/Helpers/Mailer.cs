using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net.Mail;

namespace IntranetSite.Helpers
{
    public static class Mailer
    {
        public static bool SendMailFromUTAD(string mailTo, string mailBCC, string mailSubject, string mailBody, List<Attachment> mailAttachmentList)
        {
            bool success = false;

            try
            {
                using (SmtpClient mailSMTP = new SmtpClient("smtp.utad.pt", 25))
                {
                    mailSMTP.DeliveryMethod = SmtpDeliveryMethod.Network;
                    mailSMTP.EnableSsl = false;

                    MailMessage mail = new MailMessage();

                    mail.From = new MailAddress("noreply@utad.pt");
                    mail.To.Add(mailTo);

                    if (string.IsNullOrEmpty(mailBCC) == false)
                    {
                        mail.Bcc.Add(mailBCC);
                    }

                    mail.Subject = mailSubject;
                    mail.Body = mailBody;

                    if (mailAttachmentList != null)
                    {
                        foreach (Attachment mailAttachment in mailAttachmentList)
                        {
                            mail.Attachments.Add(mailAttachment);
                        }
                    }

                    mailSMTP.Send(mail);

                    success = true;
                }
            }
            catch (Exception e)
            {

            }

            return success;
        }
    }
}