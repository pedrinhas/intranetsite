using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;
using System.Security.Cryptography;

namespace IntranetSite.Helpers
{
    public static class PasswordGenerator
    {
        public static string GeneratePassword()
        {
            string password = "";

            string passwordValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

            StringBuilder passwordStringBuilder = new StringBuilder();

            Random passwordRandomChar = new Random();

            for (int i = 0; i < 8; i++)
            {
                int passwordNextRandomChar = passwordRandomChar.Next(passwordValidChars.Length);

                passwordStringBuilder.Append(passwordValidChars[passwordNextRandomChar]);
            }

            password = passwordStringBuilder.ToString();

            return password;
        }

        public static string EncodePassword(string password)
        {
            string encodedPassword = "";

            try
            {
                // Primeiro gera a salt que irá ser usada para fazer hashing à palavra-passe
                byte[] passwordSalt = new byte[16];

                using (RNGCryptoServiceProvider passwordSaltGenerator = new RNGCryptoServiceProvider())
                {
                    passwordSaltGenerator.GetBytes(passwordSalt);
                }

                // Seguidamente, pega na salt gerada e faz hashing à palavra-passe
                Rfc2898DeriveBytes passwordHashingAlgorithm = new Rfc2898DeriveBytes(password, passwordSalt, 10000);

                byte[] hashedPassword = new byte[20];

                hashedPassword = passwordHashingAlgorithm.GetBytes(20);

                // Por fim, pega na salt e na palavra-passe codificada e junta os dois arrays de bytes num único array de bytes
                // Este array de bytes final representa a palavra-passe codificada
                byte[] passwordBytes = new byte[36];

                Array.Copy(passwordSalt, 0, passwordBytes, 0, 16);
                Array.Copy(hashedPassword, 0, passwordBytes, 16, 20);

                encodedPassword = Utilities.EncodeBase64String(passwordBytes);
            }
            catch (Exception e)
            {

            }

            return encodedPassword;
        }
    }
}