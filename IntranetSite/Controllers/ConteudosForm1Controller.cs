﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.IO;

using IntranetSite.App_Start;
using IntranetSite.Filters;
using IntranetSite.ViewModels;

using IntranetAssemblies.Models.Intranet;
using IntranetSite.Clients;
using IntranetAssemblies.Models.QuoVadis.Cargos;

namespace IntranetSite.Controllers
{
    public class ConteudosForm1Controller : ConteudosFormsController
    {
        private readonly CargosClient _cargosClient = new CargosClient();


        public ActionResult LoadDetailsConteudoPartial(int id)
        {
            DetailsConteudoPartialViewModel detailsConteudoPartialViewModel = new DetailsConteudoPartialViewModel();

            detailsConteudoPartialViewModel.Conteudo = _intranetService.STP_Conteudos_S(id);

            if (detailsConteudoPartialViewModel.Conteudo != null && detailsConteudoPartialViewModel.Conteudo.Id > 0)
            {
                //Preencher Juri (depende do tipo de prova pública)
                List<Juri> juris = new List<Juri>();
                juris = _intranetService.STP_Juris_S_ByConteudo(detailsConteudoPartialViewModel.Conteudo.Id);

                detailsConteudoPartialViewModel.Conteudo.Presidente = new Juri();
                detailsConteudoPartialViewModel.Conteudo.Presidente = juris.Where(x => x.Numero == 0).FirstOrDefault();
                detailsConteudoPartialViewModel.Conteudo.Vogais = new Vogais();
                detailsConteudoPartialViewModel.Conteudo.Vogais.VogaisList = juris.Where(x => x.Numero != 0).ToList();


                detailsConteudoPartialViewModel.ConteudoAnexosList = _intranetService.STP_ConteudosAnexos_S_ByConteudo(id);
                detailsConteudoPartialViewModel.ConteudoEstado = GetConteudoEstado(detailsConteudoPartialViewModel.Conteudo.Estado);
                detailsConteudoPartialViewModel.ConteudoVisibilidade = GetConteudoVisibilidade(detailsConteudoPartialViewModel.Conteudo.VisibilidadePublica);
            }

            return PartialView("DetailsConteudoPartial", detailsConteudoPartialViewModel);
        }

       
        

        [IntranetLoggedIn]
        public ActionResult CreateConteudo(int idTipoConteudo, int idOrgao)
        {
            // Antes de tudo, limpa da sessão do browser quaisquer dados de conteúdos antigos que foram criados/editados
            IntranetSession.ClearConteudoSession();

            CreateEditConteudoViewModel createConteudoViewModel = new CreateEditConteudoViewModel();

            // Obtém, a partir dos respectivos ID's, todos os dados do tipo de conteúdo e do órgão do novo conteúdo
            TipoConteudo tipoConteudo = _cargosClient.GetTipoConteudo(idTipoConteudo);

            // Associa logo ao novo conteúdo os IDs do tipo de conteúdo e da unidade orgânica seleccionados
            createConteudoViewModel.Conteudo.IdTipoConteudo = idTipoConteudo;
            createConteudoViewModel.Conteudo.NomeTipoConteudo = tipoConteudo.Nome;
            createConteudoViewModel.Conteudo.IdOrgao = idOrgao;

            //Define a data de inicio de publicação por defeito para hoje
            createConteudoViewModel.Conteudo.DataInicioPublicacao = DateTime.Now;

            //Preencher as labels que são necessárias preencher no formulário
            createConteudoViewModel.Conteudo.LabelLink1 = "Link para Transmissão";
            createConteudoViewModel.Conteudo.LabelLink2 = "E-mail do Candidato";
            createConteudoViewModel.Conteudo.LabelTitulo1 = "Sala";
            createConteudoViewModel.Conteudo.LabelValor1 = "Número máximo de vogais";
            createConteudoViewModel.Conteudo.LabelValor2 = "Número mínimo de vogais";
            createConteudoViewModel.Conteudo.LabelValor3 = "Número de vogais";

            //Preencher Juri (depende do tipo de prova pública)
            createConteudoViewModel.Conteudo.Presidente = new Juri();
            createConteudoViewModel.Conteudo.Vogais = new Vogais();

            //Prova pública de mestrado
            if (idTipoConteudo == 4)
            {
                createConteudoViewModel.Conteudo.Vogais.MaxVogais = 7;
                createConteudoViewModel.Conteudo.Vogais.MinVogais = 3;
                createConteudoViewModel.Conteudo.Vogais.NVogais = 3;

                for (int i = 0; i < createConteudoViewModel.Conteudo.Vogais.MaxVogais; i++)
                {
                    Juri juri = new Juri();
                    juri.Tipo = 1;
                    juri.Numero = i + 1;
                    createConteudoViewModel.Conteudo.Vogais.VogaisList.Add(juri);
                }

            }//Prova pública de doutoramento
            if (idTipoConteudo == 7)
            {
                createConteudoViewModel.Conteudo.Vogais.MaxVogais = 8;
                createConteudoViewModel.Conteudo.Vogais.MinVogais = 4;
                createConteudoViewModel.Conteudo.Vogais.NVogais = 4;

                for (int i = 0; i < createConteudoViewModel.Conteudo.Vogais.MaxVogais; i++)
                {
                    Juri juri = new Juri();
                    juri.Tipo = 1;
                    juri.Numero = i + 1;
                    createConteudoViewModel.Conteudo.Vogais.VogaisList.Add(juri);
                }

            }

            //Prova pública de agregação
            if (idTipoConteudo == 8)
            {
                createConteudoViewModel.Conteudo.Vogais.MaxVogais = 8;
                createConteudoViewModel.Conteudo.Vogais.MinVogais = 5;
                createConteudoViewModel.Conteudo.Vogais.NVogais = 5;

                for (int i = 0; i < createConteudoViewModel.Conteudo.Vogais.MaxVogais; i++)
                {
                    Juri juri = new Juri();
                    juri.Tipo = 1;
                    juri.Numero = i + 1;
                    createConteudoViewModel.Conteudo.Vogais.VogaisList.Add(juri);
                }

            }


            RefreshCreateEditConteudoViewModel(createConteudoViewModel);

            return View(createConteudoViewModel);
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult CreateConteudo(CreateEditConteudoViewModel createConteudoViewModel, Vogais vogais)
        {
            createConteudoViewModel.Conteudo.Vogais = vogais;
            createConteudoViewModel.Conteudo.Valor1 = vogais.MaxVogais;
            createConteudoViewModel.Conteudo.Valor2 = vogais.MinVogais;
            createConteudoViewModel.Conteudo.Valor3 = vogais.NVogais;

            if (string.IsNullOrEmpty(createConteudoViewModel.Conteudo.Titulo) == true)
            {
                ModelState.AddModelError("Conteudo.Titulo", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.DataInicioPublicacao == null || createConteudoViewModel.Conteudo.DataInicioPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicioPublicacao", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.DataFimPublicacao != null && createConteudoViewModel.Conteudo.DataFimPublicacao != new DateTime())
            {
                if (createConteudoViewModel.Conteudo.DataFimPublicacao < createConteudoViewModel.Conteudo.DataInicioPublicacao)
                {
                    ModelState.AddModelError("Conteudo.DataFimPublicacao", "A data de fim de publicação tem de ser superior à de início");
                }
            }

            if (createConteudoViewModel.Conteudo.DataInicio == null || createConteudoViewModel.Conteudo.DataInicio == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicio", "Tem de preencher este campo!");
            }

            if (string.IsNullOrEmpty(createConteudoViewModel.Conteudo.Link1) == true)
            {
                ModelState.AddModelError("Conteudo.Link1", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.Link1 != null && createConteudoViewModel.Conteudo.Link1 != "")
            {
                Uri uri = null;

                if (!Uri.TryCreate(createConteudoViewModel.Conteudo.Link1, UriKind.Absolute, out uri) || null == uri)
                {
                    //URL inválida
                    ModelState.AddModelError("Conteudo.Link1", "Tem de preencher um url válido!");
                }
            }

            if (IntranetSession.Session.ConteudoAnexosFiles.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            {
                ModelState.AddModelError("ConteudoAnexoFile", "Tem de escolher um ficheiro para o edital!");
            }

            if (string.IsNullOrEmpty(createConteudoViewModel.Conteudo.Username) == true || string.IsNullOrEmpty(createConteudoViewModel.Conteudo.Nome) == true)
            {
                if (createConteudoViewModel.Conteudo.IdTipoConteudo == 8)

                    ModelState.AddModelError("Conteudo.Username", "Tem de introduzir um nome de utilizador válido!");

                if (createConteudoViewModel.Conteudo.IdTipoConteudo == 7 || createConteudoViewModel.Conteudo.IdTipoConteudo == 4)
                    ModelState.AddModelError("Conteudo.Username", "Tem de introduzir um número mecanográfico (alXXXX) válido!");
            }
            else
            {
                //Adicionar restições para o presidente e vogais
                if (createConteudoViewModel.Conteudo.IdTipoConteudo == 8 && createConteudoViewModel.Conteudo.IsForEstudante)
                    ModelState.AddModelError("Conteudo.Username", "Tem de introduzir um nome de utilizador válido!");

                if ((createConteudoViewModel.Conteudo.IdTipoConteudo == 7 || createConteudoViewModel.Conteudo.IdTipoConteudo == 4)&& !createConteudoViewModel.Conteudo.IsForEstudante)
                    ModelState.AddModelError("Conteudo.Username", "Tem de introduzir um nome de utilizador válido!");

                if (createConteudoViewModel.Conteudo.Vogais.VogaisList.Count(x => x.Nome != "" && x.Instituicao != "") < createConteudoViewModel.Conteudo.Vogais.MinVogais)
                {
                    string texto = string.Concat("Tem de introduzir no mínimo ", createConteudoViewModel.Conteudo.Vogais.MinVogais, " Vogais!");
                    ModelState.AddModelError("AdiconarVolgal", texto);
                }
                if(string.IsNullOrEmpty(createConteudoViewModel.Conteudo.Presidente.Nome) || string.IsNullOrEmpty(createConteudoViewModel.Conteudo.Presidente.Instituicao))
                {
                    ModelState.AddModelError("Conteudo.Presidente.Username", "Tem de introduzir um nome de utilizador válido!" );
                }
            }

            if (ModelState.IsValid == true)
            {
                createConteudoViewModel.Conteudo.Vogais.NVogais = createConteudoViewModel.Conteudo.Vogais.VogaisList.Count(x => x.Nome != "" && x.Instituicao != "");

                int? id = CreateConteudo(createConteudoViewModel.Conteudo);

                if (id != null && id > 0)
                {
                    TempData["success"] = "Dados submetidos com êxito!";

                    return RedirectToAction("EditConteudo", new { id = id });
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditConteudoViewModel(createConteudoViewModel);

            return View(createConteudoViewModel);
        }

        [IntranetLoggedIn]
        public ActionResult EditConteudo(int id)
        {
            // Antes de tudo, limpa da sessão do browser quaisquer dados de conteúdos antigos que foram criados/editados
            IntranetSession.ClearConteudoSession();

            CreateEditConteudoViewModel editConteudoViewModel = new CreateEditConteudoViewModel();

            editConteudoViewModel.Conteudo = _intranetService.STP_Conteudos_S(id);

            if (editConteudoViewModel.Conteudo != null && editConteudoViewModel.Conteudo.Id > 0)
            {
                //Preencher Juri (depende do tipo de prova pública)
                List<Juri> juris = new List<Juri>();
                juris = _intranetService.STP_Juris_S_ByConteudo(editConteudoViewModel.Conteudo.Id);

                editConteudoViewModel.Conteudo.Presidente = new Juri();
                editConteudoViewModel.Conteudo.Presidente = juris.Where(x => x.Numero == 0).FirstOrDefault();
                editConteudoViewModel.Conteudo.Vogais = new Vogais();
                editConteudoViewModel.Conteudo.Vogais.VogaisList = juris.Where(x=>x.Numero!=0).ToList();

                //Prova pública de mestrado
                if (editConteudoViewModel.Conteudo.IdTipoConteudo == 4)
                {
                    editConteudoViewModel.Conteudo.Vogais.MaxVogais = Convert.ToInt32(editConteudoViewModel.Conteudo.Valor1);
                    editConteudoViewModel.Conteudo.Vogais.MinVogais = Convert.ToInt32(editConteudoViewModel.Conteudo.Valor2);
                    editConteudoViewModel.Conteudo.Vogais.NVogais = Convert.ToInt32(editConteudoViewModel.Conteudo.Valor3);

                    for (int i= editConteudoViewModel.Conteudo.Vogais.NVogais; i <= editConteudoViewModel.Conteudo.Vogais.MaxVogais; i++)
                    {
                        Juri juri = new Juri();
                        juri.Tipo = 1;
                        juri.Numero = i+1;
                        editConteudoViewModel.Conteudo.Vogais.VogaisList.Add(juri);
                    }

                }//Prova pública de doutoramento
                if (editConteudoViewModel.Conteudo.IdTipoConteudo == 7)
                {
                    editConteudoViewModel.Conteudo.Vogais.MaxVogais = Convert.ToInt32(editConteudoViewModel.Conteudo.Valor1);
                    editConteudoViewModel.Conteudo.Vogais.MinVogais = Convert.ToInt32(editConteudoViewModel.Conteudo.Valor2);
                    editConteudoViewModel.Conteudo.Vogais.NVogais = Convert.ToInt32(editConteudoViewModel.Conteudo.Valor3);

                    for (int i = editConteudoViewModel.Conteudo.Vogais.NVogais; i <= editConteudoViewModel.Conteudo.Vogais.MaxVogais; i++)
                    {
                        Juri juri = new Juri();
                        juri.Tipo = 1;
                        juri.Numero = i+1;
                        editConteudoViewModel.Conteudo.Vogais.VogaisList.Add(juri);
                    }

                }

                //Prova pública de agregação
                if (editConteudoViewModel.Conteudo.IdTipoConteudo == 8)
                {
                    editConteudoViewModel.Conteudo.Vogais.MaxVogais = Convert.ToInt32(editConteudoViewModel.Conteudo.Valor1);
                    editConteudoViewModel.Conteudo.Vogais.MinVogais = Convert.ToInt32(editConteudoViewModel.Conteudo.Valor2);
                    editConteudoViewModel.Conteudo.Vogais.NVogais = Convert.ToInt32(editConteudoViewModel.Conteudo.Valor3);

                    for (int i= editConteudoViewModel.Conteudo.Vogais.NVogais; i <= editConteudoViewModel.Conteudo.Vogais.MaxVogais; i++)
                    {
                        Juri juri = new Juri();
                        juri.Tipo = 1;
                        juri.Numero = i+1;
                        editConteudoViewModel.Conteudo.Vogais.VogaisList.Add(juri);
                    }

                }

                RefreshCreateEditConteudoViewModel(editConteudoViewModel);

                // Com os dados do conteúdo obtidos, passa para a obtenção dos dados dos anexos do conteúdo
                List<ConteudoAnexo> conteudoAnexosList = _intranetService.STP_ConteudosAnexos_S_ByConteudo(id);

                if (conteudoAnexosList != null && conteudoAnexosList.Count > 0)
                {
                    try
                    {
                        // Através dos dados do conteúdo, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                        Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                        string conteudoAnexosFilesFolderPath = intranetFolderPath.Valor + editConteudoViewModel.Conteudo.CodigoOrgao + "-" + editConteudoViewModel.Conteudo.DataCriacao.Year + "-" + editConteudoViewModel.Conteudo.DataCriacao.Month + "-" + editConteudoViewModel.Conteudo.DataCriacao.Day;

                        // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                        if (Directory.Exists(conteudoAnexosFilesFolderPath) == true)
                        {
                            foreach (ConteudoAnexo conteudoAnexo in conteudoAnexosList)
                            {
                                // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                                if (System.IO.File.Exists(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexo.Anexo) == true)
                                {
                                    // Se o ficheiro existir, guarda os dados do ficheiro na sessão do browser
                                    // Na view, o plugin Dropzone.JS vai ler estes dados para apresentar os ficheiros que se encontram anexados ao conteúdo
                                    ConteudoAnexoFile conteudoAnexoFile = new ConteudoAnexoFile();

                                    FileInfo conteudoAnexoFileInfo = new FileInfo(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexo.Anexo);

                                    conteudoAnexoFile.Id = conteudoAnexo.Id;
                                    conteudoAnexoFile.Name = conteudoAnexo.Anexo;
                                    conteudoAnexoFile.Size = conteudoAnexoFileInfo.Length;

                                    IntranetSession.Session.ConteudoAnexosFiles.Add(conteudoAnexoFile);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // TO DO: Fazer log de quaisquer excepções que ocorram
                    }
                }
            }

            return View(editConteudoViewModel);
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult EditConteudo(int id, CreateEditConteudoViewModel editConteudoViewModel, Vogais vogais)
        {
            editConteudoViewModel.Conteudo.Vogais = vogais;
            editConteudoViewModel.Conteudo.Valor1 = vogais.MaxVogais;
            editConteudoViewModel.Conteudo.Valor2 = vogais.MinVogais;
            editConteudoViewModel.Conteudo.Valor3 = vogais.NVogais;


            if (string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Titulo) == true)
            {
                ModelState.AddModelError("Conteudo.Titulo", "Tem de preencher este campo!");
            }
            if (editConteudoViewModel.Conteudo.DataInicioPublicacao == null || editConteudoViewModel.Conteudo.DataInicioPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicioPublicacao", "Tem de preencher este campo!");
            }

            if (editConteudoViewModel.Conteudo.DataFimPublicacao != null && editConteudoViewModel.Conteudo.DataFimPublicacao != new DateTime())
            {
                if (editConteudoViewModel.Conteudo.DataFimPublicacao < editConteudoViewModel.Conteudo.DataInicioPublicacao)
                {
                    ModelState.AddModelError("Conteudo.DataFimPublicacao", "A data de fim de publicação tem de ser superior à de início");
                }
            }

            if (editConteudoViewModel.Conteudo.DataInicio == null || editConteudoViewModel.Conteudo.DataInicio == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicio", "Tem de preencher este campo!");
            }

            if (string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Link1) == true)
            {
                ModelState.AddModelError("Conteudo.Link1", "Tem de preencher este campo!");
            }
            if (editConteudoViewModel.Conteudo.Link1 != null && editConteudoViewModel.Conteudo.Link1 != "")
            {
                Uri uri = null;

                if (!Uri.TryCreate(editConteudoViewModel.Conteudo.Link1, UriKind.Absolute, out uri) || null == uri)
                {
                    //URL inválida
                    ModelState.AddModelError("Conteudo.Link1", "Tem de preencher um url válido!");
                }
            }

            if (IntranetSession.Session.ConteudoAnexosFiles.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            {
                ModelState.AddModelError("ConteudoAnexoFile", "Tem de escolher um ficheiro para o edital!");
            }

            if (string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Username) == true || string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Nome) == true)
            {
                if(editConteudoViewModel.Conteudo.IdTipoConteudo==8)

                    ModelState.AddModelError("Conteudo.Username", "Tem de introduzir um nome de utilizador válido!");

                if (editConteudoViewModel.Conteudo.IdTipoConteudo == 7 || editConteudoViewModel.Conteudo.IdTipoConteudo == 4)
                    ModelState.AddModelError("Conteudo.Username", "Tem de introduzir um número mecanográfico (alXXXX) válido!");
            }
            else
            {
                //Adicionar restições para o presidente e vogais
                if (editConteudoViewModel.Conteudo.IdTipoConteudo == 8 && editConteudoViewModel.Conteudo.IsForEstudante)
                    ModelState.AddModelError("Conteudo.Username", "Tem de introduzir um nome de utilizador válido!");

                if ((editConteudoViewModel.Conteudo.IdTipoConteudo == 7 || editConteudoViewModel.Conteudo.IdTipoConteudo == 4) && !editConteudoViewModel.Conteudo.IsForEstudante)
                    ModelState.AddModelError("Conteudo.Username", "Tem de introduzir um nome de utilizador válido!");

                if (editConteudoViewModel.Conteudo.Vogais.VogaisList.Count(x => x.Nome != "" && x.Instituicao != "") < editConteudoViewModel.Conteudo.Vogais.MinVogais)
                {
                    string texto = string.Concat("Tem de introduzir no mínimo ", editConteudoViewModel.Conteudo.Vogais.MinVogais, " Vogais!");
                    ModelState.AddModelError("AdiconarVolgal", texto);
                }
                if (string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Presidente.Nome) || string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Presidente.Instituicao))
                {
                    ModelState.AddModelError("Conteudo.Presidente.Username", "Tem de introduzir um nome de utilizador válido!");
                }
            }


            if (ModelState.IsValid == true)
            {
                editConteudoViewModel.Conteudo.Valor3 = editConteudoViewModel.Conteudo.Vogais.VogaisList.Count(x => x.Nome != "" && x.Instituicao != "");

                if (UpdateConteudo(id, editConteudoViewModel.Conteudo) == true)
                {
                    TempData["success"] = "Dados submetidos com êxito!";

                    return RedirectToAction("EditConteudo", new { id = id });
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditConteudoViewModel(editConteudoViewModel);

            return View(editConteudoViewModel);
        }

        private void RefreshCreateEditConteudoViewModel(CreateEditConteudoViewModel createEditConteudoViewModel)
        {
            createEditConteudoViewModel.ConteudoEstadosList = GetConteudoEstados();
            createEditConteudoViewModel.ConteudoVisibilidadesList = GetConteudoVisibilidades();
            createEditConteudoViewModel.PerfisVisibilidadeList = GetPerfisVisibilidade();
            createEditConteudoViewModel.TiposCargosVisibilidadeList = GetTiposCargosVisibilidade();
            createEditConteudoViewModel.OrgaosVisibilidadeList = GetOrgaosVisibilidade();
            createEditConteudoViewModel.GruposQuoVadisVisibilidadeList = GetGruposQuoVadisVisibilidade();
            createEditConteudoViewModel.CategoriasList = GetGategorias();
            List<ConteudoCategoria> conteudoCategorias = _intranetService.STP_ConteudosCategorias_S_ByConteudo(createEditConteudoViewModel.Conteudo.Id);
            foreach (var item in conteudoCategorias)
            {
                createEditConteudoViewModel.Categorias.Add(item.IdCategoria.ToString());
            }
        }
    }
}