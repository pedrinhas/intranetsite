using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IntranetSite.Controllers
{
    public class SecureController : BaseController
    {
        public ActionResult Index()
        {
            if (Request.ServerVariables["HTTP_REMOTEUSER"] != null)
            {
                if (Session["erroAutenticacao"] != null)
                {
                    TempData["error"] = Session["erroAutenticacao"].ToString();
                }
                else
                {
                    Session["shib"] = Request.ServerVariables["HTTP_REMOTEUSER"].ToString();

                    if (Session["returnURL"] != null)
                    {
                        return Redirect(Session["returnURL"].ToString());
                    }
                }

                return RedirectToAction("Index", "Home");
            }

            return View();
        }
    }
}