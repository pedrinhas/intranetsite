﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using System.IO;

using IntranetSite.App_Start;
using IntranetSite.Clients;
using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using IntranetAssemblies.Models.QuoVadis.RCU;
using System.Threading.Tasks;

namespace IntranetSite.Controllers
{
    public abstract partial class ConteudosFormsController : BaseController
    {
        protected readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        private readonly CargosClient _cargosClient = new CargosClient();
        private readonly GruposClient _gruposClient = new GruposClient();
        private readonly RCUClient _rcuClient = new RCUClient();

        protected int? CreateConteudo(Conteudo conteudo)
        {
            int? id = 0;

            // Obtém, a partir dos respectivos ID's, todos os dados do tipo de conteúdo e do órgão do novo conteúdo
            TipoConteudo tipoConteudo = _cargosClient.GetTipoConteudo(conteudo.IdTipoConteudo);
            Orgao orgao = _cargosClient.GetOrgao(conteudo.IdOrgao);

            // Gera um IUPI para o novo conteúdo, e define a data de criação
            Guid conteudoIUPI = Guid.NewGuid();
            DateTime conteudoDataCriacao = DateTime.Now;

            conteudo.IUPI = conteudoIUPI;
            conteudo.UsernameCriacao = IntranetSession.Session.Username;
            conteudo.DataCriacao = conteudoDataCriacao;
            conteudo.Estado = Convert.ToInt32(Enums.ConteudoEstado.RASCUNHO);
            conteudo.NomeTipoConteudo = tipoConteudo.Nome;
            conteudo.NomeOrgao = orgao.Nome;
            conteudo.CodigoOrgao = orgao.Codigo;

            //Verifica se a data de fim de publicação vem preenchida, senão atribui a data máxima
            if (conteudo.DataFimPublicacao == null || conteudo.DataFimPublicacao == new DateTime())
            {
                conteudo.DataFimPublicacao = DateTime.MaxValue;
            }

            // Insere o novo conteúdo na base de dados
            // Se a inserção correr bem retorna um ID válido, e passa para a criação dos anexos do novo conteúdo
            id = _intranetService.STP_Conteudos_I(conteudo);

            if (id != null && id.Value > 0)
            {
                //Caso tenho presidente e vogais insera na base de dados
                if (conteudo.Vogais != null && conteudo.Presidente != null)
                {
                    conteudo.Presidente.IdConteudo = id.Value;
                    _intranetService.STP_Juris_I(conteudo.Presidente);
                    int numero = 1;
                    foreach (var vogal in conteudo.Vogais.VogaisList)
                    {
                        //Só os vogais que foram preenchidos
                        vogal.IdConteudo = id.Value;
                        if (vogal.Nome != null && vogal.Instituicao != "" && vogal.Instituicao != null && vogal.Instituicao != "")
                        {
                            vogal.Numero = numero;
                            _intranetService.STP_Juris_I(vogal);
                            //Atualiza o numero de voagais
                            numero += 1;
                        }

                    }
                }
                //Caso tenha logins de pessoas associados grava na DB
                if (conteudo.Pessoas != null)
                {
                    foreach (var pessoa in conteudo.Pessoas)
                    {
                        //Guarda os logins numa tabela separada
                        pessoa.IdConteudo = id.Value;
                        _intranetService.STP_Pessoas_I(pessoa);
                    }
                }

                try
                {
                    // Em primeiro lugar, gera o caminho da pasta que irá armazenar os ficheiros que irão ser anexados ao novo conteúdo
                    Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                    string conteudoAnexosFilesFolderPath = intranetFolderPath.Valor + orgao.Codigo + "-" + conteudoDataCriacao.Year + "-" + conteudoDataCriacao.Month + "-" + conteudoDataCriacao.Day;

                    // Com o caminho anteriormente gerado, cria a pasta e verifica depois se ela foi criada com sucesso
                    Directory.CreateDirectory(conteudoAnexosFilesFolderPath);

                    if (Directory.Exists(conteudoAnexosFilesFolderPath) == true)
                    {
                        //Adiconar fotos se existirem
                        if (IntranetSession.Session.ConteudoAnexosFotos != null)
                            IntranetSession.Session.ConteudoAnexosFiles.AddRange(IntranetSession.Session.ConteudoAnexosFotos);


                        bool success = true;

                        // Processa cada ficheiro previamente seleccionado pelo utilizador, presente na sessão do browser
                        foreach (ConteudoAnexoFile conteudoAnexoFile in IntranetSession.Session.ConteudoAnexosFiles.Where(caf => caf.WasDeletedByUser == false))
                        {
                            if (success == true)
                            {
                                // Limpa o nome do ficheiro de quaisquer caracteres inválidos
                                string conteudoAnexoFileCleanName = Utilities.CleanFileName(conteudoAnexoFile.Name);

                                // Guarda o ficheiro no disco do servidor
                                System.IO.File.WriteAllBytes(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexoFileCleanName, conteudoAnexoFile.Content);

                                // Anexa o ficheiro ao novo conteúdo, registando para isso um novo anexo na base de dados
                                ConteudoAnexo conteudoAnexo = new ConteudoAnexo();

                                conteudoAnexo.IUPI = Guid.NewGuid();
                                conteudoAnexo.IdConteudo = id.Value;
                                conteudoAnexo.IUPIConteudo = conteudoIUPI;
                                conteudoAnexo.Anexo = conteudoAnexoFileCleanName;

                                //Verficar tipo de anexo (Foto ou Ficheiro)
                                if (conteudoAnexoFile.ContentType.StartsWith("image"))
                                    conteudoAnexo.Tipo = Convert.ToInt32(Enums.ConteudoAnexoTipo.FOTO);
                                else
                                    conteudoAnexo.Tipo = Convert.ToInt32(Enums.ConteudoAnexoTipo.FICHEIRO);

                                if (_intranetService.STP_ConteudosAnexos_I(conteudoAnexo) <= 0)
                                {
                                    success = false;
                                }
                            }
                        }

                        if (success == false)
                        {
                            id = 0;
                        }
                    }
                }
                catch (Exception e)
                {
                    // TO DO: Fazer log de quaisquer excepções que ocorram

                    return null;
                }
            }

            return id;
        }

        protected bool? UpdateConteudo(int id, Conteudo conteudo)
        {
            bool success = false;

            string conteudoUsernameAlteracao = IntranetSession.Session.Username;
            DateTime conteudoDataAlteracao = DateTime.Now;

            // Antes de atualizar o conteúdo, faz uma cópia do conteúdo original para a tabela de histórico
            if (_intranetService.STP_ConteudosHistorico_I(id, conteudoUsernameAlteracao, conteudoDataAlteracao) > 0)
            {
                conteudo.UsernameAlteracao = conteudoUsernameAlteracao;
                conteudo.DataAlteracao = conteudoDataAlteracao;

                // Atualiza o conteúdo na base de dados
                // Se a atualização correr bem passa para a atualização dos anexos do conteúdo
                if (_intranetService.STP_Conteudos_U(conteudo) > 0)
                {

                    //Caso tenho presidente e vogais insera na base de dados
                    if (conteudo.Vogais != null && conteudo.Presidente != null)
                    {
                        _intranetService.STP_Juris_U(conteudo.Presidente);
                        int numero = 1;
                        foreach (var vogal in conteudo.Vogais.VogaisList)
                        {
                            //Só os vogais que foram preenchidos
                            vogal.IdConteudo = conteudo.Id;
                            if (vogal.Id != 0)
                            {
                                //Atualiza os vogais 
                                if (vogal.Nome != null && vogal.Instituicao != "" && vogal.Instituicao != null && vogal.Instituicao != "")
                                {
                                    vogal.Numero = numero;
                                    _intranetService.STP_Juris_U(vogal);
                                    numero += 1;
                                }
                                else
                                {
                                    //Vogais que foram apagados
                                    _intranetService.STP_Juris_D(vogal.Id);
                                }
                            }
                            else
                            {
                                //Vogais que foram adicionados
                                if (vogal.Nome != null && vogal.Instituicao != "" && vogal.Instituicao != null && vogal.Instituicao != "")
                                {
                                    vogal.Numero = numero;
                                    _intranetService.STP_Juris_I(vogal);
                                    numero += 1;
                                }
                            }

                        }

                    }

                    //Caso tenha pessoas associadas
                    if (conteudo.Pessoas != null)
                    {
                        List<Pessoas> pessoas = _intranetService.STP_Pessoas_S_ByConteudo(conteudo.Id);

                        //Atualiza e insere as novas 
                        foreach (var pessoa in conteudo.Pessoas)
                        {
                            pessoa.IdConteudo = conteudo.Id;
                            if (!pessoas.Exists(x => x.Username == pessoa.Username))

                                _intranetService.STP_Pessoas_I(pessoa);

                        }
                        //Elimina da BD os que foram apagados
                        foreach (var item in pessoas)
                        {
                            if (!conteudo.Pessoas.Exists(x => x.Username == item.Username))
                            {
                                _intranetService.STP_Pessoas_D(item.Id);
                            }
                        }
                    }

                    try
                    {
                        // Através dos dados do conteúdo, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                        Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                        string conteudoAnexosFilesFolderPath = intranetFolderPath.Valor + conteudo.CodigoOrgao + "-" + conteudo.DataCriacao.Year + "-" + conteudo.DataCriacao.Month + "-" + conteudo.DataCriacao.Day;

                        if (Directory.Exists(conteudoAnexosFilesFolderPath) == true)
                        {
                            success = true;

                            //Adiconar fotos se existirem
                            if (IntranetSession.Session.ConteudoAnexosFotos != null)
                                IntranetSession.Session.ConteudoAnexosFiles.AddRange(IntranetSession.Session.ConteudoAnexosFotos);

                            // Processa cada ficheiro, seleccionado ou não pelo utilizador, que se encontra presente na sessão do browser
                            foreach (ConteudoAnexoFile conteudoAnexoFile in IntranetSession.Session.ConteudoAnexosFiles)
                            {
                                if (success == true)
                                {
                                    if (conteudoAnexoFile.WasDeletedByUser == true)
                                    {
                                        // Verifica se o conteúdo (em bytes) do ficheiro se encontra guardado na sessão do browser
                                        // Se não se encontra guardado, significa que este ficheiro encontrava-se anexado ao conteúdo, e que precisa de ser eliminado
                                        if (conteudoAnexoFile.Content == null)
                                        {
                                            // Elimina o ficheiro do disco
                                            System.IO.File.Delete(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexoFile.Name);

                                            // Depois apaga o registo do anexo presente na base de dados
                                            if (_intranetService.STP_ConteudosAnexos_D(conteudoAnexoFile.Id) != 1)
                                            {
                                                success = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // Verifica se o conteúdo (em bytes) do ficheiro se encontra guardado na sessão do browser
                                        // Se sim, significa que este ficheiro é um ficheiro novo, seleccionado pelo utilizador durante a edição do conteúdo - portanto é necessário guardá-lo no disco do servidor
                                        if (conteudoAnexoFile.Content != null)
                                        {
                                            // Limpa o nome do ficheiro de quaisquer caracteres inválidos
                                            string conteudoAnexoFileCleanName = Utilities.CleanFileName(conteudoAnexoFile.Name);

                                            // Guarda o ficheiro no disco
                                            System.IO.File.WriteAllBytes(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexoFileCleanName, conteudoAnexoFile.Content);

                                            // Anexa o ficheiro ao novo conteúdo, registando para isso um novo anexo na base de dados
                                            ConteudoAnexo conteudoAnexo = new ConteudoAnexo();

                                            conteudoAnexo.IUPI = Guid.NewGuid();
                                            conteudoAnexo.IdConteudo = id;
                                            conteudoAnexo.IUPIConteudo = conteudo.IUPI;
                                            conteudoAnexo.Anexo = conteudoAnexoFileCleanName;

                                            //Verficar tipo de anexo (Foto ou Ficheiro)
                                            if (conteudoAnexoFile.ContentType.StartsWith("image"))
                                                conteudoAnexo.Tipo = Convert.ToInt32(Enums.ConteudoAnexoTipo.FOTO);
                                            else
                                                conteudoAnexo.Tipo = Convert.ToInt32(Enums.ConteudoAnexoTipo.FICHEIRO);

                                            if (_intranetService.STP_ConteudosAnexos_I(conteudoAnexo) <= 0)
                                            {
                                                success = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // TO DO: Fazer log de quaisquer excepções que ocorram

                        return null;
                    }
                }
            }

            return success;
        }

        #region Definir Visibilidade do Conteúdo

        protected List<ConteudoPropriedade> GetConteudoEstados()
        {
            List<ConteudoPropriedade> conteudoEstadosList = new List<ConteudoPropriedade>();

            conteudoEstadosList.Add(new ConteudoPropriedade("Rascunho", "1"));
            conteudoEstadosList.Add(new ConteudoPropriedade("Não Publicado", "2"));
            conteudoEstadosList.Add(new ConteudoPropriedade("Publicado", "3"));
            conteudoEstadosList.Add(new ConteudoPropriedade("Arquivo", "4"));

            return conteudoEstadosList;
        }

        protected ConteudoPropriedade GetConteudoEstado(int valor)
        {
            switch (valor)
            {
                case 1:
                    return new ConteudoPropriedade("Rascunho", valor.ToString());

                case 2:
                    return new ConteudoPropriedade("Não Publicado", valor.ToString());

                case 3:
                    return new ConteudoPropriedade("Publicado", valor.ToString());

                case 4:
                    return new ConteudoPropriedade("Arquivo", valor.ToString());

                default:
                    return null;
            }
        }

        protected List<ConteudoPropriedade> GetConteudoVisibilidades()
        {
            List<ConteudoPropriedade> conteudoVisibilidadesList = new List<ConteudoPropriedade>();

            conteudoVisibilidadesList.Add(new ConteudoPropriedade("Público", "true"));
            conteudoVisibilidadesList.Add(new ConteudoPropriedade("Autenticado", "false"));

            return conteudoVisibilidadesList;
        }

        protected ConteudoPropriedade GetConteudoVisibilidade(bool valor)
        {
            switch (valor)
            {
                case true:
                    return new ConteudoPropriedade("Público", valor.ToString());

                case false:
                    return new ConteudoPropriedade("Não Publicado", valor.ToString());

                default:
                    return null;
            }
        }

        protected List<ConteudoPropriedade> GetPerfisVisibilidade()
        {
            List<ConteudoPropriedade> perfisVisibilidadeList = new List<ConteudoPropriedade>();

            List<TipoPerfil> tiposPerfilList = _rcuClient.GetTiposPerfil(true);

            if (tiposPerfilList != null && tiposPerfilList.Count > 0)
            {
                foreach (TipoPerfil tipoPerfil in tiposPerfilList)
                {
                    perfisVisibilidadeList.Add(new ConteudoPropriedade(tipoPerfil.Nome, tipoPerfil.Nome));
                }
            }

            return perfisVisibilidadeList;
        }

        protected List<ConteudoPropriedade> GetGategorias()
        {
            List<ConteudoPropriedade> perfisCategoriasList = new List<ConteudoPropriedade>();

            List<Categoria> categoriasList = _intranetService.STP_Categoria_LS();

            if (categoriasList != null && categoriasList.Count > 0)
            {
                foreach (Categoria categoria in categoriasList)
                {
                    perfisCategoriasList.Add(new ConteudoPropriedade(categoria.Nome, categoria.Id.ToString()));
                }
            }

            return perfisCategoriasList;
        }

        protected List<ConteudoPropriedade> GetTiposCargosVisibilidade()
        {
            List<ConteudoPropriedade> tiposCargosVisibilidadeList = new List<ConteudoPropriedade>();

            List<TipoCargo> tiposCargosList = _cargosClient.GetTiposCargos();

            if (tiposCargosList != null && tiposCargosList.Count > 0)
            {
                foreach (TipoCargo tipoCargo in tiposCargosList)
                {
                    tiposCargosVisibilidadeList.Add(new ConteudoPropriedade(tipoCargo.Nome, tipoCargo.Codigo));
                }
            }

            return tiposCargosVisibilidadeList;
        }

        protected List<ConteudoPropriedade> GetOrgaosVisibilidade()
        {
            List<ConteudoPropriedade> orgaosVisibilidadeList = new List<ConteudoPropriedade>();

            List<Orgao> orgaosList = _cargosClient.GetOrgaosByEstado(true);

            if (orgaosList != null && orgaosList.Count > 0)
            {
                foreach (Orgao orgao in orgaosList)
                {
                    orgaosVisibilidadeList.Add(new ConteudoPropriedade(orgao.Nome, orgao.Codigo));
                }
            }

            return orgaosVisibilidadeList;
        }

        protected List<ConteudoPropriedade> GetGruposQuoVadisVisibilidade()
        {
            List<ConteudoPropriedade> gruposQuoVadisVisibilidadeList = new List<ConteudoPropriedade>();

            // Obtém todos os grupos do QuoVadis, dos dois tipos: grupos (onde queries de SQL são usadas para agrupar os seus utilizadores) e grupos de sistema (onde utilizadores são adicionados manualmente)
            // Depois junta essas duas listas de grupos em uma
            List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> gruposQuoVadisList = _gruposClient.GetGruposByTipo(Convert.ToInt32(Enums.TipoGrupoQuoVadis.GRUPO)).Concat(_gruposClient.GetGruposByTipo(Convert.ToInt32(Enums.TipoGrupoQuoVadis.GRUPOSISTEMA))).ToList();

            if (gruposQuoVadisList != null && gruposQuoVadisList.Count > 0)
            {
                foreach (IntranetAssemblies.Models.QuoVadis.Grupos.Grupo grupo in gruposQuoVadisList)
                {
                    gruposQuoVadisVisibilidadeList.Add(new ConteudoPropriedade(grupo.Nome, grupo.Nome));
                }
            }

            return gruposQuoVadisVisibilidadeList;
        }

        #endregion


        //Verifica se o useraname existe
        public ActionResult ValidateUsername(string username)
        {
            bool isUsernameValid = false;

            string usernameSplit = Utilities.GetNumMecFromUsername(username);

            if (string.IsNullOrEmpty(usernameSplit) == false)
            {
                Guid? estudanteIUPI = _rcuClient.GetEstudanteIUPI(Convert.ToInt32(usernameSplit));

                if (estudanteIUPI != null && estudanteIUPI != new Guid())
                {
                    isUsernameValid = true;
                }
            }
            else
            {
                Guid? utilizadorIUPI = _rcuClient.GetUtilizadorIUPI(username);

                if (utilizadorIUPI != null && utilizadorIUPI != new Guid())
                {
                    isUsernameValid = true;
                }
            }

            return Json(isUsernameValid, JsonRequestBehavior.AllowGet);
        }

        //Obtem o perfil assocaido ao username
        public ActionResult GetPerfis(string username)
        {
            string usernameSplit = Utilities.GetNumMecFromUsername(username);

            if (string.IsNullOrEmpty(usernameSplit) == false)
            {
                Guid? estudanteIUPI = _rcuClient.GetEstudanteIUPI(Convert.ToInt32(usernameSplit));

                if (estudanteIUPI != null && estudanteIUPI != new Guid())
                {
                    List<EstudantePerfil> estudantePerfisList = _rcuClient.GetEstudantePerfis(estudanteIUPI.Value);

                    if (estudantePerfisList != null && estudantePerfisList.Count > 0)
                    {
                        return Json(estudantePerfisList, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                Guid? utilizadorIUPI = _rcuClient.GetUtilizadorIUPI(username);

                if (utilizadorIUPI != null && utilizadorIUPI != new Guid())
                {
                    List<UtilizadorPerfil> utilizadorPerfisList = _rcuClient.GetUtilizador(utilizadorIUPI.Value);

                    if (utilizadorPerfisList != null && utilizadorPerfisList.Count > 0)
                    {
                        return Json(utilizadorPerfisList, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }


        #region Anexos Files 

        public ActionResult DownloadConteudoAnexoFile(DateTime conteudoDataCriacao, string conteudoCodigoOrgao, string conteudoAnexo)
        {
            try
            {
                // Através dos dados do conteúdo passados como parâmetro, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                string conteudoAnexoFileFolderPath = intranetFolderPath.Valor + conteudoCodigoOrgao + "-" + conteudoDataCriacao.Year + "-" + conteudoDataCriacao.Month + "-" + conteudoDataCriacao.Day;

                // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                if (Directory.Exists(conteudoAnexoFileFolderPath) == true)
                {
                    string conteudoAnexoFilePath = conteudoAnexoFileFolderPath + @"\" + conteudoAnexo;

                    // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                    if (System.IO.File.Exists(conteudoAnexoFilePath) == true)
                    {
                        // Se o ficheiro existir, lê o ficheiro como um array de bytes
                        byte[] conteudoAnexoFileBytes = System.IO.File.ReadAllBytes(conteudoAnexoFilePath);

                        // Se o array de bytes anteriormente obtido for válido, escreve esse array de bytes diretamente na resposta HTTP
                        if (conteudoAnexoFileBytes != null && conteudoAnexoFileBytes.Count() > 0)
                        {
                            Response.ContentType = MimeMapping.GetMimeMapping(conteudoAnexo);
                            Response.AddHeader("Content-Lenght", conteudoAnexoFileBytes.Length.ToString());
                            Response.AddHeader("Content-Disposition", "inline; filename=" + conteudoAnexo);
                            Response.BinaryWrite(conteudoAnexoFileBytes);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // TO DO: Fazer log de quaisquer excepções que ocorram
            }

            return new EmptyResult();
        }

        public ActionResult GetConteudoAnexoFiles()
        {
            return Json(IntranetSession.Session.ConteudoAnexosFiles.Where(caf => caf.WasDeletedByUser == false), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateConteudoAnexoFiles(HttpPostedFileBase[] files)
        {
            foreach (HttpPostedFileBase file in files)
            {
                using (BinaryReader conteudoAnexoFileBinaryReader = new BinaryReader(file.InputStream))
                {
                    // Lê o ficheiro passado por parâmetro como um array de bytes
                    byte[] conteudoAnexoFileBytes = conteudoAnexoFileBinaryReader.ReadBytes(file.ContentLength);

                    ConteudoAnexoFile conteudoAnexoFile = new ConteudoAnexoFile();

                    conteudoAnexoFile.Name = file.FileName;
                    conteudoAnexoFile.Size = file.ContentLength;
                    conteudoAnexoFile.Content = conteudoAnexoFileBytes;
                    conteudoAnexoFile.ContentType = file.ContentType;

                    IntranetSession.Session.ConteudoAnexosFiles.Add(conteudoAnexoFile);
                }
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult DeleteConteudoAnexoFile(string conteudoAnexoFileName)
        {
            // Marca o ficheiro presente na sessão do browser para eliminação
            // O ficheiro só é eliminado mais tarde, aquando da submissão dos dados do conteúdo
            // Isto permite-nos, por exemplo, anular a eliminação de ficheiros
            IntranetSession.Session.ConteudoAnexosFiles.SingleOrDefault(caf => caf.Name == conteudoAnexoFileName).WasDeletedByUser = true;

            return new EmptyResult();
        }

        #endregion  

        #region Anexos Fotos 

        //Obter os URL das fotos através dos anexos da BD
        protected async Task<List<string>> GetFotosFromImage(Conteudo conteudo, List<ConteudoAnexo> conteudoFotos)
        {

            List<string> Fotos = new List<string>();
            try
            {

                // Através dos dados do conteúdo, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                string conteudoAnexosFotosFolderPath = intranetFolderPath.Valor + conteudo.CodigoOrgao + "-" + conteudo.DataCriacao.Year + "-" + conteudo.DataCriacao.Month + "-" + conteudo.DataCriacao.Day;

                if (Directory.Exists(conteudoAnexosFotosFolderPath) == true)
                {
                    // Processa cada ficheiro
                    foreach (ConteudoAnexo conteudoAnexoFoto in conteudoFotos)
                    {
                        //Tem de ser do tipo Foto
                        if (conteudoAnexoFoto.Tipo == Convert.ToInt32(Enums.ConteudoAnexoTipo.FOTO))
                        {
                            string conteudoAnexoFotoPath = conteudoAnexosFotosFolderPath + @"\" + conteudoAnexoFoto.Anexo;

                            // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                            if (System.IO.File.Exists(conteudoAnexoFotoPath) == true)
                            {
                                // Se o ficheiro existir, lê o ficheiro como um array de bytes
                                byte[] conteudoAnexoFotoBytes = System.IO.File.ReadAllBytes(conteudoAnexoFotoPath);

                                // Se o array de bytes anteriormente obtido for válido, cria o url para associanar na tag image
                                if (conteudoAnexoFotoBytes != null && conteudoAnexoFotoBytes.Count() > 0)
                                {
                                    string conteudoAnexoFotoBase64String = Utilities.EncodeBase64String(conteudoAnexoFotoBytes);

                                    string contentType = MimeMapping.GetMimeMapping(conteudoAnexoFoto.Anexo);

                                    string url = string.Format("data:{0};base64,{1}", contentType, conteudoAnexoFotoBase64String);

                                    Fotos.Add(url);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                // TO DO: Fazer log de quaisquer excepções que ocorram
            }
            return Fotos;
        }

        public ActionResult GetConteudoAnexoFotos()
        {
            return Json(IntranetSession.Session.ConteudoAnexosFotos.Where(caf => caf.WasDeletedByUser == false), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateConteudoAnexoFotos(HttpPostedFileBase[] files)
        {
            foreach (HttpPostedFileBase file in files)
            {
                using (BinaryReader conteudoAnexoFotoBinaryReader = new BinaryReader(file.InputStream))
                {
                    // Lê o ficheiro passado por parâmetro como um array de bytes
                    byte[] conteudoAnexoFotoBytes = conteudoAnexoFotoBinaryReader.ReadBytes(file.ContentLength);

                    ConteudoAnexoFile conteudoAnexoFoto = new ConteudoAnexoFile();

                    conteudoAnexoFoto.Name = file.FileName;
                    conteudoAnexoFoto.Size = file.ContentLength;
                    conteudoAnexoFoto.Content = conteudoAnexoFotoBytes;
                    conteudoAnexoFoto.ContentType = file.ContentType;

                    IntranetSession.Session.ConteudoAnexosFotos.Add(conteudoAnexoFoto);
                }
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult DeleteConteudoAnexoFotos(string conteudoAnexoFileName)
        {
            // Marca o ficheiro presente na sessão do browser para eliminação
            // O ficheiro só é eliminado mais tarde, aquando da submissão dos dados do conteúdo
            // Isto permite-nos, por exemplo, anular a eliminação de ficheiros
            IntranetSession.Session.ConteudoAnexosFotos.SingleOrDefault(caf => caf.Name == conteudoAnexoFileName).WasDeletedByUser = true;

            return new EmptyResult();
        }
        #endregion

        //Insere ou edita as categorias pelo conteúdo 
        public ActionResult CheckCategoria(int idConteudo, int idCategoria, bool check)
        {
            bool isSave = false;
            int sucess = 0;
            Categoria categoria = new Categoria();
            ConteudoCategoria conteudoCategoria = new ConteudoCategoria();
            List<Categoria> categoriasList = new List<Categoria>();

            if (idCategoria != 0)
            {

                categoria = _intranetService.STP_Categorias_S(idCategoria);
                conteudoCategoria = new ConteudoCategoria(categoria.Id, idConteudo, categoria.Nome, check);

                sucess = _intranetService.STP_ConteudosCategoria_IU(conteudoCategoria);

            }
            else
            {
                categoriasList = _intranetService.STP_Categoria_LS();
                foreach (var item in categoriasList)
                {
                    categoria = _intranetService.STP_Categorias_S(item.Id);
                    conteudoCategoria = new ConteudoCategoria(categoria.Id, idConteudo, categoria.Nome, check);
                    sucess = _intranetService.STP_ConteudosCategoria_IU(conteudoCategoria);
                }

            }
            if (sucess != 0)
                isSave = true;

            return Json(isSave, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddLink(string tipo, string nome, string url, DateTime? data)
        {
            Link link = new Link();
            link.Tipo = tipo;
            link.Nome = nome;
            link.URL = url;

            if (tipo == "Premios") link.Data = data.Value;

            if (IntranetSession.Session.ConteudoLinksList.Count() > 0)
                link.Id = IntranetSession.Session.ConteudoLinksList.Last().Id + 1;
            else
                link.Id = 1;

            IntranetSession.Session.ConteudoLinksList.Add(link);

            return PartialView("ListLinks", IntranetSession.Session.ConteudoLinksList.Where(x => x.Tipo == tipo));
        }

        [HttpPost]
        public ActionResult RemoveLink(int id)
        {
            // Marca o ficheiro presente na sessão do browser para eliminação
            // O ficheiro só é eliminado mais tarde, aquando da submissão dos dados do conteúdo
            // Isto permite-nos, por exemplo, anular a eliminação de ficheiros

            Link link = IntranetSession.Session.ConteudoLinksList.Where(caf => caf.Id == id).FirstOrDefault();
            IntranetSession.Session.ConteudoLinksList.Remove(link);

            return PartialView("ListLinks", IntranetSession.Session.ConteudoLinksList.Where(x => x.Tipo == link.Tipo));
        }
        //Obter os URL das fotos através dos anexos da BD
        protected List<Link> GetLinks(Conteudo conteudo)
        {
            List<Link> links = new List<Link>();

            if (conteudo.Link7 != "")
            {
                var premiosLink = conteudo.Link7.Split('\n');
                var premiosNome = conteudo.LabelLink7.Split('\n');

                for (int i = 0; i < premiosNome.Length - 1; i++)
                {
                    Link link = new Link();
                    link.Nome = premiosNome[i].Split(';')[0].Split('\r')[0];
                    link.Data = Convert.ToDateTime(premiosNome[i].Split(';')[1]);
                    link.URL = premiosLink[i].Split('\r')[0];
                    link.Tipo = "Premios";
                    link.Id = i + 1;

                    links.Add(link);
                }

            }
            if (conteudo.Link9 != "")
            {
                var informacoesLink = conteudo.Link9.Split('\n');
                var informacoesNome = conteudo.LabelLink9.Split('\n');

                for (int i = 0; i < informacoesNome.Length - 1; i++)
                {
                    Link link = new Link();
                    link.Nome = informacoesNome[i].Split('\r')[0];
                    link.URL = informacoesLink[i].Split('\r')[0];
                    link.Tipo = "Informacoes";
                    link.Id = links.Count() + 1;

                    links.Add(link);
                }
            }

            return links;
        }
    }


}