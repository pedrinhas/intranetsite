﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using System.IO;

using IntranetSite.App_Start;
using IntranetSite.Clients;
using IntranetSite.Filters;
using IntranetSite.ViewModels;
using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using IntranetAssemblies.Models;
using System.Threading.Tasks;

namespace IntranetSite.Controllers
{   
    public class ConteudosController : BaseController
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        private readonly CargosClient _cargosClient = new CargosClient();

        //Lista os Conteudos pelas categorias definidas
        public ActionResult ListCategorias(int idCategoria)
        {
            ListCategoriasViewModel listCategoriasViewModel = new ListCategoriasViewModel();

            listCategoriasViewModel.Categoria = _intranetService.STP_Categorias_S(idCategoria);
            // Obtém, a partir dos respectivos ID's, todos os dados dos tipos de conteúdos, categoria e conteudos
            List<ConteudoCategoria> conteudosCategorias = new List<ConteudoCategoria>();
            List<Conteudo> conteudos = new List<Conteudo>();

            if (Autenticacao.IsLoggedIn())
            {
                if (Autenticacao.IsLoginAdmin(IntranetSession.Session.Username))
                {
                    // Obtém, a partir dos respectivos ID's, todos os dados dos tipos de conteúdos, categoria e conteudos
                    conteudosCategorias = _intranetService.STP_ConteudosCategorias_S_ByCategoria(idCategoria, 3,false);
                }
                else
                {
                    conteudos = _intranetService.STP_Conteudos_S_ByEstado(3, false);
                    // retornar os conteudos de acordo com as definições de visualiação de cada um
                    Parallel.ForEach(CheckPermission.CheckPermissionConteudos(conteudos), (conteudo) =>
                    {
                        ConteudoCategoria conteudoCategoria = _intranetService.STP_ConteudosCategorias_S_ByCategoriaAndConteudo(idCategoria, conteudo.Id);
                        if (conteudoCategoria.Id != 0)
                        {
                            conteudosCategorias.Add(conteudoCategoria);
                        }
                    });
                }
            }
            else
            {
                conteudosCategorias.AddRange(_intranetService.STP_ConteudosCategorias_S_ByCategoria(idCategoria, 3, true));
            }

            // retornar os conteudos de acordo com as definições de visualiação de cada um
            Parallel.ForEach(conteudosCategorias, (conteudoCategoria) =>
            {
                Conteudo conteudo = _intranetService.STP_Conteudos_S(conteudoCategoria.IdConteudo);

                listCategoriasViewModel.ConteudosList.Add(conteudo);
                listCategoriasViewModel.TiposConteudoList.Add(_cargosClient.GetTipoConteudo(conteudo.IdTipoConteudo));
            });

            return View(listCategoriasViewModel);
        }

        //Lista os conteudos pelo tipo de conteudo e orgao
        public ActionResult ListConteudos(int idTipoConteudo, int idOrgao, int? Ano)
        {
            ListConteudosViewModel listConteudosViewModel = new ListConteudosViewModel();

            // Obtém, a partir do respectivo ID, todos os dados do tipo de conteúdo e do órgão do conteúdo
            listConteudosViewModel.TipoConteudo = _cargosClient.GetTipoConteudo(idTipoConteudo);
            listConteudosViewModel.Orgao = _cargosClient.GetOrgao(idOrgao);
            listConteudosViewModel.AnosList = GetAnos();
            if(Ano != null)
            {
                listConteudosViewModel.Ano = Ano.ToString();
            }

            if (Autenticacao.IsLoggedIn())
            {
                if (Autenticacao.IsLoginAdmin(IntranetSession.Session.Username))
                {
                    if (Ano == null)
                    {
                        listConteudosViewModel.ConteudosList.AddRange(_intranetService.STP_Conteudos_S_ByTipoConteudoAndOrgaoAndEstado(listConteudosViewModel.TipoConteudo.Id, listConteudosViewModel.Orgao.Id, 3, false).ToList());

                    }
                    else
                    {
                        listConteudosViewModel.ConteudosList.AddRange(_intranetService.STP_Conteudos_S_ByTipoConteudoAndOrgaoAndEstado(listConteudosViewModel.TipoConteudo.Id, listConteudosViewModel.Orgao.Id, 3, false).Where(x => x.DataCriacao.Year == Ano).ToList());
                    }
                }
                else
                {
                    if (Ano == null)
                    {
                        listConteudosViewModel.ConteudosList.AddRange(CheckPermission.CheckPermissionConteudos(_intranetService.STP_Conteudos_S_ByTipoConteudoAndOrgaoAndEstado(listConteudosViewModel.TipoConteudo.Id, listConteudosViewModel.Orgao.Id, 3, false).ToList()));

                    }
                    else
                    {
                        listConteudosViewModel.ConteudosList.AddRange(CheckPermission.CheckPermissionConteudos(_intranetService.STP_Conteudos_S_ByTipoConteudoAndOrgaoAndEstado(listConteudosViewModel.TipoConteudo.Id, listConteudosViewModel.Orgao.Id, 3, false).Where(x => x.DataCriacao.Year == Ano).ToList()));

                    }
                }
            }
            else
            {
                if (Ano == null)
                {
                    //Publicos
                    listConteudosViewModel.ConteudosList.AddRange(_intranetService.STP_Conteudos_S_ByTipoConteudoAndOrgaoAndEstado(listConteudosViewModel.TipoConteudo.Id, listConteudosViewModel.Orgao.Id, 3, true));

                }
                else
                {
                    listConteudosViewModel.ConteudosList.AddRange(_intranetService.STP_Conteudos_S_ByTipoConteudoAndOrgaoAndEstado(listConteudosViewModel.TipoConteudo.Id, listConteudosViewModel.Orgao.Id, 3, true).Where(x => x.DataCriacao.Year == Ano));

                }
            }

            return View(listConteudosViewModel);
        }

        protected List<ConteudoPropriedade> GetAnos()
        {
            List<ConteudoPropriedade> perfisAnosList = new List<ConteudoPropriedade>();
            List<Ano> anosList = new List<Ano>();
            anosList = _intranetService.STP_Conteudos_S_Anos();

            perfisAnosList.Add(new ConteudoPropriedade("Todos", ""));
            foreach (var item in anosList)
            {
                perfisAnosList.Add(new ConteudoPropriedade(item.Year.ToString(), item.Year.ToString()));
            }
            //perfisAnosList.Add(new ConteudoPropriedade("2020", "2020"));
            //perfisAnosList.Add(new ConteudoPropriedade("2021", "2021"));


            return perfisAnosList;
        }


        public ActionResult LoadDetailsConteudoPartials(int id, string intranetDetailsConteudoForm)
        {
            return RedirectToAction("LoadDetailsConteudoPartial", intranetDetailsConteudoForm, new { id = id });
        }

        [IntranetLoggedIn]
        public ActionResult CreateConteudos()
        {
            List<CreateEditConteudosViewModel> createConteudosViewModelsList = BuildCreateEditConteudosViewModelsList();

            return View(createConteudosViewModelsList);
        }

        [IntranetLoggedIn]
        public ActionResult EditConteudos()
        {
            List<CreateEditConteudosViewModel> editConteudosViewModelsList = BuildCreateEditConteudosViewModelsList();

            return View(editConteudosViewModelsList);
        }


        public ActionResult DetailsConteudos(int? idTipoConteudo)
        {
            List<CreateEditConteudosViewModel> detailsConteudosViewModelsList = BuildDetailsConteudosViewModelsList(idTipoConteudo);

            return View(detailsConteudosViewModelsList);
        }
        private List<CreateEditConteudosViewModel> BuildDetailsConteudosViewModelsList(int? idTipoConteudo)
        {
            List<Orgao> orgaoList_conteudos = new List<Orgao>();
            List<Orgao> orgaosList = new List<Orgao>();
            List<Conteudo> conteudosPermission = new List<Conteudo>();
            if (Autenticacao.IsLoggedIn())
            {
                if (Autenticacao.IsLoginAdmin(IntranetSession.Session.Username))
                {
                    if (idTipoConteudo == null)
                        orgaoList_conteudos = _intranetService.STP_Conteudos_S_OrgaoByEstado(3, false);
                    else
                        orgaoList_conteudos = _intranetService.STP_Conteudos_S_OrgaoByEstadoAndTipoConteudo(3, idTipoConteudo.Value, false);
                }
                else
                {

                    if (idTipoConteudo == null)
                        conteudosPermission = CheckPermission.CheckPermissionConteudos(_intranetService.STP_Conteudos_S_ByEstado(3, false).ToList());
                    else
                        conteudosPermission = CheckPermission.CheckPermissionConteudos(_intranetService.STP_Conteudos_S_ByTipoConteudoAndEstado(idTipoConteudo.Value, 3, false).ToList());

                    //Ordenar por IdOrgão e remover os duplicados
                    conteudosPermission = conteudosPermission
                                              .GroupBy(s => s.IdOrgao)
                                              .Select(grp => grp.FirstOrDefault())
                                              .OrderBy(s => s.IdOrgao)
                                              .ToList();

                    // retornar os conteudos de acordo com as definições de visualiação de cada um
                    Parallel.ForEach(conteudosPermission, (conteudo) =>
                    {
                        orgaosList.Add(_cargosClient.GetOrgao(conteudo.IdOrgao));
                    });
                }
                   
            }
            else
            {
                if (idTipoConteudo == null)
                    orgaoList_conteudos = _intranetService.STP_Conteudos_S_OrgaoByEstado(3, true);
                else
                    orgaoList_conteudos = _intranetService.STP_Conteudos_S_OrgaoByEstadoAndTipoConteudo(3, idTipoConteudo.Value, true);
            }


            foreach (var item in orgaoList_conteudos)
            {
                orgaosList.Add(_cargosClient.GetOrgao(item.Id));
            }

            // Com a lista de órgãos construída, constrói a lista de viewmodels
            // Cada viewmodel possui o órgão, e a respectiva lista de tipos de conteúdo
            List<CreateEditConteudosViewModel> createEditConteudoViewModelsList = new List<CreateEditConteudosViewModel>();

            foreach (Orgao orgao in orgaosList)
            {
                CreateEditConteudosViewModel createConteudoViewModel = new CreateEditConteudosViewModel();

                createConteudoViewModel.Orgao = orgao;
                //createConteudoViewModel.TiposConteudosList = _cargosClient.GetTiposConteudosByOrgao(orgao.Id);
                if (Autenticacao.IsLoggedIn())
                {
                    if (Autenticacao.IsLoginAdmin(IntranetSession.Session.Username))
                    {
                        if (idTipoConteudo == null)
                            createConteudoViewModel.TiposConteudosList = _intranetService.STP_Conteudos_S_TipoConteudoByEstadoAndOrgao(3, false, orgao.Id);
                        else
                            createConteudoViewModel.TiposConteudosList = _intranetService.STP_Conteudos_S_TipoConteudoByEstadoAndOrgaoAndTipoConteudo(3, idTipoConteudo.Value, false, orgao.Id);
                    }
                    else
                    {

                        if (idTipoConteudo == null)
                            conteudosPermission = CheckPermission.CheckPermissionConteudos(_intranetService.STP_Conteudos_S_ByOrgaoAndEstado(orgao.Id, 3, false).ToList());
                        else
                            conteudosPermission = CheckPermission.CheckPermissionConteudos(_intranetService.STP_Conteudos_S_ByTipoConteudoAndOrgaoAndEstado(idTipoConteudo.Value, orgao.Id, 3, false).ToList());

                        //Ordenar por IdTipoConteudo e remover os duplicados
                        conteudosPermission = conteudosPermission
                                               .GroupBy(s => s.IdTipoConteudo)
                                               .Select(grp => grp.FirstOrDefault())
                                               .OrderBy(s => s.IdTipoConteudo)
                                               .ToList();


                        // retornar os conteudos de acordo com as definições de visualiação de cada um
                        Parallel.ForEach(conteudosPermission, (conteudo) =>
                        {
                            createConteudoViewModel.TiposConteudosList.Add(_cargosClient.GetTipoConteudo(conteudo.IdTipoConteudo));
                        });
                    }
                }
                else
                {
                    if (idTipoConteudo == null)
                        createConteudoViewModel.TiposConteudosList = _intranetService.STP_Conteudos_S_TipoConteudoByEstadoAndOrgao(3, true, orgao.Id);
                    else
                        createConteudoViewModel.TiposConteudosList = _intranetService.STP_Conteudos_S_TipoConteudoByEstadoAndOrgaoAndTipoConteudo(3, idTipoConteudo.Value, true, orgao.Id);
                }
               

                createEditConteudoViewModelsList.Add(createConteudoViewModel);
            }

            if (idTipoConteudo != null)
            {
                ViewBag.TipoConteudo = _cargosClient.GetTipoConteudo(idTipoConteudo.Value).Nome;
            }


            return createEditConteudoViewModelsList;
        }

        private List<CreateEditConteudosViewModel> BuildCreateEditConteudosViewModelsList()
        {
            // Primeiro constrói a lista de órgãos aos quais o utilizador tem acesso para criar conteúdos na Intranet
            List<Orgao> orgaosList = new List<Orgao>();

            // Obtém os órgãos respectivos aos cargos do utilizador
            foreach (Cargo cargo in IntranetSession.Session.Cargos)
            {
                orgaosList.Add(_cargosClient.GetOrgao(cargo.IdOrgao));
            }

            // Depois obtém a lista de órgãos nos quais o utilizador faz parte do respectivo grupo de secretariado
            foreach (IntranetAssemblies.Models.QuoVadis.Grupos.Grupo grupoQuoVadis in IntranetSession.Session.GruposQuoVadis)
            {
                List<Orgao> grupoQuoVadisSecretariadoOrgaosList = _cargosClient.GetOrgaosByGrupoSecretariado(grupoQuoVadis.Nome);
                
                foreach (Orgao grupoQuoVadisSecretariadoOrgao in grupoQuoVadisSecretariadoOrgaosList)
                {
                    if (orgaosList.SingleOrDefault(o => o.Id == grupoQuoVadisSecretariadoOrgao.Id) == default(Orgao))
                    {
                        orgaosList.Add(grupoQuoVadisSecretariadoOrgao);
                    }
                }
            }

            // Com a lista de órgãos construída, constrói a lista de viewmodels
            // Cada viewmodel possui o órgão, e a respectiva lista de tipos de conteúdo
            List<CreateEditConteudosViewModel> createEditConteudoViewModelsList = new List<CreateEditConteudosViewModel>();

            orgaosList = orgaosList.OrderBy(x => x.Nome).ToList();

            foreach (Orgao orgao in orgaosList)
            {
                CreateEditConteudosViewModel createConteudoViewModel = new CreateEditConteudosViewModel();

                createConteudoViewModel.Orgao = orgao;
                createConteudoViewModel.TiposConteudosList = _cargosClient.GetTiposConteudosByOrgao(orgao.Id);
                createConteudoViewModel.anoLetivos = GetAnosLetivos();
                createEditConteudoViewModelsList.Add(createConteudoViewModel);
            }

            return createEditConteudoViewModelsList;
        }

        protected List<AnoLetivo> GetAnosLetivos()
        {

            List<AnoLetivo> anosLetivosList = _intranetService.STP_AnoLetivo_LS().Where(x=>x.Ativo).ToList();

            return anosLetivosList;
        }

        [IntranetLoggedIn]
        public ActionResult SelectConteudo(int idTipoConteudo, int idOrgao)
        {
            SelectConteudoViewModel selectConteudoViewModel = new SelectConteudoViewModel();

            // Obtém, a partir dos respectivos ID's, todos os dados do tipo de conteúdo e do órgão do novo conteúdo
            TipoConteudo tipoConteudo = _cargosClient.GetTipoConteudo(idTipoConteudo);
            Orgao orgao = _cargosClient.GetOrgao(idOrgao);

            selectConteudoViewModel.ConteudosTipoConteudo = tipoConteudo;
            selectConteudoViewModel.ConteudosNomeOrgao = orgao.Nome;
            selectConteudoViewModel.ConteudosEstadosList = GetConteudoEstados();

            List<Conteudo> conteudosList = _intranetService.STP_Conteudos_S_ByTipoConteudoAndOrgao(idTipoConteudo, idOrgao);

            // Por cada estado anteriormente obtido, separa os conteúdos que se encontram nesse mesmo estado em uma lista própria
            // Depois adiciona cada uma dessas listas ao viewmodel
            foreach (ConteudoPropriedade conteudoEstado in selectConteudoViewModel.ConteudosEstadosList)
            {
                selectConteudoViewModel.ConteudosLists.Add(conteudosList.Where(c => c.Estado == Convert.ToInt32(conteudoEstado.Valor)).ToList());
            }

            return View(selectConteudoViewModel);
        }

        private List<ConteudoPropriedade> GetConteudoEstados()
        {
            List<ConteudoPropriedade> conteudoEstadosList = new List<ConteudoPropriedade>();

            conteudoEstadosList.Add(new ConteudoPropriedade("Rascunho", "1"));
            conteudoEstadosList.Add(new ConteudoPropriedade("Não Publicado", "2"));
            conteudoEstadosList.Add(new ConteudoPropriedade("Publicado", "3"));
            conteudoEstadosList.Add(new ConteudoPropriedade("Arquivo", "4"));

            return conteudoEstadosList;
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult LoadDeleteConteudoPartial(int id)
        {
            Conteudo conteudo = _intranetService.STP_Conteudos_S(id);

            return PartialView("DeleteConteudoPartial", conteudo);
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult DeleteConteudo(int id, Conteudo conteudo)
        {
            bool success = false;

            try
            {
                // Através dos dados do conteúdo, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                string conteudoAnexosFilesFolderPath = @"C:\intranet\" + conteudo.CodigoOrgao + "-" + conteudo.DataCriacao.Year + "-" + conteudo.DataCriacao.Month + "-" + conteudo.DataCriacao.Day;

                if (Directory.Exists(conteudoAnexosFilesFolderPath) == true)
                {
                    Directory.Delete(conteudoAnexosFilesFolderPath, true);
                }

                if (_intranetService.STP_ConteudosAnexos_D_ByConteudo(id) >= 0)
                {
                    if(_intranetService.STP_Conteudos_D(id) == 1)
                    {
                        success = true;
                    }
                }
            }
            catch (Exception e)
            {
                // TO DO: Fazer log de quaisquer excepções que ocorram
            }

            if(success == false)
            {
                TempData["error"] = "Ocorreu um erro ao eliminar dados!";
            }

            return RedirectToAction("SelectConteudo", new { idTipoConteudo = conteudo.IdTipoConteudo, idOrgao = conteudo.IdOrgao });
        }
    }
}