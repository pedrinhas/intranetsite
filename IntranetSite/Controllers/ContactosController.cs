﻿using IntranetSite.App_Start;
using IntranetSite.Clients;
using IntranetSite.Helpers;
using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IntranetSite.ViewModels;

namespace IntranetSite.Controllers
{
    public class ContactosController : BaseController
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        private readonly CargosClient _cargosClient = new CargosClient();


        // GET: Contactos
        public ActionResult Index(int id=0)
        {
            ContactosOrgaosViewModel contactosorgaos = new ContactosOrgaosViewModel();
            List<OrgaosViewModel> orgaosList = new List<OrgaosViewModel>();


            OrgaosViewModel orgao = new OrgaosViewModel();
            orgao.Orgao = _cargosClient.GetOrgao(id);
            orgao.Contacto = _intranetService.STP_Contacto_S_ByOrgao(id);
            if (orgao.Contacto.Foto != null && orgao.Contacto.Foto.Count() > 0)
            {

                try
                {
                    // Através dos dados do conteúdo passados como parâmetro, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                    Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                    string conteudoAnexoFotoFolderPath = intranetFolderPath.Valor + orgao.Contacto.CodigoOrgao + "-" + orgao.Contacto.Id + "-" + orgao.Contacto.IdOrgao;

                    // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                    if (Directory.Exists(conteudoAnexoFotoFolderPath) == true)
                    {
                        string contactoAnexoFotoPath = conteudoAnexoFotoFolderPath + @"\" + orgao.Contacto.Foto;

                        // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                        if (System.IO.File.Exists(contactoAnexoFotoPath) == true)
                        {
                            // Se o ficheiro existir, lê o ficheiro como um array de bytes
                            byte[] conteudoAnexoFotoBytes = System.IO.File.ReadAllBytes(contactoAnexoFotoPath);

                            // Se o array de bytes anteriormente obtido for válido, escreve esse array de bytes diretamente na resposta HTTP
                            if (conteudoAnexoFotoBytes != null && conteudoAnexoFotoBytes.Count() > 0)
                            {
                                string conteudoAnexoFotoBase64String = Utilities.EncodeBase64String(conteudoAnexoFotoBytes);

                                string contentType = MimeMapping.GetMimeMapping(orgao.Contacto.Foto);

                                orgao.Contacto.FotoAnexo = string.Format("data:{0};base64,{1}", contentType, conteudoAnexoFotoBase64String);

                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    // TO DO: Fazer log de quaisquer excepções que ocorram
                }
            }

            //if (_cargosClient.GetOrgaosByOrgaoParent(id).Count() > 0)
            //{
            //    orgao.HasChild = true;
            //}
            orgaosList.Add(orgao);
            
            contactosorgaos.OrgaoParent = id;
            contactosorgaos.Orgaos = orgaosList;
            int idAtual = id;
            int idParent = id;
            string NomeParente;
            while (idParent != 0)
            {
                Orgao orgaoParente = _cargosClient.GetOrgao(idParent);
                idAtual = orgaoParente.Id;
                NomeParente = orgaoParente.Nome;
                idParent = orgaoParente.IdOrgaoParent;
                contactosorgaos.OrgaoParentList.Add(new ConteudoPropriedade(NomeParente, idAtual.ToString()));
            }
            contactosorgaos.OrgaoParentList = contactosorgaos.OrgaoParentList.OrderBy(x => x.Valor).ToList();
            return View(contactosorgaos);
        }

        public ActionResult FilterOrgaoContactos(string keyword, int idParent)
        {
            List<OrgaosViewModel> filteredorgaosList = new List<OrgaosViewModel>();
            foreach (var item in _cargosClient.GetOrgaosByOrgaoParent(idParent).Where(f => f.Nome.IndexOf(keyword, 0, StringComparison.CurrentCultureIgnoreCase) > -1 || f.Codigo.IndexOf(keyword, 0, StringComparison.CurrentCultureIgnoreCase) > -1).ToList())
            {
                OrgaosViewModel orgao = new OrgaosViewModel();
                orgao.Orgao = item;
                orgao.Contacto = _intranetService.STP_Contacto_S_ByOrgao(item.Id);
                if (orgao.Contacto.Foto != null && orgao.Contacto.Foto.Count() > 0)
                {


                    try
                    {
                        // Através dos dados do conteúdo passados como parâmetro, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                        Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                        string conteudoAnexoFotoFolderPath = intranetFolderPath.Valor + orgao.Contacto.CodigoOrgao + "-" + orgao.Contacto.Id + "-" + orgao.Contacto.IdOrgao;

                        // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                        if (Directory.Exists(conteudoAnexoFotoFolderPath) == true)
                        {
                            string contactoAnexoFotoPath = conteudoAnexoFotoFolderPath + @"\" + orgao.Contacto.Foto;

                            // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                            if (System.IO.File.Exists(contactoAnexoFotoPath) == true)
                            {
                                // Se o ficheiro existir, lê o ficheiro como um array de bytes
                                byte[] conteudoAnexoFotoBytes = System.IO.File.ReadAllBytes(contactoAnexoFotoPath);

                                // Se o array de bytes anteriormente obtido for válido, escreve esse array de bytes diretamente na resposta HTTP
                                if (conteudoAnexoFotoBytes != null && conteudoAnexoFotoBytes.Count() > 0)
                                {
                                    string conteudoAnexoFotoBase64String = Utilities.EncodeBase64String(conteudoAnexoFotoBytes);

                                    string contentType = MimeMapping.GetMimeMapping(orgao.Contacto.Foto);

                                    orgao.Contacto.FotoAnexo = string.Format("data:{0};base64,{1}", contentType, conteudoAnexoFotoBase64String);

                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // TO DO: Fazer log de quaisquer excepções que ocorram
                    }
                }
                if (_cargosClient.GetOrgaosByOrgaoParent(idParent).Count() > 0)
                {
                    orgao.HasChild = true;
                }
                filteredorgaosList.Add(orgao);
            }

            return PartialView("ContactosOrgaoPartial", filteredorgaosList);
        }

        // GET: Contactos
        public ActionResult ListarContactos()
        {
            List<Contactos> contactos = _intranetService.STP_Contactos_LS();

            return View(contactos);
        }


        public ActionResult FilterContactos(string keyword)
        {
            List<Contactos> filteredContactosList = _intranetService.STP_Contactos_LS().Where(f => f.NomeOrgao.IndexOf(keyword, 0, StringComparison.CurrentCultureIgnoreCase)  > -1 || f.CodigoOrgao.IndexOf(keyword, 0, StringComparison.CurrentCultureIgnoreCase) > -1).ToList();

            return PartialView("ContactosPartial", filteredContactosList);
        }

        public ActionResult DetailsContacto(int? idContacto)
        {
            Contactos contacto = _intranetService.STP_Contacto_S(idContacto.Value);

            if (contacto.Foto != null && contacto.Foto.Count() > 0)
            {


                try
                {
                    // Através dos dados do conteúdo passados como parâmetro, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                    Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                    string conteudoAnexoFotoFolderPath = intranetFolderPath.Valor + contacto.CodigoOrgao + "-" + contacto.Id + "-" + contacto.IdOrgao;

                    // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                    if (Directory.Exists(conteudoAnexoFotoFolderPath) == true)
                    {
                        string contactoAnexoFotoPath = conteudoAnexoFotoFolderPath + @"\" + contacto.Foto;

                        // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                        if (System.IO.File.Exists(contactoAnexoFotoPath) == true)
                        {
                            // Se o ficheiro existir, lê o ficheiro como um array de bytes
                            byte[] conteudoAnexoFotoBytes = System.IO.File.ReadAllBytes(contactoAnexoFotoPath);

                            // Se o array de bytes anteriormente obtido for válido, escreve esse array de bytes diretamente na resposta HTTP
                            if (conteudoAnexoFotoBytes != null && conteudoAnexoFotoBytes.Count() > 0)
                            {
                                string conteudoAnexoFotoBase64String = Utilities.EncodeBase64String(conteudoAnexoFotoBytes);

                                string contentType = MimeMapping.GetMimeMapping(contacto.Foto);

                                contacto.FotoAnexo = string.Format("data:{0};base64,{1}", contentType, conteudoAnexoFotoBase64String);

                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    // TO DO: Fazer log de quaisquer excepções que ocorram
                }
            }

            return PartialView(contacto);
        }


        // GET: Contactos
        public ActionResult LoadEditContactos(int idOrgao)
        {
            // Antes de tudo, limpa da sessão do browser quaisquer dados de conteúdos antigos que foram criados/editados
            IntranetSession.ClearConteudoSession();

            Contactos contacto = _intranetService.STP_Contacto_S_ByOrgao(idOrgao);

            if (contacto.Id == 0)
            {
                Orgao orgao = _cargosClient.GetOrgao(idOrgao);
                contacto.NomeOrgao = orgao.Nome;
                contacto.CodigoOrgao = orgao.Codigo;
            }

            if (contacto != null && contacto.Id > 0)
            {

                try
                {
                    // Através dos dados do conteúdo, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                    Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                   
                    string conteudoAnexoFileFolderPath = intranetFolderPath.Valor + contacto.CodigoOrgao + "-" + contacto.Id + "-" + contacto.IdOrgao;
                    // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                    if (Directory.Exists(conteudoAnexoFileFolderPath) == true)
                    {
                        // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                        if (System.IO.File.Exists(conteudoAnexoFileFolderPath + @"\" + contacto.Foto) == true)
                        {
                            // Se o ficheiro existir, guarda os dados do ficheiro na sessão do browser
                            // Na view, o plugin Dropzone.JS vai ler estes dados para apresentar os ficheiros que se encontram anexados ao conteúdo
                            ConteudoAnexoFile conteudoAnexoFile = new ConteudoAnexoFile();

                            FileInfo conteudoAnexoFileInfo = new FileInfo(conteudoAnexoFileFolderPath + @"\" + contacto.Foto);

                            conteudoAnexoFile.Id = contacto.Id;
                            conteudoAnexoFile.Name = contacto.Foto;
                            conteudoAnexoFile.Size = conteudoAnexoFileInfo.Length;

                            IntranetSession.Session.ConteudoAnexosFiles.Add(conteudoAnexoFile);
                        }

                    }
                }
                catch (Exception e)
                {
                    // TO DO: Fazer log de quaisquer excepções que ocorram
                }
            }

            return PartialView("LoadEditContactos",contacto);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditContactos(Contactos contacto)
        {
            try
            {
                bool success = false;
                if (contacto.Site != null && contacto.Site != "")
                {
                    Uri uri = null;

                    if (!Uri.TryCreate(contacto.Site, UriKind.Absolute, out uri) || null == uri)
                    {
                        //URL inválida
                        ModelState.AddModelError("Site", "Tem de preencher um url válido!");
                    }
                }

                if (ModelState.IsValid)
                {
                    //Create or Edite Contacto
                    int? returnValue = _intranetService.STP_Contactos_IU(contacto);

                    if (returnValue != null && returnValue > 0)
                    {
                        try
                        {
                            // Através dos dados do conteúdo, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                            Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                            string conteudoAnexosFilesFolderPath = intranetFolderPath.Valor + contacto.CodigoOrgao + "-" + contacto.Id + "-" + contacto.IdOrgao;

                            //No caso de estar a criar pela primeira vez 
                            if (Directory.Exists(conteudoAnexosFilesFolderPath) != true)
                            {
                                // Com o caminho anteriormente gerado, cria a pasta e verifica depois se ela foi criada com sucesso
                                Directory.CreateDirectory(conteudoAnexosFilesFolderPath);

                            }
                            if (Directory.Exists(conteudoAnexosFilesFolderPath) == true)
                            {
                                success = true;

                                // Processa cada ficheiro, seleccionado ou não pelo utilizador, que se encontra presente na sessão do browser
                                foreach (ConteudoAnexoFile conteudoAnexoFile in IntranetSession.Session.ConteudoAnexosFiles)
                                {
                                    if (success == true)
                                    {
                                        if (conteudoAnexoFile.WasDeletedByUser == true)
                                        {
                                            // Verifica se o conteúdo (em bytes) do ficheiro se encontra guardado na sessão do browser
                                            // Se não se encontra guardado, significa que este ficheiro encontrava-se anexado ao conteúdo, e que precisa de ser eliminado
                                            if (conteudoAnexoFile.Content == null)
                                            {
                                                // Elimina o ficheiro do disco
                                                System.IO.File.Delete(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexoFile.Name);

                                                // Depois apaga o registo do anexo presente na base de dados
                                                if (_intranetService.STP_Contactos_U_Foto(contacto.Id,contacto.IdOrgao,"") <= 0)
                                                {
                                                    success = false;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // Verifica se o conteúdo (em bytes) do ficheiro se encontra guardado na sessão do browser
                                            // Se sim, significa que este ficheiro é um ficheiro novo, seleccionado pelo utilizador durante a edição do conteúdo - portanto é necessário guardá-lo no disco do servidor
                                            if (conteudoAnexoFile.Content != null)
                                            {
                                                // Limpa o nome do ficheiro de quaisquer caracteres inválidos
                                                string conteudoAnexoFileCleanName = Utilities.CleanFileName(conteudoAnexoFile.Name);

                                                // Guarda o ficheiro no disco
                                                System.IO.File.WriteAllBytes(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexoFileCleanName, conteudoAnexoFile.Content);

                                                // Anexa o ficheiro ao novo conteúdo, registando para isso um novo anexo na base de dados
                                                contacto.Foto = conteudoAnexoFileCleanName;

                                                if (_intranetService.STP_Contactos_U_Foto(contacto.Id,contacto.IdOrgao,contacto.Foto) <= 0)
                                                {
                                                    success = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            // TO DO: Fazer log de quaisquer excepções que ocorram

                            return null;
                        }
                        return Json(returnValue);
                    }
                    else
                    {
                        return Json("");
                    }
                }
                else
                {
                    //Retorna os erros do model
                    return Json(new
                    {
                        success = false,
                        errors = ValidationModel.GetModelStateErrors(ModelState)
                    });
                }
            
            }
            catch (Exception exception)
            {

                return Json("");
            }

        }
        public ActionResult DownloadContactoAnexoFoto(string contactoCodigoOrgao, int idContacto, int idOrgao, string contactoAnexo)
        {
            try
            {
                // Através dos dados do conteúdo passados como parâmetro, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                string conteudoAnexoFileFolderPath = intranetFolderPath.Valor + contactoCodigoOrgao + "-" + idContacto + "-" + idOrgao;

                // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                if (Directory.Exists(conteudoAnexoFileFolderPath) == true)
                {
                    string contactoAnexoFilePath = conteudoAnexoFileFolderPath + @"\" + contactoAnexo;

                    // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                    if (System.IO.File.Exists(contactoAnexoFilePath) == true)
                    {
                        // Se o ficheiro existir, lê o ficheiro como um array de bytes
                        byte[] conteudoAnexoFileBytes = System.IO.File.ReadAllBytes(contactoAnexoFilePath);

                        // Se o array de bytes anteriormente obtido for válido, escreve esse array de bytes diretamente na resposta HTTP
                        if (conteudoAnexoFileBytes != null && conteudoAnexoFileBytes.Count() > 0)
                        {
                            Response.ContentType = MimeMapping.GetMimeMapping(contactoAnexo);
                            Response.AddHeader("Content-Lenght", conteudoAnexoFileBytes.Length.ToString());
                            Response.AddHeader("Content-Disposition", "inline; filename=" + contactoAnexo);
                            Response.BinaryWrite(conteudoAnexoFileBytes);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // TO DO: Fazer log de quaisquer excepções que ocorram
            }

            return new EmptyResult();
        }

        public ActionResult GetContactoAnexoFotos()
        {
            return Json(IntranetSession.Session.ConteudoAnexosFiles.Where(caf => caf.WasDeletedByUser == false), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateContactoAnexoFotos(HttpPostedFileBase[] files)
        {
            foreach (HttpPostedFileBase file in files)
            {
                using (BinaryReader conteudoAnexoFileBinaryReader = new BinaryReader(file.InputStream))
                {
                    // Lê o ficheiro passado por parâmetro como um array de bytes
                    byte[] conteudoAnexoFileBytes = conteudoAnexoFileBinaryReader.ReadBytes(file.ContentLength);

                    ConteudoAnexoFile conteudoAnexoFile = new ConteudoAnexoFile();

                    conteudoAnexoFile.Name = file.FileName;
                    conteudoAnexoFile.Size = file.ContentLength;
                    conteudoAnexoFile.Content = conteudoAnexoFileBytes;

                    IntranetSession.Session.ConteudoAnexosFiles.Add(conteudoAnexoFile);
                }
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult DeleteContactoAnexoFoto(string contactoAnexoFileName)
        {
            // Marca o ficheiro presente na sessão do browser para eliminação
            // O ficheiro só é eliminado mais tarde, aquando da submissão dos dados do conteúdo
            // Isto permite-nos, por exemplo, anular a eliminação de ficheiros
            IntranetSession.Session.ConteudoAnexosFiles.SingleOrDefault(caf => caf.Name == contactoAnexoFileName).WasDeletedByUser = true;

            return new EmptyResult();
        }
    }
}