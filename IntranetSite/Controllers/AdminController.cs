using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;

using IntranetSite.Clients;
using IntranetSite.Filters;
using IntranetSite.Helpers;
using IntranetSite.ViewModels;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Mensagens;
using IntranetAssemblies.Models.QuoVadis.RCU;
using IntranetAssemblies.Models.QuoVadis.Cargos;

namespace IntranetSite.Controllers
{
    [IntranetLoggedIn]
    [IntranetAutorizacoes]
    public class AdminController : BaseController
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        private readonly CargosClient _cargosClient = new CargosClient();
        private readonly GruposClient _gruposClient = new GruposClient();
        private readonly RCUClient _rcuClient = new RCUClient();

        public ActionResult Autorizacoes()
        {
            List<Autorizacao> autorizacoesList = _intranetService.NSI_STP_Autorizacao_LS();

            autorizacoesList = autorizacoesList.OrderBy(a => a.Pagina).ToList();

            return View(autorizacoesList);
        }

        public ActionResult CreateAutorizacao()
        {
            CreateEditAutorizacaoViewModel createAutorizacaoViewModel = new CreateEditAutorizacaoViewModel();

            RefreshCreateEditAutorizacaoViewModel(createAutorizacaoViewModel);

            // Marca logo automaticamente o grupo de administração do Intranet presente na base de dados
            // Desta forma o grupo de administração fica logo marcado como tendo acesso a este item - a não ser que o utilizador desmarque o grupo por alguma razão
            createAutorizacaoViewModel.GruposList.Single(g => g.Nome == "ADMIN").Checked = true;

            return View(createAutorizacaoViewModel);
        }

        [HttpPost]
        public ActionResult CreateAutorizacao(CreateEditAutorizacaoViewModel createAutorizacaoViewModel)
        {
            if (string.IsNullOrEmpty(createAutorizacaoViewModel.Autorizacao.ItemName) == true)
            {
                ModelState.AddModelError("Autorizacao.ItemName", "Tem de preencher este campo!");
            }

            if(ModelState.IsValid == true)
            {
                bool success = false;

                int idAutorizacao = _intranetService.NSI_STP_Autorizacao_I(createAutorizacaoViewModel.Autorizacao.ItemName, createAutorizacaoViewModel.Autorizacao.Pagina);

                if (idAutorizacao > 0)
                {
                    success = true;

                    foreach (Grupo g in createAutorizacaoViewModel.GruposList)
                    {
                        if (g.Checked == true)
                        {
                            if (_intranetService.NSI_STP_GrupoAutorizacao_I(g.Id, idAutorizacao) <= 0)
                            {
                                success = false;
                            }
                        }
                    }
                }

                if(success == true)
                {
                    return RedirectToAction("Autorizacoes");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditAutorizacaoViewModel(createAutorizacaoViewModel);

            return View(createAutorizacaoViewModel);
        }

        public ActionResult EditAutorizacao(int id)
        {
            CreateEditAutorizacaoViewModel editAutorizacaoViewModel = new CreateEditAutorizacaoViewModel();

            editAutorizacaoViewModel.Autorizacao = _intranetService.NSI_STP_Autorizacao_S(id);

            RefreshCreateEditAutorizacaoViewModel(editAutorizacaoViewModel);

            List<GrupoAutorizacao> gruposAutorizacaoList = _intranetService.NSI_STP_GrupoAutorizacao_S_ByAutorizacao(editAutorizacaoViewModel.Autorizacao.Id);

            foreach (GrupoAutorizacao ga in gruposAutorizacaoList)
            {
                editAutorizacaoViewModel.GruposList.Single(g => g.Id == ga.IdGrupo).Checked = true;
            }

            return View(editAutorizacaoViewModel);
        }

        [HttpPost]
        public ActionResult EditAutorizacao(int id, CreateEditAutorizacaoViewModel editAutorizacaoViewModel)
        {
            if(string.IsNullOrEmpty(editAutorizacaoViewModel.Autorizacao.ItemName) == true)
            {
                ModelState.AddModelError("Autorizacao.ItemName", "Tem de preencher este campo!");
            }

            if(ModelState.IsValid == true)
            {
                bool success = false;

                if (_intranetService.NSI_STP_Autorizacao_U(id, editAutorizacaoViewModel.Autorizacao.ItemName, editAutorizacaoViewModel.Autorizacao.Pagina) > 0)
                {
                    if (_intranetService.NSI_STP_GrupoAutorizacao_D_ByAutorizacao(id) >= 0)
                    {
                        success = true;

                        foreach (Grupo g in editAutorizacaoViewModel.GruposList)
                        {
                            if (g.Checked == true)
                            {
                                if (_intranetService.NSI_STP_GrupoAutorizacao_I(g.Id, id) <= 0)
                                {
                                    success = false;
                                }
                            }
                        }
                    }
                }

                if (success == true)
                {
                    return RedirectToAction("Autorizacoes");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditAutorizacaoViewModel(editAutorizacaoViewModel);

            return View(editAutorizacaoViewModel);
        }

        private void RefreshCreateEditAutorizacaoViewModel(CreateEditAutorizacaoViewModel createEditAutorizacaoViewModel)
        {
            createEditAutorizacaoViewModel.GruposList = _intranetService.NSI_STP_Grupo_LS();
        }

        public ActionResult LoadDeleteAutorizacaoPartial(int id)
        {
            return PartialView("DeleteAutorizacaoPartial", _intranetService.NSI_STP_Autorizacao_S(id));
        }

        [HttpPost]
        public ActionResult DeleteAutorizacao(int id)
        {
            bool success = false;

            if(_intranetService.NSI_STP_GrupoAutorizacao_D_ByAutorizacao(id) >= 0)
            {
                if(_intranetService.NSI_STP_Autorizacao_D(id) == 1)
                {
                    success = true;
                }
            }

            if(success == false)
            {
                TempData["error"] = "Ocorreu um erro ao eliminar dados!";
            }

            return RedirectToAction("Autorizacoes");
        }

        public ActionResult Grupos()
        {
            List<Grupo> gruposList = _intranetService.NSI_STP_Grupo_S_ByTipo(1);

            return View(gruposList);
        }

        public ActionResult CreateGrupo()
        {
            CreateEditGrupoViewModel createGrupoViewModel = new CreateEditGrupoViewModel();

            RefreshCreateEditGrupoViewModel(createGrupoViewModel);

            return View(createGrupoViewModel);
        }

        [HttpPost]
        public ActionResult CreateGrupo(CreateEditGrupoViewModel createGrupoViewModel)
        {
            if (string.IsNullOrEmpty(createGrupoViewModel.Grupo.Nome) == true)
            {
                ModelState.AddModelError("Grupo.Nome", "Tem de preencher este campo!");
            }

            if (ModelState.IsValid == true)
            {
                bool success = false;

                int idGrupo = _intranetService.NSI_STP_Grupo_I(createGrupoViewModel.Grupo.Nome, 1, createGrupoViewModel.Grupo.Descricao);

                if(idGrupo > 0)
                {
                    success = true;

                    foreach (User user in createGrupoViewModel.UsersList)
                    {
                        if (success == true)
                        {
                            if (user.Checked == true)
                            {
                                UserGrupo userGrupo = new UserGrupo();

                                if (user.Id == 0)
                                {
                                    int userID = _intranetService.NSI_STP_User_IU(user.Username, user.IUPI, user.Nome, user.LastLoginDate, user.IsFromUTAD);

                                    if (userID > 0)
                                    {
                                        userGrupo.IdUser = userID;
                                    }
                                    else
                                    {
                                        success = false;
                                    }
                                }
                                else
                                {
                                    userGrupo.IdUser = user.Id;
                                }

                                if (success == true)
                                {
                                    userGrupo.IdGrupo = idGrupo;

                                    if (_intranetService.NSI_STP_UserGrupo_I(userGrupo.IdUser, userGrupo.IdGrupo) <= 0)
                                    {
                                        success = false;
                                    }
                                }
                            }
                        }
                    }
                }

                if (success == true)
                {
                    return RedirectToAction("Grupos");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditGrupoViewModel(createGrupoViewModel);

            return View(createGrupoViewModel);
        }

        public ActionResult EditGrupo(int id)
        {
            CreateEditGrupoViewModel editGrupoViewModel = new CreateEditGrupoViewModel();

            editGrupoViewModel.Grupo = _intranetService.NSI_STP_Grupo_S(id);

            RefreshCreateEditGrupoViewModel(editGrupoViewModel);

            List<UserGrupo> usersGrupoList = _intranetService.NSI_STP_UserGrupo_S_ByGrupo(id);

            foreach (UserGrupo ug in usersGrupoList)
            {
                editGrupoViewModel.UsersList.Single(u => u.Id == ug.IdUser).Checked = true;
            }

            return View(editGrupoViewModel);
        }

        [HttpPost]
        public ActionResult EditGrupo(int id, CreateEditGrupoViewModel editGrupoViewModel)
        {
            if (string.IsNullOrEmpty(editGrupoViewModel.Grupo.Nome) == true)
            {
                ModelState.AddModelError("Grupo.Nome", "Tem de preencher este campo!");
            }

            if (ModelState.IsValid == true)
            {
                bool success = false;

                if (_intranetService.NSI_STP_Grupo_U(id, editGrupoViewModel.Grupo.Nome, Convert.ToInt32(Enums.TipoGrupoPortalBase.GRUPO), editGrupoViewModel.Grupo.Descricao) > 0)
                {
                    if (_intranetService.NSI_STP_UserGrupo_D_ByGrupo(id) >= 0)
                    {
                        success = true;

                        foreach(User user in editGrupoViewModel.UsersList)
                        {
                            if(success == true)
                            {
                                if (user.Checked == true)
                                {
                                    UserGrupo userGrupo = new UserGrupo();

                                    if (user.Id == 0)
                                    {
                                        int userID = _intranetService.NSI_STP_User_IU(user.Username, user.IUPI, user.Nome, user.LastLoginDate, user.IsFromUTAD);

                                        if (userID > 0)
                                        {
                                            userGrupo.IdUser = userID;
                                        }
                                        else
                                        {
                                            success = false;
                                        }
                                    }
                                    else
                                    {
                                        userGrupo.IdUser = user.Id;
                                    }

                                    if (success == true)
                                    {
                                        userGrupo.IdGrupo = id;

                                        if (_intranetService.NSI_STP_UserGrupo_I(userGrupo.IdUser, userGrupo.IdGrupo) <= 0)
                                        {
                                            success = false;
                                        }
                                    }
                                }
                            }
                        }

                        if (success == true)
                        {
                            return RedirectToAction("Grupos");
                        }
                        else
                        {
                            TempData["error"] = "Ocorreu um erro ao submeter dados!";
                        }
                    }
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditGrupoViewModel(editGrupoViewModel);

            return View(editGrupoViewModel);
        }

        private void RefreshCreateEditGrupoViewModel(CreateEditGrupoViewModel createEditGrupoViewModel)
        {
            // Guarda, se existirem, os utilizadores que foram previamente adicionados através do seu nome de utilizador em uma lista à parte
            List<User> addedUsersList = createEditGrupoViewModel.UsersList.Where(u => u.Id == 0).ToList();

            // Obtém da base de dados a informação dos utilizadores guardados mais recente
            createEditGrupoViewModel.UsersList = _intranetService.NSI_STP_User_LS();

            // Adiciona no fim da lista os utilizadores que foram previamente adicionados através do seu nome de utilizador
            createEditGrupoViewModel.UsersList.AddRange(addedUsersList);

            // Por fim associa a cada utilizador presente na lista um índice próprio
            for (int i = 0; i < createEditGrupoViewModel.UsersList.Count; i++)
            {
                createEditGrupoViewModel.UsersList.ElementAt(i).Index = i;
            }
        }

        [HttpPost]
        public ActionResult CreateUserForUserGrupoPartial(int index, string username)
        {
            NomeAndIUPI utilizadorNomeAndIUPI = _rcuClient.GetUtilizadorNomeAndIUPI(username);

            if(utilizadorNomeAndIUPI != null && utilizadorNomeAndIUPI.IUPI != new Guid())
            {
                User user = new User();

                user.Username = username;
                user.IUPI = utilizadorNomeAndIUPI.IUPI;
                user.Nome = utilizadorNomeAndIUPI.Nome;

                user.Index = index;
                user.Checked = true;

                return Json(user);
            }
            else
            {
                return Json("");
            }
        }

        [HttpPost]
        public ActionResult LoadUserGrupoPartial(User user)
        {
            return PartialView("UserGrupoPartial", user);
        }

        public ActionResult LoadDeleteGrupoPartial(int id)
        {
            return PartialView("DeleteGrupoPartial", _intranetService.NSI_STP_Grupo_S(id));
        }

        [HttpPost]
        public ActionResult DeleteGrupo(int id)
        {
            bool success = false;

            if (_intranetService.NSI_STP_UserGrupo_D_ByGrupo(id) >= 0)
            {
                if (_intranetService.NSI_STP_GrupoAutorizacao_D_ByGrupo(id) >= 0)
                {
                    if (_intranetService.NSI_STP_Grupo_D(id) == 1)
                    {
                        success = true;
                    }
                }
            }

            if (success == false)
            {
                TempData["error"] = "Ocorreu um erro ao eliminar dados!";
            }

            return RedirectToAction("Grupos");
        }

        public ActionResult GruposCargos()
        {
            List<Grupo> gruposCargosList = _intranetService.NSI_STP_Grupo_S_ByTipo(2);

            return View(gruposCargosList);
        }

        public ActionResult CreateGrupoCargo()
        {
            CreateEditGrupoCargoViewModel createGrupoCargoViewModel = new CreateEditGrupoCargoViewModel();

            RefreshCreateEditGrupoCargoViewModel(createGrupoCargoViewModel);

            return View(createGrupoCargoViewModel);
        }

        [HttpPost]
        public ActionResult CreateGrupoCargo(CreateEditGrupoCargoViewModel createGrupoCargoViewModel)
        {
            if (string.IsNullOrEmpty(createGrupoCargoViewModel.Grupo.Nome) == true)
            {
                ModelState.AddModelError("Grupo.Nome", "Tem de seleccionar um tipo de cargo para o grupo!");
            }

            if (ModelState.IsValid == true)
            {
                if (_intranetService.NSI_STP_Grupo_I(createGrupoCargoViewModel.Grupo.Nome, 2, createGrupoCargoViewModel.Grupo.Descricao) > 0)
                {
                    return RedirectToAction("GruposCargos");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditGrupoCargoViewModel(createGrupoCargoViewModel);

            return View(createGrupoCargoViewModel);
        }

        public ActionResult EditGrupoCargo(int id)
        {
            CreateEditGrupoCargoViewModel editGrupoCargoViewModel = new CreateEditGrupoCargoViewModel();

            editGrupoCargoViewModel.Grupo = _intranetService.NSI_STP_Grupo_S(id);

            RefreshCreateEditGrupoCargoViewModel(editGrupoCargoViewModel);

            return View(editGrupoCargoViewModel);
        }

        [HttpPost]
        public ActionResult EditGrupoCargo(int id, CreateEditGrupoCargoViewModel editGrupoCargoViewModel)
        {
            if (string.IsNullOrEmpty(editGrupoCargoViewModel.Grupo.Nome) == true)
            {
                ModelState.AddModelError("Grupo.Nome", "Tem de seleccionar um tipo de cargo para o grupo!");
            }

            if(ModelState.IsValid == true)
            {
                if (_intranetService.NSI_STP_Grupo_U(id, editGrupoCargoViewModel.Grupo.Nome, Convert.ToInt32(Enums.TipoGrupoPortalBase.CARGO), editGrupoCargoViewModel.Grupo.Descricao) > 0)
                {
                    return RedirectToAction("GruposCargos");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditGrupoCargoViewModel(editGrupoCargoViewModel);

            return View(editGrupoCargoViewModel);
        }

        private void RefreshCreateEditGrupoCargoViewModel(CreateEditGrupoCargoViewModel createEditGrupoCargoViewModel)
        {
            createEditGrupoCargoViewModel.TiposCargosList = _cargosClient.GetTiposCargos();
        }

        public ActionResult LoadDeleteGrupoCargoPartial(int id)
        {
            return PartialView("DeleteGrupoCargoPartial", _intranetService.NSI_STP_Grupo_S(id));
        }

        [HttpPost]
        public ActionResult DeleteGrupoCargo(int id)
        {
            bool success = false;

            if (_intranetService.NSI_STP_GrupoAutorizacao_D_ByGrupo(id) >= 0)
            {
                if (_intranetService.NSI_STP_Grupo_D(id) == 1)
                {
                    success = true;
                }
            }

            if (success == false)
            {
                TempData["error"] = "Ocorreu um erro ao eliminar dados!";
            }

            return RedirectToAction("GruposCargos");
        }

        public ActionResult GruposPerfis()
        {
            List<Grupo> gruposTiposPerfilList = new List<Grupo>();

            // Para grupos do tipo Perfil, é preciso apresentar todos os tipos de perfil e marcar os tipos que têm permissões no Intranet
            // Por isso em primeiro lugar obtém todos os tipos de perfil existentes no RCU
            List<TipoPerfil> tiposPerfilList = _rcuClient.GetTiposPerfil(true);

            // Por cada tipo de perfil obtido cria um objeto grupo para apresentar na tabela e guarda-o na lista do ViewModel
            foreach (TipoPerfil tipoPerfil in tiposPerfilList)
            {
                Grupo grupoTipoPerfil = new Grupo();

                grupoTipoPerfil.Nome = tipoPerfil.Nome;
                grupoTipoPerfil.IdTipo = Convert.ToInt32(Enums.TipoGrupoPortalBase.PERFIL);

                gruposTiposPerfilList.Add(grupoTipoPerfil);
            }

            // Obtém a lista de grupos do tipo Perfil presentes na base de dados do Intranet
            List<Grupo> gruposPerfisList = _intranetService.NSI_STP_Grupo_S_ByTipo(3);

            // Se houver um grupo do Tipo Perfil na base de dados correspondente ao tipo de perfil, significa que esse tipo de perfil possui permissões no Intranet
            // Portanto marca esse tipo de perfil
            foreach (Grupo grupoPerfil in gruposPerfisList)
            {
                Grupo grupoTipoPerfil = gruposTiposPerfilList.Single(g => g.Nome == grupoPerfil.Nome);

                grupoTipoPerfil.Id = grupoPerfil.Id;
                grupoTipoPerfil.Descricao = grupoPerfil.Descricao;

                grupoTipoPerfil.Checked = true;
            }

            return View(gruposTiposPerfilList);
        }

        public ActionResult LoadCreateGrupoPerfilPartial(string nome)
        {
            // Obtém todos os tipos de perfil existentes no RCU e selecciona aquele cujo nome corresponde ao que foi passado como parâmetro
            TipoPerfil tipoPerfil = _rcuClient.GetTiposPerfil(true).SingleOrDefault(tp => tp.Nome == nome);

            // Cria um objeto grupo para apresentar, com base no tipo de perfil obtido anteriormente
            Grupo grupoTipoPerfil = new Grupo();

            if (tipoPerfil != default(TipoPerfil))
            {
                grupoTipoPerfil.Nome = tipoPerfil.Nome;
                grupoTipoPerfil.IdTipo = Convert.ToInt32(Enums.TipoGrupoPortalBase.PERFIL);
            }

            return PartialView("CreateGrupoPerfilPartial", grupoTipoPerfil);
        }

        [HttpPost]
        public ActionResult CreateGrupoPerfil(string nome)
        {
            string descricao = "Este grupo contém todos os utilizadores com Perfil de " + nome;

            if (_intranetService.NSI_STP_Grupo_I(nome, Convert.ToInt32(Enums.TipoGrupoPortalBase.PERFIL), descricao) <= 0)
            {
                TempData["error"] = "Ocorreu um erro ao submeter dados!";
            }

            return RedirectToAction("GruposPerfis");
        }

        public ActionResult LoadDeleteGrupoPerfilPartial(int id)
        {
            return PartialView("DeleteGrupoPerfilPartial", _intranetService.NSI_STP_Grupo_S(id));
        }

        [HttpPost]
        public ActionResult DeleteGrupoPerfil(int id)
        {
            bool success = false;

            if (_intranetService.NSI_STP_GrupoAutorizacao_D_ByGrupo(id) >= 0)
            {
                if (_intranetService.NSI_STP_Grupo_D(id) == 1)
                {
                    success = true;
                }
            }

            if (success == false)
            {
                TempData["error"] = "Ocorreu um erro ao eliminar dados!";
            }

            return RedirectToAction("GruposPerfis");
        }

        public ActionResult GruposQuoVadis()
        {
            List<Grupo> gruposQuoVadisList = _intranetService.NSI_STP_Grupo_S_ByTipo(Convert.ToInt32(Enums.TipoGrupoPortalBase.GRUPOQUOVADIS));

            return View(gruposQuoVadisList);
        }

        public ActionResult CreateGrupoQuoVadis()
        {
            CreateEditGrupoQuoVadisViewModel createGrupoQuoVadisViewModel = new CreateEditGrupoQuoVadisViewModel();

            RefreshCreateEditGrupoQuoVadisViewModel(createGrupoQuoVadisViewModel);

            return View(createGrupoQuoVadisViewModel);
        }

        [HttpPost]
        public ActionResult CreateGrupoQuoVadis(CreateEditGrupoQuoVadisViewModel createGrupoQuoVadisViewModel)
        {
            if (string.IsNullOrEmpty(createGrupoQuoVadisViewModel.GrupoQuoVadisID) == true)
            {
                ModelState.AddModelError("GrupoQuoVadisID", "Tem de seleccionar um grupo do QuoVadis válido!");
            }

            if (ModelState.IsValid == true)
            {
                if (_intranetService.NSI_STP_Grupo_I(createGrupoQuoVadisViewModel.Grupo.Nome, Convert.ToInt32(Enums.TipoGrupoPortalBase.GRUPOQUOVADIS), createGrupoQuoVadisViewModel.Grupo.Descricao) > 0)
                {
                    return RedirectToAction("GruposQuoVadisVisibilidade");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditGrupoQuoVadisViewModel(createGrupoQuoVadisViewModel);

            return View(createGrupoQuoVadisViewModel);
        }

        public ActionResult EditGrupoQuoVadis(int id)
        {
            CreateEditGrupoQuoVadisViewModel editGrupoQuoVadisViewModel = new CreateEditGrupoQuoVadisViewModel();

            editGrupoQuoVadisViewModel.Grupo = _intranetService.NSI_STP_Grupo_S(id);

            RefreshCreateEditGrupoQuoVadisViewModel(editGrupoQuoVadisViewModel);

            return View(editGrupoQuoVadisViewModel);
        }

        [HttpPost]
        public ActionResult EditGrupoQuoVadis(int id, CreateEditGrupoQuoVadisViewModel editGrupoQuoVadisViewModel)
        {
            if (string.IsNullOrEmpty(editGrupoQuoVadisViewModel.GrupoQuoVadisID) == true)
            {
                ModelState.AddModelError("GrupoQuoVadisID", "");
            }

            if (ModelState.IsValid == true)
            {
                if (_intranetService.NSI_STP_Grupo_U(id, editGrupoQuoVadisViewModel.Grupo.Nome, Convert.ToInt32(Enums.TipoGrupoPortalBase.GRUPOQUOVADIS), editGrupoQuoVadisViewModel.Grupo.Descricao) > 0)
                {
                    return RedirectToAction("GruposQuoVadisVisibilidade");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditGrupoQuoVadisViewModel(editGrupoQuoVadisViewModel);

            return View(editGrupoQuoVadisViewModel);
        }

        private void RefreshCreateEditGrupoQuoVadisViewModel(CreateEditGrupoQuoVadisViewModel createEditGrupoQuoVadisViewModel)
        {
            // Obtém todos os grupos do QuoVadis, dos dois tipos: grupos (onde queries de SQL são usadas para agrupar os seus utilizadores) e grupos de sistema (onde utilizadores são adicionados manualmente)
            // Depois junta essas duas listas de grupos em uma, e armazena a lista resultante no viewmodel - com todos os grupos ordenados por nome
            createEditGrupoQuoVadisViewModel.GruposQuoVadisList = _gruposClient.GetGruposByTipo(Convert.ToInt32(Enums.TipoGrupoQuoVadis.GRUPO)).Concat(_gruposClient.GetGruposByTipo(Convert.ToInt32(Enums.TipoGrupoQuoVadis.GRUPOSISTEMA))).OrderBy(g => g.Nome).ToList();
        }

        [HttpPost]
        public ActionResult CreateGrupoFromGrupoQuoVadis(int grupoQuoVadisID)
        {
            Grupo grupo = new Grupo();

            IntranetAssemblies.Models.QuoVadis.Grupos.Grupo grupoQuoVadis = _gruposClient.GetGrupo(grupoQuoVadisID);

            if (grupoQuoVadis != null && grupoQuoVadis.IdGrupos > 0)
            {
                grupo.Nome = grupoQuoVadis.Nome;
                grupo.Descricao = grupoQuoVadis.Descricao;

                return Json(grupo);
            }
            else
            {
                return Json("");
            }
        }

        public ActionResult LoadDeleteGrupoQuoVadisPartial(int id)
        {
            return PartialView("DeleteGrupoQuoVadisPartial", _intranetService.NSI_STP_Grupo_S(id));
        }

        [HttpPost]
        public ActionResult DeleteGrupoQuoVadis(int id)
        {
            bool success = false;

            if (_intranetService.NSI_STP_GrupoAutorizacao_D_ByGrupo(id) >= 0)
            {
                if (_intranetService.NSI_STP_Grupo_D(id) == 1)
                {
                    success = true;
                }
            }

            if (success == false)
            {
                TempData["error"] = "Ocorreu um erro ao eliminar dados!";
            }

            return RedirectToAction("GruposQuoVadisVisibilidade");
        }

        public ActionResult Mensagens()
        {
            List<Mensagem> mensagensList = _intranetService.NSI_STP_Mensagens_LS();

            return View(mensagensList);
        }

        public ActionResult CreateMensagem()
        {
            CreateEditMensagemViewModel createMensagemViewModel = BuildCreateMensagemViewModel();

            return View(createMensagemViewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateMensagem(FormCollection formCollection)
        {
            if(string.IsNullOrEmpty(formCollection["Mensagem.Titulo"]) == true)
            {
                ModelState.AddModelError("Mensagem.Titulo", "Tem de preencher este campo!");
            }

            if (string.IsNullOrEmpty(formCollection["Mensagem.Texto"]) == true)
            {
                ModelState.AddModelError("Mensagem.Texto", "Tem de preencher este campo!");
            }

            // Instancia e verifica as datas de início e de fim da publização da mensagem
            // Primeiro instancia e verifica a data de início da publicação da mensagem
            DateTime dataHoraPublicacaoInicio = new DateTime();

            if (string.IsNullOrEmpty(formCollection["Mensagem.DataPublicacaoInicio"]) == false)
            {
                if (string.IsNullOrEmpty(formCollection["HoraPublicacaoInicio"]) == true)
                {
                    ModelState.AddModelError("HoraPublicacaoInicio", "Tem de preencher este campo!");
                }
                else
                {
                    if (DateTime.TryParse(formCollection["Mensagem.DataPublicacaoInicio"] + " " + formCollection["HoraPublicacaoInicio"], out dataHoraPublicacaoInicio) == false)
                    {
                        ModelState.AddModelError("HoraPublicacaoInicio", "Este campo encontra-se num formato incorreto!");
                    }
                }
            }

            // Depois instancia e verifica a data de fim da publicação da mensagem
            DateTime dataHoraPublicacaoFim = new DateTime();

            if (string.IsNullOrEmpty(formCollection["Mensagem.DataPublicacaoFim"]) == false)
            {
                if (string.IsNullOrEmpty(formCollection["HoraPublicacaoFim"]) == true)
                {
                    ModelState.AddModelError("HoraPublicacaoFim", "Tem de preencher este campo!");
                }
                else
                {
                    if (DateTime.TryParse(formCollection["Mensagem.DataPublicacaoFim"] + formCollection["HoraPublicacaoFim"], out dataHoraPublicacaoFim) == false)
                    {
                        ModelState.AddModelError("HoraPublicacaoFim", "Este campo encontra-se num formato incorreto!");
                    }
                }
            }

            // Se o utilizador introduziu tanto uma data de início da publicação da mensagem como uma data de fim, verifica se a data de início não é maior do que a data de fim
            if(dataHoraPublicacaoInicio != new DateTime() && dataHoraPublicacaoFim != new DateTime())
            {
                if (dataHoraPublicacaoInicio > dataHoraPublicacaoFim)
                {
                    ModelState.AddModelError("Mensagem.DataPublicacaoInicio", "A data de início da publicação da mensagem não pode ser superior à data de fim!");
                }
            }

            if (string.IsNullOrEmpty(formCollection["Mensagem.Visibilidade"]) == true)
            {
                ModelState.AddModelError("Mensagem.Visibilidade", "Tem de seleccionar uma visibilidade para a mensagem!");
            }

            if (ModelState.IsValid == true)
            {
                int returnValue = 0;

                DateTime? mensagemDataHoraPublicacaoInicio = null;
                DateTime? mensagemDataHoraPublicacaoFim = null;

                if (dataHoraPublicacaoInicio != new DateTime())
                {
                    mensagemDataHoraPublicacaoInicio = dataHoraPublicacaoInicio;
                }

                if (dataHoraPublicacaoFim != new DateTime())
                {
                    mensagemDataHoraPublicacaoFim = dataHoraPublicacaoFim;
                }

                returnValue = _intranetService.NSI_STP_Mensagens_I(formCollection["Mensagem.Titulo"], formCollection["Mensagem.Texto"], mensagemDataHoraPublicacaoInicio, mensagemDataHoraPublicacaoFim, Convert.ToInt32(formCollection["Mensagem.Visibilidade"]));

                if (returnValue > 0)
                {
                    return RedirectToAction("Mensagens");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            CreateEditMensagemViewModel createMensagemViewModel = BuildCreateMensagemViewModel();

            return View(createMensagemViewModel);
        }

        public CreateEditMensagemViewModel BuildCreateMensagemViewModel()
        {
            CreateEditMensagemViewModel createMensagemViewModel = new CreateEditMensagemViewModel();

            List<Visibilidade> visibilidadesList = new List<Visibilidade>();

            visibilidadesList.Add(new Visibilidade(2, "Mensagem Para Utilizadores Autenticados"));
            visibilidadesList.Add(new Visibilidade(3, "Mensagem Pública"));

            createMensagemViewModel.VisibilidadesList = visibilidadesList;

            return createMensagemViewModel;
        }

        public ActionResult EditMensagem(int id)
        {
            CreateEditMensagemViewModel editMensagemViewModel = new CreateEditMensagemViewModel();

            editMensagemViewModel.Mensagem = _intranetService.NSI_STP_Mensagens_S(id);

            if (editMensagemViewModel.Mensagem.DataPublicacaoInicio != null)
            {
                editMensagemViewModel.HoraPublicacaoInicio = editMensagemViewModel.Mensagem.DataPublicacaoInicio.Value.TimeOfDay.ToString();
            }

            if (editMensagemViewModel.Mensagem.DataPublicacaoFim != null)
            {
                editMensagemViewModel.HoraPublicacaoFim = editMensagemViewModel.Mensagem.DataPublicacaoFim.Value.TimeOfDay.ToString();
            }

            RefreshEditMensagemViewModel(editMensagemViewModel);

            List<GrupoMensagem> gruposMensagemList = _intranetService.NSI_STP_GrupoMensagem_S_ByMensagem(id);

            foreach (GrupoMensagem gm in gruposMensagemList)
            {
                editMensagemViewModel.GruposList.Single(g => g.Id == gm.IdGrupo).Checked = true;
            }

            return View(editMensagemViewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditMensagem(int id, CreateEditMensagemViewModel editMensagemViewModel)
        {
            if (string.IsNullOrEmpty(editMensagemViewModel.Mensagem.Titulo) == true)
            {
                ModelState.AddModelError("Mensagem.Titulo", "Tem de preencher este campo!");
            }

            if (string.IsNullOrEmpty(editMensagemViewModel.Mensagem.Texto) == true)
            {
                ModelState.AddModelError("Mensagem.Texto", "Tem de preencher este campo!");
            }

            // Instancia e verifica as datas de início e de fim da publização da mensagem
            // Primeiro instancia e verifica a data de início da publicação da mensagem
            DateTime dataHoraPublicacaoInicio = new DateTime();

            if (editMensagemViewModel.Mensagem.DataPublicacaoInicio != null)
            {
                if (string.IsNullOrEmpty(editMensagemViewModel.HoraPublicacaoInicio) == true)
                {
                    ModelState.AddModelError("HoraPublicacaoInicio", "Tem de preencher este campo!");
                }
                else
                {
                    if (DateTime.TryParse(editMensagemViewModel.Mensagem.DataPublicacaoInicio + " " + editMensagemViewModel.HoraPublicacaoInicio, out dataHoraPublicacaoInicio) == false)
                    {
                        ModelState.AddModelError("HoraPublicacaoInicio", "Este campo encontra-se num formato incorreto!");
                    }
                }
            }

            // Depois instancia e verifica a data de fim da publicação da mensagem
            DateTime dataHoraPublicacaoFim = new DateTime();

            if (editMensagemViewModel.Mensagem.DataPublicacaoFim != null)
            {
                if (string.IsNullOrEmpty(editMensagemViewModel.HoraPublicacaoFim) == true)
                {
                    ModelState.AddModelError("HoraPublicacaoFim", "Tem de preencher este campo!");
                }
                else
                {
                    if (DateTime.TryParse(editMensagemViewModel.Mensagem.DataPublicacaoFim + editMensagemViewModel.HoraPublicacaoFim, out dataHoraPublicacaoFim) == false)
                    {
                        ModelState.AddModelError("HoraPublicacaoFim", "Este campo encontra-se num formato incorreto!");
                    }
                }
            }

            // Se o utilizador introduziu tanto uma data de início da publicação da mensagem como uma data de fim, verifica se a data de início não é maior do que a data de fim
            if (dataHoraPublicacaoInicio != new DateTime() && dataHoraPublicacaoFim != new DateTime())
            {
                if (dataHoraPublicacaoInicio > dataHoraPublicacaoFim)
                {
                    ModelState.AddModelError("Mensagem.DataPublicacaoInicio", "A data de início da publicação da mensagem não pode ser superior à data de fim!");
                }
            }

            if (ModelState.IsValid == true)
            {
                DateTime? mensagemDataHoraPublicacaoInicio = null;
                DateTime? mensagemDataHoraPublicacaoFim = null;

                if (dataHoraPublicacaoInicio != new DateTime())
                {
                    mensagemDataHoraPublicacaoInicio = dataHoraPublicacaoInicio;
                }

                if (dataHoraPublicacaoFim != new DateTime())
                {
                    mensagemDataHoraPublicacaoFim = dataHoraPublicacaoFim;
                }

                bool success = false;

                if(_intranetService.NSI_STP_Mensagens_U(id, editMensagemViewModel.Mensagem.Titulo, editMensagemViewModel.Mensagem.Texto, mensagemDataHoraPublicacaoInicio, mensagemDataHoraPublicacaoFim, editMensagemViewModel.Mensagem.Visibilidade) > 0)
                {
                    if(_intranetService.NSI_STP_GrupoMensagem_D_ByMensagem(id) >= 0)
                    {
                        success = true;

                        foreach(Grupo g in editMensagemViewModel.GruposList)
                        {
                            if(g.Checked == true)
                            {
                                if(_intranetService.NSI_STP_GrupoMensagem_I(g.Id, id) <= 0)
                                {
                                    success = false;
                                }
                            }
                        }
                    }
                }

                if (success == true)
                {
                    return RedirectToAction("Mensagens");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshEditMensagemViewModel(editMensagemViewModel);

            return View(editMensagemViewModel);
        }

        public void RefreshEditMensagemViewModel(CreateEditMensagemViewModel editMensagemViewModel)
        {
            List<Visibilidade> visibilidadesList = new List<Visibilidade>();

            visibilidadesList.Add(new Visibilidade(2, "Mensagem Para Utilizadores Autenticados"));
            visibilidadesList.Add(new Visibilidade(3, "Mensagem Pública"));

            editMensagemViewModel.VisibilidadesList = visibilidadesList;

            editMensagemViewModel.GruposList = _intranetService.NSI_STP_Grupo_LS();
        }

        public ActionResult LoadDeleteMensagemPartial(int id)
        {
            return PartialView("DeleteMensagemPartial", _intranetService.NSI_STP_Mensagens_S(id));
        }

        [HttpPost]
        public ActionResult DeleteMensagem(int id)
        {
            if (_intranetService.NSI_STP_Mensagens_D(id) != 1)
            {
                TempData["error"] = "Ocorreu um erro ao eliminar dados!";
            }

            return RedirectToAction("Mensagens");
        }

        public ActionResult Parametros()
        {
            List<Parametro> parametrosList = _intranetService.NSI_STP_Parametros_LS().OrderBy(p => p.Chave).ToList();

            return View(parametrosList);
        }

        public ActionResult CreateParametro()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateParametro(Parametro parametro)
        {
            if(string.IsNullOrEmpty(parametro.Chave) == true)
            {
                ModelState.AddModelError("Chave", "Tem de preencher este campo!");
            }

            if (string.IsNullOrEmpty(parametro.Valor) == true)
            {
                ModelState.AddModelError("Valor", "Tem de preencher este campo!");
            }

            if (ModelState.IsValid == true)
            {
                if (_intranetService.NSI_STP_Parametros_I(parametro.Chave, parametro.Valor, parametro.Valor1, parametro.Valor2) > 0)
                {
                    return RedirectToAction("Parametros");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            return View(parametro);
        }

        public ActionResult EditParametro(string chave)
        {
            Parametro parametro = _intranetService.NSI_STP_Parametros_S_ByChave(chave);

            return View(parametro);
        }

        [HttpPost]
        public ActionResult EditParametro(int id, Parametro parametro)
        {
            if (string.IsNullOrEmpty(parametro.Chave) == true)
            {
                ModelState.AddModelError("Chave", "Tem de preencher este campo!");
            }

            if (string.IsNullOrEmpty(parametro.Valor) == true)
            {
                ModelState.AddModelError("Valor", "Tem de preencher este campo!");
            }

            if(ModelState.IsValid == true)
            {
                if(_intranetService.NSI_STP_Parametros_U(id, parametro.Chave, parametro.Valor, parametro.Valor1, parametro.Valor2) > 0)
                {
                    return RedirectToAction("Parametros");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            return View(parametro);
        }

        public ActionResult LoadDeleteParametroPartial(string chave)
        {
            return PartialView("DeleteParametroPartial", _intranetService.NSI_STP_Parametros_S_ByChave(chave));
        }

        [HttpPost]
        public ActionResult DeleteParametro(int id)
        {
            if(_intranetService.NSI_STP_Parametros_D(id) != 1)
            {
                TempData["error"] = "Ocorreu um erro ao eliminar dados!";
            }

            return RedirectToAction("Parametros");
        }

        public ActionResult Users()
        {
            List<User> usersList = _intranetService.NSI_STP_User_LS();

            return View(usersList);
        }

        public ActionResult CreateCredenciaisTemporarias()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCredenciaisTemporarias(CredenciaisTemporariasInsert credenciaisTemporariasInsert)
        {
            if(string.IsNullOrEmpty(credenciaisTemporariasInsert.Nome) == true)
            {
                ModelState.AddModelError("Nome", "Tem de introduzir um nome válido!");
            }

            if (string.IsNullOrEmpty(credenciaisTemporariasInsert.Email) == true)
            {
                ModelState.AddModelError("Email", "Tem de introduzir um endereço de e-mail válido!");
            }
            else
            {
                if (Utilities.CheckEmailString(credenciaisTemporariasInsert.Email) == false)
                {
                    ModelState.AddModelError("Email", "O endereço de e-mail introduzido não é válido!");
                }
            }

            if (string.IsNullOrEmpty(credenciaisTemporariasInsert.DefaultLanguage) == true)
            {
                ModelState.AddModelError("DefaultLanguage", "Tem de seleccionar um idioma de apresentação!");
            }

            if (ModelState.IsValid == true)
            {
                Parametro credenciaisTemporariasDominio = _intranetService.NSI_STP_Parametros_S_ByChave("Auth.CredenciaisTemporarias.Dominio");
                Parametro mailBCC = _intranetService.NSI_STP_Parametros_S_ByChave("Mail.DeveloperAddress");

                if (string.IsNullOrEmpty(credenciaisTemporariasDominio.Valor) == false)
                {
                    credenciaisTemporariasInsert.IUPI = Guid.NewGuid();
                    credenciaisTemporariasInsert.Dominio = credenciaisTemporariasDominio.Valor;

                    // Gera uma palavra-passe de 8 caracteres aleatórios
                    // Esta palavra-passe descodificada irá ser enviada por e-mail para o novo utilizador temporário
                    string password = PasswordGenerator.GeneratePassword();

                    // Pega na palavra-passe gerada anteriormente e codifica-a
                    // Esta palavra-passe codificada irá ser armazenada na base de dados de credenciais temporárias
                    credenciaisTemporariasInsert.Password = PasswordGenerator.EncodePassword(password);
                    credenciaisTemporariasInsert.SistemaCriacao = ConfigurationManager.AppSettings["Aplicacao.IdentificadorPortalBase"];

                    // Regista as novas credenciais temporárias na base de dados, obtendo no fim o novo nome de utilizador
                    string username = _rcuClient.CreateCredenciaisTemporarias(credenciaisTemporariasInsert);

                    // Se o registo anterior for efetuado com sucesso, retorna um nome de utilizador válido
                    // Se esse for o caso, procede
                    if (string.IsNullOrEmpty(username) == false)
                    {
                        // Compõe e envia um e-mail de confirmação de registo, com o nome de utilizador e a palavra-passe criadas anteriormente, para o endereço de e-mail especificado
                        string registoCredenciaisTemporariasMailSubject = ConfigurationManager.AppSettings["Aplicacao.Titulo"] + " - Registo de Novas Credenciais Temporárias";
                        string registoCredenciaisTemporariasMailBody = "Caro(a) " + credenciaisTemporariasInsert.Nome + "," + Environment.NewLine + Environment.NewLine + "O seu registo foi efetuado com sucesso. Enviamos neste e-mail as credenciais temporárias que deve usar para se autenticar na plataforma " + ConfigurationManager.AppSettings["Aplicacao.Titulo"] + ":" + Environment.NewLine + Environment.NewLine + "Nome de Utilizador: " + username + Environment.NewLine + "Palavra-Passe: " + password;

                        if(credenciaisTemporariasInsert.DiasValidade != null && credenciaisTemporariasInsert.DiasValidade > 0)
                        {
                            registoCredenciaisTemporariasMailBody = registoCredenciaisTemporariasMailBody + Environment.NewLine + Environment.NewLine + "Informamos também que as suas credenciais temporárias possuem uma validade de " + credenciaisTemporariasInsert.DiasValidade + " dias.";
                        }

                        // Se conseguir enviar o e-mail com sucesso, o processo fica concluído
                        if (Mailer.SendMailFromUTAD(credenciaisTemporariasInsert.Email, mailBCC.Valor, registoCredenciaisTemporariasMailSubject, registoCredenciaisTemporariasMailBody, null) == true)
                        {
                            return RedirectToAction("Users");
                        }
                    }
                }

                TempData["error"] = "Ocorreu um erro ao submeter dados!";
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            return View(credenciaisTemporariasInsert);
        }

        public ActionResult LoadDeleteUserPartial(int id)
        {
            return PartialView("DeleteUserPartial", _intranetService.NSI_STP_User_S(id));
        }

        [HttpPost]
        public ActionResult DeleteUser(int id)
        {
            bool success = false;

            if (_intranetService.NSI_STP_UserGrupo_D_ByUser(id) >= 0)
            {
                if (_intranetService.NSI_STP_User_D(id) == 1)
                {
                    success = true;
                }
            }

            if (success == false)
            {
                TempData["error"] = "Ocorreu um erro ao eliminar dados!";
            }

            return RedirectToAction("Users");
        }

        public ActionResult Categorias()
        {
            List<Categoria> categoriasList = _intranetService.STP_Categoria_LS();

            return View(categoriasList);
        }

        public ActionResult CreateCategoria()
        {
            Categoria categoria = new Categoria();

            return View(categoria);
        }

        [HttpPost]
        public ActionResult CreateCategoria(Categoria categoria)
        {
            if (string.IsNullOrEmpty(categoria.Nome) == true)
            {
                ModelState.AddModelError("Nome", "Tem de preencher este campo!");
            }
            if (string.IsNullOrEmpty(categoria.Codigo) == true)
            {
                ModelState.AddModelError("Codigo", "Tem de preencher este campo!");
            }

            if (ModelState.IsValid == true)
            {
                bool success = false;

                int idCategoria = _intranetService.STP_Categorias_I(categoria.Nome, categoria.Descricao, categoria.Codigo);

                if (idCategoria > 0)
                {
                    success = true;

                  
                }

                if (success == true)
                {
                    return RedirectToAction("Categorias");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            return View(categoria);
        }

        public ActionResult EditCategoria(int id)
        {
            Categoria categoria = new Categoria();

            categoria = _intranetService.STP_Categorias_S(id);

           
            return View(categoria);
        }

        [HttpPost]
        public ActionResult EditCategoria(int id, Categoria categoria)
        {
            if (string.IsNullOrEmpty(categoria.Nome) == true)
            {
                ModelState.AddModelError("Nome", "Tem de preencher este campo!");
            }

            if (string.IsNullOrEmpty(categoria.Codigo) == true)
            {
                ModelState.AddModelError("Codigo", "Tem de preencher este campo!");
            }

            if (ModelState.IsValid == true)
            {
                bool success = false;

                if (_intranetService.STP_Categorias_U(id, categoria.Nome, categoria.Descricao, categoria.Codigo) > 0)
                {
                    success = true;

                    if (success == true)
                    {
                        return RedirectToAction("Categorias");
                    }
                    else
                    {
                        TempData["error"] = "Ocorreu um erro ao submeter dados!";
                    }
                }
                   
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            return View(categoria);
        }

        public ActionResult LoadDeleteCategoriaPartial(int id)
        {
            return PartialView("DeleteCategoriaPartial", _intranetService.STP_Categorias_S(id));
        }

        [HttpPost]
        public ActionResult DeleteCategoria(int id)
        {
            bool success = false;

            int contador= _intranetService.STP_ConteudosCategorias_S_Count(id);

            if (contador == 0)
            {

                if (_intranetService.STP_Categorias_D(id) >= 0)
                {

                    success = true;

                }

                if (success == false)
                {
                    TempData["error"] = "Ocorreu um erro ao eliminar dados!";
                }
            }
            else
            {
                TempData["error"] = "Existem contéudos criados com a categoria!";
            }

            return RedirectToAction("Categorias");
        }


        public ActionResult AnosLetivos()
        {
            List<AnoLetivo> anosLetivosList = _intranetService.STP_AnoLetivo_LS();

            return View(anosLetivosList);
        }

        public ActionResult CreateAnoLetivo()
        {
            AnoLetivo anoLetivo = new AnoLetivo();

            return View(anoLetivo);
        }

        [HttpPost]
        public ActionResult CreateAnoLetivo(AnoLetivo anoLetivo)
        {
            if (anoLetivo.Ano == 0)
            {
                ModelState.AddModelError("Ano", "Tem de preencher este campo!");
            }
           

            if (ModelState.IsValid == true)
            {
                bool success = false;

                int idAnoLetivo = _intranetService.STP_AnoLetivo_I(anoLetivo);

                if (idAnoLetivo > 0)
                {
                    success = true;


                }

                if (success == true)
                {
                    return RedirectToAction("AnosLetivos");
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            return View(anoLetivo);
        }

        public ActionResult EditAnoLetivo(int id)
        {
            AnoLetivo anoLetivo = new AnoLetivo();

            anoLetivo = _intranetService.STP_AnoLetivo_S(id);


            return View(anoLetivo);
        }

        [HttpPost]
        public ActionResult EditAnoLetivo(int id, AnoLetivo anoLetivo)
        {
            if (anoLetivo.Ano == 0)
            {
                ModelState.AddModelError("Ano", "Tem de preencher este campo!");
            }

            if (ModelState.IsValid == true)
            {
                bool success = false;

                if (_intranetService.STP_AnoLetivo_U(anoLetivo) > 0)
                {
                    success = true;

                    if (success == true)
                    {
                        return RedirectToAction("AnosLetivos");
                    }
                    else
                    {
                        TempData["error"] = "Ocorreu um erro ao submeter dados!";
                    }
                }

            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            return View(anoLetivo);
        }

        public ActionResult LoadDeleteAnoLetivoPartial(int id)
        {
            return PartialView("DeleteAnoLetivoPartial", _intranetService.STP_AnoLetivo_S(id));
        }

        [HttpPost]
        public ActionResult DeleteAnoLetivo(int id)
        {
            bool success = false;


                if (_intranetService.STP_AnoLetivo_D(id) >= 0)
                {

                    success = true;

                }

                if (success == false)
                {
                    TempData["error"] = "Ocorreu um erro ao eliminar dados!";
                }
            

            return RedirectToAction("AnosLetivos");
        }

    }
}