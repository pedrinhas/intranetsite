﻿using IntranetSite.App_Start;
using IntranetSite.Clients;
using IntranetSite.Filters;
using IntranetSite.ViewModels;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace IntranetSite.Controllers
{
    public class FormEventosController : ConteudosFormsController
    {
        private readonly CargosClient _cargosClient = new CargosClient();


        //Lista os conteudos pelo tipo de conteudo e orgao
        public async Task<ActionResult> Index()
        {
            ListConteudosViewModel listConteudosViewModel = new ListConteudosViewModel();

            // Obtém, a partir do respectivo ID, todos os dados do tipo de conteúdo e do órgão do conteúdo
            listConteudosViewModel.TipoConteudo = _cargosClient.GetTipoConteudo(15);

            //Publicos
            listConteudosViewModel.ConteudosList.AddRange(_intranetService.STP_Conteudos_S_ByTipoConteudoAndEstado(listConteudosViewModel.TipoConteudo.Id, 3, true).OrderByDescending(x => x.DataInicioPublicacao));
            foreach (var item in listConteudosViewModel.ConteudosList)
            {
                var Fotos = await GetFotosFromImage(item, _intranetService.STP_ConteudosAnexos_S_ByConteudoAndTipo(item.Id, 1));
                if (Fotos.Count > 0)
                {
                    item.LinkFoto = Fotos.First();
                }
            }
            return View(listConteudosViewModel);
        }

        public async Task<ActionResult> LoadDetailsConteudoPartial(int id)
        {
            DetailsConteudoPartialViewModel detailsConteudoPartialViewModel = new DetailsConteudoPartialViewModel();

            detailsConteudoPartialViewModel.Conteudo = _intranetService.STP_Conteudos_S(id);

            if (detailsConteudoPartialViewModel.Conteudo != null && detailsConteudoPartialViewModel.Conteudo.Id > 0)
            {
                detailsConteudoPartialViewModel.ConteudoAnexosList = _intranetService.STP_ConteudosAnexos_S_ByConteudoAndTipo(id,2);
                detailsConteudoPartialViewModel.Fotos = await GetFotosFromImage(detailsConteudoPartialViewModel.Conteudo, _intranetService.STP_ConteudosAnexos_S_ByConteudoAndTipo(id, 1));
                detailsConteudoPartialViewModel.ConteudoEstado = GetConteudoEstado(detailsConteudoPartialViewModel.Conteudo.Estado);
                detailsConteudoPartialViewModel.ConteudoVisibilidade = GetConteudoVisibilidade(detailsConteudoPartialViewModel.Conteudo.VisibilidadePublica);
            }

            return PartialView("DetailsConteudoPartial", detailsConteudoPartialViewModel);
        }

        [IntranetLoggedIn]
        public ActionResult CreateConteudo(int idTipoConteudo, int idOrgao)
        {
            // Antes de tudo, limpa da sessão do browser quaisquer dados de conteúdos antigos que foram criados/editados
            IntranetSession.ClearConteudoSession();

            CreateEditConteudoViewModel createConteudoViewModel = new CreateEditConteudoViewModel();

            // Obtém, a partir dos respectivos ID's, todos os dados do tipo de conteúdo e do órgão do novo conteúdo
            TipoConteudo tipoConteudo = _cargosClient.GetTipoConteudo(idTipoConteudo);

            // Associa logo ao novo conteúdo os IDs do tipo de conteúdo e da unidade orgânica seleccionados
            createConteudoViewModel.Conteudo.IdTipoConteudo = idTipoConteudo;
            createConteudoViewModel.Conteudo.NomeTipoConteudo = tipoConteudo.Nome;
            createConteudoViewModel.Conteudo.IdOrgao = idOrgao;

            //Define a data de inicio de publicação por defeito para hoje
            createConteudoViewModel.Conteudo.DataInicioPublicacao = DateTime.Now;

            //Preencher as labels que são necessárias preencher no formulário
            createConteudoViewModel.Conteudo.LabelLink1 = "Link para vídeo/áudio";
            createConteudoViewModel.Conteudo.LabelLink2 = "Link para site do evento";
            createConteudoViewModel.Conteudo.LabelLink3 = "Link para site da inscrição";

            RefreshCreateEditConteudoViewModel(createConteudoViewModel);

            return View(createConteudoViewModel);
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult CreateConteudo(CreateEditConteudoViewModel createConteudoViewModel)
        {
            if (string.IsNullOrEmpty(createConteudoViewModel.Conteudo.Titulo) == true)
            {
                ModelState.AddModelError("Conteudo.Titulo", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.DataInicioPublicacao == null || createConteudoViewModel.Conteudo.DataInicioPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicioPublicacao", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.DataFimPublicacao != null && createConteudoViewModel.Conteudo.DataFimPublicacao != new DateTime())
            {
                if (createConteudoViewModel.Conteudo.DataFimPublicacao < createConteudoViewModel.Conteudo.DataInicioPublicacao)
                {
                    ModelState.AddModelError("Conteudo.DataFimPublicacao", "A data de fim de publicação tem de ser superior à de início");
                }
            }

            if (createConteudoViewModel.Conteudo.DataInicio == null || createConteudoViewModel.Conteudo.DataInicio == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicio", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.Link1 != null && createConteudoViewModel.Conteudo.Link1 != "")
            {
                Uri uri = null;

                if (!Uri.TryCreate(createConteudoViewModel.Conteudo.Link1, UriKind.Absolute, out uri) || null == uri)
                {
                    //URL inválida
                    ModelState.AddModelError("Conteudo.Link1", "Tem de preencher um url válido!");
                }
            }
            if (createConteudoViewModel.Conteudo.Link2 != null && createConteudoViewModel.Conteudo.Link2 != "")
            {
                Uri uri = null;

                if (!Uri.TryCreate(createConteudoViewModel.Conteudo.Link2, UriKind.Absolute, out uri) || null == uri)
                {
                    //URL inválida
                    ModelState.AddModelError("Conteudo.Link2", "Tem de preencher um url válido!");
                }
            }
            if (createConteudoViewModel.Conteudo.Link3 != null && createConteudoViewModel.Conteudo.Link3 != "")
            {
                Uri uri = null;

                if (!Uri.TryCreate(createConteudoViewModel.Conteudo.Link3, UriKind.Absolute, out uri) || null == uri)
                {
                    //URL inválida
                    ModelState.AddModelError("Conteudo.Link3", "Tem de preencher um url válido!");
                }
            }

            if (IntranetSession.Session.ConteudoAnexosFotos.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            {
                ModelState.AddModelError("ConteudoAnexoFoto", "Tem de escolher uma foto!");
            }

            if (ModelState.IsValid == true)
            {
                int? id = CreateConteudo(createConteudoViewModel.Conteudo);

                if (id != null && id > 0)
                {
                    TempData["success"] = "Dados submetidos com êxito!";

                    return RedirectToAction("EditConteudo", new { id = id });
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }


            RefreshCreateEditConteudoViewModel(createConteudoViewModel);

            return View(createConteudoViewModel);
        }

        [IntranetLoggedIn]
        public ActionResult EditConteudo(int id)
        {
            // Antes de tudo, limpa da sessão do browser quaisquer dados de conteúdos antigos que foram criados/editados
            IntranetSession.ClearConteudoSession();

            CreateEditConteudoViewModel editConteudoViewModel = new CreateEditConteudoViewModel();

            editConteudoViewModel.Conteudo = _intranetService.STP_Conteudos_S(id);

            if (editConteudoViewModel.Conteudo != null && editConteudoViewModel.Conteudo.Id > 0)
            {
                RefreshCreateEditConteudoViewModel(editConteudoViewModel);

                // Com os dados do conteúdo obtidos, passa para a obtenção dos dados dos anexos do conteúdo
                List<ConteudoAnexo> conteudoAnexosList = _intranetService.STP_ConteudosAnexos_S_ByConteudo(id);

                if (conteudoAnexosList != null && conteudoAnexosList.Count > 0)
                {
                    try
                    {
                        // Através dos dados do conteúdo, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                        Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                        string conteudoAnexosFilesFolderPath = intranetFolderPath.Valor + editConteudoViewModel.Conteudo.CodigoOrgao + "-" + editConteudoViewModel.Conteudo.DataCriacao.Year + "-" + editConteudoViewModel.Conteudo.DataCriacao.Month + "-" + editConteudoViewModel.Conteudo.DataCriacao.Day;

                        // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                        if (Directory.Exists(conteudoAnexosFilesFolderPath) == true)
                        {
                            foreach (ConteudoAnexo conteudoAnexo in conteudoAnexosList)
                            {
                                // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                                if (System.IO.File.Exists(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexo.Anexo) == true)
                                {
                                    // Se o ficheiro existir, guarda os dados do ficheiro na sessão do browser
                                    // Na view, o plugin Dropzone.JS vai ler estes dados para apresentar os ficheiros que se encontram anexados ao conteúdo
                                    ConteudoAnexoFile conteudoAnexoFile = new ConteudoAnexoFile();

                                    FileInfo conteudoAnexoFileInfo = new FileInfo(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexo.Anexo);

                                    conteudoAnexoFile.Id = conteudoAnexo.Id;
                                    conteudoAnexoFile.Name = conteudoAnexo.Anexo;
                                    conteudoAnexoFile.Size = conteudoAnexoFileInfo.Length;

                                    if (conteudoAnexo.Tipo == 1)
                                    {
                                        IntranetSession.Session.ConteudoAnexosFotos.Add(conteudoAnexoFile);
                                    }
                                    else
                                    {
                                        IntranetSession.Session.ConteudoAnexosFiles.Add(conteudoAnexoFile);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // TO DO: Fazer log de quaisquer excepções que ocorram
                    }
                }
            }

            return View(editConteudoViewModel);
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult EditConteudo(int id, CreateEditConteudoViewModel editConteudoViewModel)
        {
            if (string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Titulo) == true)
            {
                ModelState.AddModelError("Conteudo.Titulo", "Tem de preencher este campo!");
            }

            if (editConteudoViewModel.Conteudo.DataInicioPublicacao == null || editConteudoViewModel.Conteudo.DataInicioPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicioPublicacao", "Tem de preencher este campo!");
            }

            if (editConteudoViewModel.Conteudo.DataFimPublicacao == null || editConteudoViewModel.Conteudo.DataFimPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataFimPublicacao", "Tem de preencher este campo!");
            }

            if (editConteudoViewModel.Conteudo.DataFimPublicacao != null && editConteudoViewModel.Conteudo.DataFimPublicacao != new DateTime())
            {
                if (editConteudoViewModel.Conteudo.DataFimPublicacao < editConteudoViewModel.Conteudo.DataInicioPublicacao)
                {
                    ModelState.AddModelError("Conteudo.DataFimPublicacao", "A data de fim de publicação tem de ser superior à de início");
                }
            }

            if (editConteudoViewModel.Conteudo.DataInicio == null || editConteudoViewModel.Conteudo.DataInicio == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicio", "Tem de preencher este campo!");
            }

            if (string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Link1) == true)
            {
                ModelState.AddModelError("Conteudo.Link1", "Tem de preencher este campo!");
            }
            if (editConteudoViewModel.Conteudo.Link1 != null && editConteudoViewModel.Conteudo.Link1 != "")
            {
                Uri uri = null;

                if (!Uri.TryCreate(editConteudoViewModel.Conteudo.Link1, UriKind.Absolute, out uri) || null == uri)
                {
                    //URL inválida
                    ModelState.AddModelError("Conteudo.Link1", "Tem de preencher um url válido!");
                }
            }
            if (editConteudoViewModel.Conteudo.Link2 != null && editConteudoViewModel.Conteudo.Link2 != "")
            {
                Uri uri = null;

                if (!Uri.TryCreate(editConteudoViewModel.Conteudo.Link2, UriKind.Absolute, out uri) || null == uri)
                {
                    //URL inválida
                    ModelState.AddModelError("Conteudo.Link2", "Tem de preencher um url válido!");
                }
            }
            if (editConteudoViewModel.Conteudo.Link3 != null && editConteudoViewModel.Conteudo.Link3 != "")
            {
                Uri uri = null;

                if (!Uri.TryCreate(editConteudoViewModel.Conteudo.Link3, UriKind.Absolute, out uri) || null == uri)
                {
                    //URL inválida
                    ModelState.AddModelError("Conteudo.Link3", "Tem de preencher um url válido!");
                }
            }

            if (IntranetSession.Session.ConteudoAnexosFotos.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            {
                ModelState.AddModelError("ConteudoAnexoFoto", "Tem de escolher uma foto!");
            }

            if (ModelState.IsValid == true)
            {
                if (UpdateConteudo(id, editConteudoViewModel.Conteudo) == true)
                {
                    TempData["success"] = "Dados submetidos com êxito!";

                    return RedirectToAction("EditConteudo", new { id = id });
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditConteudoViewModel(editConteudoViewModel);

            return View(editConteudoViewModel);
        }

        private void RefreshCreateEditConteudoViewModel(CreateEditConteudoViewModel createEditConteudoViewModel)
        {
            createEditConteudoViewModel.ConteudoEstadosList = GetConteudoEstados();
            createEditConteudoViewModel.ConteudoVisibilidadesList = GetConteudoVisibilidades();
            createEditConteudoViewModel.PerfisVisibilidadeList = GetPerfisVisibilidade();
            createEditConteudoViewModel.TiposCargosVisibilidadeList = GetTiposCargosVisibilidade();
            createEditConteudoViewModel.OrgaosVisibilidadeList = GetOrgaosVisibilidade();
            createEditConteudoViewModel.GruposQuoVadisVisibilidadeList = GetGruposQuoVadisVisibilidade();
            createEditConteudoViewModel.CategoriasList = GetGategorias();
            List<ConteudoCategoria> conteudoCategorias = _intranetService.STP_ConteudosCategorias_S_ByConteudo(createEditConteudoViewModel.Conteudo.Id);
            foreach (var item in conteudoCategorias)
            {
                createEditConteudoViewModel.Categorias.Add(item.IdCategoria.ToString());
            }
        }
    }
}