using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;

using IntranetSite.App_Start;
using IntranetSite.Clients;
using IntranetSite.Filters;
using IntranetSite.ViewModels;

using IntranetAssemblies.DataAcess;

using IntranetAssemblies.Models.QuoVadis.Aplicacoes;
using IntranetAssemblies.Models.QuoVadis.Mensagens;
using IntranetAssemblies.Models.QuoVadis.RCU;
using IntranetAssemblies.Models.QuoVadis.Dashboard;

namespace IntranetSite.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        private readonly AplicacoesClient _aplicacoesClient = new AplicacoesClient();
        private readonly MensagensClient _mensagensClient = new MensagensClient();
        private readonly RCUClient _rcuClient = new RCUClient();
        private readonly DashboardClient _dashboardClient = new DashboardClient();

        public ActionResult Index()
        {
            //Serve para atualizar o menu todo
            Apresentacao.LoadLinguagem("pt-PT");

            List<AplicacaoPortalBase> aplicacoesPortalBaseDestaquesList = new List<AplicacaoPortalBase>();

            IntranetSession.ClearIndexSession();

            // Se o utilizador estiver autenticado, obt�m, atrav�s do seu IUPI, os seus favoritos e guarda-os na sess�o do browser
            if (Autenticacao.IsLoggedIn() == true)
            {
                //N�o usa os Favoritos do APPS mas sim da DB da intranet
                //IntranetSession.Session.Favoritos = _intranetService.STP_Favoritos_S_ByIUPI(IntranetSession.Session.IUPI);

                //Favoritos da APPS
                IntranetSession.Session.Favoritos = _dashboardClient.GetFavoritosByIUPI(IntranetSession.Session.IUPI);
            }

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.GetAplicacoesFromQuoVadis"]) == true || Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.Mensagens"]) == true)
            {
                // Constr�i as strings dos perfis, tipos de cargos, �rg�os e do username do utilizador, para enviar por URL para os servi�os
                string perfis = "''";
                string tiposCargos = "''";
                string orgaos = "''";
                string username = "''";

                if (Autenticacao.IsLoggedIn() == true)
                {
                    perfis = string.Join(";", IntranetSession.Session.Perfis.Where(sp => sp.Ativo == true).Select(p => p.Tipo).Distinct());
                    tiposCargos = string.Join(";", IntranetSession.Session.Cargos.Select(p => p.CodigoTipoCargo).Distinct());
                    orgaos = string.Join(";", IntranetSession.Session.Cargos.Select(p => p.CodigoOrgao).Distinct());
                    username = IntranetSession.Session.Username;

                    if (perfis == "")
                    {
                        perfis = "''";
                    }

                    if (tiposCargos == "")
                    {
                        tiposCargos = "''";
                    }

                    if (orgaos == "")
                    {
                        orgaos = "''";
                    }
                }

                // Se tal estiver definido no Web.config, obt�m todas as aplica��es do Portal Base para mostrar na p�gina inicial
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.GetAplicacoesFromQuoVadis"]) == true)
                {
                    if (Autenticacao.IsLoggedIn() == true)
                    {
                        List<AplicacaoPortalBase> aplicacoesPortalBaseList = _aplicacoesClient.GetAplicacoesPortalBase(perfis, tiposCargos, orgaos, username, IntranetSession.Session.SelectedLinguagem.Symbol);

                        if(aplicacoesPortalBaseList.Count > 0)
                        {
                            // Obt�m, do RCU, todos os grupos de menu dispon�veis
                            List<GrupoMenu> gruposMenuList = _rcuClient.GetGruposMenu();

                            foreach (AplicacaoPortalBase aplicacaoPortalBase in aplicacoesPortalBaseList)
                            {
                                // Verifica se a aplica��o se encontra em destaque
                                if (aplicacaoPortalBase.DataInicioDashboard <= DateTime.Now && aplicacaoPortalBase.DataFimDashboard >= DateTime.Now)
                                {
                                    aplicacoesPortalBaseDestaquesList.Add(aplicacaoPortalBase);
                                }

                                if (string.IsNullOrEmpty(aplicacaoPortalBase.GruposMenuPortalBase) == false)
                                {
                                    string[] aplicacaoPortalBaseGruposMenu = aplicacaoPortalBase.GruposMenuPortalBase.Split(';');

                                    foreach (string aplicacaoPortalBaseGrupoMenu in aplicacaoPortalBaseGruposMenu)
                                    {
                                        GrupoMenu grupoMenu = gruposMenuList.Single(gm => gm.Codigo == aplicacaoPortalBaseGrupoMenu);

                                        IndexViewModel indexViewModel = IntranetSession.Session.IndexViewModels.SingleOrDefault(ivm => ivm.GrupoMenu == grupoMenu);

                                        if (indexViewModel != default(IndexViewModel))
                                        {
                                            indexViewModel.AplicacoesPortalBaseList.Add(aplicacaoPortalBase);
                                        }
                                        else
                                        {
                                            indexViewModel = new IndexViewModel();

                                            indexViewModel.GrupoMenu = grupoMenu;

                                            switch (indexViewModel.GrupoMenu.Codigo)
                                            {
                                                case "E":
                                                    indexViewModel.GrupoMenuIconHTML = @"<span class=""fa fa-graduation-cap""></span>";

                                                    break;

                                                case "I":
                                                    indexViewModel.GrupoMenuIconHTML = @"<span class=""fa fa-book""></span>";

                                                    break;

                                                case "S":
                                                    indexViewModel.GrupoMenuIconHTML = @"<span class=""fa fa-users""></span>";

                                                    break;

                                                default:
                                                    break;
                                            }

                                            indexViewModel.AplicacoesPortalBaseList.Add(aplicacaoPortalBase);

                                            IntranetSession.Session.IndexViewModels.Add(indexViewModel);
                                        }
                                    }
                                }
                            }

                            IntranetSession.Session.IndexViewModels = IntranetSession.Session.IndexViewModels.OrderBy(ivm => ivm.GrupoMenu.Id).ToList();

                            // Atualiza o menu lateral com as aplica��es anteriormente obtidas
                            IntranetSession.Session.AplicacoesPortalBaseMenuLateral = Apresentacao.BuildAplicacoesPortalBaseMenuLateral(aplicacoesPortalBaseList, gruposMenuList);

                            Apresentacao.SetMenuLateralItemIDs(IntranetSession.Session.TiposConteudoMenuLateral.Concat(IntranetSession.Session.MenuLateral.Concat(IntranetSession.Session.AplicacoesPortalBaseMenuLateral.Concat(IntranetSession.Session.FavoritosMenuLateral))).ToList(), 0);
                        }
                    }
                }

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.Mensagens"]) == true)
                {
                    // Obt�m todas as mensagens a apresentar, guardando-as na sess�o do browser
                    // Come�a por obter as mensagens locais da aplica��o, que se encontram armazenadas na base de dados do Intranet
                    int mensagensDashboardVisibilidade = 0;

                    if (Autenticacao.IsLoggedIn() == true)
                    {
                        mensagensDashboardVisibilidade = 2;
                    }
                    else
                    {
                        mensagensDashboardVisibilidade = 3;
                    }

                    IntranetSession.Session.Mensagens.AddRange(_intranetService.NSI_STP_Mensagens_S_ByVisibilidade(mensagensDashboardVisibilidade));

                    // Depois obt�m as mensagens globais, que s�o definidas a partir do QuoVadis
                    List<Mensagem> mensagensQuoVadisList = _mensagensClient.GetMensagensByVisibilidades(perfis, tiposCargos, orgaos, username);

                    foreach(Mensagem mensagem in mensagensQuoVadisList)
                    {
                        mensagem.IsFromQuoVadis = true;
                    }

                    IntranetSession.Session.Mensagens.AddRange(mensagensQuoVadisList);
                }
            }

            return View(aplicacoesPortalBaseDestaquesList);
        }

        public ActionResult FilterAplicacoes(string keyword)
        {
            List<IndexViewModel> filteredIndexViewModelsList = new List<IndexViewModel>();

            foreach (IndexViewModel indexViewModel in IntranetSession.Session.IndexViewModels)
            {
                IndexViewModel filteredIndexViewModel = new IndexViewModel();

                filteredIndexViewModel.GrupoMenu = indexViewModel.GrupoMenu;
                filteredIndexViewModel.GrupoMenuIconHTML = indexViewModel.GrupoMenuIconHTML;
                filteredIndexViewModel.AplicacoesPortalBaseList = indexViewModel.AplicacoesPortalBaseList.Where(apb => apb.Nome.IndexOf(keyword, 0, StringComparison.CurrentCultureIgnoreCase) > -1 || apb.Descricao.IndexOf(keyword, 0, StringComparison.CurrentCultureIgnoreCase) > -1).ToList();

                filteredIndexViewModelsList.Add(filteredIndexViewModel);
            }

            return PartialView("AplicacoesPartial", filteredIndexViewModelsList);
        }

        [IntranetLoggedIn]
        public ActionResult FilterFavoritos(string keyword)
        {
            List<Favorito> filteredFavoritosList = IntranetSession.Session.Favoritos.Where(f => f.Nome.IndexOf(keyword, 0, StringComparison.CurrentCultureIgnoreCase) > -1).ToList();

            return PartialView("FavoritosPartial", filteredFavoritosList);
        }

        [IntranetLoggedIn]
        public ActionResult LoadCreateFavoritoPartial()
        {
            return PartialView("CreateFavoritoPartial", new Favorito());
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult CreateFavorito(Favorito favorito)
        {
            favorito.IUPI = IntranetSession.Session.IUPI;
            favorito.SiglaDashboard = GetFavoritoSiglaDashboardFromLink(favorito);

            //Favoritos da intranet
            //int? returnValue = _intranetService.STP_Favoritos_I(favorito);

            //Favoritos da APPS
            int? returnValue = _dashboardClient.CreateFavorito(favorito);

            if (returnValue != null && returnValue > 0)
            {
                return Json(returnValue);
            }
            else
            {
                return Json("");
            }
        }

        [IntranetLoggedIn]
        public ActionResult LoadEditFavoritoPartial(int id)
        {
            //Favoritos da intranet
            //return PartialView("EditFavoritoPartial", _intranetService.STP_Favoritos_S(id));

            //Favoritos da APPS
            return PartialView("EditFavoritoPartial", _dashboardClient.GetFavorito(id));
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult EditFavorito(Favorito favorito)
        {
            favorito.SiglaDashboard = GetFavoritoSiglaDashboardFromLink(favorito);

            //Favoritos da intranet
            //int? returnValue = _intranetService.STP_Favoritos_U(favorito);

            //Favoritos da APPS
            int? returnValue = _dashboardClient.UpdateFavorito(favorito);

            if (returnValue != null && returnValue > 0)
            {
                return Json(returnValue);
            }
            else
            {
                return Json("");
            }
        }

        private string GetFavoritoSiglaDashboardFromLink(Favorito favorito)
        {
            string siglaDashboard = "";

            try
            {
                // Obt�m, do link introduzido pelo utilizador, a primeira letra para usar como sigla do �cone
                Uri favoritoLinkURI = null;

                try
                {
                    favoritoLinkURI = new Uri(favorito.Link);
                }
                catch (Exception e)
                {
                    favorito.Link = favorito.Link.Replace("www.", "");
                    favorito.Link = "http://www." + favorito.Link;

                    favoritoLinkURI = new Uri(favorito.Link);
                }

                // Obt�m o host do website, salta a string "www." � frente, retira o primeiro caracter e coloca-o como mai�scula
                // Por exemplo: www.youtube.com -> Y
                siglaDashboard = favoritoLinkURI.Host[4].ToString().ToUpper();
            }
            catch (Exception e)
            {
                return null;
            }

            return siglaDashboard;
        }

        [IntranetLoggedIn]
        public ActionResult LoadDeleteFavoritoPartial(int id)
        {
            //Favoritos da intranet
            //return PartialView("DeleteFavoritoPartial", _intranetService.STP_Favoritos_S(id));

            //Favoritos da APPS
            return PartialView("DeleteFavoritoPartial", _dashboardClient.GetFavorito(id));
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult DeleteFavorito(int id)
        {
            //Favoritos da intranet
            // int? rowCount = _intranetService.STP_Favoritos_D(id);

            //Favoritos da APPS
            int? rowCount = _dashboardClient.DeleteFavorito(id);

            if (rowCount != null && rowCount == 1)
            {
                return Json(rowCount);
            }
            else
            {
                return Json("");
            }
        }

        [IntranetLoggedIn]
        public ActionResult RefreshFavoritosPartial()
        {
            //Favoritos da intranet
            // Atrav�s do IUPI do utilizador, obt�m de novo os seus favoritos do Dashboard e guarda-os na sess�o do browser
            //IntranetSession.Session.Favoritos = _intranetService.STP_Favoritos_S_ByIUPI(IntranetSession.Session.IUPI);

            //Favoritos da APPS
            // Atrav�s do IUPI do utilizador, obt�m de novo os seus favoritos do Dashboard e guarda-os na sess�o do browser
            IntranetSession.Session.Favoritos = _dashboardClient.GetFavoritosByIUPI(IntranetSession.Session.IUPI);

            return PartialView("FavoritosPartial", IntranetSession.Session.Favoritos);
        }

        [IntranetLoggedIn]
        public ActionResult RefreshFavoritosMenuLateralPartial()
        {
            // Com base nos favoritos modificados pelo utilizador, reconstr�i o menu lateral respectivo a esses favoritos, definindo novos ID's para os items desse menu
            IntranetSession.Session.FavoritosMenuLateral = Apresentacao.BuildFavoritosMenuLateral(IntranetSession.Session.Favoritos);

            Apresentacao.SetMenuLateralItemIDs(IntranetSession.Session.TiposConteudoMenuLateral.Concat(IntranetSession.Session.MenuLateral.Concat(IntranetSession.Session.AplicacoesPortalBaseMenuLateral.Concat(IntranetSession.Session.FavoritosMenuLateral))).ToList(), 0);

            return PartialView("_MenuLateralPartial", IntranetSession.Session.FavoritosMenuLateral);
        }

        public ActionResult FilterMensagens(string keyword)
        {
            List<Mensagem> filteredMensagensList = IntranetSession.Session.Mensagens.Where(m => m.Titulo.IndexOf(keyword, 0, StringComparison.CurrentCultureIgnoreCase) > -1).ToList();

            return PartialView("MensagensPartial", filteredMensagensList);
        }

        public ActionResult LoadDetailsMensagemPartial(int id, bool isFromQuoVadis)
        {
            Mensagem mensagem = new Mensagem();

            if(isFromQuoVadis == true)
            {
                mensagem = _mensagensClient.GetMensagem(id);
            }
            else
            {
                mensagem = _intranetService.NSI_STP_Mensagens_S(id);
            }

            return PartialView("DetailsMensagemPartial", mensagem);
        }
    }
}