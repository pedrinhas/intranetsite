using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using System.Threading;
using System.Web.Routing;
using System.Globalization;

using IntranetSite.App_Start;
using IntranetSite.Clients;
using IntranetSite.ViewModels;

using IntranetAssemblies.Models;
using IntranetAssemblies.Models.QuoVadis.Pessoas;

namespace IntranetSite.Controllers
{
    public abstract partial class BaseController : Controller
    {
        private readonly IUTADClient _iUTADClient = new IUTADClient();

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            // Se as linguagens suportadas ainda não foram obtidas, obtém essas linguagens do QuoVadis
            // Depois carrega a linguagem respectiva à máquina do utilizador
            if (IntranetSession.Session.Linguagens.Count == 0)
            {
                bool success = Apresentacao.LoadSupportedLinguagens() && Apresentacao.LoadLinguagem(Thread.CurrentThread.CurrentUICulture.Name);

                if (success == false)
                {
                    TempData["error"] = "Ocorreu um erro ao carregar informação!";
                }
            }

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(IntranetSession.Session.SelectedLinguagem.Symbol);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(IntranetSession.Session.SelectedLinguagem.Symbol);

            // Verifica se é necessário proceder à autenticação por Shibboleth
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.UTAD.Shibboleth"]) == true)
            { 
                if (Autenticacao.IsLoggedIn() == false && IntranetSession.Session.IsInShibbolethLogin == false)
                {
                    // Se encontrar um login do Shibboleth na sessão do browser, tenta autenticá-lo no Intranet
                    if (Session["shib"] != null)
                    {
                        // Coloca a true a flag que diz que o Intranet encontra-se neste momento em uma autenticação por Shibboleth
                        // Esta flag previne que o Intranet entre nesta função múltiplas vezes
                        IntranetSession.Session.IsInShibbolethLogin = true;

                        Login login = Autenticacao.AutenticarLoginShibboleth(Session["shib"].ToString());

                        // Se o login tiver sido autenticado com sucesso, procede para a função de carregamento do login
                        if (login != null && login.IUPI != null && login.IUPI != new Guid())
                        {
                            // -1 - Login não possui permissões
                            // 0 - Ocorreu um erro ao carregar o login
                            // 1 - Login carregado com sucesso
                            // 2 - Login carregado com sucesso (estudante)
                            int success = Autenticacao.LoadLogin(login);

                            // Se é um estudante que está a ser autenticado no Intranet por Shibboleth, o processo só acaba mais à frente, com a selecção de um perfil (matrícula) do estudante
                            // Para o resto dos casos, o processo de autenticação por Shibboleth termina aqui
                            // Portanto coloca a respectiva flag a false
                            if (success == -1 || success == 0 || success == 1)
                            {
                                IntranetSession.Session.IsInShibbolethLogin = false;
                            }

                            switch (success)
                            {
                                case -1:
                                    TempData["error"] = "Não possui permissões para aceder a esta aplicação!";

                                    break;

                                case 0:
                                    TempData["error"] = "Ocorreu um erro ao carregar a informação pessoal!";

                                    break;

                                case 1:
                                    if (Apresentacao.LoadLinguagem(Thread.CurrentThread.CurrentUICulture.Name) == false)
                                    {
                                        TempData["error"] = "Ocorreu um erro ao carregar informação!";
                                    }

                                    break;

                                case 2:
                                    requestContext.HttpContext.Response.Redirect(Url.Action("LoadEstudantePerfil", "Session", new { iupi = login.IUPI }));

                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }

        // Esta é a função principal que é executada sempre que uma página é carregada no Intranet
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            // Só executa o código apenas quando uma view totalmente nova é retornada
            // Sem isto, todo este código é efectuado para qualquer coisa retornada - views, partial views, redirects, etc.
            if (filterContext.Result.GetType() == typeof(ViewResult))
            {
                BaseViewModel layoutModel = new BaseViewModel();

                if (Autenticacao.IsLoggedIn() == true)
                {
                    // Carrega os elementos HTML autorizados para o utilizador autenticado
                    // Primeiro verifica se o mecanismo de autorizações está ativado para o Intranet
                    layoutModel.IsAutorizacoesEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.Autorizacoes"]);

                    if (layoutModel.IsAutorizacoesEnabled == true)
                    {
                        // Da lista de elementos HTML autorizados guardada previamente na sessão do browser, obtém e guarda aqueles que correspondem à página a ser carregada
                        string autorizacoesCurrentURL = RouteData.Values["controller"].ToString() + "/" + RouteData.Values["action"].ToString();

                        layoutModel.AutorizacoesList.AddRange(IntranetSession.Session.Autorizacoes.Where(a => a.Pagina == autorizacoesCurrentURL).ToList());
                    }
                }

                filterContext.Controller.ViewData["LayoutModel"] = layoutModel;
            }
        }

        public ActionResult ChangeLinguagem(string symbol)
        {
            if (Apresentacao.LoadLinguagem(symbol) == false)
            {
                TempData["error"] = "Ocorreu um erro ao carregar informação!";
            }

            return Redirect(Request.UrlReferrer.AbsoluteUri);
        }

        public ActionResult LoadProfilePartial()
        {
            return PartialView("_ProfilePartial");
        }

        public ActionResult SearchPessoas(string searchQuery)
        {
            List<Pessoa> pessoasList = new List<Pessoa>();

            int visibilidade = 0;

            if(Autenticacao.IsLoggedIn() == true)
            {
                visibilidade = 2;
            }
            else
            {
                visibilidade = 3;
            }

            pessoasList = _iUTADClient.SearchPessoas(visibilidade, searchQuery);

            return PartialView("_MenuLateralSearchPessoasPartial", pessoasList);
        }


    }
}