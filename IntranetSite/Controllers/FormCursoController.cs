﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.IO;

using IntranetSite.App_Start;
using IntranetSite.Filters;
using IntranetSite.ViewModels;

using IntranetAssemblies.Models.Intranet;
using IntranetSite.Clients;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using System.Threading.Tasks;

namespace IntranetSite.Controllers
{
    public class FornCursoController : ConteudosFormsController
    {
        private readonly CargosClient _cargosClient = new CargosClient();
        private readonly SIGACADClient _SIGACADClient = new SIGACADClient();
        private readonly DSDClient _DSDClient = new DSDClient();

        //Lista os conteudos pelo tipo de conteudo e orgao
        public async Task<ActionResult> Index(int id)
        {
            ListConteudosViewModel listConteudosViewModel = new ListConteudosViewModel();

            // Obtém, a partir do respectivo ID, todos os dados do tipo de conteúdo
            listConteudosViewModel.TipoConteudo = _cargosClient.GetTipoConteudo(17);

            //Publicos
            var ConteudosList = _intranetService.STP_Conteudos_S_ByTipoConteudoAndEstado(listConteudosViewModel.TipoConteudo.Id, 3, true).OrderByDescending(x => x.DataInicioPublicacao);
            foreach (var item in ConteudosList)
            {
                var Fotos = await GetFotosFromImage(item, _intranetService.STP_ConteudosAnexos_S_ByConteudoAndTipo(item.Id, 1));
                if (Fotos.Count > 0)
                {
                    item.LinkFoto = Fotos.First();
                }
                var Ficha_Curso_Dados = _SIGACADClient.GetCurso(item.CodigoOrgao);

                if (id == 12)
                {
                    if (Ficha_Curso_Dados.IdTipoCurso==id || Ficha_Curso_Dados.IdTipoCurso == 13)
                    {
                        listConteudosViewModel.ConteudosList.Add(item);
                    }
                }
                else
                {
                    if (Ficha_Curso_Dados.IdTipoCurso == id)
                    {
                        listConteudosViewModel.ConteudosList.Add(item);
                    }
                }
            }
            if (id == 12)
            {
                ViewBag.TipoCurso = "Licenciaturas e Mestrados integrados";
            }
            if (id == 14)
            {
                ViewBag.TipoCurso = "Mestrados";
            }
            if (id == 16)
            {
                ViewBag.TipoCurso = "Doutoramentos";
            }


            return View(listConteudosViewModel);
        }


        public async Task<ActionResult> LoadDetailsConteudoPartial(int id)
        {
            DetailsConteudoPartialViewModel detailsConteudoPartialViewModel = new DetailsConteudoPartialViewModel();

            detailsConteudoPartialViewModel.Conteudo = _intranetService.STP_Conteudos_S(id);

            if (detailsConteudoPartialViewModel.Conteudo != null && detailsConteudoPartialViewModel.Conteudo.Id > 0)
            {
                detailsConteudoPartialViewModel.Conteudo.Representante1 = new Pessoas();
                detailsConteudoPartialViewModel.Conteudo.Representante2 = new Pessoas();
                var reprensentantes = _intranetService.STP_Pessoas_S_ByConteudo(id);
                for (int i = 1; i <= reprensentantes.Count(); i++)
                {
                    if (i == 1)
                    {
                        detailsConteudoPartialViewModel.Conteudo.Representante1 = reprensentantes[i - 1];
                    }
                    if (i == 2)
                    {
                        detailsConteudoPartialViewModel.Conteudo.Representante2 = reprensentantes[i - 1];
                    }
                }
                detailsConteudoPartialViewModel.ConteudoAnexosList = _intranetService.STP_ConteudosAnexos_S_ByConteudoAndTipo(id, 2);
                detailsConteudoPartialViewModel.Fotos = await GetFotosFromImage(detailsConteudoPartialViewModel.Conteudo, _intranetService.STP_ConteudosAnexos_S_ByConteudoAndTipo(id, 1));
                detailsConteudoPartialViewModel.Links = GetLinks(detailsConteudoPartialViewModel.Conteudo);
                detailsConteudoPartialViewModel.ConteudoEstado = GetConteudoEstado(detailsConteudoPartialViewModel.Conteudo.Estado);
                detailsConteudoPartialViewModel.ConteudoVisibilidade = GetConteudoVisibilidade(detailsConteudoPartialViewModel.Conteudo.VisibilidadePublica);
                detailsConteudoPartialViewModel.Ficha_Curso_Dados = GetDadosFichaCurso(Convert.ToInt32(detailsConteudoPartialViewModel.Conteudo.Titulo1), detailsConteudoPartialViewModel.Conteudo.CodigoOrgao);
            }

            ViewBag.Detalhes = true;

            return PartialView("DetailsConteudoPartial", detailsConteudoPartialViewModel);
        }

        [IntranetLoggedIn]
        public ActionResult CreateConteudo(int idTipoConteudo, int idOrgao, int Ano)
        {
            bool exist = _intranetService.STP_Conteudos_S_ByTipoConteudoAndOrgao(idTipoConteudo, idOrgao).Exists(x => x.Titulo1 == Ano.ToString());

            if (exist)
            {
                TempData["warning"] = "Já existe uma ficha criada para o ano letivo selecionado! Edite a mesma!";
                return RedirectToAction("CreateConteudos", "Conteudos");
            }
            else
            {

                // Antes de tudo, limpa da sessão do browser quaisquer dados de conteúdos antigos que foram criados/editados
                IntranetSession.ClearConteudoSession();

                CreateEditConteudoViewModel createConteudoViewModel = new CreateEditConteudoViewModel();

                // Obtém, a partir dos respectivos ID's, todos os dados do tipo de conteúdo e do órgão do novo conteúdo
                TipoConteudo tipoConteudo = _cargosClient.GetTipoConteudo(idTipoConteudo);
                Orgao orgao = _cargosClient.GetOrgao(idOrgao);

                // Associa logo ao novo conteúdo os IDs do tipo de conteúdo e da unidade orgânica seleccionados
                createConteudoViewModel.Conteudo.IdTipoConteudo = idTipoConteudo;
                createConteudoViewModel.Conteudo.IdOrgao = idOrgao;
                createConteudoViewModel.Conteudo.NomeTipoConteudo = tipoConteudo.Nome;
                createConteudoViewModel.Conteudo.NomeOrgao = orgao.Nome;
                createConteudoViewModel.Conteudo.CodigoOrgao = orgao.Codigo;

                //Define a data de inicio de publicação por defeito para hoje
                createConteudoViewModel.Conteudo.DataInicioPublicacao = DateTime.Now;

                //Preencher as labels que são necessárias preencher no formulário
                createConteudoViewModel.Conteudo.LabelLink1 = "Link para vídeo";
                createConteudoViewModel.Conteudo.LabelTitulo1 = "Ano Letivo";
                createConteudoViewModel.Conteudo.Titulo1 = Ano.ToString();
                createConteudoViewModel.Conteudo.LabelCheck1 = "Ativo";
                //Colocar partial view com os representantes


                //Apresentação
                createConteudoViewModel.Conteudo.LabelTitulo2 = "Grau Académico";
                createConteudoViewModel.Conteudo.LabelDescricao1 = "Objetivos";
                createConteudoViewModel.Conteudo.LabelTitulo3 = "Área CNAEF";
                //createConteudoViewModel.Conteudo.LabelLink2 = "Área CNAEF";
                //createConteudoViewModel.Conteudo.LabelLink3 = "Plano de Estudos/Diário da República";
                //createConteudoViewModel.Conteudo.LabelLink4 = "Unidades Curriculares";
                //Colocar partial view com plano de estudos
                createConteudoViewModel.Conteudo.LabelTitulo4 = "Horário";
                createConteudoViewModel.Conteudo.LabelTitulo5 = "Duração";
                createConteudoViewModel.Conteudo.LabelValor1 = "ECTS";

                //createConteudoViewModel.Conteudo.LabelLink5 = "Acordos de Mobilidade";
                //createConteudoViewModel.Conteudo.LabelLink6 = "Normas Regulamentares";
                //createConteudoViewModel.Conteudo.LabelLink7 = "Prémios ou Destaques";

                //Saídas
                createConteudoViewModel.Conteudo.LabelDescricao2 = "Saídas Profissionais";
                createConteudoViewModel.Conteudo.LabelDescricao3 = "Perfil do Diplomado";
                createConteudoViewModel.Conteudo.LabelDescricao4 = "Entidades onde Exercer";

                //Candidatura
                //createConteudoViewModel.Conteudo.LabelLink8 = "Código DGES";
                createConteudoViewModel.Conteudo.LabelValor2 = "Vagas";
                createConteudoViewModel.Conteudo.LabelDescricao5 = "Condições de Acesso";
                createConteudoViewModel.Conteudo.LabelDescricao6 = "Provas de Ingresso";
                createConteudoViewModel.Conteudo.LabelDescricao7 = "Nota Mínima";
                createConteudoViewModel.Conteudo.LabelTitulo6 = "Nota do Último Aluno Colocado na 1ª Fase";
                createConteudoViewModel.Conteudo.LabelTitulo7 = "Cálculo da Nota de Acesso";
                //createConteudoViewModel.Conteudo.LabelLink9 = "Mais Informações";

                //Acreditação
                createConteudoViewModel.Conteudo.LabelData1 = "Data da Decisão";
                //createConteudoViewModel.Conteudo.LabelLink10 = "Deliberação da A3ES";
                createConteudoViewModel.Conteudo.LabelData2 = "Data do Registo";
                createConteudoViewModel.Conteudo.LabelData3 = "Validade";

                RefreshCreateEditConteudoViewModel(createConteudoViewModel);

                createConteudoViewModel.Conteudo.Check1 = createConteudoViewModel.Ficha_Curso_Dados.Curso.Ativo;

                int ECTS = createConteudoViewModel.Ficha_Curso_Dados.UCs.Sum(x => x.ECTS);
                createConteudoViewModel.Conteudo.Valor1 = ECTS;

                createConteudoViewModel.Conteudo.LabelLink8 = createConteudoViewModel.Ficha_Curso_Dados.Curso.CodNacional;
                createConteudoViewModel.Conteudo.Link8 = string.Format("https://www.dges.gov.pt/guias/detcursopi.asp?codc={0}&code={1}", createConteudoViewModel.Ficha_Curso_Dados.Curso.CodNacional, createConteudoViewModel.Ficha_Curso_Dados.Curso.CodNacionalEscola);


                Link link = new Link(1, "Informacoes", "Informação estatística", string.Format("http://infocursos.mec.pt/dges.asp?code={0}&codc={1}", createConteudoViewModel.Ficha_Curso_Dados.Curso.CodNacionalEscola, createConteudoViewModel.Ficha_Curso_Dados.Curso.CodNacional));
                IntranetSession.Session.ConteudoLinksList.Add(link);

                createConteudoViewModel.Conteudo.Representante1 = new Pessoas();
                createConteudoViewModel.Conteudo.Representante2 = new Pessoas();

                return View(createConteudoViewModel);
            }
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult CreateConteudo(CreateEditConteudoViewModel createConteudoViewModel)
        {
            if (string.IsNullOrEmpty(createConteudoViewModel.Conteudo.Titulo) == true)
            {
                ModelState.AddModelError("Conteudo.Titulo", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.DataInicioPublicacao == null || createConteudoViewModel.Conteudo.DataInicioPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicioPublicacao", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.DataFimPublicacao != null && createConteudoViewModel.Conteudo.DataFimPublicacao != new DateTime())
            {
                if (createConteudoViewModel.Conteudo.DataFimPublicacao < createConteudoViewModel.Conteudo.DataInicioPublicacao)
                {
                    ModelState.AddModelError("Conteudo.DataFimPublicacao", "A data de fim de publicação tem de ser superior à de início");
                }
            }

            //if (IntranetSession.Session.ConteudoAnexosFiles.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            //{
            //    ModelState.AddModelError("ConteudoAnexoFile", "Tem de escolher no mínimo um ficheiro!");
            //}
            for (int i = 1; i <= 10; i++)
            {
                if (i != 7 && i != 9)
                {


                    var link = createConteudoViewModel.Conteudo.GetType().GetProperty("Link" + i).GetValue(createConteudoViewModel.Conteudo, null).ToString();
                    if (link != null && link != "")
                    {
                        Uri uri = null;

                        if (!Uri.TryCreate(link, UriKind.Absolute, out uri) || null == uri)
                        {
                            //URL inválida
                            ModelState.AddModelError("Conteudo.Link" + i, "Tem de preencher um url válido!");
                        }
                        var Labellink = createConteudoViewModel.Conteudo.GetType().GetProperty("LabelLink" + i).GetValue(createConteudoViewModel.Conteudo, null).ToString();
                        if (Labellink == null || Labellink == "")
                        {
                            ModelState.AddModelError("Conteudo.LabelLink" + i, "Preencha um nome para o link!");
                        }
                    }
                }
            }


            //if (IntranetSession.Session.ConteudoAnexosFotos.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            //{
            //    ModelState.AddModelError("ConteudoAnexoFoto", "Tem de escolher uma foto!");
            //}
            if (IntranetSession.Session.ConteudoLinksList != new List<Link>())
            {
                // Use StringBuilder for concatenation in tight loops.
                var premioLink = new System.Text.StringBuilder();
                var informacaoLink = new System.Text.StringBuilder();
                var premioNome = new System.Text.StringBuilder();
                var informacaoNome = new System.Text.StringBuilder();
                foreach (var item in IntranetSession.Session.ConteudoLinksList)
                {
                    if (item.Tipo == "Premios")
                    {
                        premioNome.AppendLine(string.Concat(item.Nome, ";", item.Data.ToString()));
                        premioLink.AppendLine(string.Concat(item.URL));
                    }
                    else
                    {
                        informacaoNome.AppendLine(string.Concat(item.Nome));
                        informacaoLink.AppendLine(string.Concat(item.URL));
                    }
                }
                createConteudoViewModel.Conteudo.LabelLink7 = premioNome.ToString();
                createConteudoViewModel.Conteudo.Link7 = premioLink.ToString();
                createConteudoViewModel.Conteudo.LabelLink9 = informacaoNome.ToString();
                createConteudoViewModel.Conteudo.Link9 = informacaoLink.ToString();
            }

            if (ModelState.IsValid == true)
            {
                //Cria uma lista com todos os usernames inseridos
                if ((createConteudoViewModel.Conteudo.Representante1.Nome != null && createConteudoViewModel.Conteudo.Representante1.Nome != "") || (createConteudoViewModel.Conteudo.Representante2.Nome != null && createConteudoViewModel.Conteudo.Representante2.Nome != ""))
                {
                    createConteudoViewModel.Conteudo.Pessoas = new List<Pessoas>();
                    //Cria uma lista com todos os usernames inseridos
                    if (createConteudoViewModel.Conteudo.Representante1.Nome != null && createConteudoViewModel.Conteudo.Representante1.Nome != "")
                    {


                        createConteudoViewModel.Conteudo.Pessoas.Add(createConteudoViewModel.Conteudo.Representante1);

                    }
                    if (createConteudoViewModel.Conteudo.Representante2.Nome != null && createConteudoViewModel.Conteudo.Representante2.Nome != "")
                    {


                        createConteudoViewModel.Conteudo.Pessoas.Add(createConteudoViewModel.Conteudo.Representante2);

                    }
                }
                int? id = CreateConteudo(createConteudoViewModel.Conteudo);

                if (id != null && id > 0)
                {
                    TempData["success"] = "Dados submetidos com êxito!";

                    return RedirectToAction("EditConteudo", new { id = id });
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditConteudoViewModel(createConteudoViewModel);

            return View(createConteudoViewModel);
        }

        [IntranetLoggedIn]
        public ActionResult EditConteudo(int id)
        {
            // Antes de tudo, limpa da sessão do browser quaisquer dados de conteúdos antigos que foram criados/editados
            IntranetSession.ClearConteudoSession();

            CreateEditConteudoViewModel editConteudoViewModel = new CreateEditConteudoViewModel();

            editConteudoViewModel.Conteudo = _intranetService.STP_Conteudos_S(id);

            if (editConteudoViewModel.Conteudo != null && editConteudoViewModel.Conteudo.Id > 0)
            {
                editConteudoViewModel.Conteudo.Representante1 = new Pessoas();
                editConteudoViewModel.Conteudo.Representante2 = new Pessoas();
                var reprensentantes = _intranetService.STP_Pessoas_S_ByConteudo(id);
                for (int i = 1; i <= reprensentantes.Count(); i++)
                {
                    if (i == 1)
                    {
                        editConteudoViewModel.Conteudo.Representante1 = reprensentantes[i - 1];
                    }
                    if (i == 2)
                    {
                        editConteudoViewModel.Conteudo.Representante2 = reprensentantes[i - 1];
                    }
                }


                RefreshCreateEditConteudoViewModel(editConteudoViewModel);

                IntranetSession.Session.ConteudoLinksList = GetLinks(editConteudoViewModel.Conteudo);


                // Com os dados do conteúdo obtidos, passa para a obtenção dos dados dos anexos do conteúdo
                List<ConteudoAnexo> conteudoAnexosList = _intranetService.STP_ConteudosAnexos_S_ByConteudo(id);

                if (conteudoAnexosList != null && conteudoAnexosList.Count > 0)
                {
                    try
                    {
                        // Através dos dados do conteúdo, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                        Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                        string conteudoAnexosFilesFolderPath = intranetFolderPath.Valor + editConteudoViewModel.Conteudo.CodigoOrgao + "-" + editConteudoViewModel.Conteudo.DataCriacao.Year + "-" + editConteudoViewModel.Conteudo.DataCriacao.Month + "-" + editConteudoViewModel.Conteudo.DataCriacao.Day;

                        // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                        if (Directory.Exists(conteudoAnexosFilesFolderPath) == true)
                        {
                            foreach (ConteudoAnexo conteudoAnexo in conteudoAnexosList)
                            {
                                // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                                if (System.IO.File.Exists(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexo.Anexo) == true)
                                {
                                    // Se o ficheiro existir, guarda os dados do ficheiro na sessão do browser
                                    // Na view, o plugin Dropzone.JS vai ler estes dados para apresentar os ficheiros que se encontram anexados ao conteúdo
                                    ConteudoAnexoFile conteudoAnexoFile = new ConteudoAnexoFile();

                                    FileInfo conteudoAnexoFileInfo = new FileInfo(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexo.Anexo);

                                    conteudoAnexoFile.Id = conteudoAnexo.Id;
                                    conteudoAnexoFile.Name = conteudoAnexo.Anexo;
                                    conteudoAnexoFile.Size = conteudoAnexoFileInfo.Length;

                                    if (conteudoAnexo.Tipo == 1)
                                    {
                                        IntranetSession.Session.ConteudoAnexosFotos.Add(conteudoAnexoFile);
                                    }
                                    else
                                    {
                                        IntranetSession.Session.ConteudoAnexosFiles.Add(conteudoAnexoFile);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // TO DO: Fazer log de quaisquer excepções que ocorram
                    }
                }
            }

            return View(editConteudoViewModel);
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult EditConteudo(int id, CreateEditConteudoViewModel editConteudoViewModel)
        {
            if (string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Titulo) == true)
            {
                ModelState.AddModelError("Conteudo.Titulo", "Tem de preencher este campo!");
            }

            if (editConteudoViewModel.Conteudo.DataInicioPublicacao == null || editConteudoViewModel.Conteudo.DataInicioPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicioPublicacao", "Tem de preencher este campo!");
            }

            if (editConteudoViewModel.Conteudo.DataFimPublicacao == null || editConteudoViewModel.Conteudo.DataFimPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataFimPublicacao", "Tem de preencher este campo!");
            }

            //if (editConteudoViewModel.Conteudo.DataFimPublicacao < editConteudoViewModel.Conteudo.DataInicioPublicacao)
            //{
            //    ModelState.AddModelError("Conteudo.DataFimPublicacao", "A data de fim de publicação tem de ser superior à de início");
            //}
            if (editConteudoViewModel.Conteudo.DataFimPublicacao != null && editConteudoViewModel.Conteudo.DataFimPublicacao != new DateTime())
            {
                if (editConteudoViewModel.Conteudo.DataFimPublicacao < editConteudoViewModel.Conteudo.DataInicioPublicacao)
                {
                    ModelState.AddModelError("Conteudo.DataFimPublicacao", "A data de fim de publicação tem de ser superior à de início");
                }
            }
            //if (IntranetSession.Session.ConteudoAnexosFotos.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            //{
            //    ModelState.AddModelError("ConteudoAnexoFoto", "Tem de escolher uma foto!");
            //}

            for (int i = 1; i <= 10; i++)
            {
                if (i != 7 && i != 9)
                {


                    var link = editConteudoViewModel.Conteudo.GetType().GetProperty("Link" + i).GetValue(editConteudoViewModel.Conteudo, null).ToString();
                    if (link != null && link != "")
                    {
                        Uri uri = null;

                        if (!Uri.TryCreate(link, UriKind.Absolute, out uri) || null == uri)
                        {
                            //URL inválida
                            ModelState.AddModelError("Conteudo.Link" + i, "Tem de preencher um url válido!");
                        }
                        var Labellink = editConteudoViewModel.Conteudo.GetType().GetProperty("LabelLink" + i).GetValue(editConteudoViewModel.Conteudo, null).ToString();
                        if (Labellink == null || Labellink == "")
                        {
                            ModelState.AddModelError("Conteudo.LabelLink" + i, "Preencha um nome para o link!");
                        }
                    }
                }
            }

            //if (IntranetSession.Session.ConteudoAnexosFiles.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            //{
            //    ModelState.AddModelError("ConteudoAnexoFile", "Tem de escolher no mínimo um ficheiro!");
            //}
            if (IntranetSession.Session.ConteudoLinksList != new List<Link>())
            {
                // Use StringBuilder for concatenation in tight loops.
                var premioLink = new System.Text.StringBuilder();
                var informacaoLink = new System.Text.StringBuilder();
                var premioNome = new System.Text.StringBuilder();
                var informacaoNome = new System.Text.StringBuilder();
                foreach (var item in IntranetSession.Session.ConteudoLinksList)
                {
                    if (item.Tipo == "Premios")
                    {
                        premioNome.AppendLine(string.Concat(item.Nome, ";", item.Data.ToString()));
                        premioLink.AppendLine(string.Concat(item.URL));
                    }
                    else
                    {
                        informacaoNome.AppendLine(string.Concat(item.Nome));
                        informacaoLink.AppendLine(string.Concat(item.URL));
                    }
                }
                editConteudoViewModel.Conteudo.LabelLink7 = premioNome.ToString();
                editConteudoViewModel.Conteudo.Link7 = premioLink.ToString();
                editConteudoViewModel.Conteudo.LabelLink9 = informacaoNome.ToString();
                editConteudoViewModel.Conteudo.Link9 = informacaoLink.ToString();
            }

            if (ModelState.IsValid == true)
            {
                //Cria uma lista com todos os usernames inseridos
                if ((editConteudoViewModel.Conteudo.Representante1.Nome != null && editConteudoViewModel.Conteudo.Representante1.Nome != "") || (editConteudoViewModel.Conteudo.Representante2.Nome != null && editConteudoViewModel.Conteudo.Representante2.Nome != ""))
                {
                    editConteudoViewModel.Conteudo.Pessoas = new List<Pessoas>();
                    //Cria uma lista com todos os usernames inseridos
                    if (editConteudoViewModel.Conteudo.Representante1.Nome != null && editConteudoViewModel.Conteudo.Representante1.Nome != "")
                    {


                        editConteudoViewModel.Conteudo.Pessoas.Add(editConteudoViewModel.Conteudo.Representante1);

                    }
                    if (editConteudoViewModel.Conteudo.Representante2.Nome != null && editConteudoViewModel.Conteudo.Representante2.Nome != "")
                    {

                        editConteudoViewModel.Conteudo.Pessoas.Add(editConteudoViewModel.Conteudo.Representante2);

                    }
                }

                if (UpdateConteudo(id, editConteudoViewModel.Conteudo) == true)
                {
                    TempData["success"] = "Dados submetidos com êxito!";

                    return RedirectToAction("EditConteudo", new { id = id });
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditConteudoViewModel(editConteudoViewModel);

            return View(editConteudoViewModel);
        }

        private void RefreshCreateEditConteudoViewModel(CreateEditConteudoViewModel createEditConteudoViewModel)
        {
            createEditConteudoViewModel.ConteudoEstadosList = GetConteudoEstados();
            createEditConteudoViewModel.ConteudoVisibilidadesList = GetConteudoVisibilidades();
            createEditConteudoViewModel.PerfisVisibilidadeList = GetPerfisVisibilidade();
            createEditConteudoViewModel.TiposCargosVisibilidadeList = GetTiposCargosVisibilidade();
            createEditConteudoViewModel.OrgaosVisibilidadeList = GetOrgaosVisibilidade();
            createEditConteudoViewModel.GruposQuoVadisVisibilidadeList = GetGruposQuoVadisVisibilidade();
            createEditConteudoViewModel.CategoriasList = GetGategorias();
            List<ConteudoCategoria> conteudoCategorias = _intranetService.STP_ConteudosCategorias_S_ByConteudo(createEditConteudoViewModel.Conteudo.Id);
            foreach (var item in conteudoCategorias)
            {
                createEditConteudoViewModel.Categorias.Add(item.IdCategoria.ToString());
            }
            createEditConteudoViewModel.Ficha_Curso_Dados = GetDadosFichaCurso(Convert.ToInt32(createEditConteudoViewModel.Conteudo.Titulo1), createEditConteudoViewModel.Conteudo.CodigoOrgao);

        }

        private Ficha_Curso GetDadosFichaCurso(int Ano, string CodCurso)
        {
            Ficha_Curso ficha_Curso = new Ficha_Curso();

            ficha_Curso.Curso = _SIGACADClient.GetCurso(CodCurso);
            ficha_Curso.CursoDiretores = _DSDClient.GetCurso_Diretores(CodCurso, Ano);
            ficha_Curso.Planos = _SIGACADClient.GetPlanos_Ativos_Curso(Ano, CodCurso);
            ficha_Curso.UCs = _SIGACADClient.GetPlano_UCs(ficha_Curso.Planos.First(x => x.CodEstadoActual == 1).IdRamo);

            return ficha_Curso;
        }
    }
}