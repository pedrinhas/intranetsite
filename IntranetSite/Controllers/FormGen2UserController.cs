﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.IO;

using IntranetSite.App_Start;
using IntranetSite.Filters;
using IntranetSite.ViewModels;

using IntranetAssemblies.Models.Intranet;
using IntranetSite.Clients;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using IntranetSite.Helpers;
using IntranetAssemblies.Models.QuoVadis.RCU;

namespace IntranetSite.Controllers
{
    public class FormGen2UserController : ConteudosFormsController
    {
        private readonly CargosClient _cargosClient = new CargosClient();
        private readonly RCUClient _rcuClient = new RCUClient();

        public ActionResult LoadDetailsConteudoPartial(int id)
        {
            DetailsConteudoPartialViewModel detailsConteudoPartialViewModel = new DetailsConteudoPartialViewModel();

            detailsConteudoPartialViewModel.Conteudo = _intranetService.STP_Conteudos_S(id);

            if (detailsConteudoPartialViewModel.Conteudo != null && detailsConteudoPartialViewModel.Conteudo.Id > 0)
            {
                detailsConteudoPartialViewModel.Conteudo.Pessoas = _intranetService.STP_Pessoas_S_ByConteudo(id);
                detailsConteudoPartialViewModel.ConteudoAnexosList = _intranetService.STP_ConteudosAnexos_S_ByConteudo(id);
                detailsConteudoPartialViewModel.ConteudoEstado = GetConteudoEstado(detailsConteudoPartialViewModel.Conteudo.Estado);
                detailsConteudoPartialViewModel.ConteudoVisibilidade = GetConteudoVisibilidade(detailsConteudoPartialViewModel.Conteudo.VisibilidadePublica);
            }

            return PartialView("DetailsConteudoPartial", detailsConteudoPartialViewModel);
        }

        [IntranetLoggedIn]
        public ActionResult CreateConteudo(int idTipoConteudo, int idOrgao)
        {
            // Antes de tudo, limpa da sessão do browser quaisquer dados de conteúdos antigos que foram criados/editados
            IntranetSession.ClearConteudoSession();

            CreateEditConteudoViewModel createConteudoViewModel = new CreateEditConteudoViewModel();

            // Obtém, a partir dos respectivos ID's, todos os dados do tipo de conteúdo e do órgão do novo conteúdo
            TipoConteudo tipoConteudo = _cargosClient.GetTipoConteudo(idTipoConteudo);

            // Associa logo ao novo conteúdo os IDs do tipo de conteúdo e da unidade orgânica seleccionados
            createConteudoViewModel.Conteudo.IdTipoConteudo = idTipoConteudo;
            createConteudoViewModel.Conteudo.IdOrgao = idOrgao;
            createConteudoViewModel.Conteudo.NomeTipoConteudo = tipoConteudo.Nome;

            //Define a data de inicio de publicação por defeito para hoje
            createConteudoViewModel.Conteudo.DataInicioPublicacao = DateTime.Now;

            //Preencher as labels que são necessárias preencher no formulário
            createConteudoViewModel.Conteudo.LabelData1 = "Data do Documento";
            createConteudoViewModel.Conteudo.LabelDescricao1 = "Pessoas";

            RefreshCreateEditConteudoViewModel(createConteudoViewModel);

            return View(createConteudoViewModel);
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult CreateConteudo(CreateEditConteudoViewModel createConteudoViewModel)
        {
            if (string.IsNullOrEmpty(createConteudoViewModel.Conteudo.Titulo) == true)
            {
                ModelState.AddModelError("Conteudo.Titulo", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.DataInicioPublicacao == null || createConteudoViewModel.Conteudo.DataInicioPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicioPublicacao", "Tem de preencher este campo!");
            }

            if (createConteudoViewModel.Conteudo.DataFimPublicacao != null && createConteudoViewModel.Conteudo.DataFimPublicacao != new DateTime())
            {
                if (createConteudoViewModel.Conteudo.DataFimPublicacao < createConteudoViewModel.Conteudo.DataInicioPublicacao)
                {
                    ModelState.AddModelError("Conteudo.DataFimPublicacao", "A data de fim de publicação tem de ser superior à de início");
                }
            }

            if (IntranetSession.Session.ConteudoAnexosFiles.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            {
                ModelState.AddModelError("ConteudoAnexoFile", "Tem de escolher no mínimo um ficheiro!");
            }

            if (ModelState.IsValid == true)
            {
                //Cria uma lista com todos os usernames inseridos
                if (createConteudoViewModel.Conteudo.Descricao1 != null && createConteudoViewModel.Conteudo.Descricao1 != "")
                {
                    createConteudoViewModel.Conteudo.Pessoas = new List<Pessoas>();
                    List<string> usernames = createConteudoViewModel.Conteudo.Descricao1.Split(';').ToList();
                    foreach (var username in usernames)
                    {
                        Pessoas pessoa = GetPessoa(username);
                        if (pessoa != null)
                            createConteudoViewModel.Conteudo.Pessoas.Add(pessoa);
                    }
                }

                int? id = CreateConteudo(createConteudoViewModel.Conteudo);

                if (id != null && id > 0)
                {
                    TempData["success"] = "Dados submetidos com êxito!";

                    return RedirectToAction("EditConteudo", new { id = id });
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditConteudoViewModel(createConteudoViewModel);

            return View(createConteudoViewModel);
        }

        [IntranetLoggedIn]
        public ActionResult EditConteudo(int id)
        {
            // Antes de tudo, limpa da sessão do browser quaisquer dados de conteúdos antigos que foram criados/editados
            IntranetSession.ClearConteudoSession();

            CreateEditConteudoViewModel editConteudoViewModel = new CreateEditConteudoViewModel();

            editConteudoViewModel.Conteudo = _intranetService.STP_Conteudos_S(id);

            if (editConteudoViewModel.Conteudo != null && editConteudoViewModel.Conteudo.Id > 0)
            {
                RefreshCreateEditConteudoViewModel(editConteudoViewModel);

                // Com os dados do conteúdo obtidos, passa para a obtenção dos dados dos anexos do conteúdo
                List<ConteudoAnexo> conteudoAnexosList = _intranetService.STP_ConteudosAnexos_S_ByConteudo(id);

                if (conteudoAnexosList != null && conteudoAnexosList.Count > 0)
                {
                    try
                    {
                        // Através dos dados do conteúdo, obtém o caminho da pasta onde os ficheiros dos anexos se encontram
                        Parametro intranetFolderPath = _intranetService.NSI_STP_Parametros_S_ByChave("Aplicacao.FolderPath");

                        string conteudoAnexosFilesFolderPath = intranetFolderPath.Valor + editConteudoViewModel.Conteudo.CodigoOrgao + "-" + editConteudoViewModel.Conteudo.DataCriacao.Year + "-" + editConteudoViewModel.Conteudo.DataCriacao.Month + "-" + editConteudoViewModel.Conteudo.DataCriacao.Day;

                        // Verifica se a pasta correspondente ao caminho anteriormente obtido existe
                        if (Directory.Exists(conteudoAnexosFilesFolderPath) == true)
                        {
                            foreach (ConteudoAnexo conteudoAnexo in conteudoAnexosList)
                            {
                                // Verifica se o ficheiro correspondente ao anexo do conteúdo existe
                                if (System.IO.File.Exists(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexo.Anexo) == true)
                                {
                                    // Se o ficheiro existir, guarda os dados do ficheiro na sessão do browser
                                    // Na view, o plugin Dropzone.JS vai ler estes dados para apresentar os ficheiros que se encontram anexados ao conteúdo
                                    ConteudoAnexoFile conteudoAnexoFile = new ConteudoAnexoFile();

                                    FileInfo conteudoAnexoFileInfo = new FileInfo(conteudoAnexosFilesFolderPath + @"\" + conteudoAnexo.Anexo);

                                    conteudoAnexoFile.Id = conteudoAnexo.Id;
                                    conteudoAnexoFile.Name = conteudoAnexo.Anexo;
                                    conteudoAnexoFile.Size = conteudoAnexoFileInfo.Length;

                                    IntranetSession.Session.ConteudoAnexosFiles.Add(conteudoAnexoFile);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // TO DO: Fazer log de quaisquer excepções que ocorram
                    }
                }
            }

            return View(editConteudoViewModel);
        }

        [HttpPost]
        [IntranetLoggedIn]
        public ActionResult EditConteudo(int id, CreateEditConteudoViewModel editConteudoViewModel)
        {
            if (string.IsNullOrEmpty(editConteudoViewModel.Conteudo.Titulo) == true)
            {
                ModelState.AddModelError("Conteudo.Titulo", "Tem de preencher este campo!");
            }

            if (editConteudoViewModel.Conteudo.DataInicioPublicacao == null || editConteudoViewModel.Conteudo.DataInicioPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataInicioPublicacao", "Tem de preencher este campo!");
            }

            if (editConteudoViewModel.Conteudo.DataFimPublicacao == null || editConteudoViewModel.Conteudo.DataFimPublicacao == new DateTime())
            {
                ModelState.AddModelError("Conteudo.DataFimPublicacao", "Tem de preencher este campo!");
            }

            if (editConteudoViewModel.Conteudo.DataFimPublicacao != null && editConteudoViewModel.Conteudo.DataFimPublicacao != new DateTime())
            {
                if (editConteudoViewModel.Conteudo.DataFimPublicacao < editConteudoViewModel.Conteudo.DataInicioPublicacao)
                {
                    ModelState.AddModelError("Conteudo.DataFimPublicacao", "A data de fim de publicação tem de ser superior à de início");
                }
            }

            if (IntranetSession.Session.ConteudoAnexosFiles.Where(caf => caf.WasDeletedByUser == false).Count() < 1)
            {
                ModelState.AddModelError("ConteudoAnexoFile", "Tem de escolher no mínimo um ficheiro!");
            }

            if (ModelState.IsValid == true)
            {
                //Cria uma lista com os usernames 
                if (editConteudoViewModel.Conteudo.Descricao1 != null && editConteudoViewModel.Conteudo.Descricao1 != "")
                {
                    editConteudoViewModel.Conteudo.Pessoas = new List<Pessoas>();
                    List<string> usernames = editConteudoViewModel.Conteudo.Descricao1.Split(';').ToList();
                    foreach (var username in usernames)
                    {
                        Pessoas pessoa = GetPessoa(username);
                        if (pessoa != null)
                            editConteudoViewModel.Conteudo.Pessoas.Add(pessoa);
                    }
                }

                if (UpdateConteudo(id, editConteudoViewModel.Conteudo) == true)
                {
                    TempData["success"] = "Dados submetidos com êxito!";

                    return RedirectToAction("EditConteudo", new { id = id });
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados!";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            RefreshCreateEditConteudoViewModel(editConteudoViewModel);

            return View(editConteudoViewModel);
        }

        private void RefreshCreateEditConteudoViewModel(CreateEditConteudoViewModel createEditConteudoViewModel)
        {
            createEditConteudoViewModel.ConteudoEstadosList = GetConteudoEstados();
            createEditConteudoViewModel.ConteudoVisibilidadesList = GetConteudoVisibilidades();
            createEditConteudoViewModel.PerfisVisibilidadeList = GetPerfisVisibilidade();
            createEditConteudoViewModel.TiposCargosVisibilidadeList = GetTiposCargosVisibilidade();
            createEditConteudoViewModel.OrgaosVisibilidadeList = GetOrgaosVisibilidade();
            createEditConteudoViewModel.GruposQuoVadisVisibilidadeList = GetGruposQuoVadisVisibilidade();
            createEditConteudoViewModel.CategoriasList = GetGategorias();
            List<ConteudoCategoria> conteudoCategorias = _intranetService.STP_ConteudosCategorias_S_ByConteudo(createEditConteudoViewModel.Conteudo.Id);
            foreach (var item in conteudoCategorias)
            {
                createEditConteudoViewModel.Categorias.Add(item.IdCategoria.ToString());
            }
        }

        //Através do usermame associa o nome e email
        private Pessoas GetPessoa(string username)
        {
            string usernameSplit = Utilities.GetNumMecFromUsername(username);

            if (string.IsNullOrEmpty(usernameSplit) == false)
            {
                Guid? estudanteIUPI = _rcuClient.GetEstudanteIUPI(Convert.ToInt32(usernameSplit));

                if (estudanteIUPI != null && estudanteIUPI != new Guid())
                {
                    List<EstudantePerfil> estudantePerfisList = _rcuClient.GetEstudantePerfis(estudanteIUPI.Value);

                    if (estudantePerfisList != null && estudantePerfisList.Count > 0)
                    {
                        EstudantePerfil estudante = estudantePerfisList.Where(x => x.IdEstado == "1").FirstOrDefault();
                        Pessoas pessoa = new Pessoas(username, estudante.Nome, estudante.Email);
                        return pessoa;
                    }
                }
            }
            else
            {
                Guid? utilizadorIUPI = _rcuClient.GetUtilizadorIUPI(username);

                if (utilizadorIUPI != null && utilizadorIUPI != new Guid())
                {
                    List<UtilizadorPerfil> utilizadorPerfisList = _rcuClient.GetUtilizador(utilizadorIUPI.Value);

                    if (utilizadorPerfisList != null && utilizadorPerfisList.Count > 0)
                    {
                        UtilizadorPerfil utilizador = utilizadorPerfisList.Where(x => x.UtilizadorAtivo == true).FirstOrDefault();
                        Pessoas pessoa = new Pessoas(username, utilizador.Nome, utilizador.Email);
                        return pessoa;
                    }
                }
            }

            return null;
        }
    }
}