﻿using IntranetSite.Helpers;
using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.SIGACAD;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace IntranetSite.Clients
{
    public class SIGACADClient
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        public Curso GetCurso(string codCurso)
        {
            Curso curso = new Curso();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.SIGACAD.GetCurso");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, codCurso);

                        curso = JsonConvert.DeserializeObject<Curso>(wcWS.DownloadString(url).ToUTF8());
                    }

                }
            }
            catch (Exception e)
            {
                return null;
            }

            return curso;
        }

       
        public List<Plano> GetPlanos_Ativos_Curso(int ano, string codCurso)
        {
            List<Plano> planosList = new List<Plano>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.SIGACAD.GetPlanos_Ativos_Curso");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, ano, codCurso);

                        planosList = JsonConvert.DeserializeObject<List<Plano>>(wcWS.DownloadString(url).ToUTF8());
                    }

                }
            }
            catch (Exception e)
            {
                return null;
            }

            return planosList;
        }

        public List<UC> GetPlano_UCs(int idRamo)
        {
            List<UC> ucsList = new List<UC>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.SIGACAD.GetPlano_UCs");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, idRamo);

                        ucsList = JsonConvert.DeserializeObject<List<UC>>(wcWS.DownloadString(url).ToUTF8());
                    }

                }
            }
            catch (Exception e)
            {
                return null;
            }

            return ucsList;
        }
    }
}