using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using System.Net;
using System.Text;

using Newtonsoft.Json;

using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Pessoas;

namespace IntranetSite.Clients
{
    public class IUTADClient
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        public Login Auth(string username, string password)
        {
            Login login = new Login();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Autenticar");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));
                        wcWS.Encoding = Encoding.UTF8;
                        wcWS.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                        LoginCredenciais loginCredenciais = new LoginCredenciais();

                        loginCredenciais.Username = username;
                        loginCredenciais.Password = password;

                        string bodyWS = JsonConvert.SerializeObject(loginCredenciais);

                        login = JsonConvert.DeserializeObject<Login>(wcWS.UploadString(urlWS.Valor, bodyWS));
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return login;
        }

        public List<Pessoa> SearchPessoas(int visibilidade, string searchQuery)
        {
            List<Pessoa> pessoasList = new List<Pessoa>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.IUTAD.SearchPessoas");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, visibilidade, searchQuery);

                        pessoasList = JsonConvert.DeserializeObject<List<Pessoa>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return pessoasList;
        }
    }
}