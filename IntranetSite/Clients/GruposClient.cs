using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;

using Newtonsoft.Json;

using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Grupos;

namespace IntranetSite.Clients
{
    public class GruposClient
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        public IntranetAssemblies.Models.QuoVadis.Grupos.Grupo GetGrupo(int id)
        {
            IntranetAssemblies.Models.QuoVadis.Grupos.Grupo grupo = new IntranetAssemblies.Models.QuoVadis.Grupos.Grupo();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Grupos.GetGrupo");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, id);

                        grupo = JsonConvert.DeserializeObject<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return grupo;
        }

        public List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> GetGruposByTipo(int grupoTipo)
        {
            List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> gruposList = new List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Grupos.GetGruposByTipo");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, grupoTipo);

                        gruposList = JsonConvert.DeserializeObject<List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return gruposList;
        }

        public List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> GetGruposByUtilizador(Guid utilizadorIUPI)
        {
            List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> gruposList = new List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Grupos.GetGruposByUtilizador");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, utilizadorIUPI);

                        gruposList = JsonConvert.DeserializeObject<List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return gruposList;
        }

        public List<Utilizador> GetGrupoUtilizadores(string nome)
        {
            List<Utilizador> utilizadoresList = new List<Utilizador>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Grupos.GetGrupoUtilizadores");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, nome);

                        utilizadoresList = JsonConvert.DeserializeObject<List<Utilizador>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return utilizadoresList;
        }
    }
}