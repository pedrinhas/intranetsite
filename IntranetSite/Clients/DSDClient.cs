﻿using IntranetSite.Helpers;
using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.DSD;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace IntranetSite.Clients
{
    public class DSDClient
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        public List<CursoDiretor> GetCurso_Diretores(string idCurso, int ano)
        {
            List<CursoDiretor> cursoDiretoresList = new List<CursoDiretor>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.DSD.GetCurso_Diretores");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, idCurso,ano);

                        cursoDiretoresList = JsonConvert.DeserializeObject<List<CursoDiretor>>(wcWS.DownloadString(url).ToUTF8());
                    }

                }
            }
            catch (Exception e)
            {
                return null;
            }

            return cursoDiretoresList;
        }
    }
}