using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;

using Newtonsoft.Json;

using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Aplicacoes;

namespace IntranetSite.Clients
{
    public class AplicacoesClient
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        public List<Linguagem> GetLinguagens()
        {
            List<Linguagem> linguagensList = new List<Linguagem>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Aplicacoes.GetLinguagens");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        linguagensList = JsonConvert.DeserializeObject<List<Linguagem>>(wcWS.DownloadString(urlWS.Valor).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return linguagensList;
        }

        public List<AplicacaoPortalBase> GetAplicacoesPortalBase(string perfisIntranet, string tiposCargosIntranet, string orgaosIntranet, string pessoasIntranet, string symbolLinguagem)
        {
            List<AplicacaoPortalBase> aplicacoesDashboardList = new List<AplicacaoPortalBase>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Aplicacoes.GetAplicacoesPortalBase");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, perfisIntranet, tiposCargosIntranet, orgaosIntranet, pessoasIntranet, symbolLinguagem);

                        aplicacoesDashboardList = JsonConvert.DeserializeObject<List<AplicacaoPortalBase>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return aplicacoesDashboardList;
        }

        public AplicacaoPortalBase GetAplicacaoPortalBase(string identificadorIntranet, string symbolLinguagem)
        {
            AplicacaoPortalBase aplicacaoIntranet = new AplicacaoPortalBase();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Aplicacoes.GetAplicacaoPortalBase");

                if(string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, identificadorIntranet, symbolLinguagem);

                        aplicacaoIntranet = JsonConvert.DeserializeObject<AplicacaoPortalBase>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch(Exception e)
            {
                return null;
            }

            return aplicacaoIntranet;
        }
    }
}