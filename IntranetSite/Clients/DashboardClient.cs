﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;

using Newtonsoft.Json;

using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using System.Text;
using System.Net.Http;
using IntranetAssemblies.Models.QuoVadis.Dashboard;


namespace IntranetSite.Clients
{
    public class DashboardClient
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

   
        public Favorito GetFavorito(int id)
        {
            Favorito favorito = new Favorito();
            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Dashboard.GetFavorito");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, id);

                        favorito= JsonConvert.DeserializeObject<Favorito>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return favorito;
        }

        public List<Favorito> GetFavoritosByIUPI(Guid iupi)
        {
            List<Favorito> favoritosList = new List<Favorito>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Dashboard.GetFavoritosByIUPI");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, iupi);

                        favoritosList = JsonConvert.DeserializeObject<List<Favorito>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch(Exception e)
            {
                return null;
            }

            return favoritosList;
        }

        public int? CreateFavorito(Favorito favorito)
        {
            int returnValue = 0;

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Dashboard.CreateFavorito");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));
                        wcWS.Encoding = Encoding.UTF8;
                        wcWS.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                        string bodyWS = JsonConvert.SerializeObject(favorito);

                        returnValue = JsonConvert.DeserializeObject<int>(wcWS.UploadString(urlWS.Valor, bodyWS));
                    }
                }
            }
            catch(Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public int? UpdateFavorito(Favorito favorito)
        {
            int returnValue = 0;

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Dashboard.UpdateFavorito");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));
                        wcWS.Encoding = Encoding.UTF8;
                        wcWS.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                        string bodyWS = JsonConvert.SerializeObject(favorito);

                        returnValue = JsonConvert.DeserializeObject<int>(wcWS.UploadString(urlWS.Valor, bodyWS));
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public int? DeleteFavorito(int id)
        {
            int rowCount = 0;

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Dashboard.DeleteFavorito");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (HttpClientHandler httpWSHandler = new HttpClientHandler())
                    {
                        httpWSHandler.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        using (HttpClient httpWS = new HttpClient(httpWSHandler))
                        {
                            string url = string.Format(urlWS.Valor, id);

                            HttpResponseMessage httpWSResponse = httpWS.DeleteAsync(url).Result;

                            rowCount = JsonConvert.DeserializeObject<int>(httpWSResponse.Content.ReadAsStringAsync().Result);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                return null;
            }

            return rowCount;
        }
    }
}