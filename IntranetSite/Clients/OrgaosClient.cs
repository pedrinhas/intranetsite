using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;

using Newtonsoft.Json;

using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using System.Data.SqlClient;
using IntranetAssemblies.Extensions;

namespace IntranetSite.Clients
{
    public class CargosClient
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);
        private readonly string _cargosDBString = ConfigurationManager.ConnectionStrings["cargosDBString"].ConnectionString;

        public List<Cargo> GetCargosByPessoa(string pessoa)
        {
            List<Cargo> cargosList = new List<Cargo>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetCargosByPessoa");

                if(string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, pessoa);

                        cargosList = JsonConvert.DeserializeObject<List<Cargo>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return cargosList;
        }

        public List<Categoria> GetCategorias()
        {
            List<Categoria> categoriasList = new List<Categoria>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetCategorias");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        categoriasList = JsonConvert.DeserializeObject<List<Categoria>>(wcWS.DownloadString(urlWS.Valor).ToUTF8());
                    }

                }
            }
            catch (Exception e)
            {
                return null;
            }

            return categoriasList;
        }

        public Categoria GetCategoria(int id)
        {
            Categoria categoria = new Categoria();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetCategoria");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, id);

                        categoria = JsonConvert.DeserializeObject<Categoria>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return categoria;
        }

        public Orgao GetOrgaosTree()
        {
            Orgao res = null;

            try
            {
                var orgaosList = GetOrgaosByEstado(true).ToArray(); //evitar usar muitas vezes o mesmo enumerable, este m�todo pode ser mudado para algo que fa�a queries e esteja atr�s de um ienumerable

                //TODO: assume-se que o �rg�o topo tem id de parent a 0, e � apenas um
                if (orgaosList.Count(x => x.IdOrgaoParent == 0) != 1)
                {
                    throw new InvalidOperationException("N�o existe um �rg�o de topo (idParent a 0) ou existe mais que um");
                }

                res = orgaosList.GenerateTree(x => x.Id, x => x.IdOrgaoParent, 0).Single(); //s� deveria gerar um elemento no topo, porque s� um �rg�o devia ter um parent == 0
            }
            catch (Exception e)
            {
                res = null;
            }

            return res;
        }

        public ITreeItemHard<Orgao> GetOrgaosTreeHard()
        {
            ITreeItemHard<Orgao> res = null;

            try
            {
                var orgaosList = GetOrgaosByEstado(true).ToArray(); //evitar usar muitas vezes o mesmo enumerable, este m�todo pode ser mudado para algo que fa�a queries e esteja atr�s de um ienumerable

                //TODO: assume-se que o �rg�o topo tem id de parent a 0, e � apenas um
                if (orgaosList.Count(x => x.IdOrgaoParent == 0) != 1)
                {
                    throw new InvalidOperationException("N�o existe um �rg�o de topo (idParent a 0) ou existe mais que um");
                }

                res = orgaosList.GenerateHardTree(x => x.Id, x => x.IdOrgaoParent, 0).Single(); //s� deveria gerar um elemento no topo, porque s� um �rg�o devia ter um parent == 0
            }
            catch (Exception e)
            {
                res = null;
            }

            return res;
        }

        public List<Orgao> GetOrgaosByOrgaoParent(int idOrgaoParent)
        {
            List<Orgao> orgaosList = new List<Orgao>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_cargosDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "spGetOrgaosByOrgaoParent";
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgaoParent", idOrgaoParent));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Orgao orgao = new Orgao();

                                orgao.Id = dbReader.GetInt32(0);
                                orgao.IdTipoOrgao = dbReader.GetInt32(1);
                                orgao.CodigoTipoOrgao= dbReader.GetString(2);
                                orgao.Nome = dbReader.GetString(3);
                                orgao.Contato = dbReader.GetString(4);
                                orgao.Morada = dbReader.GetString(5);
                                orgao.Codigo= dbReader.GetString(6);
                                orgao.Grupos = dbReader.GetString(7);
                                orgao.CodigoRH = dbReader.GetString(8);
                                orgao.Extensao = dbReader.GetString(9);
                                orgao.IdOrgaoParent = idOrgaoParent;
                                orgao.Ativo = true;
                                orgaosList.Add(orgao);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return orgaosList;
        }

        public List<Orgao> GetOrgaosByEstado(bool ativo)
        {
            List<Orgao> orgaosList = new List<Orgao>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetOrgaosByEstado");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, ativo);

                        orgaosList = JsonConvert.DeserializeObject<List<Orgao>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return orgaosList;
        }

        public List<Orgao> GetOrgaosByGrupoSecretariado(string grupoSecretariado)
        {
            List<Orgao> orgaosList = new List<Orgao>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetOrgaosByGrupoSecretariado");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, grupoSecretariado);

                        orgaosList = JsonConvert.DeserializeObject<List<Orgao>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return orgaosList;
        }

        public Orgao GetOrgao(int id)
        {
            Orgao orgao = new Orgao();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetOrgao");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, id);

                        orgao = JsonConvert.DeserializeObject<Orgao>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return orgao;
        }

        public List<TipoCargo> GetTiposCargos()
        {
            List<TipoCargo> tiposCargosList = new List<TipoCargo>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetTiposCargos");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        tiposCargosList = JsonConvert.DeserializeObject<List<TipoCargo>>(wcWS.DownloadString(urlWS.Valor).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return tiposCargosList;
        }

        public List<TipoConteudo> GetTiposConteudos()
        {
            List<TipoConteudo> tiposConteudosList = new List<TipoConteudo>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetTiposConteudos");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        tiposConteudosList = JsonConvert.DeserializeObject<List<TipoConteudo>>(wcWS.DownloadString(urlWS.Valor).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return tiposConteudosList;
        }

        public List<TipoConteudo> GetTiposConteudosByCategoria(int idCategoria)
        {
            List<TipoConteudo> tiposConteudosList = new List<TipoConteudo>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetTiposConteudosByCategoria");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, idCategoria);

                        tiposConteudosList = JsonConvert.DeserializeObject<List<TipoConteudo>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return tiposConteudosList;
        }

        public List<TipoConteudo> GetTiposConteudosByOrgao(int idOrgao)
        {
            List<TipoConteudo> tiposConteudosList = new List<TipoConteudo>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetTiposConteudosByOrgao");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, idOrgao);

                        tiposConteudosList = JsonConvert.DeserializeObject<List<TipoConteudo>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
            tiposConteudosList = tiposConteudosList.OrderBy(x => x.Nome).ToList(); ;
            return tiposConteudosList;
        }

        public TipoConteudo GetTipoConteudo(int id)
        {
            TipoConteudo tipoConteudo = new TipoConteudo();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetTipoConteudo");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, id);

                        tipoConteudo = JsonConvert.DeserializeObject<TipoConteudo>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return tipoConteudo;
        }
    }
}