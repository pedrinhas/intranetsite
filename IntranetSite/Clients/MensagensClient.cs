using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;

using Newtonsoft.Json;

using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Mensagens;

namespace IntranetSite.Clients
{
    public class MensagensClient
    {
        private readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        public Mensagem GetMensagem(int id)
        {
            Mensagem mensagem = new Mensagem();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Mensagens.GetMensagem");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, id);

                        mensagem = JsonConvert.DeserializeObject<Mensagem>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return mensagem;
        }

        public List<Mensagem> GetMensagensByVisibilidades(string perfisPublicacao, string tiposCargosPublicacao, string orgaosPublicacao, string pessoasPublicacao)
        {
            List<Mensagem> mensagensList = new List<Mensagem>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("QuoVadis.Mensagens.GetMensagensByVisibilidades");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, perfisPublicacao, tiposCargosPublicacao, orgaosPublicacao, pessoasPublicacao);

                        mensagensList = JsonConvert.DeserializeObject<List<Mensagem>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return mensagensList;
        }
    }
}