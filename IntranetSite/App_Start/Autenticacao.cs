using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;

using IntranetSite.Clients;
using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using IntranetAssemblies.Models.QuoVadis.RCU;

namespace IntranetSite.App_Start
{
    public class Autenticacao
    {
        private static readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        private static readonly CargosClient _cargosClient = new CargosClient();
        private static readonly GruposClient _gruposClient = new GruposClient();
        private static readonly RCUClient _rcuClient = new RCUClient();

        public static bool IsLoginAdmin(string username)
        {
            bool success = false;

            try
            {
                // Obtém o IUPI do suposto administrador e só procede se o mesmo realmente se encontrar no grupo de administração do Intranet
                Guid? adminImpersonacaoIUPI = _rcuClient.GetUtilizadorIUPI(username);

                if (adminImpersonacaoIUPI != null && adminImpersonacaoIUPI != new Guid())
                {
                    // Para verificar se o suposto administrador é mesmo um administrador no Intranet, obtém prematuramente os grupos a que esse utilizador pertence, e verifica se um desses grupos é o grupo de administração
                    if (_intranetService.NSI_STP_Grupo_S_ByIUPI(adminImpersonacaoIUPI.Value).SingleOrDefault(g => g.Nome == "ADMIN") != default(Grupo))
                    {
                        success = true;
                    }
                }
            }
            catch (Exception e)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        public static bool IsLoggedIn()
        {
            return GetLoginStatus();
        }

        public static bool IsAdmin()
        {
            if(GetLoginStatus() == true)
            {
                if (IsInGrupo("ADMIN", Enums.TipoGrupoPortalBase.GRUPO) == true)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsEstudante()
        {
            if(GetLoginStatus() == true)
            {
                if (string.IsNullOrEmpty(Utilities.GetNumMecFromUsername(IntranetSession.Session.Username)) == false)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsTemporario()
        {
            if(GetLoginStatus() == true)
            {
                Parametro credenciaisTemporariasDominio = _intranetService.NSI_STP_Parametros_S_ByChave("Auth.CredenciaisTemporarias.Dominio");

                if (string.IsNullOrEmpty(credenciaisTemporariasDominio.Valor) == false)
                {
                    if (IntranetSession.Session.Username.Contains(credenciaisTemporariasDominio.Valor) == true)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool IsInGrupo(string nomeGrupo, Enums.TipoGrupoPortalBase tipoGrupo)
        {
            if (GetLoginStatus() == true)
            {
                if (IntranetSession.Session.Grupos.SingleOrDefault(g => g.Nome == nomeGrupo && g.IdTipo == Convert.ToInt32(tipoGrupo)) != default(Grupo))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool GetLoginStatus()
        {
            bool isLoggedIn = false;

            // Primeiro verifica se a sessão do browser possui dados de um login
            if (string.IsNullOrEmpty(IntranetSession.Session.Username) == false && (IntranetSession.Session.IUPI != null && IntranetSession.Session.IUPI != new Guid()))
            {
                // Depois verifica se a sessão do browser possui a informação pessoal desse login
                // Isto é necessário para verificar, por exemplo, se um estudante seleccionou um perfil (matrícula) para entrar no Intranet
                // Se esse estudante ainda não seleccionou um perfil (matrícula), então esse estudante ainda não se encontra totalmente autenticado
                if (string.IsNullOrEmpty(IntranetSession.Session.Nome) == false)
                {
                    isLoggedIn = true;
                }
            }

            return isLoggedIn;
        }

        public static Login AutenticarLoginShibboleth(string shibbolethUsername)
        {
            Login login = new Login();

            try
            {
                login.Username = shibbolethUsername;

                string shibbolethUsernameSplit = Utilities.GetNumMecFromUsername(login.Username);

                if (string.IsNullOrEmpty(shibbolethUsernameSplit) == false)
                {
                    NomeAndIUPI estudanteNomeAndIUPI = _rcuClient.GetEstudanteNomeAndIUPI(shibbolethUsernameSplit);

                    login.Nome = estudanteNomeAndIUPI.Nome;
                    login.IUPI = estudanteNomeAndIUPI.IUPI;
                }
                else
                {
                    NomeAndIUPI utilizadorNomeAndIUPI = _rcuClient.GetUtilizadorNomeAndIUPI(shibbolethUsername);

                    login.Nome = utilizadorNomeAndIUPI.Nome;
                    login.IUPI = utilizadorNomeAndIUPI.IUPI;
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return login;
        }

        public static int LoadLogin(Login login)
        {
            // Começa por registar o acesso deste login, guardando o seu ID na sessão
            bool success = RegistarLoginAcesso(login.Username, login.IUPI, login.Nome, DateTime.Now, true);

            if (success == true)
            {
                // Obtém os cargos e os perfis do login (incluíndo os não ativos), guardando todos esses dados nas respetivas variáveis de sessão
                // Dependendo do valor da flag de simulação de autenticação presente no Web.config, usa diferentes mecanismos para obter esses cargos e perfis
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.UTAD.Simul"]) == true)
                {
                    success = AutenticacaoSimul.LoadLoginCargosAndPerfisSimul(login.Username, login.IUPI);
                }
                else
                {
                    success = LoadLoginCargosAndPerfis(login.Username, login.IUPI);
                }

                // Só procede com a verificação de permissões apenas se tal mecanismo estiver ativado no Web.config
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.Autorizacoes"]) == true)
                {
                    if (success == true)
                    {
                        // Com os cargos e os perfis obtidos anteriormente, verifica se o login possui permissões para aceder à aplicação, verificando as suas autorizações
                        success = LoadLoginGrupos() && LoadLoginGruposQuoVadis(login.IUPI) && LoadLoginAutorizacoes();

                        if (success == false)
                        {
                            return -1;
                        }
                    }
                }

                if (success == true)
                {
                    // Carrega toda a informação pessoal do login
                    // A partir daqui o login é considerado autenticado pelo Intranet, e "Autenticacao.IsLoggedIn" passa a retornar true
                    if (LoadLoginInfo(login.Username, login.IUPI) == true)
                    {
                        string usernameSplit = Utilities.GetNumMecFromUsername(login.Username);

                        if (string.IsNullOrEmpty(usernameSplit) == false)
                        {
                            return 2;
                        }
                        else
                        {
                            return 1;
                        }
                    }
                }
            }

            return 0;
        }

        public static int LoadLoginCredenciaisTemporarias(CredenciaisTemporarias credenciaisTemporarias)
        {
            bool success = RegistarLoginAcesso(credenciaisTemporarias.Username, credenciaisTemporarias.IUPI, credenciaisTemporarias.Nome, DateTime.Now, false);

            if(success == true)
            {
                // Com os dados das credenciais temporárias, cria um objeto perfil e guarda-o sessão do browser
                // Com estes dados ir-se-á verificar se o login possui permissões para aceder à aplicação
                SessionPerfil credenciaisTemporariasPerfil = new SessionPerfil();

                credenciaisTemporariasPerfil.Id = credenciaisTemporarias.Id;
                credenciaisTemporariasPerfil.Ano = credenciaisTemporarias.DataCriacao.Year;
                credenciaisTemporariasPerfil.Tipo = credenciaisTemporarias.TipoPerfil;
                credenciaisTemporariasPerfil.Ativo = true;

                IntranetSession.Session.Perfis.Add(credenciaisTemporariasPerfil);

                // Só procede com a verificação de permissões apenas se tal mecanismo estiver ativado no Web.config
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.Autorizacoes"]) == true)
                {
                    if (success == true)
                    {
                        // Verifica se o login possui permissões para aceder à aplicação, verificando as suas autorizações
                        success = LoadLoginGrupos() && LoadLoginAutorizacoes();

                        if (success == false)
                        {
                            return -1;
                        }
                    }
                }

                if(success == true)
                {
                    // Carrega toda a informação pessoal das credenciais temporárias
                    // A partir daqui as credenciais temporárias são consideradas autenticadas pelo Intranet, e "Autenticacao.IsLoggedIn" passa a retornar true
                    IntranetSession.Session.NMec = credenciaisTemporarias.NMec;
                    IntranetSession.Session.Username = credenciaisTemporarias.Username;
                    IntranetSession.Session.IUPI = credenciaisTemporarias.IUPI;
                    IntranetSession.Session.Nome = credenciaisTemporarias.Nome;

                    return 1;
                }
            }

            return 0;
        }

        private static bool RegistarLoginAcesso(string username, Guid iupi, string nome, DateTime loginDate, bool isFromUTAD)
        {
            bool success = false;

            try
            {
                int idUser = _intranetService.NSI_STP_User_IU(username, iupi, nome, loginDate, isFromUTAD);

                if (idUser > 0)
                {
                    IntranetSession.Session.IdUser = idUser;

                    success = true;
                }
            }
            catch (Exception e)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        private static bool LoadLoginCargosAndPerfis(string username, Guid iupi)
        {
            bool success = false;

            try
            {
                string usernameSplit = Utilities.GetNumMecFromUsername(username);

                if (string.IsNullOrEmpty(usernameSplit) == false)
                {
                    // Como os estudantes não possuem cargos, inicializa a lista de cargos do estudante como vazia
                    IntranetSession.Session.Cargos = new List<Cargo>();

                    List<EstudantePerfil> estudantePerfis = _rcuClient.GetEstudantePerfis(iupi);

                    foreach(EstudantePerfil estudantePerfil in estudantePerfis)
                    {
                        SessionPerfil sessionPerfil = new SessionPerfil();

                        sessionPerfil.Id = estudantePerfil.IdMatricula;
                        sessionPerfil.Ano = Convert.ToInt32(estudantePerfil.AnoLectivo);
                        sessionPerfil.Tipo = "ALUNOS";
                        sessionPerfil.Categoria = estudantePerfil.Estado;
                        sessionPerfil.CodigoCategoria = estudantePerfil.IdEstado;
                        sessionPerfil.UnidadeOrganica = estudantePerfil.Curso;
                        sessionPerfil.CodigoUnidadeOrganica = estudantePerfil.CodCurso;
                        
                        if(estudantePerfil.IdEstado == "1")
                        {
                            sessionPerfil.Ativo = true;
                        }

                        IntranetSession.Session.Perfis.Add(sessionPerfil);
                    }
                }
                else
                {
                    IntranetSession.Session.Cargos = _cargosClient.GetCargosByPessoa(username);

                    List<UtilizadorPerfil> utilizador = _rcuClient.GetUtilizador(iupi);

                    foreach(UtilizadorPerfil utilizadorPerfil in utilizador)
                    {
                        SessionPerfil sessionPerfil = new SessionPerfil();

                        sessionPerfil.Id = utilizadorPerfil.Id;
                        sessionPerfil.Ano = utilizadorPerfil.DataEntrada.Year;
                        sessionPerfil.Tipo = utilizadorPerfil.PerfilTipo;
                        sessionPerfil.Categoria = utilizadorPerfil.Categoria;
                        sessionPerfil.CodigoCategoria = utilizadorPerfil.CodCategoria;
                        sessionPerfil.UnidadeOrganica = utilizadorPerfil.Entidade;
                        sessionPerfil.CodigoUnidadeOrganica = utilizadorPerfil.CodEntidade;
                        sessionPerfil.Ativo = utilizadorPerfil.PerfilAtivo;

                        IntranetSession.Session.Perfis.Add(sessionPerfil);
                    }
                }

                success = true;
            }
            catch(Exception e)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        private static bool LoadLoginGrupos()
        {
            bool success = false;

            try
            {
                List<Grupo> gruposList = new List<Grupo>();

                // Verifica cada perfil que o login possui, e vê se existe um grupo correspondente na base de dados
                // Se existir, adiciona esse grupo obtido à lista de grupos
                foreach (SessionPerfil perfil in IntranetSession.Session.Perfis.Where(sp => sp.Ativo == true))
                {
                    Grupo perfilGrupo = _intranetService.NSI_STP_Grupo_S_ByNomeAndTipo(perfil.Tipo, Convert.ToInt32(Enums.TipoGrupoPortalBase.PERFIL));

                    if (perfilGrupo.Id != 0)
                    {
                        // No entanto, se já existir na lista de grupos um grupo com o mesmo nome, então não adiciona o grupo obtido à lista de grupos
                        if (gruposList.SingleOrDefault(g => g.Nome == perfilGrupo.Nome) == default(Grupo))
                        {
                            gruposList.Add(perfilGrupo);
                        }
                    }
                }

                // De seguida verifica cada cargo que o login possui, e vê se existe um grupo correspondente na base de dados
                // Se existir, adiciona de igual forma esse grupo obtido à lista de grupos
                foreach (Cargo cargo in IntranetSession.Session.Cargos)
                {
                    Grupo cargoGrupo = _intranetService.NSI_STP_Grupo_S_ByNomeAndTipo(cargo.TipoCargo, Convert.ToInt32(Enums.TipoGrupoPortalBase.CARGO));

                    if (cargoGrupo.Id != 0)
                    {
                        // No entanto, se já existir na lista de grupos um grupo com o mesmo nome, então não adiciona o grupo obtido à lista de grupos
                        if (gruposList.SingleOrDefault(g => g.Nome == cargoGrupo.Nome) == default(Grupo))
                        {
                            gruposList.Add(cargoGrupo);
                        }
                    }
                }

                // Por fim, através do ID de acesso do login guardado anteriormente, obtém e adiciona todos os grupos (do tipo "G"), definidos na própria base de dados, a que esse login pertence
                gruposList.AddRange(_intranetService.NSI_STP_Grupo_S_ByUser(IntranetSession.Session.IdUser));

                // Guarda todos os grupos obtidos na sessão do browser
                IntranetSession.Session.Grupos.AddRange(gruposList);

                success = true;
            }
            catch (Exception e)
            {

            }

            if (success == false)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        private static bool LoadLoginGruposQuoVadis(Guid iupi)
        {
            bool success = false;

            try
            {
                List<Grupo> userGruposList = new List<Grupo>();

                // Obtém todos os grupos do QuoVadis a que o login pertence
                // Como os grupos do QuoVadis são de dois tipos totalmente distintos, é necessário obter os grupos de cada um desses tipos de maneiras diferentes
                List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> gruposQuoVadisList = new List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo>();

                // Primeiro obtemos os grupos de sistema (onde utilizadores são adicionados manualmente) a que o login pertence
                gruposQuoVadisList.AddRange(_gruposClient.GetGruposByUtilizador(iupi));

                // Depois obtemos os grupos (onde queries de SQL são usadas para agrupar os seus utilizadores)
                List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> gruposQuoVadisSQLList = _gruposClient.GetGruposByTipo(Convert.ToInt32(Enums.TipoGrupoQuoVadis.GRUPO));

                foreach (IntranetAssemblies.Models.QuoVadis.Grupos.Grupo grupoQuoVadisSQL in gruposQuoVadisSQLList)
                {
                    // Por cada grupo obtido anteriormente, obtém os seus respectivos utilizadores
                    List<IntranetAssemblies.Models.QuoVadis.Grupos.Utilizador> utilizadoresGrupoQuoVadisSQLList = _gruposClient.GetGrupoUtilizadores(grupoQuoVadisSQL.Nome);

                    // Através do IUPI, verifica se o login pertence ao grupo
                    if (utilizadoresGrupoQuoVadisSQLList.SingleOrDefault(u => u.Iupi == iupi) != default(IntranetAssemblies.Models.QuoVadis.Grupos.Utilizador))
                    {
                        gruposQuoVadisList.Add(grupoQuoVadisSQL);
                    }
                }

                // Estando obtidos todos os grupos do QuoVadis a que o utilizador pertence, guarda esses grupos na sessão do browser
                IntranetSession.Session.GruposQuoVadis = gruposQuoVadisList;

                // Verifica se algum dos grupos do QuoVadis a que o utilizador pertence possui permissões no Intranet
                // Se sim, guarda esse grupo na sessão do browser
                foreach (IntranetAssemblies.Models.QuoVadis.Grupos.Grupo grupoQuoVadis in gruposQuoVadisList)
                {
                    Grupo quoVadisGrupo = _intranetService.NSI_STP_Grupo_S_ByNomeAndTipo(grupoQuoVadis.Nome, Convert.ToInt32(Enums.TipoGrupoPortalBase.GRUPOQUOVADIS));

                    if (quoVadisGrupo.Id != 0)
                    {
                        if (userGruposList.SingleOrDefault(g => g.Nome == quoVadisGrupo.Nome) == default(Grupo))
                        {
                            userGruposList.Add(quoVadisGrupo);
                        }
                    }
                }

                IntranetSession.Session.Grupos.AddRange(userGruposList);

                success = true;
            }
            catch (Exception e)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        private static bool LoadLoginAutorizacoes()
        {
            bool success = false;

            try
            {
                // Apenas continua se o login pertence a pelo menos um grupo
                if (IntranetSession.Session.Grupos.Count > 0)
                {
                    // Por cada grupo a que o login pertence, obtém os elementos HTML autorizados a esse mesmo grupo e guarda-os, sem repetições, na respetiva variável de sessão
                    foreach (Grupo grupo in IntranetSession.Session.Grupos)
                    {
                        List<Autorizacao> grupoAutorizacoes = _intranetService.NSI_STP_Autorizacao_S_ByGrupo(grupo.Id);

                        foreach (Autorizacao grupoAutorizacao in grupoAutorizacoes)
                        {
                            if (IntranetSession.Session.Autorizacoes.SingleOrDefault(a => a.Id == grupoAutorizacao.Id) == default(Autorizacao))
                            {
                                IntranetSession.Session.Autorizacoes.Add(grupoAutorizacao);
                            }
                        }
                    }

                    success = true;
                }
            }
            catch (Exception e)
            {

            }

            if (success == false)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        private static bool LoadLoginInfo(string username, Guid iupi)
        {
            // Guarda o username e o IUPI do login na sessão do browser
            IntranetSession.Session.Username = username;
            IntranetSession.Session.IUPI = iupi;

            string usernameSplit = Utilities.GetNumMecFromUsername(username);

            try
            {
                if (string.IsNullOrEmpty(usernameSplit) == true)
                {
                    // De todos os perfis ativos do login, selecciona automaticamente o primeiro 
                    UtilizadorPerfil utilizadorPerfil = _rcuClient.GetUtilizador(iupi).Where(up => up.PerfilAtivo == true).First();

                    IntranetSession.Session.Nome = utilizadorPerfil.Nome;

                    return true;
                }
                else
                {
                    // Obtém a informação da foto do estudante e guarda-a também na sessão do browser
                    // Por agora obtém apenas a foto - os dados do estudante são carregados mais à frente
                    EstudanteFoto estudanteFoto = _rcuClient.GetEstudanteFoto(iupi);

                    if (estudanteFoto.Foto != null && estudanteFoto.Foto.Count() > 0)
                    {
                        string estudanteFotoBase64String = Utilities.EncodeBase64String(estudanteFoto.Foto);

                        IntranetSession.Session.FotoHTMLSrc = string.Format("data:image/jpg;base64,{0}", estudanteFotoBase64String);
                    }

                    return true;
                }
            }
            catch(Exception e)
            {
                IntranetSession.ClearLoginSession();
            }

            return false;
        }

        public static void LoadEstudantePerfilInfo(string nome, string codigoCurso, string regime)
        {
            IntranetSession.Session.Nome = nome;
            IntranetSession.Session.CodigoCurso = codigoCurso;
            IntranetSession.Session.Regime = regime;
        }
    }
}