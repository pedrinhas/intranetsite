using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Text;
using System.Configuration;

using Newtonsoft.Json;

using IntranetSite.Helpers;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;

namespace IntranetSite.App_Start
{
    public class AutenticacaoSimul
    {
        private static readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        public static Login AutenticarLoginWSSimul(string username, string password)
        {
            Login login = new Login();

            try
            {
                Parametro urlWS = new Parametro();

                if (string.IsNullOrEmpty(Utilities.GetNumMecFromUsername(username)) == false)
                {
                    urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("API.UTAD.AutenticarEstudanteSimul");
                }
                else
                {
                    urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("API.UTAD.AutenticarUtilizadorSimul");
                }

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));
                        wcWS.Encoding = Encoding.UTF8;
                        wcWS.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                        string bodyWS = JsonConvert.SerializeObject(new { username = username, password = password });

                        login = JsonConvert.DeserializeObject<Login>(wcWS.UploadString(urlWS.Valor, bodyWS));
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return login;
        }

        public static bool LoadLoginCargosAndPerfisSimul(string username, Guid iupi)
        {
            bool success = false;

            try
            {
                string usernameSplit = Utilities.GetNumMecFromUsername(username);

                if (string.IsNullOrEmpty(usernameSplit) == false)
                {
                    // Como os estudantes não possuem cargos, inicializa a lista de cargos do estudante como vazia
                    IntranetSession.Session.Cargos = new List<Cargo>();

                    IntranetSession.Session.Perfis = GetEstudantePerfisSimul(usernameSplit);
                }
                else
                {
                    IntranetSession.Session.Cargos = GetUtilizadorCargosSimul(iupi);
                    IntranetSession.Session.Perfis = GetUtilizadorPerfisSimul(iupi);
                }

                success = true;
            }
            catch (Exception e)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        private static List<SessionPerfil> GetEstudantePerfisSimul(string numero)
        {
            List<SessionPerfil> perfisList = new List<SessionPerfil>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("API.UTAD.GetEstudantePerfisSimul");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, numero);

                        perfisList = JsonConvert.DeserializeObject<List<SessionPerfil>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return perfisList;
        }

        private static List<Cargo> GetUtilizadorCargosSimul(Guid iupi)
        {
            List<Cargo> cargosList = new List<Cargo>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("API.UTAD.GetUtilizadorCargosSimul");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, iupi);

                        cargosList = JsonConvert.DeserializeObject<List<Cargo>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return cargosList;
        }

        private static List<SessionPerfil> GetUtilizadorPerfisSimul(Guid iupi)
        {
            List<SessionPerfil> perfisList = new List<SessionPerfil>();

            try
            {
                Parametro urlWS = _intranetService.NSI_STP_Parametros_S_ByChave("API.UTAD.GetUtilizadorPerfisSimul");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, iupi);

                        perfisList = JsonConvert.DeserializeObject<List<SessionPerfil>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return perfisList;
        }
    }
}