using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetSite.Models;
using IntranetSite.ViewModels;

using IntranetAssemblies.Models;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using IntranetAssemblies.Models.QuoVadis.Dashboard;
using IntranetAssemblies.Models.QuoVadis.Mensagens;

namespace IntranetSite.App_Start
{
    public class IntranetSession
    {
        public static Session Session
        {
            get
            {
                Session session = (Session)HttpContext.Current.Session["IntranetSession"];

                if (session == null)
                {
                    session = new Session();

                    HttpContext.Current.Session["IntranetSession"] = session;
                }

                return session;
            }
        }

        public static void ClearLoginSession()
        {
            ClearConteudoSession();
            ClearIndexSession();

            Session.IsInShibbolethLogin = false;

            Session.IdUser = 0;
            Session.NMec = "";
            Session.IUPI = new Guid();
            Session.Username = "";
            Session.Nome = "";
            Session.FotoHTMLSrc = "";

            Session.CodigoCurso = "";
            Session.Regime = "";

            Session.Cargos = new List<Cargo>();
            Session.Perfis = new List<SessionPerfil>();
            Session.GruposQuoVadis = new List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo>();

            Session.Grupos = new List<Grupo>();
            Session.Autorizacoes = new List<Autorizacao>();

            Session.TiposConteudoMenuLateral = new List<MenuItem>();
            Session.CategoriasMenuLateral = new List<MenuItem>();
            Session.Contactos = new List<MenuItem>();

            Session.MenuLateral = new List<MenuItem>();
            Session.FavoritosMenuLateral = new List<MenuItem>();
            Session.AplicacoesPortalBaseMenuLateral = new List<MenuItem>();

            Session.NoticasMenuLateral = new List<MenuItem>();
            Session.EventosMenuLateral = new List<MenuItem>();
            Session.OrgaosMenuLateral = new List<MenuItem>();
            Session.EstudarMenuLateral = new List<MenuItem>();
        }

        public static void ClearIndexSession()
        {
            Session.IndexViewModels = new List<IndexViewModel>();
            Session.Favoritos = new List<Favorito>();
            Session.Mensagens = new List<Mensagem>();
        }

        public static void ClearConteudoSession()
        {
            Session.ConteudoAnexosFiles = new List<ConteudoAnexoFile>();
            Session.ConteudoAnexosFotos = new List<ConteudoAnexoFile>();
            Session.ConteudoLinksList = new List<Link>();
        }

        
    }
}