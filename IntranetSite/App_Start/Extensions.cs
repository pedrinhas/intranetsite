using System.Text;

namespace IntranetSite
{
    public static class Extensions
    {
        public static string ToUTF8(this string s)
        {
            byte[] sBytes = Encoding.Default.GetBytes(s);

            return Encoding.UTF8.GetString(sBytes);
        }
    }
}