using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using System.Web.Mvc;

using IntranetSite.Clients;

using IntranetAssemblies.DataAcess;
using IntranetAssemblies.Models;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Aplicacoes;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using IntranetAssemblies.Models.QuoVadis.RCU;
using IntranetAssemblies.Models.QuoVadis.Dashboard;
using System.Threading.Tasks;
using IntranetSite.Helpers;


namespace IntranetSite.App_Start
{
    public class Apresentacao
    {
        private static readonly IntranetService _intranetService = new IntranetService(ConfigurationManager.ConnectionStrings["intranetDBString"].ConnectionString);

        private static readonly AplicacoesClient _aplicacoesClient = new AplicacoesClient();
        private static readonly CargosClient _cargosClient = new CargosClient();
        private static readonly DashboardClient _dashboardClient = new DashboardClient();
        private static readonly PortalBaseClient _portalBaseClient = new PortalBaseClient();
        private static readonly RCUClient _rcuClient = new RCUClient();

        public static bool LoadSupportedLinguagens()
        {
            bool success = false;

            try
            {
                // Obtém os dados de todas as linguagens do QuoVadis
                List<Linguagem> linguagensList = _aplicacoesClient.GetLinguagens();

                // Depois obtém, do Web.config, a lista de linguagens suportadas pelo Intranet
                string[] intranetSupportedLinguagens = ConfigurationManager.AppSettings["Aplicacao.SupportedLinguagens"].Split(';');

                // Da lista de linguagens do QuoVadis, selecciona apenas as suportadas
                IntranetSession.Session.Linguagens = linguagensList.Where(l => intranetSupportedLinguagens.Contains(l.Symbol) == true).ToList();

                // Se acontecer algum erro, faz fallback para a linguagem portuguesa ("pt-PT")
                if (IntranetSession.Session.Linguagens == null || IntranetSession.Session.Linguagens.Count <= 0)
                {
                    IntranetSession.Session.Linguagens.Add(linguagensList.Single(l => l.Symbol == "pt-PT"));
                }

                success = true;
            }
            catch (Exception e)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        public static bool LoadLinguagem(string symbol)
        {
            bool success = false;

            try
            {
                // Verifica se o símbolo da linguagem passado como parâmetro existe na lista de linguagens suportadas
                // Se tal símbolo não existir, faz fallback para a linguagem portuguesa ("pt-PT")
                symbol = IntranetSession.Session.Linguagens.Any(l => l.Symbol == symbol) ? symbol : "pt-PT";

                IntranetSession.Session.SelectedLinguagem = IntranetSession.Session.Linguagens.Single(l => l.Symbol == symbol);

                if (IntranetSession.Session.SelectedLinguagem != default(Linguagem))
                {
                    success = LoadApresentacaoData();
                }
            }
            catch (Exception e)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        private static bool LoadApresentacaoData()
        {
            bool success = false;

            try
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.Publico"]) == false)
                {
                    // Obtém os dados da aplicação do próprio Intranet
                    IntranetSession.Session.AplicacaoPortalBase = _aplicacoesClient.GetAplicacaoPortalBase(ConfigurationManager.AppSettings["Aplicacao.IdentificadorPortalBase"], IntranetSession.Session.SelectedLinguagem.Symbol);

                    //Vais buscar a lista de contactos
                    List<Contactos> contactos = _intranetService.STP_Contactos_LS();
                    if (contactos.Count > 0)
                    {
                        IntranetSession.Session.Contactos = BuildContactosMenuLateral(contactos);
                    }

                    // Obtém, a partir dos respectivos ID's, todos os dados dos tipos de conteúdos, categoria
                    //Vai buscar as categorias
                    List<Categoria> categoriasList = new List<Categoria>();
                    List<TipoConteudo> tiposConteudoList = new List<TipoConteudo>();
                    List<TipoConteudo> tiposConteudoList_conteudos = new List<TipoConteudo>();

                    if (Autenticacao.IsLoggedIn())
                    {
                        //No caso de ser admin na plataforma
                        if (Autenticacao.IsLoginAdmin(IntranetSession.Session.Username))
                        {
                            // Obtém, a partir dos respectivos ID's, todos os dados dos tipos de conteúdos, categoria e conteudos
                            //categoriasList.AddRange(_intranetService.STP_Conteudos_S_CategoriasByEstado(3, false));
                            tiposConteudoList_conteudos.AddRange(_intranetService.STP_Conteudos_S_TipoConteudoByEstado(3, false));

                            //Completa a informação dos tipos de conteudos
                            foreach (var item in tiposConteudoList_conteudos)
                            {
                                tiposConteudoList.Add(_cargosClient.GetTipoConteudo(item.Id));
                            }
                        }
                        else
                        {
                            //retorna os conteudos que não são publicos e verifica quais o utilizador tem acesso
                            List<Conteudo> conteudos = new List<Conteudo>();
                            conteudos = _intranetService.STP_Conteudos_S_ByEstado(3, false);
                            List<Conteudo> conteudosPermission = new List<Conteudo>();
                            conteudosPermission = CheckPermission.CheckPermissionConteudos(conteudos);

                            // retornar os conteudos de acordo com as definições de visualiação de cada um
                            Parallel.ForEach(conteudosPermission, (conteudo) =>
                            {
                            //Categorias
                            List<ConteudoCategoria> conteudosCategorias = _intranetService.STP_ConteudosCategorias_S_ByConteudoAndEstado(conteudo.Id, 3, false).ToList();
                                if (conteudosCategorias != null)
                                {
                                    foreach (var conteudoCategoria in conteudosCategorias)
                                    {
                                        categoriasList.Add(_intranetService.STP_Categorias_S(conteudoCategoria.IdCategoria));
                                    }
                                }
                            //Tipos de Conteudo
                            tiposConteudoList.Add(_cargosClient.GetTipoConteudo(conteudo.IdTipoConteudo));

                            });
                        }
                    }
                    else
                    {
                        //Públicos
                        categoriasList.AddRange(_intranetService.STP_Conteudos_S_CategoriasByEstado(3, true));
                        tiposConteudoList_conteudos.AddRange(_intranetService.STP_Conteudos_S_TipoConteudoByEstado(3, true));

                    }

                    //Completa a informação dos tipos de conteudos
                    foreach (var item in tiposConteudoList_conteudos)
                    {
                        tiposConteudoList.Add(_cargosClient.GetTipoConteudo(item.Id));
                    }

                    //Ordena os tipos de conteudos e remove os duplicados
                    tiposConteudoList = tiposConteudoList
                                                     .GroupBy(s => s.Id)
                                                     .Select(grp => grp.FirstOrDefault())
                                                     .OrderBy(s => s.Nome)
                                                     .ToList();
                    //Ordena as categorias e remove os duplicados
                    categoriasList = categoriasList
                                                   .GroupBy(s => s.Id)
                                                   .Select(grp => grp.FirstOrDefault())
                                                   .OrderBy(s => s.Nome)
                                                   .ToList();



                    //Constroi o menu dos tipos de conteudos
                    if (tiposConteudoList.Count > 0)
                    {
                        IntranetSession.Session.TiposConteudoMenuLateral = BuildTiposConteudoMenuLateral(tiposConteudoList);
                    }

                    //Constroi o menu das categorias
                    if (categoriasList.Count > 0)
                    {
                        IntranetSession.Session.CategoriasMenuLateral = BuildCategoriasMenuLateral(categoriasList);
                    }

                    // Só é necessário obter estes recursos (aplicações do Intranet, menu lateral, etc.) se o utilizador estiver autenticado
                    if (Autenticacao.IsLoggedIn() == true)
                    {
                        // Define os items e subitems do menu lateral do Intranet
                        IntranetSession.Session.MenuLateral = BuildMenuLateral();

                        // Obtém os favoritos do utilizador e, com eles, constrói o respectivo menu lateral
                        //Usa os favoritos do APPS 
                        List<Favorito> favoritosList = _dashboardClient.GetFavoritosByIUPI(IntranetSession.Session.IUPI);


                        if (favoritosList.Count > 0)
                        {
                            IntranetSession.Session.FavoritosMenuLateral = BuildFavoritosMenuLateral(favoritosList);
                        }

                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.GetAplicacoesFromQuoVadis"]) == true)
                        {
                            string perfisIntranet = string.Join(";", IntranetSession.Session.Perfis.Where(sp => sp.Ativo == true).Select(p => p.Tipo).Distinct());
                            string tiposCargosIntranet = string.Join(";", IntranetSession.Session.Cargos.Select(p => p.CodigoTipoCargo).Distinct());
                            string orgaosIntranet = string.Join(";", IntranetSession.Session.Cargos.Select(p => p.CodigoOrgao).Distinct());

                            if (perfisIntranet == "")
                            {
                                perfisIntranet = "''";
                            }

                            if (tiposCargosIntranet == "")
                            {
                                tiposCargosIntranet = "''";
                            }

                            if (orgaosIntranet == "")
                            {
                                orgaosIntranet = "''";
                            }

                            List<AplicacaoPortalBase> aplicacoesIntranet = _aplicacoesClient.GetAplicacoesPortalBase(perfisIntranet, tiposCargosIntranet, orgaosIntranet, IntranetSession.Session.Username, IntranetSession.Session.SelectedLinguagem.Symbol);

                            // Obtém, do RCU, todos os grupos de menu disponíveis
                            List<GrupoMenu> gruposMenu = _rcuClient.GetGruposMenu();

                            if (aplicacoesIntranet.Count > 0)
                            {
                                IntranetSession.Session.AplicacoesPortalBaseMenuLateral = BuildAplicacoesPortalBaseMenuLateral(aplicacoesIntranet, gruposMenu);
                            }
                        }

                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.Autorizacoes"]) == true)
                        {
                            // Se sim, filtra os items e os subitems do menu lateral, para desta forma ficarem apenas aqueles que o utilizador autenticado pode ver
                            CheckMenuLateralAutorizacoes(IntranetSession.Session.MenuLateral);
                        }
                    }

                    SetMenuLateralItemIDs(IntranetSession.Session.Contactos.Concat(IntranetSession.Session.TiposConteudoMenuLateral.Concat(IntranetSession.Session.CategoriasMenuLateral.Concat(IntranetSession.Session.MenuLateral.Concat(IntranetSession.Session.AplicacoesPortalBaseMenuLateral.Concat(IntranetSession.Session.FavoritosMenuLateral))))).ToList(), 0);
                }
                else
                {
                    List<ConteudoPropriedade> EstudarTipo = new List<ConteudoPropriedade>();
                    EstudarTipo.Add(new ConteudoPropriedade("Licenciaturas", "12"));
                    EstudarTipo.Add(new ConteudoPropriedade("Mestrados", "14"));
                    EstudarTipo.Add(new ConteudoPropriedade("Doutoramentos", "16"));
                    IntranetSession.Session.NoticasMenuLateral= BuildNoticiasMenuLateral();
                    IntranetSession.Session.EventosMenuLateral = BuildEventosMenuLateral();
                    IntranetSession.Session.OrgaosMenuLateral = BuildOrgaosMenuLateral();
                    IntranetSession.Session.EstudarMenuLateral = BuildEstudarMenuLateral(EstudarTipo);

                    SetMenuLateralItemIDs(IntranetSession.Session.NoticasMenuLateral.Concat(IntranetSession.Session.EventosMenuLateral.Concat(IntranetSession.Session.OrgaosMenuLateral.Concat(IntranetSession.Session.EstudarMenuLateral))).ToList(), 0);

                }

                // Obtém o rodapé para apresentar no Intranet
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.GetFooterFromQuoVadis"]) == true)
                {
                    IntranetSession.Session.Footer = _portalBaseClient.GetFooter();
                }

                success = true;
            }
            catch (Exception e)
            {
                IntranetSession.ClearLoginSession();
            }

            return success;
        }

        private static List<MenuItem> BuildNoticiasMenuLateral()
        {
            UrlHelper menuLateralURLHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            string iconHTML = @"<span class=""fa fa-{0} fa-fw""></span>";
            string searchIconHTML = string.Format(iconHTML, "search");

            List<MenuItem> noticiasMenu = new List<MenuItem>();


            noticiasMenu.Add(new MenuItem(0, "NoticasMenuItem", "Notícias", "Notícias", menuLateralURLHelper.Action("Index", "FormNoticias"), searchIconHTML));

            return noticiasMenu;
        }
        private static List<MenuItem> BuildEventosMenuLateral()
        {
            UrlHelper menuLateralURLHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            string iconHTML = @"<span class=""fa fa-{0} fa-fw""></span>";
            string searchIconHTML = string.Format(iconHTML, "search");

            List<MenuItem> eventosMenu = new List<MenuItem>();


            eventosMenu.Add(new MenuItem(0, "EventosMenuItem", "Eventos", "Eventos", menuLateralURLHelper.Action("Index", "FormEventos"), searchIconHTML));

            return eventosMenu;
        }
        private static List<MenuItem> BuildOrgaosMenuLateral()
        {
            UrlHelper menuLateralURLHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            string iconHTML = @"<span class=""fa fa-{0} fa-fw""></span>";
            string searchIconHTML = string.Format(iconHTML, "search");

            List<MenuItem> orgaoMenu = new List<MenuItem>();


            orgaoMenu.Add(new MenuItem(0, "OrgaosMenuIntem", "Orgãos da UTAD", "Orgãos da UTAD", menuLateralURLHelper.Action("Index", "Contactos"), searchIconHTML));

            //adminMenuItemChildList.Add(new MenuItem(1, "GruposSubMenuItem", "Gerir Grupos", "Gerir Grupos", angleDoubleRightIconHTML, gruposSubMenuItemChildList, false));

            return orgaoMenu;
        }
        private static List<MenuItem> BuildEstudarMenuLateral(List<ConteudoPropriedade> EstudarTipo)
        {
            UrlHelper menuLateralURLHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            string iconHTML = @"<span class=""fa fa-{0} fa-fw""></span>";
            string searchIconHTML = string.Format(iconHTML, "search");

            List<MenuItem> estudarMenu = new List<MenuItem>();
            List<MenuItem> estudarSubMenuItemChildList = new List<MenuItem>();

            foreach (ConteudoPropriedade estudar in EstudarTipo)
            {
                estudarSubMenuItemChildList.Add(new MenuItem(1, "Estudar" + estudar.Valor + "SubMenuItem", estudar.Nome, estudar.Nome, menuLateralURLHelper.Action("Index", "FornCurso", new { id = estudar.Valor }), searchIconHTML));
            }

            estudarMenu.Add(new MenuItem(0, "EstudarMenuItem", "Estudar", "Estudar", searchIconHTML, estudarSubMenuItemChildList,false));

            return estudarMenu;
        }

        private static List<MenuItem> BuildMenuLateral()
        {
            UrlHelper menuLateralURLHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            string iconHTML = @"<span class=""fa fa-{0} fa-fw""></span>";
            string angleDoubleRightIconHTML = string.Format(iconHTML, "angle-double-right");
            string plusIconHTML = string.Format(iconHTML, "plus");
            string editIconHTML = string.Format(iconHTML, "edit");
            string cogsIconHTML = string.Format(iconHTML, "cogs");

            List<MenuItem> menu = new List<MenuItem>();

            List<MenuItem> conteudosMenuItemChildList = new List<MenuItem>();

            conteudosMenuItemChildList.Add(new MenuItem(1, "CreateConteudosSubMenuItem", "Criar Conteúdos", "Criar Conteúdos", menuLateralURLHelper.Action("CreateConteudos", "Conteudos"), plusIconHTML));
            conteudosMenuItemChildList.Add(new MenuItem(1, "EditConteudosSubMenuItem", "Editar Conteúdos", "Editar Conteúdos", menuLateralURLHelper.Action("EditConteudos", "Conteudos"), editIconHTML));

            menu.Add(new MenuItem(0, "ConteudosMenuItem", "Conteúdos", "Conteúdos", angleDoubleRightIconHTML, conteudosMenuItemChildList));

            List<MenuItem> gruposSubMenuItemChildList = new List<MenuItem>();

            gruposSubMenuItemChildList.Add(new MenuItem(2, "GruposSubSubMenuItem", "Grupos", "Grupos", menuLateralURLHelper.Action("Grupos", "Admin"), angleDoubleRightIconHTML));
            gruposSubMenuItemChildList.Add(new MenuItem(2, "GruposCargosSubSubMenuItem", "Cargos", "Cargos", menuLateralURLHelper.Action("GruposCargos", "Admin"), angleDoubleRightIconHTML));
            gruposSubMenuItemChildList.Add(new MenuItem(2, "GruposPerfisSubSubMenuItem", "Perfis", "Perfis", menuLateralURLHelper.Action("GruposPerfis", "Admin"), angleDoubleRightIconHTML));
            gruposSubMenuItemChildList.Add(new MenuItem(2, "GruposQuoVadisSubSubMenuItem", "Grupos do QuoVadis", "Grupos do QuoVadis", menuLateralURLHelper.Action("GruposQuoVadis", "Admin"), angleDoubleRightIconHTML));

            List<MenuItem> adminMenuItemChildList = new List<MenuItem>();

            adminMenuItemChildList.Add(new MenuItem(1, "AnosLetivosSubMenuItem", "Gerir Anos Letivos", "Gerir Anos Letivos", menuLateralURLHelper.Action("AnosLetivos", "Admin"), angleDoubleRightIconHTML));
            adminMenuItemChildList.Add(new MenuItem(1, "CategoriasSubMenuItem", "Gerir Categorias", "Gerir Categorias", menuLateralURLHelper.Action("Categorias", "Admin"), angleDoubleRightIconHTML));
            adminMenuItemChildList.Add(new MenuItem(1, "AutorizacoesSubMenuItem", "Gerir Autorizações", "Gerir Autorizações", menuLateralURLHelper.Action("Autorizacoes", "Admin"), angleDoubleRightIconHTML));
            adminMenuItemChildList.Add(new MenuItem(1, "GruposSubMenuItem", "Gerir Grupos", "Gerir Grupos", angleDoubleRightIconHTML, gruposSubMenuItemChildList, false));
            adminMenuItemChildList.Add(new MenuItem(1, "MensagensSubMenuItem", "Gerir Mensagens", "Gerir Mensagens", menuLateralURLHelper.Action("Mensagens", "Admin"), angleDoubleRightIconHTML));
            adminMenuItemChildList.Add(new MenuItem(1, "ParametrosSubMenuItem", "Gerir Parâmetros", "Gerir Parâmetros", menuLateralURLHelper.Action("Parametros", "Admin"), angleDoubleRightIconHTML));
            adminMenuItemChildList.Add(new MenuItem(1, "UsersSubMenuItem", "Gerir Utilizadores", "Gerir Utilizadores", menuLateralURLHelper.Action("Users", "Admin"), angleDoubleRightIconHTML));

            menu.Add(new MenuItem(0, "AdminMenuItem", "Gestor do " + ConfigurationManager.AppSettings["Aplicacao.Titulo"], "Gestor do " + ConfigurationManager.AppSettings["Aplicacao.Titulo"], cogsIconHTML, adminMenuItemChildList, false));

            return menu;
        }

        private static List<MenuItem> BuildCategoriasMenuLateral(List<Categoria> categoriasList)
        {
            UrlHelper menuLateralURLHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            string iconHTML = @"<span class=""fa fa-{0} fa-fw""></span>";
            string searchIconHTML = string.Format(iconHTML, "search");

            List<MenuItem> categoriasMenu = new List<MenuItem>();

            List<MenuItem> categoriasSubMenu = new List<MenuItem>();

            foreach (Categoria categoria in categoriasList)
            {
                categoriasSubMenu.Add(new MenuItem(1, "Categoria" + categoria.Id + "SubMenuItem", categoria.Nome, categoria.Nome, menuLateralURLHelper.Action("ListCategorias", "Conteudos", new { idCategoria = categoria.Id }), searchIconHTML));
            }

            categoriasMenu.Add(new MenuItem(0, "CategoriasMenuItem", "Categorias", "Categorias", searchIconHTML, categoriasSubMenu, false));

            return categoriasMenu;
        }


        private static List<MenuItem> BuildTiposConteudoMenuLateral(List<TipoConteudo> tiposConteudoList)
        {
            UrlHelper menuLateralURLHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            string iconHTML = @"<span class=""fa fa-{0} fa-fw""></span>";
            string searchIconHTML = string.Format(iconHTML, "search");

            List<MenuItem> tiposConteudoMenu = new List<MenuItem>();

            List<MenuItem> tiposConteudoSubMenu = new List<MenuItem>();

            tiposConteudoSubMenu.Add(new MenuItem(0, "Documentos", "Documentos", "Documentos", menuLateralURLHelper.Action("DetailsConteudos", "Conteudos"), searchIconHTML));

            foreach (TipoConteudo tipoConteudo in tiposConteudoList)
            {
                tiposConteudoSubMenu.Add(new MenuItem(1, "TipoConteudo" + tipoConteudo.Id + "SubMenuItem", tipoConteudo.Nome, tipoConteudo.Nome, menuLateralURLHelper.Action("DetailsConteudos", "Conteudos", new { idTipoConteudo = tipoConteudo.Id }), searchIconHTML));
            }

            return tiposConteudoSubMenu;
        }

        private static List<MenuItem> BuildContactosMenuLateral(List<Contactos> contactosList)
        {
            UrlHelper menuLateralURLHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            string iconHTML = @"<span class=""fa fa-{0} fa-fw""></span>";
            string searchIconHTML = string.Format(iconHTML, "address-book");

            List<MenuItem> contactosMenu = new List<MenuItem>();


            contactosMenu.Add(new MenuItem(0, "Contactos", "Contactos", "Contactos", menuLateralURLHelper.Action("ListarContactos", "Contactos"), searchIconHTML));

            return contactosMenu;
        }


        public static List<MenuItem> BuildFavoritosMenuLateral(List<Favorito> favoritos)
        {
            List<MenuItem> favoritosMenu = new List<MenuItem>();

            // Guarda um código HTML genérico respectivo aos ícones dos favoritos do Dashboard
            string favoritosIconHTML = @"<span class=""portalbase-app-square small menu"" style=""background-color: {0}""><span>{1}</span></span>";

            List<MenuItem> favoritosMenuList = new List<MenuItem>();

            foreach (Favorito favorito in favoritos)
            {
                // Com os dados da sigla e da cor, constrói o ícone do favorito a apresentar no menu lateral
                string favoritoIconHTML = string.Format(favoritosIconHTML, favorito.CorDashboard, favorito.SiglaDashboard);

                favoritosMenuList.Add(new MenuItem(1, "Favorito" + favorito.Id + "SubMenuItem", favorito.Nome, favorito.Nome, favorito.Link, true, favoritoIconHTML));
            }

            favoritosMenu.Add(new MenuItem(0, "FavoritosMenuItem", "favUTAD", "favUTAD", @"<span class=""fa fa-star""></span>", favoritosMenuList, true));

            return favoritosMenu;
        }

        public static List<MenuItem> BuildAplicacoesPortalBaseMenuLateral(List<AplicacaoPortalBase> aplicacoesIntranet, List<GrupoMenu> gruposMenu)
        {
            // Guarda um código HTML genérico respectivo aos ícones das aplicações do Intranet
            string aplicacoesIntranetIconHTML = @"<span class=""portalbase-app-square small"" style=""background-color: {0}""><span>{1}</span></span>";

            List<MenuItem> aplicacoesIntranetMenu = new List<MenuItem>();
            List<MenuItem> aplicacoesIntranetMenuGruposList = new List<MenuItem>();

            // Por cada grupo de menu obtido, filtra as aplicações do Intranet que pertencam a esse mesmo grupo de menu
            // Depois, para cada uma dessas aplicações filtradas, cria um item de menu lateral e guarda-o na lista
            foreach (GrupoMenu grupoMenu in gruposMenu)
            {
                List<MenuItem> aplicacoesIntranetMenuGrupoList = new List<MenuItem>();

                List<AplicacaoPortalBase> aplicacoesIntranetGrupoList = aplicacoesIntranet.Where(apb => apb.GruposMenuPortalBase.Contains(grupoMenu.Codigo)).ToList();

                foreach (AplicacaoPortalBase aplicacaoIntranet in aplicacoesIntranetGrupoList)
                {
                    // Com os dados da sigla e da cor, constrói o ícone da aplicação a apresentar no menu lateral
                    string aplicacaoIntranetIconHTML = string.Format(aplicacoesIntranetIconHTML, aplicacaoIntranet.CorPortalBase, aplicacaoIntranet.SiglaPortalBase);

                    aplicacoesIntranetMenuGrupoList.Add(new MenuItem(2, "AplicacoesPortalBase" + aplicacaoIntranet.IdentificadorPortalBase + "SubSubMenuItem", aplicacaoIntranet.Descricao, aplicacaoIntranet.Nome, aplicacaoIntranet.Link, true, aplicacaoIntranetIconHTML));
                }

                if (aplicacoesIntranetMenuGrupoList.Count > 0)
                {
                    string grupoMenuIconHTML = "";

                    switch (grupoMenu.Codigo)
                    {
                        case "E":
                            grupoMenuIconHTML = @"<span class=""fa fa-graduation-cap""></span>";

                            break;

                        case "I":
                            grupoMenuIconHTML = @"<span class=""fa fa-book""></span>";

                            break;

                        case "S":
                            grupoMenuIconHTML = @"<span class=""fa fa-users""></span>";

                            break;

                        default:
                            break;
                    }

                    aplicacoesIntranetMenuGruposList.Add(new MenuItem(1, "AplicacoesPortalBase" + grupoMenu.Codigo + "SubMenuItem", grupoMenu.Grupo, grupoMenu.Grupo, grupoMenuIconHTML, aplicacoesIntranetMenuGrupoList, false));
                }
            }

            if (aplicacoesIntranetMenuGruposList.Count > 0)
            {
                aplicacoesIntranetMenu.Add(new MenuItem(0, "AplicacoesPortalBaseMenuItem", "As Minhas Aplicações", "As Minhas Aplicações", @"<span class=""fa fa-desktop""></span>", aplicacoesIntranetMenuGruposList));
            }

            return aplicacoesIntranetMenu;
        }

        private static void CheckMenuLateralAutorizacoes(List<MenuItem> menuItemList)
        {
            // Instancia uma cópia da lista, para desta forma ser possível enumerar e remover items da lista ao mesmo tempo
            List<MenuItem> oldMenuItemList = new List<MenuItem>(menuItemList);

            foreach (MenuItem menuItem in oldMenuItemList)
            {
                // Se o utilizador autenticado não estiver autorizado a ver um item ou subitem, então esse item ou subitem é removido
                if (IntranetSession.Session.Autorizacoes.SingleOrDefault(a => a.Pagina == "" && a.ItemName == menuItem.Name) == default(Autorizacao))
                {
                    menuItemList.Remove(menuItem);
                }

                if (menuItem.MenuItemChildList.Count > 0)
                {
                    CheckMenuLateralAutorizacoes(menuItem.MenuItemChildList);
                }
            }
        }

        public static int SetMenuLateralItemIDs(List<MenuItem> menuLateral, int startingItemID)
        {
            for (int i = 0; i < menuLateral.Count; i++)
            {
                menuLateral[i].Id = startingItemID;

                startingItemID++;

                if (menuLateral[i].MenuItemChildList.Count > 0)
                {
                    startingItemID = SetMenuLateralItemIDs(menuLateral[i].MenuItemChildList, startingItemID);
                }
            }

            return startingItemID;
        }
    }
}