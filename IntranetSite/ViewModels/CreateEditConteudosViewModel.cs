﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;

namespace IntranetSite.ViewModels
{
    public class CreateEditConteudosViewModel
    {
        public Orgao Orgao { get; set; }
        public List<TipoConteudo> TiposConteudosList { get; set; }

        public List<AnoLetivo> anoLetivos  { get; set; }

        public CreateEditConteudosViewModel()
        {
            Orgao = new Orgao();
            TiposConteudosList = new List<TipoConteudo>();
            anoLetivos = new List<AnoLetivo>();
        }
    }
}