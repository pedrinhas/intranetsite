﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;

namespace IntranetSite.ViewModels
{
    public class ListConteudosViewModel
    {
        //public Categoria Categoria { get; set; }
        //public List<TipoConteudo> TiposConteudoList { get; set; }
        public TipoConteudo TipoConteudo { get; set; }
        public Orgao Orgao { get; set; }
        public List<Conteudo> ConteudosList { get; set; }

        public List<ConteudoPropriedade> AnosList { get; set; }
        public string Ano { get; set; }

        public ListConteudosViewModel()
        {
            //Categoria = new Categoria();
            //TiposConteudoList = new List<TipoConteudo>(9);
            ConteudosList = new List<Conteudo>();
            TipoConteudo = new TipoConteudo();
            Orgao = new Orgao();
            AnosList = new List<ConteudoPropriedade>();
            Ano = "";
        }
    }

    public class ListCategoriasViewModel
    {
        public Categoria Categoria { get; set; }
        public List<TipoConteudo> TiposConteudoList { get; set; }
        //public TipoConteudo TipoConteudo { get; set; }
        //public Orgao Orgao { get; set; }
        public List<Conteudo> ConteudosList { get; set; }

        public ListCategoriasViewModel()
        {
            Categoria = new Categoria();
            TiposConteudoList = new List<TipoConteudo>(9);
            ConteudosList = new List<Conteudo>();
            //TipoConteudo = new TipoConteudo();
            //Orgao = new Orgao();
        }
    }
}