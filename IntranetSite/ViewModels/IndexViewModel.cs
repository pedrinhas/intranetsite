﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetAssemblies.Models.QuoVadis.Aplicacoes;
using IntranetAssemblies.Models.QuoVadis.RCU;

namespace IntranetSite.ViewModels
{
    public class IndexViewModel
    {
        public GrupoMenu GrupoMenu { get; set; }
        public string GrupoMenuIconHTML { get; set; }

        public List<AplicacaoPortalBase> AplicacoesPortalBaseList { get; set; }

        public IndexViewModel()
        {
            GrupoMenu = new GrupoMenu();
            GrupoMenuIconHTML = "";

            AplicacoesPortalBaseList = new List<AplicacaoPortalBase>();
        }
    }
}