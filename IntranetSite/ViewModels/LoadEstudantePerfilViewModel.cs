using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetSite.ViewModels
{
    public class LoadEstudantePerfilViewModel
    {
        public string Nome { get; set; }
        public string CodCurso { get; set; }
        public string Curso { get; set; }
        public string NomeAcesso { get; set; }

        public LoadEstudantePerfilViewModel()
        {
            Nome = "";
            CodCurso = "";
            Curso = "";
            NomeAcesso = "";
        }
    }
}