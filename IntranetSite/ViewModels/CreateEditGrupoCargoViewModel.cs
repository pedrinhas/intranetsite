using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;

namespace IntranetSite.ViewModels
{
    public class CreateEditGrupoCargoViewModel
    {
        public Grupo Grupo { get; set; }
        public List<TipoCargo> TiposCargosList { get; set; }

        public CreateEditGrupoCargoViewModel()
        {
            Grupo = new Grupo();
            TiposCargosList = new List<TipoCargo>();
        }
    }
}