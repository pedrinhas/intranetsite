﻿using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetSite.ViewModels
{
    public class OrgaosViewModel
    {
        public Contactos Contacto { get; set; }
        public Orgao Orgao { get; set; }
        public bool HasChild { get; set; }

        public OrgaosViewModel()
        {
            Contacto = new Contactos();
            Orgao = new Orgao();
            HasChild = false;
        }
    }
    public class ContactosOrgaosViewModel
    {
        public List<OrgaosViewModel> Orgaos { get; set; }
        public List<ConteudoPropriedade> OrgaoParentList { get; set; }
        public int OrgaoParent { get; set; }
        public double MenuItemPadding { get; set; }

        public ContactosOrgaosViewModel()
        {
            Orgaos = new List<OrgaosViewModel>();
            OrgaoParentList = new List<ConteudoPropriedade>();
            OrgaoParent = 0;
            MenuItemPadding = 0;
        }
    }
}