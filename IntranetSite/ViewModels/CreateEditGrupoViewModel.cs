using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetAssemblies.Models.Intranet;

namespace IntranetSite.ViewModels
{
    public class CreateEditGrupoViewModel
    {
        public Grupo Grupo { get; set; }
        public List<User> UsersList { get; set; }
        public string Username { get; set; }

        public CreateEditGrupoViewModel()
        {
            Grupo = new Grupo();
            UsersList = new List<User>();
            Username = "";
        }
    }
}