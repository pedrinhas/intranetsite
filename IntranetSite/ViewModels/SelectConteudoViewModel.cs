﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Cargos;

namespace IntranetSite.ViewModels
{
    public class SelectConteudoViewModel
    {
        public TipoConteudo ConteudosTipoConteudo { get; set; }
        public string ConteudosNomeOrgao { get; set; }

        public List<List<Conteudo>> ConteudosLists { get; set; }
        public List<ConteudoPropriedade> ConteudosEstadosList { get; set; }

        public SelectConteudoViewModel()
        {
            ConteudosTipoConteudo = new TipoConteudo();
            ConteudosNomeOrgao = "";

            ConteudosLists = new List<List<Conteudo>>();
            ConteudosEstadosList = new List<ConteudoPropriedade>();
        }
    }
}