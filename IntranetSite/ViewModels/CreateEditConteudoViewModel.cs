﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.DSD;
using IntranetAssemblies.Models.QuoVadis.SIGACAD;

namespace IntranetSite.ViewModels
{
    public class CreateEditConteudoViewModel
    {
        public Conteudo Conteudo { get; set; }

        public List<ConteudoPropriedade> ConteudoEstadosList { get; set; }
        public List<ConteudoPropriedade> ConteudoVisibilidadesList { get; set; }
        public List<ConteudoPropriedade> PerfisVisibilidadeList { get; set; }
        public List<ConteudoPropriedade> TiposCargosVisibilidadeList { get; set; }
        public List<ConteudoPropriedade> OrgaosVisibilidadeList { get; set; }
        public List<ConteudoPropriedade> GruposQuoVadisVisibilidadeList { get; set; }

        public List<ConteudoPropriedade> CategoriasList { get; set; }

        public Ficha_Curso Ficha_Curso_Dados { get; set; }

        public string PerfilVisibilidade { get; set; }
        public string TipoCargoVisibilidade { get; set; }
        public string OrgaoVisibilidade { get; set; }
        public string PessoaVisibilidade { get; set; }
        public string GrupoQuoVadisVisibilidade { get; set; }

        public List<string> Categorias { get; set; }

        public CreateEditConteudoViewModel()
        {
            Conteudo = new Conteudo();

            ConteudoEstadosList = new List<ConteudoPropriedade>();
            ConteudoVisibilidadesList = new List<ConteudoPropriedade>();
            PerfisVisibilidadeList = new List<ConteudoPropriedade>();
            TiposCargosVisibilidadeList = new List<ConteudoPropriedade>();
            OrgaosVisibilidadeList = new List<ConteudoPropriedade>();
            GruposQuoVadisVisibilidadeList = new List<ConteudoPropriedade>();

            CategoriasList = new List<ConteudoPropriedade>();

            Ficha_Curso_Dados = new Ficha_Curso();

            PerfilVisibilidade = "";
            TipoCargoVisibilidade = "";
            OrgaoVisibilidade = "";
            PessoaVisibilidade = "";
            GrupoQuoVadisVisibilidade = "";

            Categorias = new List<string>();
        }
    }

    public class Ficha_Curso
    {
        public List<UC> UCs { get; set; }
        public Curso Curso { get; set; }
        public List<CursoDiretor> CursoDiretores { get; set; }
        public List<Plano> Planos { get; set; }

        public Ficha_Curso()
        {
            UCs = new List<UC>();
            Curso = new Curso();
            CursoDiretores = new List<CursoDiretor>();
            Planos = new List<Plano>();
        }


    }
}