using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetAssemblies.Models.Intranet;

namespace IntranetSite.ViewModels
{
    public class CreateEditGrupoQuoVadisViewModel
    {
        public Grupo Grupo { get; set; }

        public List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo> GruposQuoVadisList { get; set; }
        public string GrupoQuoVadisID { get; set; }

        public CreateEditGrupoQuoVadisViewModel()
        {
            Grupo = new Grupo();

            GruposQuoVadisList = new List<IntranetAssemblies.Models.QuoVadis.Grupos.Grupo>();
            GrupoQuoVadisID = "";
        }
    }
}