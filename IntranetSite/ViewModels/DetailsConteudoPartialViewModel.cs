﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetAssemblies.Models.Intranet;

namespace IntranetSite.ViewModels
{
    public class DetailsConteudoPartialViewModel
    {
        public Conteudo Conteudo { get; set; }
        public List<ConteudoAnexo> ConteudoAnexosList { get; set; }
        public List<string> Fotos { get; set; }
        public List<Link> Links { get; set; }
        public ConteudoPropriedade ConteudoEstado { get; set; }
        public ConteudoPropriedade ConteudoVisibilidade { get; set; }

        public Ficha_Curso Ficha_Curso_Dados { get; set; }

        public DetailsConteudoPartialViewModel()
        {
            Conteudo = new Conteudo();
            ConteudoAnexosList = new List<ConteudoAnexo>();
            ConteudoEstado = new ConteudoPropriedade();
            ConteudoVisibilidade = new ConteudoPropriedade();
            Fotos = new List<string>();
            Links = new List<Link>();
            Ficha_Curso_Dados = new Ficha_Curso();
        }
    }
}