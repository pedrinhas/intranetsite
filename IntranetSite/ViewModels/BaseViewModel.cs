using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Mensagens;

namespace IntranetSite.ViewModels
{
    public class BaseViewModel
    {
        public bool IsAutorizacoesEnabled { get; set; }
        public List<Autorizacao> AutorizacoesList { get; set; }

        public BaseViewModel()
        {
            IsAutorizacoesEnabled = false;
            AutorizacoesList = new List<Autorizacao>();
        }

        public bool CheckAutorizacoesListForItem(string itemName, string pagina)
        {
            if (IsAutorizacoesEnabled == false || AutorizacoesList.SingleOrDefault(a => a.Pagina == pagina && a.ItemName == itemName) != default(Autorizacao))
            {
                return true;
            }

            return false;
        }
    }
}