$(document).ready(function () {

    var culture = $('#cultureValue').val();

    $.datepicker.setDefaults($.datepicker.regional[culture]);
    $('input[type=date]').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:' + (new Date().getFullYear() + 2).toString()
    }).prop('type', 'text');
    $('input[type=datetime]').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:' + (new Date().getFullYear() + 2).toString()
    }).prop('type', 'text');

    $('.btn-datas').click(function () {
        $(this).closest('.input-group-btn').siblings('input').datepicker('show');
    });

});