using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Configuration;

using IntranetSite.App_Start;

namespace IntranetSite.Filters
{
    public class IntranetLoggedInAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return Autenticacao.IsLoggedIn();
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["error"] = "Tentativa de acesso inválida! Tem primeiro de iniciar sessão.";

            filterContext.HttpContext.Session["returnURL"] = filterContext.HttpContext.Request.FilePath;

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.UTAD.Shibboleth"]) == true)
            {
                filterContext.Result = new RedirectResult("~/Secure/Index");
            }
            else
            {
                filterContext.Result = new RedirectResult("~/Session/Login");
            }
        }
    }
}