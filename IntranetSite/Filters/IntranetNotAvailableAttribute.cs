using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace IntranetSite.Filters
{
    public class IntranetNotAvailableAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["warning"] = "Esta operação não se encontra disponível.";

            filterContext.Result = new RedirectResult("~/Home/Index");
        }
    }
}