using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Configuration;

namespace IntranetSite.Filters
{
    public class IntranetCredenciaisTemporariasRegistoAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.CredenciaisTemporarias"]);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.UTAD.Shibboleth"]) == true)
            {
                filterContext.Result = new RedirectResult("~/Secure/Index");
            }
            else
            {
                filterContext.Result = new RedirectResult("~/Session/Login");
            }
        }
    }
}