using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

using IntranetSite.App_Start;
using IntranetSite.Helpers;

using IntranetAssemblies.Models.Intranet;

namespace IntranetSite.Filters
{
    public class IntranetAutorizacoesAttribute : AuthorizeAttribute
    {
        public string[] GruposNomes { get; set; }
        public Enums.IntranetAutorizacoesAttributeModo Modo { get; set; }

        public IntranetAutorizacoesAttribute()
        {
            GruposNomes = null;
            Modo = Enums.IntranetAutorizacoesAttributeModo.DEFAULT;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            // Se o utilizador for administrador da aplicação, retorna logo true
            // Caso contrário, procede
            if (Autenticacao.IsAdmin() == true)
            {
                return true;
            }
            else
            {
                // Verifica em primeiro lugar se foi passada uma lista de nomes de grupos por parâmetro
                if (GruposNomes != null && GruposNomes.Count() >= 0)
                {
                    // Verifica o modo
                    if (Modo == Enums.IntranetAutorizacoesAttributeModo.ALLOWGRUPOS)
                    {
                        // Se o modo for ALLOWGRUPOS, autoriza todos os utilizadores que pertencam a pelo menos um dos grupos passados como parâmetro
                        foreach (string grupoNome in GruposNomes)
                        {
                            if (IntranetSession.Session.Grupos.SingleOrDefault(g => g.Nome == grupoNome) != default(Grupo))
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        // Caso contrário, se o modo for DENYGRUPOS, autoriza apenas os utilizadores que não pertencam a nenhum dos grupos passados como parâmetro
                        foreach (string grupoNome in GruposNomes)
                        {
                            if (IntranetSession.Session.Grupos.SingleOrDefault(g => g.Nome == grupoNome) != default(Grupo))
                            {
                                return false;
                            }
                        }

                        return true;
                    }
                }
            }

            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["error"] = "Tentativa de acesso inválida! Não tem permissões para aceder a esta funcionalidade.";

            filterContext.Result = new RedirectResult("~/Home/Index");
        }
    }
}