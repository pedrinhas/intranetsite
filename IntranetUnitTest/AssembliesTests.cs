using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntranetUnitTest
{
    [TestClass]
    public class AssembliesTests : BaseTests
    {
        [TestMethod]
        public void GetOrgaosTree()
        {
            var tree = _cargosClient.GetOrgaosTree();

            Assert.IsNotNull(tree);
            Assert.IsNotNull(tree.Children);
            Assert.AreEqual(0, tree.IdOrgaoParent);
        }

        [TestMethod]
        public void GetOrgaosTreeHard()
        {
            var tree = _cargosClient.GetOrgaosTreeHard();

            Assert.IsNotNull(tree);
            Assert.IsNotNull(tree.Item);
            Assert.IsNotNull(tree.Children);
            Assert.AreEqual(0, tree.Item.IdOrgaoParent);
        }
    }
}
