using System;
using System.Collections.Generic;
using System.Linq;

using System.Data.SqlClient;

using IntranetAssemblies.Models.Intranet;
using IntranetAssemblies.Models.QuoVadis.Mensagens;
using IntranetAssemblies.Models.QuoVadis.Cargos;
using IntranetAssemblies.Models.QuoVadis.Dashboard;

namespace IntranetAssemblies.DataAcess
{
    public class IntranetService
    {
        private string intranetDBString;

        public IntranetService(string _intranetDBString)
        {
            intranetDBString = _intranetDBString;
        }

        private object GetSafeSqlValue(SqlDataReader dbReader, int i)
        {
            if (dbReader.IsDBNull(i) == false)
            {
                return dbReader.GetValue(i);
            }
            else
            {
                return null;
            }
        }

        #region PortalBase

        public List<Autorizacao> NSI_STP_Autorizacao_LS()
        {
            List<Autorizacao> autorizacoesList = new List<Autorizacao>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Autorizacao autorizacao = new Autorizacao();

                                autorizacao.Id = dbReader.GetInt32(0);
                                autorizacao.ItemName = dbReader.GetString(1);
                                autorizacao.Pagina = dbReader.GetString(2);

                                autorizacoesList.Add(autorizacao);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return autorizacoesList;
        }

        public Autorizacao NSI_STP_Autorizacao_S(int id)
        {
            Autorizacao autorizacao = new Autorizacao();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if(dbReader.Read() == true)
                            {
                                autorizacao.Id = id;
                                autorizacao.ItemName = dbReader.GetString(0);
                                autorizacao.Pagina = dbReader.GetString(1);
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {

            }

            return autorizacao;
        }

        public List<Autorizacao> NSI_STP_Autorizacao_S_ByGrupo(int idGrupo)
        {
            List<Autorizacao> autorizacoesList = new List<Autorizacao>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_S_ByGrupo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Autorizacao autorizacao = new Autorizacao();

                                autorizacao.Id = dbReader.GetInt32(0);
                                autorizacao.ItemName = dbReader.GetString(1);
                                autorizacao.Pagina = dbReader.GetString(2);

                                autorizacoesList.Add(autorizacao);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return autorizacoesList;
        }

        public int NSI_STP_Autorizacao_I(string itemName, string pagina)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_itemName", itemName));
                        dbCommand.Parameters.Add(new SqlParameter("@_pagina", pagina));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Autorizacao_U(int id, string itemName, string pagina)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_itemName", itemName));
                        dbCommand.Parameters.Add(new SqlParameter("@_pagina", pagina));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Autorizacao_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<Grupo> NSI_STP_Grupo_LS()
        {
            List<Grupo> gruposList = new List<Grupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Grupo grupo = new Grupo();

                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = dbReader.GetString(1);
                                grupo.IdTipo = dbReader.GetInt32(2);
                                grupo.NomeTipo = dbReader.GetString(3);
                                grupo.Descricao = dbReader.GetString(4);

                                gruposList.Add(grupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {
                
            }

            return gruposList;
        }

        public Grupo NSI_STP_Grupo_S(int id)
        {
            Grupo grupo = new Grupo();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if(dbReader.Read() == true)
                            {
                                grupo.Id = id;
                                grupo.Nome = dbReader.GetString(0);
                                grupo.IdTipo = dbReader.GetInt32(1);
                                grupo.Descricao = dbReader.GetString(2);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return grupo;
        }

        public Grupo NSI_STP_Grupo_S_ByNomeAndTipo(string nome, int idTipo)
        {
            Grupo grupo = new Grupo();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S_ByNomeAndTipo";
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipo", idTipo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = nome;
                                grupo.IdTipo = idTipo;
                                grupo.Descricao = dbReader.GetString(1);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return grupo;
        }

        public List<Grupo> NSI_STP_Grupo_S_ByTipo(int idTipo)
        {
            List<Grupo> gruposList = new List<Grupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S_ByTipo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipo", idTipo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Grupo grupo = new Grupo();

                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = dbReader.GetString(1);
                                grupo.IdTipo = idTipo;
                                grupo.Descricao = dbReader.GetString(2);

                                gruposList.Add(grupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposList;
        }

        public List<Grupo> NSI_STP_Grupo_S_ByUser(int idUser)
        {
            List<Grupo> gruposList = new List<Grupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S_ByUser";
                        dbCommand.Parameters.Add(new SqlParameter("@_idUser", idUser));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Grupo grupo = new Grupo();

                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = dbReader.GetString(1);
                                grupo.IdTipo = 1;
                                grupo.Descricao = dbReader.GetString(2);

                                gruposList.Add(grupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposList;
        }

        public List<Grupo> NSI_STP_Grupo_S_ByIUPI(Guid iupi)
        {
            List<Grupo> gruposList = new List<Grupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S_ByIUPI";
                        dbCommand.Parameters.Add(new SqlParameter("@_iupi", iupi));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Grupo grupo = new Grupo();

                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = dbReader.GetString(1);
                                grupo.IdTipo = 1;
                                grupo.Descricao = dbReader.GetString(2);

                                gruposList.Add(grupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposList;
        }

        public int NSI_STP_Grupo_I(string nome, int idTipo, string descricao)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipo", idTipo));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao", descricao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Grupo_U(int id, string nome, int idTipo, string descricao)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipo", idTipo));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao", descricao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Grupo_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<GrupoAutorizacao> NSI_STP_GrupoAutorizacao_S_ByAutorizacao(int idAutorizacao)
        {
            List<GrupoAutorizacao> gruposAutorizacaoList = new List<GrupoAutorizacao>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoAutorizacao_S_ByAutorizacao";
                        dbCommand.Parameters.Add(new SqlParameter("@_idAutorizacao", idAutorizacao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                GrupoAutorizacao grupoAutorizacao = new GrupoAutorizacao();

                                grupoAutorizacao.Id = dbReader.GetInt32(0);
                                grupoAutorizacao.IdGrupo = dbReader.GetInt32(1);
                                grupoAutorizacao.IdAutorizacao = idAutorizacao;

                                gruposAutorizacaoList.Add(grupoAutorizacao);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposAutorizacaoList;
        }

        public int NSI_STP_GrupoAutorizacao_I(int idGrupo, int idAutorizacao)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoAutorizacao_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Parameters.Add(new SqlParameter("@_idAutorizacao", idAutorizacao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_GrupoAutorizacao_D_ByGrupo(int idGrupo)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoAutorizacao_D_ByGrupo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public int NSI_STP_GrupoAutorizacao_D_ByAutorizacao(int idAutorizacao)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoAutorizacao_D_ByAutorizacao";
                        dbCommand.Parameters.Add(new SqlParameter("@_idAutorizacao", idAutorizacao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<GrupoMensagem> NSI_STP_GrupoMensagem_S_ByMensagem(int idMensagem)
        {
            List<GrupoMensagem> gruposMensagemList = new List<GrupoMensagem>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoMensagem_S_ByMensagem";
                        dbCommand.Parameters.Add(new SqlParameter("@_idMensagem", idMensagem));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                GrupoMensagem grupoMensagem = new GrupoMensagem();

                                grupoMensagem.Id = dbReader.GetInt32(0);
                                grupoMensagem.IdGrupo = dbReader.GetInt32(1);
                                grupoMensagem.IdMensagem = idMensagem;

                                gruposMensagemList.Add(grupoMensagem);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposMensagemList;
        }

        public int NSI_STP_GrupoMensagem_I(int idGrupo, int idMensagem)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoMensagem_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Parameters.Add(new SqlParameter("@_idMensagem", idMensagem));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_GrupoMensagem_D_ByMensagem(int idMensagem)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoMensagem_D_ByMensagem";
                        dbCommand.Parameters.Add(new SqlParameter("@_idMensagem", idMensagem));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return rowCount;
        }

        public List<Mensagem> NSI_STP_Mensagens_LS()
        {
            List<Mensagem> mensagensList = new List<Mensagem>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Mensagem mensagem = new Mensagem();

                                mensagem.Id = dbReader.GetInt32(0);
                                mensagem.Titulo = dbReader.GetString(1);
                                mensagem.Texto = dbReader.GetString(2);

                                if (dbReader.IsDBNull(3) == true)
                                {
                                    mensagem.DataPublicacaoInicio = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoInicio = Convert.ToDateTime(dbReader.GetDateTime(3));
                                }

                                if(dbReader.IsDBNull(4) == true)
                                {
                                    mensagem.DataPublicacaoFim = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoFim = Convert.ToDateTime(dbReader.GetDateTime(4));
                                }

                                mensagem.Visibilidade = dbReader.GetInt32(5);

                                mensagensList.Add(mensagem);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return mensagensList;
        }

        public Mensagem NSI_STP_Mensagens_S(int id)
        {
            Mensagem mensagem = new Mensagem();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                mensagem.Id = id;
                                mensagem.Titulo = dbReader.GetString(0);
                                mensagem.Texto = dbReader.GetString(1);

                                if (dbReader.IsDBNull(2) == true)
                                {
                                    mensagem.DataPublicacaoInicio = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoInicio = Convert.ToDateTime(dbReader.GetDateTime(2));
                                }

                                if (dbReader.IsDBNull(3) == true)
                                {
                                    mensagem.DataPublicacaoFim = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoFim = Convert.ToDateTime(dbReader.GetDateTime(3));
                                }

                                mensagem.Visibilidade = dbReader.GetInt32(4);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return mensagem;
        }

        public List<Mensagem> NSI_STP_Mensagens_S_ByVisibilidade(int visibilidade)
        {
            List<Mensagem> mensagensList = new List<Mensagem>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_S_ByVisibilidade";
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidade", visibilidade));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Mensagem mensagem = new Mensagem();

                                mensagem.Id = dbReader.GetInt32(0);
                                mensagem.Titulo = dbReader.GetString(1);
                                mensagem.Texto = dbReader.GetString(2);

                                if (dbReader.IsDBNull(3) == true)
                                {
                                    mensagem.DataPublicacaoInicio = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoInicio = Convert.ToDateTime(dbReader.GetDateTime(3));
                                }

                                if (dbReader.IsDBNull(4) == true)
                                {
                                    mensagem.DataPublicacaoFim = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoFim = Convert.ToDateTime(dbReader.GetDateTime(4));
                                }

                                mensagem.Visibilidade = visibilidade;

                                mensagensList.Add(mensagem);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return mensagensList;
        }

        public int NSI_STP_Mensagens_I(string titulo, string texto, DateTime? dataPublicacaoInicio, DateTime? dataPublicacaoFim, int visibilidade)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo", titulo));
                        dbCommand.Parameters.Add(new SqlParameter("@_texto", texto));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataPublicacaoInicio", dataPublicacaoInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataPublicacaoFim", dataPublicacaoFim));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidade", visibilidade));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Mensagens_U(int id, string titulo, string texto, DateTime? dataPublicacaoInicio, DateTime? dataPublicacaoFim, int visibilidade)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo", titulo));
                        dbCommand.Parameters.Add(new SqlParameter("@_texto", texto));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataPublicacaoInicio", dataPublicacaoInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataPublicacaoFim", dataPublicacaoFim));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidade", visibilidade));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Mensagens_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return rowCount;
        }

        public List<Parametro> NSI_STP_Parametros_LS()
        {
            List<Parametro> parametrosList = new List<Parametro>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Parametro parametro = new Parametro();

                                parametro.Id = dbReader.GetInt32(0);
                                parametro.Chave = dbReader.GetString(1);
                                parametro.Valor = dbReader.GetString(2);
                                parametro.Valor1 = dbReader.GetString(3);
                                parametro.Valor2 = dbReader.GetString(4);

                                parametrosList.Add(parametro);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return parametrosList;
        }

        public Parametro NSI_STP_Parametros_S_ByChave(string chave)
        {
            Parametro parametro = new Parametro();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_S_ByChave";
                        dbCommand.Parameters.Add(new SqlParameter("@_chave", chave));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                parametro.Id = dbReader.GetInt32(0);
                                parametro.Chave = chave;
                                parametro.Valor = dbReader.GetString(1);
                                parametro.Valor1 = dbReader.GetString(2);
                                parametro.Valor2 = dbReader.GetString(3);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return parametro;
        }

        public int NSI_STP_Parametros_I(string chave, string valor, string valor1, string valor2)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_chave", chave));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor", valor));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor1", valor1));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor2", valor2));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Parametros_U(int id, string chave, string valor, string valor1, string valor2)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_chave", chave));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor", valor));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor1", valor1));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor2", valor2));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Parametros_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<User> NSI_STP_User_LS()
        {
            List<User> usersList = new List<User>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_User_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                User user = new User();

                                user.Id = dbReader.GetInt32(0);
                                user.Username = dbReader.GetString(1);
                                user.IUPI = dbReader.GetGuid(2);
                                user.Nome = dbReader.GetString(3);
                                user.LastLoginDate = Convert.ToDateTime(GetSafeSqlValue(dbReader, 4));
                                user.IsFromUTAD = dbReader.GetBoolean(5);

                                usersList.Add(user);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return usersList;
        }

        public User NSI_STP_User_S(int id)
        {
            User user = new User();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_User_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                user.Id = id;
                                user.Username = dbReader.GetString(0);
                                user.IUPI = dbReader.GetGuid(1);
                                user.Nome = dbReader.GetString(2);
                                user.LastLoginDate = Convert.ToDateTime(GetSafeSqlValue(dbReader, 3));
                                user.IsFromUTAD = dbReader.GetBoolean(4);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return user;
        }

        public int NSI_STP_User_IU(string username, Guid iupi, string nome, DateTime? lastLoginDate, bool isFromUTAD)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_User_IU";
                        dbCommand.Parameters.Add(new SqlParameter("@_username", username));
                        dbCommand.Parameters.Add(new SqlParameter("@_iupi", iupi));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_lastLoginDate", lastLoginDate));
                        dbCommand.Parameters.Add(new SqlParameter("@_isFromUTAD", isFromUTAD));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }   
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_User_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_User_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<UserGrupo> NSI_STP_UserGrupo_S_ByGrupo(int idGrupo)
        {
            List<UserGrupo> usersGrupoList = new List<UserGrupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_UserGrupo_S_ByGrupo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                UserGrupo userGrupo = new UserGrupo();

                                userGrupo.Id = dbReader.GetInt32(0);
                                userGrupo.IdUser = dbReader.GetInt32(1);
                                userGrupo.IdGrupo = idGrupo;

                                usersGrupoList.Add(userGrupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return usersGrupoList;
        }

        public int NSI_STP_UserGrupo_I(int idUser, int idGrupo)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_UserGrupo_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_idUser", idUser));
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_UserGrupo_D_ByUser(int idUser)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_UserGrupo_D_ByUser";
                        dbCommand.Parameters.Add(new SqlParameter("@_idUser", idUser));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public int NSI_STP_UserGrupo_D_ByGrupo(int idGrupo)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_UserGrupo_D_ByGrupo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        #endregion


        #region Intranet

        #region Orgao
        public List<Orgao> STP_Conteudos_S_OrgaoByEstado(int estado, bool visibilidadePublica)
        {
            List<Orgao> orgaosList = new List<Orgao>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_OrgaoByEstado";
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Orgao orgao = new Orgao();

                                
                                orgao.Id = dbReader.GetInt32(0);
                                orgao.Nome = dbReader.GetString(1);
                                orgao.Codigo = dbReader.GetString(2);
                                


                                orgaosList.Add(orgao);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return orgaosList;
        }

        public List<Orgao> STP_Conteudos_S_OrgaoByEstadoAndTipoConteudo(int estado, int tipoConteudo, bool visibilidadePublica)
        {
            List<Orgao> orgaosList = new List<Orgao>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_OrgaoByEstadoAndTipoConteudo";
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_tipoConteudo", tipoConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Orgao orgao = new Orgao();


                                orgao.Id = dbReader.GetInt32(0);
                                orgao.Nome = dbReader.GetString(1);
                                orgao.Codigo = dbReader.GetString(2);



                                orgaosList.Add(orgao);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return orgaosList;
        }

        #endregion

        #region Tipo Conteudo
        public List<TipoConteudo> STP_Conteudos_S_TipoConteudoByEstadoAndOrgao(int estado, bool visibilidadePublica, int idOrgao)
        {
            List<TipoConteudo> tipoConteudoList = new List<TipoConteudo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_TipoConteudoByEstadoAndOrgao";
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgao", idOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));
                    
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                TipoConteudo tipoConteudo = new TipoConteudo();


                                tipoConteudo.Id = dbReader.GetInt32(0);
                                tipoConteudo.Nome = dbReader.GetString(1);

                                tipoConteudoList.Add(tipoConteudo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return tipoConteudoList;
        }

        public List<TipoConteudo> STP_Conteudos_S_TipoConteudoByEstado(int estado, bool visibilidadePublica)
        {
            List<TipoConteudo> tipoConteudoList = new List<TipoConteudo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_TipoConteudoByEstado";
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));

                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                TipoConteudo tipoConteudo = new TipoConteudo();


                                tipoConteudo.Id = dbReader.GetInt32(0);
                                tipoConteudo.Nome = dbReader.GetString(1);

                                tipoConteudoList.Add(tipoConteudo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return tipoConteudoList;
        }

        public List<TipoConteudo> STP_Conteudos_S_TipoConteudoByEstadoAndOrgaoAndTipoConteudo(int estado,int idtipoConteudo, bool visibilidadePublica, int idOrgao)
        {
            List<TipoConteudo> tipoConteudoList = new List<TipoConteudo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_TipoConteudoByEstadoAndOrgaoAndTipoConteudo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgao", idOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_tipoConteudo", idtipoConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));

                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                TipoConteudo tipoConteudo = new TipoConteudo();


                                tipoConteudo.Id = dbReader.GetInt32(0);
                                tipoConteudo.Nome = dbReader.GetString(1);

                                tipoConteudoList.Add(tipoConteudo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return tipoConteudoList;
        }

        #endregion

        #region Favoritos
        //Favoritos presentns na base de dados da aplica��o intranet
        //Usar Web Services do dashboard/Apps para carregar os favoritos, de forma a ficar os mesmos favoritos em todas as aplica��es
        public Favorito STP_Favoritos_S(int id)
        {
            Favorito favorito = new Favorito();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Favoritos_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                favorito.Id = id;
                                favorito.IUPI = dbReader.GetGuid(0);
                                favorito.Nome = dbReader.GetString(1);
                                favorito.Link = dbReader.GetString(2);
                                favorito.SiglaDashboard = dbReader.GetString(3);
                                favorito.CorDashboard = dbReader.GetString(4);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return favorito;
        }

        public List<Favorito> STP_Favoritos_S_ByIUPI(Guid iupi)
        {
            List<Favorito> favoritosList = new List<Favorito>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Favoritos_S_ByIUPI";
                        dbCommand.Parameters.Add(new SqlParameter("@_iupi", iupi));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Favorito favorito = new Favorito();

                                favorito.Id = dbReader.GetInt32(0);
                                favorito.IUPI = iupi;
                                favorito.Nome = dbReader.GetString(1);
                                favorito.Link = dbReader.GetString(2);
                                favorito.SiglaDashboard = dbReader.GetString(3);
                                favorito.CorDashboard = dbReader.GetString(4);

                                favoritosList.Add(favorito);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return favoritosList;
        }

        public int? STP_Favoritos_U(Favorito favorito)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Favoritos_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", favorito.Id));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", favorito.Nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_link", favorito.Link));
                        dbCommand.Parameters.Add(new SqlParameter("@_sigla", favorito.SiglaDashboard));
                        dbCommand.Parameters.Add(new SqlParameter("@_cor", favorito.CorDashboard));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public int? STP_Favoritos_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Favoritos_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return rowCount;
        }

        public int? STP_Favoritos_I(Favorito favorito)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Favoritos_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_iupi", favorito.IUPI));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", favorito.Nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_link", favorito.Link));
                        dbCommand.Parameters.Add(new SqlParameter("@_sigla", favorito.SiglaDashboard));
                        dbCommand.Parameters.Add(new SqlParameter("@_cor", favorito.CorDashboard));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }
        #endregion

        #region Conteudo

        public int? STP_Conteudos_I(Conteudo conteudo)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_iupi", conteudo.IUPI));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo", conteudo.Titulo));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao", conteudo.Descricao));
                        dbCommand.Parameters.Add(new SqlParameter("@_username", conteudo.Username));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", conteudo.Nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_isForEstudante", conteudo.IsForEstudante));
                        dbCommand.Parameters.Add(new SqlParameter("@_usernameCriacao", conteudo.UsernameCriacao));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataCriacao", conteudo.DataCriacao));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", conteudo.Estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataInicio", conteudo.DataInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@_horaInicio", conteudo.HoraInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataFim", conteudo.DataFim));
                        dbCommand.Parameters.Add(new SqlParameter("@_horaFim", conteudo.HoraFim));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataInicioPublicacao", conteudo.DataInicioPublicacao));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataFimPublicacao", conteudo.DataFimPublicacao));
                        dbCommand.Parameters.Add(new SqlParameter("@_primeiraPagina", conteudo.PrimeiraPagina));
                        dbCommand.Parameters.Add(new SqlParameter("@_nomeFoto", conteudo.NomeFoto));
                        dbCommand.Parameters.Add(new SqlParameter("@_linkFoto", conteudo.LinkFoto));
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipoConteudo", conteudo.IdTipoConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_nomeTipoConteudo", conteudo.NomeTipoConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgao", conteudo.IdOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_nomeOrgao", conteudo.NomeOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_codigoOrgao", conteudo.CodigoOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_acao", conteudo.Acao));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData1", conteudo.LabelData1));
                        dbCommand.Parameters.Add(new SqlParameter("@_data1", conteudo.Data1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData2", conteudo.LabelData2));
                        dbCommand.Parameters.Add(new SqlParameter("@_data2", conteudo.Data2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData3", conteudo.LabelData3));
                        dbCommand.Parameters.Add(new SqlParameter("@_data3", conteudo.Data3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData4", conteudo.LabelData4));
                        dbCommand.Parameters.Add(new SqlParameter("@_data4", conteudo.Data4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData5", conteudo.LabelData5));
                        dbCommand.Parameters.Add(new SqlParameter("@_data5", conteudo.Data5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora1", conteudo.LabelHora1));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora1", conteudo.Hora1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora2", conteudo.LabelHora2));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora2", conteudo.Hora2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora3", conteudo.LabelHora3));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora3", conteudo.Hora3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora4", conteudo.LabelHora4));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora4", conteudo.Hora4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora5", conteudo.LabelHora5));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora5", conteudo.Hora5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo1", conteudo.LabelTitulo1));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo1", conteudo.Titulo1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo2", conteudo.LabelTitulo2));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo2", conteudo.Titulo2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo3", conteudo.LabelTitulo3));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo3", conteudo.Titulo3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo4", conteudo.LabelTitulo4));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo4", conteudo.Titulo4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo5", conteudo.LabelTitulo5));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo5", conteudo.Titulo5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo6", conteudo.LabelTitulo6));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo6", conteudo.Titulo6));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo7", conteudo.LabelTitulo7));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo7", conteudo.Titulo7));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao1", conteudo.LabelDescricao1));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao1", conteudo.Descricao1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao2", conteudo.LabelDescricao2));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao2", conteudo.Descricao2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao3", conteudo.LabelDescricao3));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao3", conteudo.Descricao3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao4", conteudo.LabelDescricao4));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao4", conteudo.Descricao4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao5", conteudo.LabelDescricao5));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao5", conteudo.Descricao5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao6", conteudo.LabelDescricao6));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao6", conteudo.Descricao6));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao7", conteudo.LabelDescricao7));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao7", conteudo.Descricao7));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink1", conteudo.LabelLink1));
                        dbCommand.Parameters.Add(new SqlParameter("@_link1", conteudo.Link1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink2", conteudo.LabelLink2));
                        dbCommand.Parameters.Add(new SqlParameter("@_link2", conteudo.Link2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink3", conteudo.LabelLink3));
                        dbCommand.Parameters.Add(new SqlParameter("@_link3", conteudo.Link3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink4", conteudo.LabelLink4));
                        dbCommand.Parameters.Add(new SqlParameter("@_link4", conteudo.Link4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink5", conteudo.LabelLink5));
                        dbCommand.Parameters.Add(new SqlParameter("@_link5", conteudo.Link5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink6", conteudo.LabelLink6));
                        dbCommand.Parameters.Add(new SqlParameter("@_link6", conteudo.Link6));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink7", conteudo.LabelLink7));
                        dbCommand.Parameters.Add(new SqlParameter("@_link7", conteudo.Link7));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink8", conteudo.LabelLink8));
                        dbCommand.Parameters.Add(new SqlParameter("@_link8", conteudo.Link8));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink9", conteudo.LabelLink9));
                        dbCommand.Parameters.Add(new SqlParameter("@_link9", conteudo.Link9));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink10", conteudo.LabelLink5));
                        dbCommand.Parameters.Add(new SqlParameter("@_link10", conteudo.Link10));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor1", conteudo.LabelValor1));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor1", conteudo.Valor1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor2", conteudo.LabelValor2));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor2", conteudo.Valor2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor3", conteudo.LabelValor3));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor3", conteudo.Valor3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor4", conteudo.LabelValor4));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor4", conteudo.Valor4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor5", conteudo.LabelValor5));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor5", conteudo.Valor5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelCheck1", conteudo.LabelCheck1));
                        dbCommand.Parameters.Add(new SqlParameter("@_check1", conteudo.Check1));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", conteudo.VisibilidadePublica));
                        dbCommand.Parameters.Add(new SqlParameter("@_perfisVisibilidade", conteudo.PerfisVisibilidade));
                        dbCommand.Parameters.Add(new SqlParameter("@_tiposCargosVisibilidade", conteudo.TiposCargosVisibilidade));
                        dbCommand.Parameters.Add(new SqlParameter("@_orgaosVisibilidade", conteudo.OrgaosVisibilidade));
                        dbCommand.Parameters.Add(new SqlParameter("@_pessoasVisibilidade", conteudo.PessoasVisibilidade));
                        dbCommand.Parameters.Add(new SqlParameter("@_gruposQuoVadisVisibilidade", conteudo.GruposQuoVadisVisibilidade));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public int? STP_ConteudosHistorico_I(int idConteudo, string usernameHistorico, DateTime dataHistorico)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosHistorico_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", idConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_usernameHistorico", usernameHistorico));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataHistorico", dataHistorico));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public int? STP_Conteudos_U(Conteudo conteudo)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", conteudo.Id));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo", conteudo.Titulo));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao", conteudo.Descricao));
                        dbCommand.Parameters.Add(new SqlParameter("@_username", conteudo.Username));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", conteudo.Nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_isForEstudante", conteudo.IsForEstudante));
                        dbCommand.Parameters.Add(new SqlParameter("@_usernameAlteracao", conteudo.UsernameAlteracao));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataAlteracao", conteudo.DataAlteracao));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", conteudo.Estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataInicio", conteudo.DataInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@_horaInicio", conteudo.HoraInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataFim", conteudo.DataFim));
                        dbCommand.Parameters.Add(new SqlParameter("@_horaFim", conteudo.HoraFim));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataInicioPublicacao", conteudo.DataInicioPublicacao));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataFimPublicacao", conteudo.DataFimPublicacao));
                        dbCommand.Parameters.Add(new SqlParameter("@_primeiraPagina", conteudo.PrimeiraPagina));
                        dbCommand.Parameters.Add(new SqlParameter("@_nomeFoto", conteudo.NomeFoto));
                        dbCommand.Parameters.Add(new SqlParameter("@_linkFoto", conteudo.LinkFoto));
                        dbCommand.Parameters.Add(new SqlParameter("@_acao", conteudo.Acao));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData1", conteudo.LabelData1));
                        dbCommand.Parameters.Add(new SqlParameter("@_data1", conteudo.Data1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData2", conteudo.LabelData2));
                        dbCommand.Parameters.Add(new SqlParameter("@_data2", conteudo.Data2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData3", conteudo.LabelData3));
                        dbCommand.Parameters.Add(new SqlParameter("@_data3", conteudo.Data3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData4", conteudo.LabelData4));
                        dbCommand.Parameters.Add(new SqlParameter("@_data4", conteudo.Data4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelData5", conteudo.LabelData5));
                        dbCommand.Parameters.Add(new SqlParameter("@_data5", conteudo.Data5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora1", conteudo.LabelHora1));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora1", conteudo.Hora1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora2", conteudo.LabelHora2));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora2", conteudo.Hora2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora3", conteudo.LabelHora3));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora3", conteudo.Hora3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora4", conteudo.LabelHora4));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora4", conteudo.Hora4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelHora5", conteudo.LabelHora5));
                        dbCommand.Parameters.Add(new SqlParameter("@_hora5", conteudo.Hora5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo1", conteudo.LabelTitulo1));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo1", conteudo.Titulo1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo2", conteudo.LabelTitulo2));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo2", conteudo.Titulo2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo3", conteudo.LabelTitulo3));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo3", conteudo.Titulo3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo4", conteudo.LabelTitulo4));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo4", conteudo.Titulo4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo5", conteudo.LabelTitulo5));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo5", conteudo.Titulo5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo6", conteudo.LabelTitulo6));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo6", conteudo.Titulo6));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelTitulo7", conteudo.LabelTitulo7));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo7", conteudo.Titulo7));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao1", conteudo.LabelDescricao1));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao1", conteudo.Descricao1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao2", conteudo.LabelDescricao2));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao2", conteudo.Descricao2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao3", conteudo.LabelDescricao3));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao3", conteudo.Descricao3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao4", conteudo.LabelDescricao4));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao4", conteudo.Descricao4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao5", conteudo.LabelDescricao5));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao5", conteudo.Descricao5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao6", conteudo.LabelDescricao6));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao6", conteudo.Descricao6));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelDescricao7", conteudo.LabelDescricao7));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao7", conteudo.Descricao7));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink1", conteudo.LabelLink1));
                        dbCommand.Parameters.Add(new SqlParameter("@_link1", conteudo.Link1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink2", conteudo.LabelLink2));
                        dbCommand.Parameters.Add(new SqlParameter("@_link2", conteudo.Link2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink3", conteudo.LabelLink3));
                        dbCommand.Parameters.Add(new SqlParameter("@_link3", conteudo.Link3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink4", conteudo.LabelLink4));
                        dbCommand.Parameters.Add(new SqlParameter("@_link4", conteudo.Link4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink5", conteudo.LabelLink5));
                        dbCommand.Parameters.Add(new SqlParameter("@_link5", conteudo.Link5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink6", conteudo.LabelLink6));
                        dbCommand.Parameters.Add(new SqlParameter("@_link6", conteudo.Link6));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink7", conteudo.LabelLink7));
                        dbCommand.Parameters.Add(new SqlParameter("@_link7", conteudo.Link7));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink8", conteudo.LabelLink8));
                        dbCommand.Parameters.Add(new SqlParameter("@_link8", conteudo.Link8));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink9", conteudo.LabelLink9));
                        dbCommand.Parameters.Add(new SqlParameter("@_link9", conteudo.Link9));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelLink10", conteudo.LabelLink5));
                        dbCommand.Parameters.Add(new SqlParameter("@_link10", conteudo.Link10));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor1", conteudo.LabelValor1));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor1", conteudo.Valor1));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor2", conteudo.LabelValor2));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor2", conteudo.Valor2));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor3", conteudo.LabelValor3));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor3", conteudo.Valor3));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor4", conteudo.LabelValor4));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor4", conteudo.Valor4));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelValor5", conteudo.LabelValor5));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor5", conteudo.Valor5));
                        dbCommand.Parameters.Add(new SqlParameter("@_labelCheck1", conteudo.LabelCheck1));
                        dbCommand.Parameters.Add(new SqlParameter("@_check1", conteudo.Check1));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", conteudo.VisibilidadePublica));
                        dbCommand.Parameters.Add(new SqlParameter("@_perfisVisibilidade", conteudo.PerfisVisibilidade));
                        dbCommand.Parameters.Add(new SqlParameter("@_tiposCargosVisibilidade", conteudo.TiposCargosVisibilidade));
                        dbCommand.Parameters.Add(new SqlParameter("@_orgaosVisibilidade", conteudo.OrgaosVisibilidade));
                        dbCommand.Parameters.Add(new SqlParameter("@_pessoasVisibilidade", conteudo.PessoasVisibilidade));
                        dbCommand.Parameters.Add(new SqlParameter("@_gruposQuoVadisVisibilidade", conteudo.GruposQuoVadisVisibilidade));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public int? STP_Conteudos_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return rowCount;
        }

        public Conteudo STP_Conteudos_S(int id)
        {
            Conteudo conteudo = new Conteudo();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                conteudo.Id = (int)dbReader["Id"];
                                conteudo.IUPI = (Guid)dbReader["IUPI"];
                                conteudo.Titulo = (string)dbReader["Titulo"];
                                conteudo.Descricao = (string)dbReader["Descricao"];
                                conteudo.Username = (string)dbReader["Username"];
                                conteudo.Nome = (string)dbReader["Nome"];
                                conteudo.IsForEstudante = (bool)dbReader["IsForEstudante"];
                                conteudo.UsernameCriacao = (string)dbReader["UsernameCriacao"];
                                conteudo.DataCriacao = (DateTime)dbReader["DataCriacao"];
                                conteudo.UsernameAlteracao = (string)dbReader["UsernameAlteracao"];
                                conteudo.DataAlteracao = dbReader["DataAlteracao"] == System.DBNull.Value ? null : (DateTime?)dbReader["DataAlteracao"];
                                conteudo.Estado = (int)dbReader["Estado"];
                                conteudo.DataEstado = dbReader["DataEstado"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataEstado"];
                                conteudo.DataInicio = dbReader["DataInicio"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataInicio"];
                                conteudo.HoraInicio = (TimeSpan)dbReader["HoraInicio"];
                                conteudo.DataFim = dbReader["DataFim"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFim"];
                                conteudo.HoraFim = (TimeSpan)dbReader["HoraFim"];
                                conteudo.DataInicioPublicacao = dbReader["DataInicioPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataInicioPublicacao"];
                                conteudo.DataFimPublicacao = dbReader["DataFimPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFimPublicacao"];
                                conteudo.PrimeiraPagina = (bool)dbReader["PrimeiraPagina"];
                                conteudo.AprovacaoPrimeiraPagina = (bool)dbReader["AprovacaoPrimeiraPagina"];
                                conteudo.UsernameAprovacaoPrimeiraPagina = (string)dbReader["UsernameAprovacaoPrimeiraPagina"];
                                conteudo.DataAprovacaoPrimeiraPagina = dbReader["DataAprovacaoPrimeiraPagina"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataAprovacaoPrimeiraPagina"];
                                conteudo.NomeFoto = (string)dbReader["NomeFoto"];
                                conteudo.LinkFoto = (string)dbReader["LinkFoto"];
                                conteudo.IdTipoConteudo = (int)dbReader["IdTipoConteudo"];
                                conteudo.NomeTipoConteudo = (string)dbReader["NomeTipoConteudo"];
                                conteudo.IdOrgao = (int)dbReader["IdOrgao"];
                                conteudo.NomeOrgao = (string)dbReader["NomeOrgao"];
                                conteudo.CodigoOrgao = (string)dbReader["CodigoOrgao"];
                                conteudo.Acao = (string)dbReader["Acao"];
                                conteudo.LabelData1 = (string)dbReader["LabelData1"];
                                conteudo.Data1 = dbReader["Data1"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data1"];
                                conteudo.LabelData2 = (string)dbReader["LabelData2"];
                                conteudo.Data2 = dbReader["Data2"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data2"];
                                conteudo.LabelData3 = (string)dbReader["LabelData3"];
                                conteudo.Data3 = dbReader["Data3"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data3"];
                                conteudo.LabelData4 = (string)dbReader["LabelData4"];
                                conteudo.Data4 = dbReader["Data4"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data4"];
                                conteudo.LabelData5 = dbReader["LabelData5"].ToString();
                                conteudo.Data5 = dbReader["Data5"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data5"];
                                conteudo.LabelHora1 = (string)dbReader["LabelHora1"];
                                conteudo.Hora1 = (TimeSpan)dbReader["Hora1"];
                                conteudo.LabelHora2 = dbReader["LabelHora2"].ToString();
                                conteudo.Hora2 = (TimeSpan)dbReader["Hora2"];
                                conteudo.LabelHora3 = dbReader["LabelHora3"].ToString();
                                conteudo.Hora3 = (TimeSpan)dbReader["Hora3"];
                                conteudo.LabelHora4 = dbReader["LabelHora4"].ToString();
                                conteudo.Hora4 = (TimeSpan)dbReader["Hora4"];
                                conteudo.LabelHora5 = dbReader["LabelHora5"].ToString();
                                conteudo.Hora5 = (TimeSpan)dbReader["Hora5"];
                                conteudo.LabelTitulo1 = (string)dbReader["LabelTitulo1"];
                                conteudo.Titulo1 = (string)dbReader["Titulo1"];
                                conteudo.LabelTitulo2 = (string)dbReader["LabelTitulo2"];
                                conteudo.Titulo2 = (string)dbReader["Titulo2"];
                                conteudo.LabelTitulo3 = (string)dbReader["LabelTitulo3"];
                                conteudo.Titulo3 = (string)dbReader["Titulo3"];
                                conteudo.LabelTitulo4 = (string)dbReader["LabelTitulo4"];
                                conteudo.Titulo4 = (string)dbReader["Titulo4"];
                                conteudo.LabelTitulo5 = (string)dbReader["LabelTitulo5"];
                                conteudo.Titulo5 = (string)dbReader["Titulo5"];
                                conteudo.LabelTitulo6 = (string)dbReader["LabelTitulo6"];
                                conteudo.Titulo6 = (string)dbReader["Titulo6"];
                                conteudo.LabelTitulo7 = (string)dbReader["LabelTitulo7"];
                                conteudo.Titulo7 = (string)dbReader["Titulo7"];
                                conteudo.LabelDescricao1 = (string)dbReader["LabelDescricao1"];
                                conteudo.Descricao1 = (string)dbReader["Descricao1"];
                                conteudo.LabelDescricao2 = (string)dbReader["LabelDescricao2"];
                                conteudo.Descricao2 = (string)dbReader["Descricao2"];
                                conteudo.LabelDescricao3 = (string)dbReader["LabelDescricao3"];
                                conteudo.Descricao3 = (string)dbReader["Descricao3"];
                                conteudo.LabelDescricao4 = (string)dbReader["LabelDescricao4"];
                                conteudo.Descricao4 = (string)dbReader["Descricao4"];
                                conteudo.LabelDescricao5 = (string)dbReader["LabelDescricao5"];
                                conteudo.Descricao5 = (string)dbReader["Descricao5"];
                                conteudo.LabelDescricao6 = (string)dbReader["LabelDescricao6"];
                                conteudo.Descricao6 = (string)dbReader["Descricao6"];
                                conteudo.LabelDescricao7 = (string)dbReader["LabelDescricao7"];
                                conteudo.Descricao7 = (string)dbReader["Descricao7"];
                                conteudo.LabelLink1 = (string)dbReader["LabelLink1"];
                                conteudo.Link1 = (string)dbReader["Link1"];
                                conteudo.LabelLink2 = (string)dbReader["LabelLink2"];
                                conteudo.Link2 = (string)dbReader["Link2"];
                                conteudo.LabelLink3 = (string)dbReader["LabelLink3"];
                                conteudo.Link3 = (string)dbReader["Link3"];
                                conteudo.LabelLink4 = (string)dbReader["LabelLink4"];
                                conteudo.Link4 = (string)dbReader["Link4"];
                                conteudo.LabelLink5 = (string)dbReader["LabelLink5"];
                                conteudo.Link5 = (string)dbReader["Link5"];
                                conteudo.LabelLink6 = (string)dbReader["LabelLink6"];
                                conteudo.Link6 = (string)dbReader["Link6"];
                                conteudo.LabelLink7 = (string)dbReader["LabelLink7"];
                                conteudo.Link7 = (string)dbReader["Link7"];
                                conteudo.LabelLink8 = (string)dbReader["LabelLink8"];
                                conteudo.Link8 = (string)dbReader["Link8"];
                                conteudo.LabelLink9 = (string)dbReader["LabelLink9"];
                                conteudo.Link9 = (string)dbReader["Link9"];
                                conteudo.LabelLink10 = (string)dbReader["LabelLink10"];
                                conteudo.Link10 = (string)dbReader["Link10"];
                                conteudo.LabelValor1 = (string)dbReader["LabelValor1"];
                                conteudo.Valor1 = Convert.ToSingle(dbReader["Valor1"]);
                                conteudo.LabelValor2 = dbReader["LabelValor2"].ToString();
                                conteudo.Valor2 = Convert.ToSingle(dbReader["Valor2"]);
                                conteudo.LabelValor3 = dbReader["LabelValor3"].ToString();
                                conteudo.Valor3 = Convert.ToSingle(dbReader["Valor3"]);
                                conteudo.LabelValor4 = dbReader["LabelValor4"].ToString();
                                conteudo.Valor4 = Convert.ToSingle(dbReader["Valor4"]);
                                conteudo.LabelValor5 = (string)dbReader["LabelValor5"];
                                conteudo.Valor5 = Convert.ToSingle(dbReader["Valor5"]);
                                conteudo.LabelCheck1 = (string)dbReader["LabelCheck1"];
                                conteudo.Check1 = dbReader["Check1"] == System.DBNull.Value ? null : (bool?)dbReader["Check1"];
                                conteudo.VisibilidadePublica = (bool)dbReader["VisibilidadePublica"];
                                conteudo.PerfisVisibilidade = dbReader["PerfisVisibilidade"].ToString();
                                conteudo.TiposCargosVisibilidade = dbReader["TiposCargosVisibilidade"].ToString();
                                conteudo.OrgaosVisibilidade = dbReader["OrgaosVisibilidade"].ToString();
                                conteudo.PessoasVisibilidade = dbReader["PessoasVisibilidade"].ToString();
                                conteudo.GruposQuoVadisVisibilidade = dbReader["GruposQuoVadisVisibilidade"].ToString();


                                conteudo.PerfisVisibilidadeList = conteudo.PerfisVisibilidade == "" ? new List<string>() : conteudo.PerfisVisibilidade.Split(';').ToList();
                                conteudo.TiposCargosVisibilidadeList = conteudo.TiposCargosVisibilidade == "" ? new List<string>() : conteudo.TiposCargosVisibilidade.Split(';').ToList();
                                conteudo.OrgaosVisibilidadeList = conteudo.OrgaosVisibilidade == "" ? new List<string>() : conteudo.OrgaosVisibilidade.Split(';').ToList();
                                conteudo.PessoasVisibilidadeList = conteudo.PessoasVisibilidade == "" ? new List<string>() : conteudo.PessoasVisibilidade.Split(';').ToList();
                                conteudo.GruposQuoVadisVisibilidadeList = conteudo.GruposQuoVadisVisibilidade == "" ? new List<string>() : conteudo.GruposQuoVadisVisibilidade.Split(';').ToList();
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return conteudo;
        }

        public List<Conteudo> STP_Conteudos_S_ByTipoConteudo(int idTipoConteudo)
        {
            List<Conteudo> conteudosList = new List<Conteudo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_ByTipoConteudo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipoConteudo", idTipoConteudo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Conteudo conteudo = new Conteudo();

                                conteudo.Id = (int)dbReader["Id"];
                                conteudo.IUPI = (Guid)dbReader["IUPI"];
                                conteudo.Titulo = (string)dbReader["Titulo"];
                                conteudo.Descricao = (string)dbReader["Descricao"];
                                conteudo.Username = (string)dbReader["Username"];
                                conteudo.Nome = (string)dbReader["Nome"];
                                conteudo.IsForEstudante = (bool)dbReader["IsForEstudante"];
                                conteudo.UsernameCriacao = (string)dbReader["UsernameCriacao"];
                                conteudo.DataCriacao = (DateTime)dbReader["DataCriacao"];
                                conteudo.UsernameAlteracao = (string)dbReader["UsernameAlteracao"];
                                conteudo.DataAlteracao = dbReader["DataAlteracao"] == System.DBNull.Value ? null : (DateTime?)dbReader["DataAlteracao"];
                                conteudo.Estado = (int)dbReader["Estado"];
                                conteudo.DataEstado = dbReader["DataEstado"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataEstado"];
                                conteudo.DataInicio = dbReader["DataInicio"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataInicio"];
                                conteudo.HoraInicio = (TimeSpan)dbReader["HoraInicio"];
                                conteudo.DataFim = dbReader["DataFim"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFim"];
                                conteudo.HoraFim = (TimeSpan)dbReader["HoraFim"];
                                conteudo.DataInicioPublicacao = dbReader["DataInicioPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataInicioPublicacao"];
                                conteudo.DataFimPublicacao = dbReader["DataFimPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFimPublicacao"];
                                conteudo.PrimeiraPagina = (bool)dbReader["PrimeiraPagina"];
                                conteudo.AprovacaoPrimeiraPagina = (bool)dbReader["AprovacaoPrimeiraPagina"];
                                conteudo.UsernameAprovacaoPrimeiraPagina = (string)dbReader["UsernameAprovacaoPrimeiraPagina"];
                                conteudo.DataAprovacaoPrimeiraPagina = dbReader["DataAprovacaoPrimeiraPagina"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataAprovacaoPrimeiraPagina"];
                                conteudo.NomeFoto = (string)dbReader["NomeFoto"];
                                conteudo.LinkFoto = (string)dbReader["LinkFoto"];
                                conteudo.IdTipoConteudo = (int)dbReader["IdTipoConteudo"];
                                conteudo.NomeTipoConteudo = (string)dbReader["NomeTipoConteudo"];
                                conteudo.IdOrgao = (int)dbReader["IdOrgao"];
                                conteudo.NomeOrgao = (string)dbReader["NomeOrgao"];
                                conteudo.CodigoOrgao = (string)dbReader["CodigoOrgao"];
                                conteudo.Acao = (string)dbReader["Acao"];
                                conteudo.LabelData1 = (string)dbReader["LabelData1"];
                                conteudo.Data1 = dbReader["Data1"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data1"];
                                conteudo.LabelData2 = (string)dbReader["LabelData2"];
                                conteudo.Data2 = dbReader["Data2"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data2"];
                                conteudo.LabelData3 = (string)dbReader["LabelData3"];
                                conteudo.Data3 = dbReader["Data3"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data3"];
                                conteudo.LabelData4 = (string)dbReader["LabelData4"];
                                conteudo.Data4 = dbReader["Data4"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data4"];
                                conteudo.LabelData5 = dbReader["LabelData5"].ToString();
                                conteudo.Data5 = dbReader["Data5"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data5"];
                                conteudo.LabelHora1 = (string)dbReader["LabelHora1"];
                                conteudo.Hora1 = (TimeSpan)dbReader["Hora1"];
                                conteudo.LabelHora2 = dbReader["LabelHora2"].ToString();
                                conteudo.Hora2 = (TimeSpan)dbReader["Hora2"];
                                conteudo.LabelHora3 = dbReader["LabelHora3"].ToString();
                                conteudo.Hora3 = (TimeSpan)dbReader["Hora3"];
                                conteudo.LabelHora4 = dbReader["LabelHora4"].ToString();
                                conteudo.Hora4 = (TimeSpan)dbReader["Hora4"];
                                conteudo.LabelHora5 = dbReader["LabelHora5"].ToString();
                                conteudo.Hora5 = (TimeSpan)dbReader["Hora5"];
                                conteudo.LabelTitulo1 = (string)dbReader["LabelTitulo1"];
                                conteudo.Titulo1 = (string)dbReader["Titulo1"];
                                conteudo.LabelTitulo2 = (string)dbReader["LabelTitulo2"];
                                conteudo.Titulo2 = (string)dbReader["Titulo2"];
                                conteudo.LabelTitulo3 = (string)dbReader["LabelTitulo3"];
                                conteudo.Titulo3 = (string)dbReader["Titulo3"];
                                conteudo.LabelTitulo4 = (string)dbReader["LabelTitulo4"];
                                conteudo.Titulo4 = (string)dbReader["Titulo4"];
                                conteudo.LabelTitulo5 = (string)dbReader["LabelTitulo5"];
                                conteudo.Titulo5 = (string)dbReader["Titulo5"];
                                conteudo.LabelTitulo6 = (string)dbReader["LabelTitulo6"];
                                conteudo.Titulo6 = (string)dbReader["Titulo6"];
                                conteudo.LabelTitulo7 = (string)dbReader["LabelTitulo7"];
                                conteudo.Titulo7 = (string)dbReader["Titulo7"];
                                conteudo.LabelDescricao1 = (string)dbReader["LabelDescricao1"];
                                conteudo.Descricao1 = (string)dbReader["Descricao1"];
                                conteudo.LabelDescricao2 = (string)dbReader["LabelDescricao2"];
                                conteudo.Descricao2 = (string)dbReader["Descricao2"];
                                conteudo.LabelDescricao3 = (string)dbReader["LabelDescricao3"];
                                conteudo.Descricao3 = (string)dbReader["Descricao3"];
                                conteudo.LabelDescricao4 = (string)dbReader["LabelDescricao4"];
                                conteudo.Descricao4 = (string)dbReader["Descricao4"];
                                conteudo.LabelDescricao5 = (string)dbReader["LabelDescricao5"];
                                conteudo.Descricao5 = (string)dbReader["Descricao5"];
                                conteudo.LabelDescricao6 = (string)dbReader["LabelDescricao6"];
                                conteudo.Descricao6 = (string)dbReader["Descricao6"];
                                conteudo.LabelDescricao7 = (string)dbReader["LabelDescricao7"];
                                conteudo.Descricao7 = (string)dbReader["Descricao7"];
                                conteudo.LabelLink1 = (string)dbReader["LabelLink1"];
                                conteudo.Link1 = (string)dbReader["Link1"];
                                conteudo.LabelLink2 = (string)dbReader["LabelLink2"];
                                conteudo.Link2 = (string)dbReader["Link2"];
                                conteudo.LabelLink3 = (string)dbReader["LabelLink3"];
                                conteudo.Link3 = (string)dbReader["Link3"];
                                conteudo.LabelLink4 = (string)dbReader["LabelLink4"];
                                conteudo.Link4 = (string)dbReader["Link4"];
                                conteudo.LabelLink5 = (string)dbReader["LabelLink5"];
                                conteudo.Link5 = (string)dbReader["Link5"];
                                conteudo.LabelLink6 = (string)dbReader["LabelLink6"];
                                conteudo.Link6 = (string)dbReader["Link6"];
                                conteudo.LabelLink7 = (string)dbReader["LabelLink7"];
                                conteudo.Link7 = (string)dbReader["Link7"];
                                conteudo.LabelLink8 = (string)dbReader["LabelLink8"];
                                conteudo.Link8 = (string)dbReader["Link8"];
                                conteudo.LabelLink9 = (string)dbReader["LabelLink9"];
                                conteudo.Link9 = (string)dbReader["Link9"];
                                conteudo.LabelLink10 = (string)dbReader["LabelLink10"];
                                conteudo.Link10 = (string)dbReader["Link10"];
                                conteudo.LabelValor1 = (string)dbReader["LabelValor1"];
                                conteudo.Valor1 = Convert.ToSingle(dbReader["Valor1"]);
                                conteudo.LabelValor2 = dbReader["LabelValor2"].ToString();
                                conteudo.Valor2 = Convert.ToSingle(dbReader["Valor2"]);
                                conteudo.LabelValor3 = dbReader["LabelValor3"].ToString();
                                conteudo.Valor3 = Convert.ToSingle(dbReader["Valor3"]);
                                conteudo.LabelValor4 = dbReader["LabelValor4"].ToString();
                                conteudo.Valor4 = Convert.ToSingle(dbReader["Valor4"]);
                                conteudo.LabelValor5 = (string)dbReader["LabelValor5"];
                                conteudo.Valor5 = Convert.ToSingle(dbReader["Valor5"]);
                                conteudo.LabelCheck1 = (string)dbReader["LabelCheck1"];
                                conteudo.Check1 = dbReader["Check1"] == System.DBNull.Value ? null : (bool?)dbReader["Check1"];
                                conteudo.VisibilidadePublica = (bool)dbReader["VisibilidadePublica"];
                                conteudo.PerfisVisibilidade = dbReader["PerfisVisibilidade"].ToString();
                                conteudo.TiposCargosVisibilidade = dbReader["TiposCargosVisibilidade"].ToString();
                                conteudo.OrgaosVisibilidade = dbReader["OrgaosVisibilidade"].ToString();
                                conteudo.PessoasVisibilidade = dbReader["PessoasVisibilidade"].ToString();
                                conteudo.GruposQuoVadisVisibilidade = dbReader["GruposQuoVadisVisibilidade"].ToString();


                                conteudo.PerfisVisibilidadeList = conteudo.PerfisVisibilidade == "" ? new List<string>() : conteudo.PerfisVisibilidade.Split(';').ToList();
                                conteudo.TiposCargosVisibilidadeList = conteudo.TiposCargosVisibilidade == "" ? new List<string>() : conteudo.TiposCargosVisibilidade.Split(';').ToList();
                                conteudo.OrgaosVisibilidadeList = conteudo.OrgaosVisibilidade == "" ? new List<string>() : conteudo.OrgaosVisibilidade.Split(';').ToList();
                                conteudo.PessoasVisibilidadeList = conteudo.PessoasVisibilidade == "" ? new List<string>() : conteudo.PessoasVisibilidade.Split(';').ToList();
                                conteudo.GruposQuoVadisVisibilidadeList = conteudo.GruposQuoVadisVisibilidade == "" ? new List<string>() : conteudo.GruposQuoVadisVisibilidade.Split(';').ToList();


                                conteudosList.Add(conteudo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return conteudosList;
        }

        public List<Conteudo> STP_Conteudos_S_ByTipoConteudoAndOrgao(int idTipoConteudo, int idOrgao)
        {
            List<Conteudo> conteudosList = new List<Conteudo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_ByTipoConteudoAndOrgao";
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipoConteudo", idTipoConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgao", idOrgao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Conteudo conteudo = new Conteudo();

                                conteudo.Id = (int)dbReader["Id"];
                                conteudo.IUPI = (Guid)dbReader["IUPI"];
                                conteudo.Titulo = (string)dbReader["Titulo"];
                                conteudo.Descricao = (string)dbReader["Descricao"];
                                conteudo.Username = (string)dbReader["Username"];
                                conteudo.Nome = (string)dbReader["Nome"];
                                conteudo.IsForEstudante = (bool)dbReader["IsForEstudante"];
                                conteudo.UsernameCriacao = (string)dbReader["UsernameCriacao"];
                                conteudo.DataCriacao = (DateTime)dbReader["DataCriacao"];
                                conteudo.UsernameAlteracao = (string)dbReader["UsernameAlteracao"];
                                conteudo.DataAlteracao = dbReader["DataAlteracao"] == System.DBNull.Value ? null : (DateTime?)dbReader["DataAlteracao"];
                                conteudo.Estado = (int)dbReader["Estado"];
                                conteudo.DataEstado = dbReader["DataEstado"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataEstado"];
                                conteudo.DataInicio = dbReader["DataInicio"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataInicio"];
                                conteudo.HoraInicio = (TimeSpan)dbReader["HoraInicio"];
                                conteudo.DataFim = dbReader["DataFim"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFim"];
                                conteudo.HoraFim = (TimeSpan)dbReader["HoraFim"];
                                conteudo.DataInicioPublicacao = dbReader["DataInicioPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataInicioPublicacao"];
                                conteudo.DataFimPublicacao = dbReader["DataFimPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFimPublicacao"];
                                conteudo.PrimeiraPagina = (bool)dbReader["PrimeiraPagina"];
                                conteudo.AprovacaoPrimeiraPagina = (bool)dbReader["AprovacaoPrimeiraPagina"];
                                conteudo.UsernameAprovacaoPrimeiraPagina = (string)dbReader["UsernameAprovacaoPrimeiraPagina"];
                                conteudo.DataAprovacaoPrimeiraPagina = dbReader["DataAprovacaoPrimeiraPagina"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataAprovacaoPrimeiraPagina"];
                                conteudo.NomeFoto = (string)dbReader["NomeFoto"];
                                conteudo.LinkFoto = (string)dbReader["LinkFoto"];
                                conteudo.IdTipoConteudo = (int)dbReader["IdTipoConteudo"];
                                conteudo.NomeTipoConteudo = (string)dbReader["NomeTipoConteudo"];
                                conteudo.IdOrgao = (int)dbReader["IdOrgao"];
                                conteudo.NomeOrgao = (string)dbReader["NomeOrgao"];
                                conteudo.CodigoOrgao = (string)dbReader["CodigoOrgao"];
                                conteudo.Acao = (string)dbReader["Acao"];
                                conteudo.LabelData1 = (string)dbReader["LabelData1"];
                                conteudo.Data1 = dbReader["Data1"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data1"];
                                conteudo.LabelData2 = (string)dbReader["LabelData2"];
                                conteudo.Data2 = dbReader["Data2"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data2"];
                                conteudo.LabelData3 = (string)dbReader["LabelData3"];
                                conteudo.Data3 = dbReader["Data3"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data3"];
                                conteudo.LabelData4 = (string)dbReader["LabelData4"];
                                conteudo.Data4 = dbReader["Data4"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data4"];
                                conteudo.LabelData5 = dbReader["LabelData5"].ToString();
                                conteudo.Data5 = dbReader["Data5"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data5"];
                                conteudo.LabelHora1 = (string)dbReader["LabelHora1"];
                                conteudo.Hora1 = (TimeSpan)dbReader["Hora1"];
                                conteudo.LabelHora2 = dbReader["LabelHora2"].ToString();
                                conteudo.Hora2 = (TimeSpan)dbReader["Hora2"];
                                conteudo.LabelHora3 = dbReader["LabelHora3"].ToString();
                                conteudo.Hora3 = (TimeSpan)dbReader["Hora3"];
                                conteudo.LabelHora4 = dbReader["LabelHora4"].ToString();
                                conteudo.Hora4 = (TimeSpan)dbReader["Hora4"];
                                conteudo.LabelHora5 = dbReader["LabelHora5"].ToString();
                                conteudo.Hora5 = (TimeSpan)dbReader["Hora5"];
                                conteudo.LabelTitulo1 = (string)dbReader["LabelTitulo1"];
                                conteudo.Titulo1 = (string)dbReader["Titulo1"];
                                conteudo.LabelTitulo2 = (string)dbReader["LabelTitulo2"];
                                conteudo.Titulo2 = (string)dbReader["Titulo2"];
                                conteudo.LabelTitulo3 = (string)dbReader["LabelTitulo3"];
                                conteudo.Titulo3 = (string)dbReader["Titulo3"];
                                conteudo.LabelTitulo4 = (string)dbReader["LabelTitulo4"];
                                conteudo.Titulo4 = (string)dbReader["Titulo4"];
                                conteudo.LabelTitulo5 = (string)dbReader["LabelTitulo5"];
                                conteudo.Titulo5 = (string)dbReader["Titulo5"];
                                conteudo.LabelTitulo6 = (string)dbReader["LabelTitulo6"];
                                conteudo.Titulo6 = (string)dbReader["Titulo6"];
                                conteudo.LabelTitulo7 = (string)dbReader["LabelTitulo7"];
                                conteudo.Titulo7 = (string)dbReader["Titulo7"];
                                conteudo.LabelDescricao1 = (string)dbReader["LabelDescricao1"];
                                conteudo.Descricao1 = (string)dbReader["Descricao1"];
                                conteudo.LabelDescricao2 = (string)dbReader["LabelDescricao2"];
                                conteudo.Descricao2 = (string)dbReader["Descricao2"];
                                conteudo.LabelDescricao3 = (string)dbReader["LabelDescricao3"];
                                conteudo.Descricao3 = (string)dbReader["Descricao3"];
                                conteudo.LabelDescricao4 = (string)dbReader["LabelDescricao4"];
                                conteudo.Descricao4 = (string)dbReader["Descricao4"];
                                conteudo.LabelDescricao5 = (string)dbReader["LabelDescricao5"];
                                conteudo.Descricao5 = (string)dbReader["Descricao5"];
                                conteudo.LabelDescricao6 = (string)dbReader["LabelDescricao6"];
                                conteudo.Descricao6 = (string)dbReader["Descricao6"];
                                conteudo.LabelDescricao7 = (string)dbReader["LabelDescricao7"];
                                conteudo.Descricao7 = (string)dbReader["Descricao7"];
                                conteudo.LabelLink1 = (string)dbReader["LabelLink1"];
                                conteudo.Link1 = (string)dbReader["Link1"];
                                conteudo.LabelLink2 = (string)dbReader["LabelLink2"];
                                conteudo.Link2 = (string)dbReader["Link2"];
                                conteudo.LabelLink3 = (string)dbReader["LabelLink3"];
                                conteudo.Link3 = (string)dbReader["Link3"];
                                conteudo.LabelLink4 = (string)dbReader["LabelLink4"];
                                conteudo.Link4 = (string)dbReader["Link4"];
                                conteudo.LabelLink5 = (string)dbReader["LabelLink5"];
                                conteudo.Link5 = (string)dbReader["Link5"];
                                conteudo.LabelLink6 = (string)dbReader["LabelLink6"];
                                conteudo.Link6 = (string)dbReader["Link6"];
                                conteudo.LabelLink7 = (string)dbReader["LabelLink7"];
                                conteudo.Link7 = (string)dbReader["Link7"];
                                conteudo.LabelLink8 = (string)dbReader["LabelLink8"];
                                conteudo.Link8 = (string)dbReader["Link8"];
                                conteudo.LabelLink9 = (string)dbReader["LabelLink9"];
                                conteudo.Link9 = (string)dbReader["Link9"];
                                conteudo.LabelLink10 = (string)dbReader["LabelLink10"];
                                conteudo.Link10 = (string)dbReader["Link10"];
                                conteudo.LabelValor1 = (string)dbReader["LabelValor1"];
                                conteudo.Valor1 = Convert.ToSingle(dbReader["Valor1"]);
                                conteudo.LabelValor2 = dbReader["LabelValor2"].ToString();
                                conteudo.Valor2 = Convert.ToSingle(dbReader["Valor2"]);
                                conteudo.LabelValor3 = dbReader["LabelValor3"].ToString();
                                conteudo.Valor3 = Convert.ToSingle(dbReader["Valor3"]);
                                conteudo.LabelValor4 = dbReader["LabelValor4"].ToString();
                                conteudo.Valor4 = Convert.ToSingle(dbReader["Valor4"]);
                                conteudo.LabelValor5 = (string)dbReader["LabelValor5"];
                                conteudo.Valor5 = Convert.ToSingle(dbReader["Valor5"]);
                                conteudo.LabelCheck1 = (string)dbReader["LabelCheck1"];
                                conteudo.Check1 = dbReader["Check1"] == System.DBNull.Value ? null : (bool?)dbReader["Check1"];
                                conteudo.VisibilidadePublica = (bool)dbReader["VisibilidadePublica"];
                                conteudo.PerfisVisibilidade = dbReader["PerfisVisibilidade"].ToString();
                                conteudo.TiposCargosVisibilidade = dbReader["TiposCargosVisibilidade"].ToString();
                                conteudo.OrgaosVisibilidade = dbReader["OrgaosVisibilidade"].ToString();
                                conteudo.PessoasVisibilidade = dbReader["PessoasVisibilidade"].ToString();
                                conteudo.GruposQuoVadisVisibilidade = dbReader["GruposQuoVadisVisibilidade"].ToString();


                                conteudo.PerfisVisibilidadeList = conteudo.PerfisVisibilidade == "" ? new List<string>() : conteudo.PerfisVisibilidade.Split(';').ToList();
                                conteudo.TiposCargosVisibilidadeList = conteudo.TiposCargosVisibilidade == "" ? new List<string>() : conteudo.TiposCargosVisibilidade.Split(';').ToList();
                                conteudo.OrgaosVisibilidadeList = conteudo.OrgaosVisibilidade == "" ? new List<string>() : conteudo.OrgaosVisibilidade.Split(';').ToList();
                                conteudo.PessoasVisibilidadeList = conteudo.PessoasVisibilidade == "" ? new List<string>() : conteudo.PessoasVisibilidade.Split(';').ToList();
                                conteudo.GruposQuoVadisVisibilidadeList = conteudo.GruposQuoVadisVisibilidade == "" ? new List<string>() : conteudo.GruposQuoVadisVisibilidade.Split(';').ToList();


                                conteudosList.Add(conteudo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return conteudosList;
        }

        public List<Conteudo> STP_Conteudos_S_ByTipoConteudoAndOrgaoAndEstado(int idTipoConteudo, int idOrgao, int estado, bool visibilidadePublica)
        {
            List<Conteudo> conteudosList = new List<Conteudo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_ByTipoConteudoAndOrgaoAndEstado";
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipoConteudo", idTipoConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgao", idOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Conteudo conteudo = new Conteudo();

                                conteudo.Id = (int)dbReader["Id"];
                                conteudo.IUPI = (Guid)dbReader["IUPI"];
                                conteudo.Titulo = (string)dbReader["Titulo"];
                                conteudo.Descricao = (string)dbReader["Descricao"];
                                conteudo.Username = (string)dbReader["Username"];
                                conteudo.Nome = (string)dbReader["Nome"];
                                conteudo.IsForEstudante = (bool)dbReader["IsForEstudante"];
                                conteudo.UsernameCriacao = (string)dbReader["UsernameCriacao"];
                                conteudo.DataCriacao = (DateTime)dbReader["DataCriacao"];
                                conteudo.UsernameAlteracao = (string)dbReader["UsernameAlteracao"];
                                conteudo.DataAlteracao = dbReader["DataAlteracao"] == System.DBNull.Value ? null : (DateTime?)dbReader["DataAlteracao"];
                                conteudo.Estado = (int)dbReader["Estado"];
                                conteudo.DataEstado = dbReader["DataEstado"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataEstado"];
                                conteudo.DataInicio = dbReader["DataInicio"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataInicio"];
                                conteudo.HoraInicio = (TimeSpan)dbReader["HoraInicio"];
                                conteudo.DataFim = dbReader["DataFim"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFim"];
                                conteudo.HoraFim = (TimeSpan)dbReader["HoraFim"];
                                conteudo.DataInicioPublicacao = dbReader["DataInicioPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataInicioPublicacao"];
                                conteudo.DataFimPublicacao = dbReader["DataFimPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFimPublicacao"];
                                conteudo.PrimeiraPagina = (bool)dbReader["PrimeiraPagina"];
                                conteudo.AprovacaoPrimeiraPagina = (bool)dbReader["AprovacaoPrimeiraPagina"];
                                conteudo.UsernameAprovacaoPrimeiraPagina = (string)dbReader["UsernameAprovacaoPrimeiraPagina"];
                                conteudo.DataAprovacaoPrimeiraPagina = dbReader["DataAprovacaoPrimeiraPagina"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataAprovacaoPrimeiraPagina"];
                                conteudo.NomeFoto = (string)dbReader["NomeFoto"];
                                conteudo.LinkFoto = (string)dbReader["LinkFoto"];
                                conteudo.IdTipoConteudo = (int)dbReader["IdTipoConteudo"];
                                conteudo.NomeTipoConteudo = (string)dbReader["NomeTipoConteudo"];
                                conteudo.IdOrgao = (int)dbReader["IdOrgao"];
                                conteudo.NomeOrgao = (string)dbReader["NomeOrgao"];
                                conteudo.CodigoOrgao = (string)dbReader["CodigoOrgao"];
                                conteudo.Acao = (string)dbReader["Acao"];
                                conteudo.LabelData1 = (string)dbReader["LabelData1"];
                                conteudo.Data1 = dbReader["Data1"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data1"];
                                conteudo.LabelData2 = (string)dbReader["LabelData2"];
                                conteudo.Data2 = dbReader["Data2"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data2"];
                                conteudo.LabelData3 = (string)dbReader["LabelData3"];
                                conteudo.Data3 = dbReader["Data3"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data3"];
                                conteudo.LabelData4 = (string)dbReader["LabelData4"];
                                conteudo.Data4 = dbReader["Data4"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data4"];
                                conteudo.LabelData5 = dbReader["LabelData5"].ToString();
                                conteudo.Data5 = dbReader["Data5"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data5"];
                                conteudo.LabelHora1 = (string)dbReader["LabelHora1"];
                                conteudo.Hora1 = (TimeSpan)dbReader["Hora1"];
                                conteudo.LabelHora2 = dbReader["LabelHora2"].ToString();
                                conteudo.Hora2 = (TimeSpan)dbReader["Hora2"];
                                conteudo.LabelHora3 = dbReader["LabelHora3"].ToString();
                                conteudo.Hora3 = (TimeSpan)dbReader["Hora3"];
                                conteudo.LabelHora4 = dbReader["LabelHora4"].ToString();
                                conteudo.Hora4 = (TimeSpan)dbReader["Hora4"];
                                conteudo.LabelHora5 = dbReader["LabelHora5"].ToString();
                                conteudo.Hora5 = (TimeSpan)dbReader["Hora5"];
                                conteudo.LabelTitulo1 = (string)dbReader["LabelTitulo1"];
                                conteudo.Titulo1 = (string)dbReader["Titulo1"];
                                conteudo.LabelTitulo2 = (string)dbReader["LabelTitulo2"];
                                conteudo.Titulo2 = (string)dbReader["Titulo2"];
                                conteudo.LabelTitulo3 = (string)dbReader["LabelTitulo3"];
                                conteudo.Titulo3 = (string)dbReader["Titulo3"];
                                conteudo.LabelTitulo4 = (string)dbReader["LabelTitulo4"];
                                conteudo.Titulo4 = (string)dbReader["Titulo4"];
                                conteudo.LabelTitulo5 = (string)dbReader["LabelTitulo5"];
                                conteudo.Titulo5 = (string)dbReader["Titulo5"];
                                conteudo.LabelTitulo6 = (string)dbReader["LabelTitulo6"];
                                conteudo.Titulo6 = (string)dbReader["Titulo6"];
                                conteudo.LabelTitulo7 = (string)dbReader["LabelTitulo7"];
                                conteudo.Titulo7 = (string)dbReader["Titulo7"];
                                conteudo.LabelDescricao1 = (string)dbReader["LabelDescricao1"];
                                conteudo.Descricao1 = (string)dbReader["Descricao1"];
                                conteudo.LabelDescricao2 = (string)dbReader["LabelDescricao2"];
                                conteudo.Descricao2 = (string)dbReader["Descricao2"];
                                conteudo.LabelDescricao3 = (string)dbReader["LabelDescricao3"];
                                conteudo.Descricao3 = (string)dbReader["Descricao3"];
                                conteudo.LabelDescricao4 = (string)dbReader["LabelDescricao4"];
                                conteudo.Descricao4 = (string)dbReader["Descricao4"];
                                conteudo.LabelDescricao5 = (string)dbReader["LabelDescricao5"];
                                conteudo.Descricao5 = (string)dbReader["Descricao5"];
                                conteudo.LabelDescricao6 = (string)dbReader["LabelDescricao6"];
                                conteudo.Descricao6 = (string)dbReader["Descricao6"];
                                conteudo.LabelDescricao7 = (string)dbReader["LabelDescricao7"];
                                conteudo.Descricao7 = (string)dbReader["Descricao7"];
                                conteudo.LabelLink1 = (string)dbReader["LabelLink1"];
                                conteudo.Link1 = (string)dbReader["Link1"];
                                conteudo.LabelLink2 = (string)dbReader["LabelLink2"];
                                conteudo.Link2 = (string)dbReader["Link2"];
                                conteudo.LabelLink3 = (string)dbReader["LabelLink3"];
                                conteudo.Link3 = (string)dbReader["Link3"];
                                conteudo.LabelLink4 = (string)dbReader["LabelLink4"];
                                conteudo.Link4 = (string)dbReader["Link4"];
                                conteudo.LabelLink5 = (string)dbReader["LabelLink5"];
                                conteudo.Link5 = (string)dbReader["Link5"];
                                conteudo.LabelLink6 = (string)dbReader["LabelLink6"];
                                conteudo.Link6 = (string)dbReader["Link6"];
                                conteudo.LabelLink7 = (string)dbReader["LabelLink7"];
                                conteudo.Link7 = (string)dbReader["Link7"];
                                conteudo.LabelLink8 = (string)dbReader["LabelLink8"];
                                conteudo.Link8 = (string)dbReader["Link8"];
                                conteudo.LabelLink9 = (string)dbReader["LabelLink9"];
                                conteudo.Link9 = (string)dbReader["Link9"];
                                conteudo.LabelLink10 = (string)dbReader["LabelLink10"];
                                conteudo.Link10 = (string)dbReader["Link10"];
                                conteudo.LabelValor1 = (string)dbReader["LabelValor1"];
                                conteudo.Valor1 = Convert.ToSingle(dbReader["Valor1"]);
                                conteudo.LabelValor2 = dbReader["LabelValor2"].ToString();
                                conteudo.Valor2 = Convert.ToSingle(dbReader["Valor2"]);
                                conteudo.LabelValor3 = dbReader["LabelValor3"].ToString();
                                conteudo.Valor3 = Convert.ToSingle(dbReader["Valor3"]);
                                conteudo.LabelValor4 = dbReader["LabelValor4"].ToString();
                                conteudo.Valor4 = Convert.ToSingle(dbReader["Valor4"]);
                                conteudo.LabelValor5 = (string)dbReader["LabelValor5"];
                                conteudo.Valor5 = Convert.ToSingle(dbReader["Valor5"]);
                                conteudo.LabelCheck1 = (string)dbReader["LabelCheck1"];
                                conteudo.Check1 = dbReader["Check1"] == System.DBNull.Value ? null : (bool?)dbReader["Check1"];
                                conteudo.VisibilidadePublica = (bool)dbReader["VisibilidadePublica"];
                                conteudo.PerfisVisibilidade = dbReader["PerfisVisibilidade"].ToString();
                                conteudo.TiposCargosVisibilidade = dbReader["TiposCargosVisibilidade"].ToString();
                                conteudo.OrgaosVisibilidade = dbReader["OrgaosVisibilidade"].ToString();
                                conteudo.PessoasVisibilidade = dbReader["PessoasVisibilidade"].ToString();
                                conteudo.GruposQuoVadisVisibilidade = dbReader["GruposQuoVadisVisibilidade"].ToString();


                                conteudo.PerfisVisibilidadeList = conteudo.PerfisVisibilidade == "" ? new List<string>() : conteudo.PerfisVisibilidade.Split(';').ToList();
                                conteudo.TiposCargosVisibilidadeList = conteudo.TiposCargosVisibilidade == "" ? new List<string>() : conteudo.TiposCargosVisibilidade.Split(';').ToList();
                                conteudo.OrgaosVisibilidadeList = conteudo.OrgaosVisibilidade == "" ? new List<string>() : conteudo.OrgaosVisibilidade.Split(';').ToList();
                                conteudo.PessoasVisibilidadeList = conteudo.PessoasVisibilidade == "" ? new List<string>() : conteudo.PessoasVisibilidade.Split(';').ToList();
                                conteudo.GruposQuoVadisVisibilidadeList = conteudo.GruposQuoVadisVisibilidade == "" ? new List<string>() : conteudo.GruposQuoVadisVisibilidade.Split(';').ToList();


                                conteudosList.Add(conteudo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return conteudosList;
        }

        public List<Conteudo> STP_Conteudos_S_ByTipoConteudoAndEstado(int idTipoConteudo, int estado, bool visibilidadePublica)
        {
            List<Conteudo> conteudosList = new List<Conteudo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_ByTipoConteudoAndEstado";
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipoConteudo", idTipoConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Conteudo conteudo = new Conteudo();

                                conteudo.Id = (int)dbReader["Id"];
                                conteudo.IUPI = (Guid)dbReader["IUPI"];
                                conteudo.Titulo = (string)dbReader["Titulo"];
                                conteudo.Descricao = (string)dbReader["Descricao"];
                                conteudo.Username = (string)dbReader["Username"];
                                conteudo.Nome = (string)dbReader["Nome"];
                                conteudo.IsForEstudante = (bool)dbReader["IsForEstudante"];
                                conteudo.UsernameCriacao = (string)dbReader["UsernameCriacao"];
                                conteudo.DataCriacao = (DateTime)dbReader["DataCriacao"];
                                conteudo.UsernameAlteracao = (string)dbReader["UsernameAlteracao"];
                                conteudo.DataAlteracao = dbReader["DataAlteracao"] == System.DBNull.Value ? null : (DateTime?)dbReader["DataAlteracao"];
                                conteudo.Estado = (int)dbReader["Estado"];
                                conteudo.DataEstado = dbReader["DataEstado"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataEstado"];
                                conteudo.DataInicio = dbReader["DataInicio"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataInicio"];
                                conteudo.HoraInicio = (TimeSpan)dbReader["HoraInicio"];
                                conteudo.DataFim = dbReader["DataFim"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFim"];
                                conteudo.HoraFim = (TimeSpan)dbReader["HoraFim"];
                                conteudo.DataInicioPublicacao = dbReader["DataInicioPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataInicioPublicacao"];
                                conteudo.DataFimPublicacao = dbReader["DataFimPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFimPublicacao"];
                                conteudo.PrimeiraPagina = (bool)dbReader["PrimeiraPagina"];
                                conteudo.AprovacaoPrimeiraPagina = (bool)dbReader["AprovacaoPrimeiraPagina"];
                                conteudo.UsernameAprovacaoPrimeiraPagina = (string)dbReader["UsernameAprovacaoPrimeiraPagina"];
                                conteudo.DataAprovacaoPrimeiraPagina = dbReader["DataAprovacaoPrimeiraPagina"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataAprovacaoPrimeiraPagina"];
                                conteudo.NomeFoto = (string)dbReader["NomeFoto"];
                                conteudo.LinkFoto = (string)dbReader["LinkFoto"];
                                conteudo.IdTipoConteudo = (int)dbReader["IdTipoConteudo"];
                                conteudo.NomeTipoConteudo = (string)dbReader["NomeTipoConteudo"];
                                conteudo.IdOrgao = (int)dbReader["IdOrgao"];
                                conteudo.NomeOrgao = (string)dbReader["NomeOrgao"];
                                conteudo.CodigoOrgao = (string)dbReader["CodigoOrgao"];
                                conteudo.Acao = (string)dbReader["Acao"];
                                conteudo.LabelData1 = (string)dbReader["LabelData1"];
                                conteudo.Data1 = dbReader["Data1"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data1"];
                                conteudo.LabelData2 = (string)dbReader["LabelData2"];
                                conteudo.Data2 = dbReader["Data2"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data2"];
                                conteudo.LabelData3 = (string)dbReader["LabelData3"];
                                conteudo.Data3 = dbReader["Data3"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data3"];
                                conteudo.LabelData4 = (string)dbReader["LabelData4"];
                                conteudo.Data4 = dbReader["Data4"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data4"];
                                conteudo.LabelData5 = dbReader["LabelData5"].ToString();
                                conteudo.Data5 = dbReader["Data5"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data5"];
                                conteudo.LabelHora1 = (string)dbReader["LabelHora1"];
                                conteudo.Hora1 = (TimeSpan)dbReader["Hora1"];
                                conteudo.LabelHora2 = dbReader["LabelHora2"].ToString();
                                conteudo.Hora2 = (TimeSpan)dbReader["Hora2"];
                                conteudo.LabelHora3 = dbReader["LabelHora3"].ToString();
                                conteudo.Hora3 = (TimeSpan)dbReader["Hora3"];
                                conteudo.LabelHora4 = dbReader["LabelHora4"].ToString();
                                conteudo.Hora4 = (TimeSpan)dbReader["Hora4"];
                                conteudo.LabelHora5 = dbReader["LabelHora5"].ToString();
                                conteudo.Hora5 = (TimeSpan)dbReader["Hora5"];
                                conteudo.LabelTitulo1 = (string)dbReader["LabelTitulo1"];
                                conteudo.Titulo1 = (string)dbReader["Titulo1"];
                                conteudo.LabelTitulo2 = (string)dbReader["LabelTitulo2"];
                                conteudo.Titulo2 = (string)dbReader["Titulo2"];
                                conteudo.LabelTitulo3 = (string)dbReader["LabelTitulo3"];
                                conteudo.Titulo3 = (string)dbReader["Titulo3"];
                                conteudo.LabelTitulo4 = (string)dbReader["LabelTitulo4"];
                                conteudo.Titulo4 = (string)dbReader["Titulo4"];
                                conteudo.LabelTitulo5 = (string)dbReader["LabelTitulo5"];
                                conteudo.Titulo5 = (string)dbReader["Titulo5"];
                                conteudo.LabelTitulo6 = (string)dbReader["LabelTitulo6"];
                                conteudo.Titulo6 = (string)dbReader["Titulo6"];
                                conteudo.LabelTitulo7 = (string)dbReader["LabelTitulo7"];
                                conteudo.Titulo7 = (string)dbReader["Titulo7"];
                                conteudo.LabelDescricao1 = (string)dbReader["LabelDescricao1"];
                                conteudo.Descricao1 = (string)dbReader["Descricao1"];
                                conteudo.LabelDescricao2 = (string)dbReader["LabelDescricao2"];
                                conteudo.Descricao2 = (string)dbReader["Descricao2"];
                                conteudo.LabelDescricao3 = (string)dbReader["LabelDescricao3"];
                                conteudo.Descricao3 = (string)dbReader["Descricao3"];
                                conteudo.LabelDescricao4 = (string)dbReader["LabelDescricao4"];
                                conteudo.Descricao4 = (string)dbReader["Descricao4"];
                                conteudo.LabelDescricao5 = (string)dbReader["LabelDescricao5"];
                                conteudo.Descricao5 = (string)dbReader["Descricao5"];
                                conteudo.LabelDescricao6 = (string)dbReader["LabelDescricao6"];
                                conteudo.Descricao6 = (string)dbReader["Descricao6"];
                                conteudo.LabelDescricao7 = (string)dbReader["LabelDescricao7"];
                                conteudo.Descricao7 = (string)dbReader["Descricao7"];
                                conteudo.LabelLink1 = (string)dbReader["LabelLink1"];
                                conteudo.Link1 = (string)dbReader["Link1"];
                                conteudo.LabelLink2 = (string)dbReader["LabelLink2"];
                                conteudo.Link2 = (string)dbReader["Link2"];
                                conteudo.LabelLink3 = (string)dbReader["LabelLink3"];
                                conteudo.Link3 = (string)dbReader["Link3"];
                                conteudo.LabelLink4 = (string)dbReader["LabelLink4"];
                                conteudo.Link4 = (string)dbReader["Link4"];
                                conteudo.LabelLink5 = (string)dbReader["LabelLink5"];
                                conteudo.Link5 = (string)dbReader["Link5"];
                                conteudo.LabelLink6 = (string)dbReader["LabelLink6"];
                                conteudo.Link6 = (string)dbReader["Link6"];
                                conteudo.LabelLink7 = (string)dbReader["LabelLink7"];
                                conteudo.Link7 = (string)dbReader["Link7"];
                                conteudo.LabelLink8 = (string)dbReader["LabelLink8"];
                                conteudo.Link8 = (string)dbReader["Link8"];
                                conteudo.LabelLink9 = (string)dbReader["LabelLink9"];
                                conteudo.Link9 = (string)dbReader["Link9"];
                                conteudo.LabelLink10 = (string)dbReader["LabelLink10"];
                                conteudo.Link10 = (string)dbReader["Link10"];
                                conteudo.LabelValor1 = (string)dbReader["LabelValor1"];
                                conteudo.Valor1 = Convert.ToSingle(dbReader["Valor1"]);
                                conteudo.LabelValor2 = dbReader["LabelValor2"].ToString();
                                conteudo.Valor2 = Convert.ToSingle(dbReader["Valor2"]);
                                conteudo.LabelValor3 = dbReader["LabelValor3"].ToString();
                                conteudo.Valor3 = Convert.ToSingle(dbReader["Valor3"]);
                                conteudo.LabelValor4 = dbReader["LabelValor4"].ToString();
                                conteudo.Valor4 = Convert.ToSingle(dbReader["Valor4"]);
                                conteudo.LabelValor5 = (string)dbReader["LabelValor5"];
                                conteudo.Valor5 = Convert.ToSingle(dbReader["Valor5"]);
                                conteudo.LabelCheck1 = (string)dbReader["LabelCheck1"];
                                conteudo.Check1 = dbReader["Check1"] == System.DBNull.Value ? null : (bool?)dbReader["Check1"];
                                conteudo.VisibilidadePublica = (bool)dbReader["VisibilidadePublica"];
                                conteudo.PerfisVisibilidade = dbReader["PerfisVisibilidade"].ToString();
                                conteudo.TiposCargosVisibilidade = dbReader["TiposCargosVisibilidade"].ToString();
                                conteudo.OrgaosVisibilidade = dbReader["OrgaosVisibilidade"].ToString();
                                conteudo.PessoasVisibilidade = dbReader["PessoasVisibilidade"].ToString();
                                conteudo.GruposQuoVadisVisibilidade = dbReader["GruposQuoVadisVisibilidade"].ToString();


                                conteudo.PerfisVisibilidadeList = conteudo.PerfisVisibilidade == "" ? new List<string>() : conteudo.PerfisVisibilidade.Split(';').ToList();
                                conteudo.TiposCargosVisibilidadeList = conteudo.TiposCargosVisibilidade == "" ? new List<string>() : conteudo.TiposCargosVisibilidade.Split(';').ToList();
                                conteudo.OrgaosVisibilidadeList = conteudo.OrgaosVisibilidade == "" ? new List<string>() : conteudo.OrgaosVisibilidade.Split(';').ToList();
                                conteudo.PessoasVisibilidadeList = conteudo.PessoasVisibilidade == "" ? new List<string>() : conteudo.PessoasVisibilidade.Split(';').ToList();
                                conteudo.GruposQuoVadisVisibilidadeList = conteudo.GruposQuoVadisVisibilidade == "" ? new List<string>() : conteudo.GruposQuoVadisVisibilidade.Split(';').ToList();


                                conteudosList.Add(conteudo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return conteudosList;
        }

        public List<Conteudo> STP_Conteudos_S_ByOrgaoAndEstado(int idOrgao, int estado, bool visibilidadePublica)
        {
            List<Conteudo> conteudosList = new List<Conteudo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_ByOrgaoAndEstado";
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgao", idOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Conteudo conteudo = new Conteudo();

                                conteudo.Id = (int)dbReader["Id"];
                                conteudo.IUPI = (Guid)dbReader["IUPI"];
                                conteudo.Titulo = (string)dbReader["Titulo"];
                                conteudo.Descricao = (string)dbReader["Descricao"];
                                conteudo.Username = (string)dbReader["Username"];
                                conteudo.Nome = (string)dbReader["Nome"];
                                conteudo.IsForEstudante = (bool)dbReader["IsForEstudante"];
                                conteudo.UsernameCriacao = (string)dbReader["UsernameCriacao"];
                                conteudo.DataCriacao = (DateTime)dbReader["DataCriacao"];
                                conteudo.UsernameAlteracao = (string)dbReader["UsernameAlteracao"];
                                conteudo.DataAlteracao = dbReader["DataAlteracao"] == System.DBNull.Value ? null : (DateTime?)dbReader["DataAlteracao"];
                                conteudo.Estado = (int)dbReader["Estado"];
                                conteudo.DataEstado = dbReader["DataEstado"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataEstado"];
                                conteudo.DataInicio = dbReader["DataInicio"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataInicio"];
                                conteudo.HoraInicio = (TimeSpan)dbReader["HoraInicio"];
                                conteudo.DataFim = dbReader["DataFim"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFim"];
                                conteudo.HoraFim = (TimeSpan)dbReader["HoraFim"];
                                conteudo.DataInicioPublicacao = dbReader["DataInicioPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataInicioPublicacao"];
                                conteudo.DataFimPublicacao = dbReader["DataFimPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFimPublicacao"];
                                conteudo.PrimeiraPagina = (bool)dbReader["PrimeiraPagina"];
                                conteudo.AprovacaoPrimeiraPagina = (bool)dbReader["AprovacaoPrimeiraPagina"];
                                conteudo.UsernameAprovacaoPrimeiraPagina = (string)dbReader["UsernameAprovacaoPrimeiraPagina"];
                                conteudo.DataAprovacaoPrimeiraPagina = dbReader["DataAprovacaoPrimeiraPagina"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataAprovacaoPrimeiraPagina"];
                                conteudo.NomeFoto = (string)dbReader["NomeFoto"];
                                conteudo.LinkFoto = (string)dbReader["LinkFoto"];
                                conteudo.IdTipoConteudo = (int)dbReader["IdTipoConteudo"];
                                conteudo.NomeTipoConteudo = (string)dbReader["NomeTipoConteudo"];
                                conteudo.IdOrgao = (int)dbReader["IdOrgao"];
                                conteudo.NomeOrgao = (string)dbReader["NomeOrgao"];
                                conteudo.CodigoOrgao = (string)dbReader["CodigoOrgao"];
                                conteudo.Acao = (string)dbReader["Acao"];
                                conteudo.LabelData1 = (string)dbReader["LabelData1"];
                                conteudo.Data1 = dbReader["Data1"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data1"];
                                conteudo.LabelData2 = (string)dbReader["LabelData2"];
                                conteudo.Data2 = dbReader["Data2"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data2"];
                                conteudo.LabelData3 = (string)dbReader["LabelData3"];
                                conteudo.Data3 = dbReader["Data3"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data3"];
                                conteudo.LabelData4 = (string)dbReader["LabelData4"];
                                conteudo.Data4 = dbReader["Data4"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data4"];
                                conteudo.LabelData5 = dbReader["LabelData5"].ToString();
                                conteudo.Data5 = dbReader["Data5"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data5"];
                                conteudo.LabelHora1 = (string)dbReader["LabelHora1"];
                                conteudo.Hora1 = (TimeSpan)dbReader["Hora1"];
                                conteudo.LabelHora2 = dbReader["LabelHora2"].ToString();
                                conteudo.Hora2 = (TimeSpan)dbReader["Hora2"];
                                conteudo.LabelHora3 = dbReader["LabelHora3"].ToString();
                                conteudo.Hora3 = (TimeSpan)dbReader["Hora3"];
                                conteudo.LabelHora4 = dbReader["LabelHora4"].ToString();
                                conteudo.Hora4 = (TimeSpan)dbReader["Hora4"];
                                conteudo.LabelHora5 = dbReader["LabelHora5"].ToString();
                                conteudo.Hora5 = (TimeSpan)dbReader["Hora5"];
                                conteudo.LabelTitulo1 = (string)dbReader["LabelTitulo1"];
                                conteudo.Titulo1 = (string)dbReader["Titulo1"];
                                conteudo.LabelTitulo2 = (string)dbReader["LabelTitulo2"];
                                conteudo.Titulo2 = (string)dbReader["Titulo2"];
                                conteudo.LabelTitulo3 = (string)dbReader["LabelTitulo3"];
                                conteudo.Titulo3 = (string)dbReader["Titulo3"];
                                conteudo.LabelTitulo4 = (string)dbReader["LabelTitulo4"];
                                conteudo.Titulo4 = (string)dbReader["Titulo4"];
                                conteudo.LabelTitulo5 = (string)dbReader["LabelTitulo5"];
                                conteudo.Titulo5 = (string)dbReader["Titulo5"];
                                conteudo.LabelTitulo6 = (string)dbReader["LabelTitulo6"];
                                conteudo.Titulo6 = (string)dbReader["Titulo6"];
                                conteudo.LabelTitulo7 = (string)dbReader["LabelTitulo7"];
                                conteudo.Titulo7 = (string)dbReader["Titulo7"];
                                conteudo.LabelDescricao1 = (string)dbReader["LabelDescricao1"];
                                conteudo.Descricao1 = (string)dbReader["Descricao1"];
                                conteudo.LabelDescricao2 = (string)dbReader["LabelDescricao2"];
                                conteudo.Descricao2 = (string)dbReader["Descricao2"];
                                conteudo.LabelDescricao3 = (string)dbReader["LabelDescricao3"];
                                conteudo.Descricao3 = (string)dbReader["Descricao3"];
                                conteudo.LabelDescricao4 = (string)dbReader["LabelDescricao4"];
                                conteudo.Descricao4 = (string)dbReader["Descricao4"];
                                conteudo.LabelDescricao5 = (string)dbReader["LabelDescricao5"];
                                conteudo.Descricao5 = (string)dbReader["Descricao5"];
                                conteudo.LabelDescricao6 = (string)dbReader["LabelDescricao6"];
                                conteudo.Descricao6 = (string)dbReader["Descricao6"];
                                conteudo.LabelDescricao7 = (string)dbReader["LabelDescricao7"];
                                conteudo.Descricao7 = (string)dbReader["Descricao7"];
                                conteudo.LabelLink1 = (string)dbReader["LabelLink1"];
                                conteudo.Link1 = (string)dbReader["Link1"];
                                conteudo.LabelLink2 = (string)dbReader["LabelLink2"];
                                conteudo.Link2 = (string)dbReader["Link2"];
                                conteudo.LabelLink3 = (string)dbReader["LabelLink3"];
                                conteudo.Link3 = (string)dbReader["Link3"];
                                conteudo.LabelLink4 = (string)dbReader["LabelLink4"];
                                conteudo.Link4 = (string)dbReader["Link4"];
                                conteudo.LabelLink5 = (string)dbReader["LabelLink5"];
                                conteudo.Link5 = (string)dbReader["Link5"];
                                conteudo.LabelLink6 = (string)dbReader["LabelLink6"];
                                conteudo.Link6 = (string)dbReader["Link6"];
                                conteudo.LabelLink7 = (string)dbReader["LabelLink7"];
                                conteudo.Link7 = (string)dbReader["Link7"];
                                conteudo.LabelLink8 = (string)dbReader["LabelLink8"];
                                conteudo.Link8 = (string)dbReader["Link8"];
                                conteudo.LabelLink9 = (string)dbReader["LabelLink9"];
                                conteudo.Link9 = (string)dbReader["Link9"];
                                conteudo.LabelLink10 = (string)dbReader["LabelLink10"];
                                conteudo.Link10 = (string)dbReader["Link10"];
                                conteudo.LabelValor1 = (string)dbReader["LabelValor1"];
                                conteudo.Valor1 = Convert.ToSingle(dbReader["Valor1"]);
                                conteudo.LabelValor2 = dbReader["LabelValor2"].ToString();
                                conteudo.Valor2 = Convert.ToSingle(dbReader["Valor2"]);
                                conteudo.LabelValor3 = dbReader["LabelValor3"].ToString();
                                conteudo.Valor3 = Convert.ToSingle(dbReader["Valor3"]);
                                conteudo.LabelValor4 = dbReader["LabelValor4"].ToString();
                                conteudo.Valor4 = Convert.ToSingle(dbReader["Valor4"]);
                                conteudo.LabelValor5 = (string)dbReader["LabelValor5"];
                                conteudo.Valor5 = Convert.ToSingle(dbReader["Valor5"]);
                                conteudo.LabelCheck1 = (string)dbReader["LabelCheck1"];
                                conteudo.Check1 = dbReader["Check1"] == System.DBNull.Value ? null : (bool?)dbReader["Check1"];
                                conteudo.VisibilidadePublica = (bool)dbReader["VisibilidadePublica"];
                                conteudo.PerfisVisibilidade = dbReader["PerfisVisibilidade"].ToString();
                                conteudo.TiposCargosVisibilidade = dbReader["TiposCargosVisibilidade"].ToString();
                                conteudo.OrgaosVisibilidade = dbReader["OrgaosVisibilidade"].ToString();
                                conteudo.PessoasVisibilidade = dbReader["PessoasVisibilidade"].ToString();
                                conteudo.GruposQuoVadisVisibilidade = dbReader["GruposQuoVadisVisibilidade"].ToString();


                                conteudo.PerfisVisibilidadeList = conteudo.PerfisVisibilidade == "" ? new List<string>() : conteudo.PerfisVisibilidade.Split(';').ToList();
                                conteudo.TiposCargosVisibilidadeList = conteudo.TiposCargosVisibilidade == "" ? new List<string>() : conteudo.TiposCargosVisibilidade.Split(';').ToList();
                                conteudo.OrgaosVisibilidadeList = conteudo.OrgaosVisibilidade == "" ? new List<string>() : conteudo.OrgaosVisibilidade.Split(';').ToList();
                                conteudo.PessoasVisibilidadeList = conteudo.PessoasVisibilidade == "" ? new List<string>() : conteudo.PessoasVisibilidade.Split(';').ToList();
                                conteudo.GruposQuoVadisVisibilidadeList = conteudo.GruposQuoVadisVisibilidade == "" ? new List<string>() : conteudo.GruposQuoVadisVisibilidade.Split(';').ToList();


                                conteudosList.Add(conteudo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return conteudosList;
        }

        public List<Conteudo> STP_Conteudos_S_ByEstado(int estado, bool visibilidadePublica)
        {
            List<Conteudo> conteudosList = new List<Conteudo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_ByEstado";
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Conteudo conteudo = new Conteudo();

                                conteudo.Id = (int)dbReader["Id"];
                                conteudo.IUPI = (Guid)dbReader["IUPI"];
                                conteudo.Titulo = (string)dbReader["Titulo"];
                                conteudo.Descricao = (string)dbReader["Descricao"];
                                conteudo.Username = (string)dbReader["Username"];
                                conteudo.Nome = (string)dbReader["Nome"];
                                conteudo.IsForEstudante = (bool)dbReader["IsForEstudante"];
                                conteudo.UsernameCriacao = (string)dbReader["UsernameCriacao"];
                                conteudo.DataCriacao = (DateTime)dbReader["DataCriacao"];
                                conteudo.UsernameAlteracao = (string)dbReader["UsernameAlteracao"];
                                conteudo.DataAlteracao = dbReader["DataAlteracao"] == System.DBNull.Value ? null : (DateTime?)dbReader["DataAlteracao"];
                                conteudo.Estado = (int)dbReader["Estado"];
                                conteudo.DataEstado = dbReader["DataEstado"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataEstado"];
                                conteudo.DataInicio = dbReader["DataInicio"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataInicio"];
                                conteudo.HoraInicio = (TimeSpan)dbReader["HoraInicio"];
                                conteudo.DataFim = dbReader["DataFim"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFim"];
                                conteudo.HoraFim = (TimeSpan)dbReader["HoraFim"];
                                conteudo.DataInicioPublicacao = dbReader["DataInicioPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataInicioPublicacao"];
                                conteudo.DataFimPublicacao = dbReader["DataFimPublicacao"] == System.DBNull.Value ? new DateTime() : (DateTime)dbReader["DataFimPublicacao"];
                                conteudo.PrimeiraPagina = (bool)dbReader["PrimeiraPagina"];
                                conteudo.AprovacaoPrimeiraPagina = (bool)dbReader["AprovacaoPrimeiraPagina"];
                                conteudo.UsernameAprovacaoPrimeiraPagina = (string)dbReader["UsernameAprovacaoPrimeiraPagina"];
                                conteudo.DataAprovacaoPrimeiraPagina = dbReader["DataAprovacaoPrimeiraPagina"] == System.DBNull.Value ? new DateTime() : (DateTime?)dbReader["DataAprovacaoPrimeiraPagina"];
                                conteudo.NomeFoto = (string)dbReader["NomeFoto"];
                                conteudo.LinkFoto = (string)dbReader["LinkFoto"];
                                conteudo.IdTipoConteudo = (int)dbReader["IdTipoConteudo"];
                                conteudo.NomeTipoConteudo = (string)dbReader["NomeTipoConteudo"];
                                conteudo.IdOrgao = (int)dbReader["IdOrgao"];
                                conteudo.NomeOrgao = (string)dbReader["NomeOrgao"];
                                conteudo.CodigoOrgao = (string)dbReader["CodigoOrgao"];
                                conteudo.Acao = (string)dbReader["Acao"];
                                conteudo.LabelData1 = (string)dbReader["LabelData1"];
                                conteudo.Data1 = dbReader["Data1"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data1"];
                                conteudo.LabelData2 = (string)dbReader["LabelData2"];
                                conteudo.Data2 = dbReader["Data2"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data2"];
                                conteudo.LabelData3 = (string)dbReader["LabelData3"];
                                conteudo.Data3 = dbReader["Data3"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data3"];
                                conteudo.LabelData4 = (string)dbReader["LabelData4"];
                                conteudo.Data4 = dbReader["Data4"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data4"];
                                conteudo.LabelData5 = dbReader["LabelData5"].ToString();
                                conteudo.Data5 = dbReader["Data5"] == System.DBNull.Value ? null : (DateTime?)dbReader["Data5"];
                                conteudo.LabelHora1 = (string)dbReader["LabelHora1"];
                                conteudo.Hora1 = (TimeSpan)dbReader["Hora1"];
                                conteudo.LabelHora2 = dbReader["LabelHora2"].ToString();
                                conteudo.Hora2 = (TimeSpan)dbReader["Hora2"];
                                conteudo.LabelHora3 = dbReader["LabelHora3"].ToString();
                                conteudo.Hora3 = (TimeSpan)dbReader["Hora3"];
                                conteudo.LabelHora4 = dbReader["LabelHora4"].ToString();
                                conteudo.Hora4 = (TimeSpan)dbReader["Hora4"];
                                conteudo.LabelHora5 = dbReader["LabelHora5"].ToString();
                                conteudo.Hora5 = (TimeSpan)dbReader["Hora5"];
                                conteudo.LabelTitulo1 = (string)dbReader["LabelTitulo1"];
                                conteudo.Titulo1 = (string)dbReader["Titulo1"];
                                conteudo.LabelTitulo2 = (string)dbReader["LabelTitulo2"];
                                conteudo.Titulo2 = (string)dbReader["Titulo2"];
                                conteudo.LabelTitulo3 = (string)dbReader["LabelTitulo3"];
                                conteudo.Titulo3 = (string)dbReader["Titulo3"];
                                conteudo.LabelTitulo4 = (string)dbReader["LabelTitulo4"];
                                conteudo.Titulo4 = (string)dbReader["Titulo4"];
                                conteudo.LabelTitulo5 = (string)dbReader["LabelTitulo5"];
                                conteudo.Titulo5 = (string)dbReader["Titulo5"];
                                conteudo.LabelTitulo6 = (string)dbReader["LabelTitulo6"];
                                conteudo.Titulo6 = (string)dbReader["Titulo6"];
                                conteudo.LabelTitulo7 = (string)dbReader["LabelTitulo7"];
                                conteudo.Titulo7 = (string)dbReader["Titulo7"];
                                conteudo.LabelDescricao1 = (string)dbReader["LabelDescricao1"];
                                conteudo.Descricao1 = (string)dbReader["Descricao1"];
                                conteudo.LabelDescricao2 = (string)dbReader["LabelDescricao2"];
                                conteudo.Descricao2 = (string)dbReader["Descricao2"];
                                conteudo.LabelDescricao3 = (string)dbReader["LabelDescricao3"];
                                conteudo.Descricao3 = (string)dbReader["Descricao3"];
                                conteudo.LabelDescricao4 = (string)dbReader["LabelDescricao4"];
                                conteudo.Descricao4 = (string)dbReader["Descricao4"];
                                conteudo.LabelDescricao5 = (string)dbReader["LabelDescricao5"];
                                conteudo.Descricao5 = (string)dbReader["Descricao5"];
                                conteudo.LabelDescricao6 = (string)dbReader["LabelDescricao6"];
                                conteudo.Descricao6 = (string)dbReader["Descricao6"];
                                conteudo.LabelDescricao7 = (string)dbReader["LabelDescricao7"];
                                conteudo.Descricao7 = (string)dbReader["Descricao7"];
                                conteudo.LabelLink1 = (string)dbReader["LabelLink1"];
                                conteudo.Link1 = (string)dbReader["Link1"];
                                conteudo.LabelLink2 = (string)dbReader["LabelLink2"];
                                conteudo.Link2 = (string)dbReader["Link2"];
                                conteudo.LabelLink3 = (string)dbReader["LabelLink3"];
                                conteudo.Link3 = (string)dbReader["Link3"];
                                conteudo.LabelLink4 = (string)dbReader["LabelLink4"];
                                conteudo.Link4 = (string)dbReader["Link4"];
                                conteudo.LabelLink5 = (string)dbReader["LabelLink5"];
                                conteudo.Link5 = (string)dbReader["Link5"];
                                conteudo.LabelLink6 = (string)dbReader["LabelLink6"];
                                conteudo.Link6 = (string)dbReader["Link6"];
                                conteudo.LabelLink7 = (string)dbReader["LabelLink7"];
                                conteudo.Link7 = (string)dbReader["Link7"];
                                conteudo.LabelLink8 = (string)dbReader["LabelLink8"];
                                conteudo.Link8 = (string)dbReader["Link8"];
                                conteudo.LabelLink9 = (string)dbReader["LabelLink9"];
                                conteudo.Link9 = (string)dbReader["Link9"];
                                conteudo.LabelLink10 = (string)dbReader["LabelLink10"];
                                conteudo.Link10 = (string)dbReader["Link10"];
                                conteudo.LabelValor1 = (string)dbReader["LabelValor1"];
                                conteudo.Valor1 = Convert.ToSingle(dbReader["Valor1"]);
                                conteudo.LabelValor2 = dbReader["LabelValor2"].ToString();
                                conteudo.Valor2 = Convert.ToSingle(dbReader["Valor2"]);
                                conteudo.LabelValor3 = dbReader["LabelValor3"].ToString();
                                conteudo.Valor3 = Convert.ToSingle(dbReader["Valor3"]);
                                conteudo.LabelValor4 = dbReader["LabelValor4"].ToString();
                                conteudo.Valor4 = Convert.ToSingle(dbReader["Valor4"]);
                                conteudo.LabelValor5 = (string)dbReader["LabelValor5"];
                                conteudo.Valor5 = Convert.ToSingle(dbReader["Valor5"]);
                                conteudo.LabelCheck1 = (string)dbReader["LabelCheck1"];
                                conteudo.Check1 = dbReader["Check1"] == System.DBNull.Value ? null : (bool?)dbReader["Check1"];
                                conteudo.VisibilidadePublica = (bool)dbReader["VisibilidadePublica"];
                                conteudo.PerfisVisibilidade = dbReader["PerfisVisibilidade"].ToString();
                                conteudo.TiposCargosVisibilidade = dbReader["TiposCargosVisibilidade"].ToString();
                                conteudo.OrgaosVisibilidade = dbReader["OrgaosVisibilidade"].ToString();
                                conteudo.PessoasVisibilidade = dbReader["PessoasVisibilidade"].ToString();
                                conteudo.GruposQuoVadisVisibilidade = dbReader["GruposQuoVadisVisibilidade"].ToString();


                                conteudo.PerfisVisibilidadeList = conteudo.PerfisVisibilidade == "" ? new List<string>() : conteudo.PerfisVisibilidade.Split(';').ToList();
                                conteudo.TiposCargosVisibilidadeList = conteudo.TiposCargosVisibilidade == "" ? new List<string>() : conteudo.TiposCargosVisibilidade.Split(';').ToList();
                                conteudo.OrgaosVisibilidadeList = conteudo.OrgaosVisibilidade == "" ? new List<string>() : conteudo.OrgaosVisibilidade.Split(';').ToList();
                                conteudo.PessoasVisibilidadeList = conteudo.PessoasVisibilidade == "" ? new List<string>() : conteudo.PessoasVisibilidade.Split(';').ToList();
                                conteudo.GruposQuoVadisVisibilidadeList = conteudo.GruposQuoVadisVisibilidade == "" ? new List<string>() : conteudo.GruposQuoVadisVisibilidade.Split(';').ToList();


                                conteudosList.Add(conteudo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return conteudosList;
        }


        #endregion

        #region ConteudoAnexo

        public int? STP_ConteudosAnexos_I(ConteudoAnexo conteudoAnexo)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosAnexos_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_iupi", conteudoAnexo.IUPI));
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", conteudoAnexo.IdConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_iupiConteudo", conteudoAnexo.IUPIConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_anexo", conteudoAnexo.Anexo));
                        dbCommand.Parameters.Add(new SqlParameter("@_tipo", conteudoAnexo.Tipo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public int? STP_ConteudosAnexos_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosAnexos_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return rowCount;
        }

        public int? STP_ConteudosAnexos_D_ByConteudo(int idConteudo)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosAnexos_D_ByConteudo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", idConteudo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return rowCount;
        }

        public List<ConteudoAnexo> STP_ConteudosAnexos_S_ByConteudo(int idConteudo)
        {
            List<ConteudoAnexo> conteudosAnexosList = new List<ConteudoAnexo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosAnexos_S_ByConteudo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", idConteudo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                ConteudoAnexo conteudoAnexo = new ConteudoAnexo();

                                conteudoAnexo.Id = dbReader.GetInt32(0);
                                conteudoAnexo.IUPI = dbReader.GetGuid(1);
                                conteudoAnexo.IdConteudo = idConteudo;
                                conteudoAnexo.IUPIConteudo = dbReader.GetGuid(2);
                                conteudoAnexo.Anexo = dbReader.GetString(3);
                                conteudoAnexo.Tipo = dbReader.GetInt32(4);

                                conteudosAnexosList.Add(conteudoAnexo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return conteudosAnexosList;
        }

        public List<ConteudoAnexo> STP_ConteudosAnexos_S_ByConteudoAndTipo(int idConteudo, int Tipo)
        {
            List<ConteudoAnexo> conteudosAnexosList = new List<ConteudoAnexo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosAnexos_S_ByConteudoAndTipo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", idConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_tipo", Tipo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                ConteudoAnexo conteudoAnexo = new ConteudoAnexo();

                                conteudoAnexo.Id = dbReader.GetInt32(0);
                                conteudoAnexo.IUPI = dbReader.GetGuid(1);
                                conteudoAnexo.IdConteudo = idConteudo;
                                conteudoAnexo.IUPIConteudo = dbReader.GetGuid(2);
                                conteudoAnexo.Anexo = dbReader.GetString(3);
                                conteudoAnexo.Tipo = dbReader.GetInt32(4);

                                conteudosAnexosList.Add(conteudoAnexo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return conteudosAnexosList;
        }

        #endregion

        #region Contactos

        public List<Contactos> STP_Contactos_LS()
        {
            List<Contactos> contactosList = new List<Contactos>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Contactos_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Contactos contacto = new Contactos();

                                contacto.Id = dbReader.GetInt32(0);
                                contacto.IdOrgao = dbReader.GetInt32(1);
                                contacto.NomeOrgao = dbReader.GetString(2);
                                contacto.CodigoOrgao = dbReader.GetString(3);
                                contacto.Titulo = GetSafeSqlValue(dbReader, 4) == null ? "" : GetSafeSqlValue(dbReader, 4).ToString();
                                contacto.Descricao = GetSafeSqlValue(dbReader, 5) == null ? "" : GetSafeSqlValue(dbReader, 5).ToString();
                                contacto.Site = GetSafeSqlValue(dbReader, 6) == null ? "" : GetSafeSqlValue(dbReader, 6).ToString();
                                contacto.Morada = GetSafeSqlValue(dbReader, 7) == null ? "" : GetSafeSqlValue(dbReader, 7).ToString();
                                contacto.Gabinete = GetSafeSqlValue(dbReader, 8) == null ? "" : GetSafeSqlValue(dbReader, 8).ToString();
                                contacto.Telefone = GetSafeSqlValue(dbReader, 9) == null ? "" : GetSafeSqlValue(dbReader, 9).ToString();
                                contacto.Extensao = GetSafeSqlValue(dbReader, 10) == null ? "" : GetSafeSqlValue(dbReader, 10).ToString();
                                contacto.Correioeletronico = GetSafeSqlValue(dbReader, 11) == null ? "" : GetSafeSqlValue(dbReader, 11).ToString();
                                contacto.Foto = GetSafeSqlValue(dbReader, 12) == null ? "" : GetSafeSqlValue(dbReader, 12).ToString();

                                contactosList.Add(contacto);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return contactosList;
        }

        public Contactos STP_Contacto_S(int id)
        {
            Contactos contacto = new Contactos();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Contactos_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                contacto.Id = dbReader.GetInt32(0);
                                contacto.IdOrgao = dbReader.GetInt32(1);
                                contacto.NomeOrgao = dbReader.GetString(2);
                                contacto.CodigoOrgao = dbReader.GetString(3);
                                contacto.Titulo = GetSafeSqlValue(dbReader, 4) == null ? "" : GetSafeSqlValue(dbReader, 4).ToString();
                                contacto.Descricao = GetSafeSqlValue(dbReader, 5) == null ? "" : GetSafeSqlValue(dbReader, 5).ToString();
                                contacto.Site = GetSafeSqlValue(dbReader, 6) == null ? "" : GetSafeSqlValue(dbReader, 6).ToString();
                                contacto.Morada = GetSafeSqlValue(dbReader, 7) == null ? "" : GetSafeSqlValue(dbReader, 7).ToString();
                                contacto.Gabinete = GetSafeSqlValue(dbReader, 8) == null ? "" : GetSafeSqlValue(dbReader, 8).ToString();
                                contacto.Telefone = GetSafeSqlValue(dbReader, 9) == null ? "" : GetSafeSqlValue(dbReader, 9).ToString();
                                contacto.Extensao = GetSafeSqlValue(dbReader, 10) == null ? "" : GetSafeSqlValue(dbReader, 10).ToString();
                                contacto.Correioeletronico = GetSafeSqlValue(dbReader, 11) == null ? "" : GetSafeSqlValue(dbReader, 11).ToString();
                                contacto.Foto = GetSafeSqlValue(dbReader, 12) == null ? "" : GetSafeSqlValue(dbReader, 12).ToString();
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return contacto;
        }

        public Contactos STP_Contacto_S_ByOrgao(int idOrgao)
        {
            Contactos contacto = new Contactos();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Contactos_S_ByOrgao";
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgao", idOrgao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                contacto.Id = dbReader.GetInt32(0);
                                contacto.IdOrgao = dbReader.GetInt32(1);
                                contacto.NomeOrgao = dbReader.GetString(2);
                                contacto.CodigoOrgao = dbReader.GetString(3);
                                contacto.Titulo = GetSafeSqlValue(dbReader, 4) == null ? "" : GetSafeSqlValue(dbReader, 4).ToString();
                                contacto.Descricao = GetSafeSqlValue(dbReader, 5) == null ? "" : GetSafeSqlValue(dbReader, 5).ToString();
                                contacto.Site = GetSafeSqlValue(dbReader, 6) == null ? "" : GetSafeSqlValue(dbReader, 6).ToString();
                                contacto.Morada = GetSafeSqlValue(dbReader, 7) == null ? "" : GetSafeSqlValue(dbReader, 7).ToString();
                                contacto.Gabinete = GetSafeSqlValue(dbReader, 8) == null ? "" : GetSafeSqlValue(dbReader, 8).ToString();
                                contacto.Telefone = GetSafeSqlValue(dbReader, 9) == null ? "" : GetSafeSqlValue(dbReader, 9).ToString();
                                contacto.Extensao = GetSafeSqlValue(dbReader, 10) == null ? "" : GetSafeSqlValue(dbReader, 10).ToString();
                                contacto.Correioeletronico = GetSafeSqlValue(dbReader, 11) == null ? "" : GetSafeSqlValue(dbReader, 11).ToString();
                                contacto.Foto = GetSafeSqlValue(dbReader, 12) == null ? "" : GetSafeSqlValue(dbReader, 12).ToString();
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return contacto;
        }

        public int STP_Contactos_IU( Contactos contacto)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Contactos_IU";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", contacto.Id));
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgao", contacto.IdOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_nomeOrgao", contacto.NomeOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_codigoOrgao", contacto.CodigoOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo", contacto.Titulo));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao", contacto.Descricao));
                        dbCommand.Parameters.Add(new SqlParameter("@_site", contacto.Site));
                        dbCommand.Parameters.Add(new SqlParameter("@_morada", contacto.Morada));
                        dbCommand.Parameters.Add(new SqlParameter("@_gabinete", contacto.Gabinete));
                        dbCommand.Parameters.Add(new SqlParameter("@_telefone", contacto.Telefone));
                        dbCommand.Parameters.Add(new SqlParameter("@_extensao", contacto.Extensao));
                        dbCommand.Parameters.Add(new SqlParameter("@_correioeletronico", contacto.Correioeletronico));
                        dbCommand.Parameters.Add(new SqlParameter("@_foto", contacto.Foto));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int STP_Contactos_U_Foto(int id, int idOrgao, string foto)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Contactos_U_Foto";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_idOrgao", idOrgao));
                        dbCommand.Parameters.Add(new SqlParameter("@_foto", foto));
                        
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        #endregion

        #region juris
        public List<Juri> STP_Juris_LS()
        {
            List<Juri> jurisList = new List<Juri>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Juris_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Juri juri = new Juri();

                                juri.Id = dbReader.GetInt32(0);
                                juri.IdConteudo = dbReader.GetInt32(1);
                                juri.Numero = dbReader.GetInt32(2);
                                juri.Username = GetSafeSqlValue(dbReader, 3) == null ? "" : GetSafeSqlValue(dbReader, 3).ToString();
                                juri.Nome = GetSafeSqlValue(dbReader, 4) == null ? "" : GetSafeSqlValue(dbReader, 4).ToString();
                                juri.Email = GetSafeSqlValue(dbReader, 5) == null ? "" : GetSafeSqlValue(dbReader, 5).ToString();
                                juri.Instituicao = GetSafeSqlValue(dbReader, 6) == null ? "" : GetSafeSqlValue(dbReader, 6).ToString();
                                juri.Externo = dbReader.GetBoolean(7);
                                juri.Tipo = dbReader.GetInt32(8);
                                

                                jurisList.Add(juri);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return jurisList;
        }

        public Juri STP_Juris_S(int id)
        {
            Juri juri = new Juri();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Juris_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                juri.Id = dbReader.GetInt32(0);
                                juri.IdConteudo = dbReader.GetInt32(1);
                                juri.Numero = dbReader.GetInt32(2);
                                juri.Username = GetSafeSqlValue(dbReader, 3) == null ? "" : GetSafeSqlValue(dbReader, 3).ToString();
                                juri.Nome = GetSafeSqlValue(dbReader, 4) == null ? "" : GetSafeSqlValue(dbReader, 4).ToString();
                                juri.Email = GetSafeSqlValue(dbReader, 5) == null ? "" : GetSafeSqlValue(dbReader, 5).ToString();
                                juri.Instituicao = GetSafeSqlValue(dbReader, 6) == null ? "" : GetSafeSqlValue(dbReader, 6).ToString();
                                juri.Externo = dbReader.GetBoolean(7);
                                juri.Tipo = dbReader.GetInt32(8);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return juri;
        }

        public List<Juri> STP_Juris_S_ByConteudo(int idConteudo)
        {
            List<Juri> jurisList = new List<Juri>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Juris_S_ByConteudo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", idConteudo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Juri juri = new Juri();

                                juri.Id = dbReader.GetInt32(0);
                                juri.IdConteudo = dbReader.GetInt32(1);
                                juri.Numero = dbReader.GetInt32(2);
                                juri.Username = GetSafeSqlValue(dbReader, 3) == null ? "" : GetSafeSqlValue(dbReader, 3).ToString();
                                juri.Nome = GetSafeSqlValue(dbReader, 4) == null ? "" : GetSafeSqlValue(dbReader, 4).ToString();
                                juri.Email = GetSafeSqlValue(dbReader, 5) == null ? "" : GetSafeSqlValue(dbReader, 5).ToString();
                                juri.Instituicao = GetSafeSqlValue(dbReader, 6) == null ? "" : GetSafeSqlValue(dbReader, 6).ToString();
                                juri.Externo = dbReader.GetBoolean(7);
                                juri.Tipo = dbReader.GetInt32(8);


                                jurisList.Add(juri);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return jurisList;
        }

        public int STP_Juris_I(Juri juri)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Juris_I";
                       
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", juri.IdConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_numero", juri.Numero));
                        dbCommand.Parameters.Add(new SqlParameter("@_username", juri.Username));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", juri.Nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_email", juri.Email));
                        dbCommand.Parameters.Add(new SqlParameter("@_instituicao", juri.Instituicao));
                        dbCommand.Parameters.Add(new SqlParameter("@_externo", juri.Externo));
                        dbCommand.Parameters.Add(new SqlParameter("@_tipo", juri.Tipo));

                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int STP_Juris_U(Juri juri)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Juris_U";

                        dbCommand.Parameters.Add(new SqlParameter("@_id", juri.Id));
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", juri.IdConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_numero", juri.Numero));
                        dbCommand.Parameters.Add(new SqlParameter("@_username", juri.Username));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", juri.Nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_email", juri.Email));
                        dbCommand.Parameters.Add(new SqlParameter("@_instituicao", juri.Instituicao));
                        dbCommand.Parameters.Add(new SqlParameter("@_externo", juri.Externo));
                        dbCommand.Parameters.Add(new SqlParameter("@_tipo", juri.Tipo));

                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int STP_Juris_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Juris_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return rowCount;
        }

        #endregion

        #region Pessoas
        public List<Pessoas> STP_Pessoas_LS()
        {
            List<Pessoas> pessoasList = new List<Pessoas>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Pessoas_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Pessoas pessoa = new Pessoas();

                                pessoa.Id = dbReader.GetInt32(0);
                                pessoa.IdConteudo = dbReader.GetInt32(1);
                                pessoa.Username = GetSafeSqlValue(dbReader, 2) == null ? "" : GetSafeSqlValue(dbReader, 2).ToString();
                                pessoa.Nome = GetSafeSqlValue(dbReader, 3) == null ? "" : GetSafeSqlValue(dbReader, 3).ToString();
                                pessoa.Email = GetSafeSqlValue(dbReader, 4) == null ? "" : GetSafeSqlValue(dbReader, 4).ToString();


                                pessoasList.Add(pessoa);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return pessoasList;
        }

        public Pessoas STP_Pessoas_S(int id)
        {
            Pessoas pessoa = new Pessoas();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Pessoas_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                pessoa.Id = dbReader.GetInt32(0);
                                pessoa.IdConteudo = dbReader.GetInt32(1);
                                pessoa.Username = GetSafeSqlValue(dbReader, 2) == null ? "" : GetSafeSqlValue(dbReader, 2).ToString();
                                pessoa.Nome = GetSafeSqlValue(dbReader, 3) == null ? "" : GetSafeSqlValue(dbReader, 3).ToString();
                                pessoa.Email = GetSafeSqlValue(dbReader, 4) == null ? "" : GetSafeSqlValue(dbReader, 4).ToString();
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return pessoa;
        }

        public List<Pessoas> STP_Pessoas_S_ByConteudo(int idConteudo)
        {
            List<Pessoas> pessoasList = new List<Pessoas>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Pessoas_S_ByConteudo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", idConteudo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Pessoas pessoa = new Pessoas();

                                pessoa.Id = dbReader.GetInt32(0);
                                pessoa.IdConteudo = dbReader.GetInt32(1);
                                pessoa.Username = GetSafeSqlValue(dbReader, 2) == null ? "" : GetSafeSqlValue(dbReader, 2).ToString();
                                pessoa.Nome = GetSafeSqlValue(dbReader, 3) == null ? "" : GetSafeSqlValue(dbReader, 3).ToString();
                                pessoa.Email = GetSafeSqlValue(dbReader, 4) == null ? "" : GetSafeSqlValue(dbReader, 4).ToString();


                                pessoasList.Add(pessoa);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return pessoasList;
        }

        public int STP_Pessoas_I(Pessoas pessoa)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Pessoas_I";

                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", pessoa.IdConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_username", pessoa.Username));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", pessoa.Nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_email", pessoa.Email));
                     

                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int STP_Pessoas_U(Pessoas pessoa)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Pessoas_U";

                        dbCommand.Parameters.Add(new SqlParameter("@_id", pessoa.Id));
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", pessoa.IdConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_username", pessoa.Username));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", pessoa.Nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_email", pessoa.Email));

                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int STP_Pessoas_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Pessoas_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return rowCount;
        }

        #endregion

        #region Conteudo_Categoria
        public List<ConteudoCategoria> STP_ConteudosCategorias_LS()
        {
            List<ConteudoCategoria> conteudosCategoriasList = new List<ConteudoCategoria>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosCategorias_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                ConteudoCategoria conteudoCategoria = new ConteudoCategoria();

                                conteudoCategoria.Id = dbReader.GetInt32(0);
                                conteudoCategoria.IdCategoria = dbReader.GetInt32(1);
                                conteudoCategoria.Nome = dbReader.GetString(2);
                                conteudoCategoria.IdConteudo = dbReader.GetInt32(3);
                                conteudoCategoria.Checked = dbReader.GetBoolean(4); ;
                                conteudoCategoria.DataAlteracao = dbReader.GetDateTime(5);
                                

                                conteudosCategoriasList.Add(conteudoCategoria);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return conteudosCategoriasList;
        }

        public List<ConteudoCategoria> STP_ConteudosCategorias_S_ByConteudo(int idConteudo)
        {
            List<ConteudoCategoria> conteudosCategoriasList = new List<ConteudoCategoria>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosCategorias_S_ByConteudo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", idConteudo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                ConteudoCategoria conteudoCategoria = new ConteudoCategoria();

                                conteudoCategoria.Id = dbReader.GetInt32(0);
                                conteudoCategoria.IdCategoria = dbReader.GetInt32(1);
                                conteudoCategoria.Nome = dbReader.GetString(2);
                                conteudoCategoria.IdConteudo = dbReader.GetInt32(3);
                                conteudoCategoria.Checked = dbReader.GetBoolean(4); ;
                                conteudoCategoria.DataAlteracao = dbReader.GetDateTime(5);


                                conteudosCategoriasList.Add(conteudoCategoria);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return conteudosCategoriasList;
        }
        public List<ConteudoCategoria> STP_ConteudosCategorias_S_ByConteudoAndEstado(int idConteudo, int estado, bool visibilidadePublica)
        {
            List<ConteudoCategoria> conteudosCategoriasList = new List<ConteudoCategoria>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosCategorias_S_ByConteudoAndEstado";
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", idConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                ConteudoCategoria conteudoCategoria = new ConteudoCategoria();

                                conteudoCategoria.Id = dbReader.GetInt32(0);
                                conteudoCategoria.IdCategoria = dbReader.GetInt32(1);
                                conteudoCategoria.Nome = dbReader.GetString(2);
                                conteudoCategoria.IdConteudo = dbReader.GetInt32(3);
                                conteudoCategoria.Checked = dbReader.GetBoolean(4); ;
                                conteudoCategoria.DataAlteracao = dbReader.GetDateTime(5);


                                conteudosCategoriasList.Add(conteudoCategoria);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return conteudosCategoriasList;
        }

        public List<ConteudoCategoria> STP_ConteudosCategorias_S_ByCategoria(int idCategoria, int estado, bool visibilidadePublica)
        {
            List<ConteudoCategoria> conteudosCategoriasList = new List<ConteudoCategoria>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosCategorias_S_ByCategoria";
                        dbCommand.Parameters.Add(new SqlParameter("@_idCategoria", idCategoria));
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                ConteudoCategoria conteudoCategoria = new ConteudoCategoria();

                                conteudoCategoria.Id = dbReader.GetInt32(0);
                                conteudoCategoria.IdCategoria = dbReader.GetInt32(1);
                                conteudoCategoria.Nome = dbReader.GetString(2);
                                conteudoCategoria.IdConteudo = dbReader.GetInt32(3);
                                conteudoCategoria.Checked = dbReader.GetBoolean(4); ;
                                conteudoCategoria.DataAlteracao = dbReader.GetDateTime(5);


                                conteudosCategoriasList.Add(conteudoCategoria);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return conteudosCategoriasList;
        }

        public ConteudoCategoria STP_ConteudosCategorias_S(int id)
        {
            ConteudoCategoria conteudoCategoria = new ConteudoCategoria();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosCategorias_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                conteudoCategoria.Id = dbReader.GetInt32(0);
                                conteudoCategoria.IdCategoria = dbReader.GetInt32(1);
                                conteudoCategoria.Nome = dbReader.GetString(2);
                                conteudoCategoria.IdConteudo = dbReader.GetInt32(3);
                                conteudoCategoria.Checked = dbReader.GetBoolean(4); ;
                                conteudoCategoria.DataAlteracao = dbReader.GetDateTime(5);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return conteudoCategoria;
        }

        public ConteudoCategoria STP_ConteudosCategorias_S_ByCategoriaAndConteudo(int idCategoria, int idConteudo)
        {
            ConteudoCategoria conteudoCategoria = new ConteudoCategoria();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosCategorias_S_ByCategoriaAndConteudo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idCategoria", idCategoria));
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", idConteudo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                conteudoCategoria.Id = dbReader.GetInt32(0);
                                conteudoCategoria.IdCategoria = dbReader.GetInt32(1);
                                conteudoCategoria.Nome = dbReader.GetString(2);
                                conteudoCategoria.IdConteudo = dbReader.GetInt32(3);
                                conteudoCategoria.Checked = dbReader.GetBoolean(4); ;
                                conteudoCategoria.DataAlteracao = dbReader.GetDateTime(5);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return conteudoCategoria;
        }

        public int STP_ConteudosCategoria_IU(ConteudoCategoria conteudoCategoria)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosCategorias_IU";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", conteudoCategoria.Id));
                        dbCommand.Parameters.Add(new SqlParameter("@_idCategoria", conteudoCategoria.IdCategoria));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", conteudoCategoria.Nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_idConteudo", conteudoCategoria.IdConteudo));
                        dbCommand.Parameters.Add(new SqlParameter("@_checked", conteudoCategoria.Checked));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataAlteracao", conteudoCategoria.DataAlteracao));

                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int STP_ConteudosCategorias_S_Count(int idCategoria)
        {
            int contador = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_ConteudosCategorias_S_Count";
                        dbCommand.Parameters.Add(new SqlParameter("@_idCategoria", idCategoria));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {                              

                                contador = dbReader.GetInt32(0);                               
                                
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return contador;
        }

        #endregion

        #region Categoria

        public List<Categoria> STP_Categoria_LS()
        {
            List<Categoria> categoriasList = new List<Categoria>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Categorias_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Categoria categoria = new Categoria();

                                categoria.Id = dbReader.GetInt32(0);                            
                                categoria.Nome = dbReader.GetString(1);
                                categoria.Codigo = GetSafeSqlValue(dbReader, 2) == null ? "" : GetSafeSqlValue(dbReader, 2).ToString();
                                categoria.Descricao = GetSafeSqlValue(dbReader, 3) == null ? "" : GetSafeSqlValue(dbReader, 3).ToString();

                                categoriasList.Add(categoria);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return categoriasList;
        }

        public Categoria STP_Categorias_S(int id)
        {
            Categoria categoria = new Categoria();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Categorias_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {                               
                                categoria.Id = dbReader.GetInt32(0);
                                categoria.Nome = dbReader.GetString(1);
                                categoria.Codigo = GetSafeSqlValue(dbReader, 2) == null ? "" : GetSafeSqlValue(dbReader, 2).ToString();
                                categoria.Descricao = GetSafeSqlValue(dbReader, 3) == null ? "" : GetSafeSqlValue(dbReader, 3).ToString();

                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return categoria;
        }

        public Categoria STP_Categorias_S_ByCodigo(string codigo)
        {
            Categoria categoria = new Categoria();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Categorias_S_ByCodigo";
                        dbCommand.Parameters.Add(new SqlParameter("@_codigo", codigo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                categoria.Id = dbReader.GetInt32(0);
                                categoria.Nome = dbReader.GetString(1);
                                categoria.Codigo = GetSafeSqlValue(dbReader, 2) == null ? "" : GetSafeSqlValue(dbReader, 2).ToString();
                                categoria.Descricao = GetSafeSqlValue(dbReader, 3) == null ? "" : GetSafeSqlValue(dbReader, 3).ToString();

                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return categoria;
        }

        public List<Categoria> STP_Conteudos_S_CategoriasByEstado(int estado, bool visibilidadePublica)
        {
            List<Categoria> categoriasList = new List<Categoria>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Conteudos_S_CategoriasByEstado";
                        dbCommand.Parameters.Add(new SqlParameter("@_estado", estado));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidadePublica", visibilidadePublica));

                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Categoria categoria = new Categoria();


                                categoria.Id = dbReader.GetInt32(0);
                                categoria.Nome = dbReader.GetString(1);

                                categoriasList.Add(categoria);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return categoriasList;
        }

        public int STP_Categorias_I(string nome, string descricao, string codigo)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Categorias_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_codigo", codigo));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao", descricao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int STP_Categorias_U(int id, string nome, string descricao, string codigo)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Categorias_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_codigo", codigo));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao", descricao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int STP_Categorias_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_Categorias_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return rowCount;
        }

        #endregion


        public List<AnoLetivo> STP_AnoLetivo_LS()
        {
            List<AnoLetivo> anosLetivos = new List<AnoLetivo>();
            try
            {
                using (SqlConnection con = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand cmd = new SqlCommand("stp_AnoLetivo_LS", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        con.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read() == true)
                            {
                                AnoLetivo aux = new AnoLetivo();
                                aux.Id = (int)reader["id"];
                                aux.Ano = (int)reader["anoletivo"];
                                aux.DataInicio = reader["dataInicio"] == System.DBNull.Value ? null : (DateTime?)reader["dataInicio"];
                                aux.DataFim = reader["dataFim"] == System.DBNull.Value ? null : (DateTime?)reader["dataFim"];
                                aux.Ativo = (bool)reader["ativo"];


                                anosLetivos.Add(aux);
                            }

                        }
                        con.Close();

                    }
                }
            }
            catch (SqlException ex)
            {
            }
            return anosLetivos;
        }

        public AnoLetivo STP_AnoLetivo_S(int id)
        {
            AnoLetivo anoLetivo = new AnoLetivo();
            try
            {
                using (SqlConnection con = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand cmd = new SqlCommand("stp_AnoLetivo_S", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                        con.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read() == true)
                            {
                                anoLetivo.Id = (int)reader["id"];
                                anoLetivo.Ano = (int)reader["anoletivo"];
                                anoLetivo.DataInicio = reader["dataInicio"] == System.DBNull.Value ? null : (DateTime?)reader["dataInicio"];
                                anoLetivo.DataFim = reader["dataFim"] == System.DBNull.Value ? null : (DateTime?)reader["dataFim"];
                                anoLetivo.Ativo = (bool)reader["ativo"];

                            }

                        }
                        con.Close();

                    }
                }
            }
            catch (SqlException ex)
            {
            }
            return anoLetivo;
        }

        public int STP_AnoLetivo_U(AnoLetivo anoLetivo)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.Connection = dbConn;
                        dbCommand.CommandText = "stp_AnoLetivo_U";

                        dbCommand.Parameters.Add(new SqlParameter("@id", anoLetivo.Id));
                        dbCommand.Parameters.Add(new SqlParameter("@anoLetivo", anoLetivo.Ano));
                        dbCommand.Parameters.Add(new SqlParameter("@dataInicio", anoLetivo.DataInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@dataFim", anoLetivo.DataFim));
                        dbCommand.Parameters.Add(new SqlParameter("@ativo", anoLetivo.Ativo));



                        dbConn.Open();
                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();

                        
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int STP_AnoLetivo_I(AnoLetivo anoLetivo)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.Connection = dbConn;
                        dbCommand.CommandText = "stp_AnoLetivo_I";

                        dbCommand.Parameters.Add(new SqlParameter("@anoLetivo", anoLetivo.Ano));
                        dbCommand.Parameters.Add(new SqlParameter("@dataInicio", anoLetivo.DataInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@dataFim", anoLetivo.DataFim));
                        dbCommand.Parameters.Add(new SqlParameter("@ativo", anoLetivo.Ativo));


                        dbConn.Open();
                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();

                      
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }
        public int STP_AnoLetivo_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "stp_AnoLetivo_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return rowCount;
        }

        public List<Ano> STP_Conteudos_S_Anos()
        {
            List<Ano> anos = new List<Ano>();
            try
            {
                using (SqlConnection con = new SqlConnection(intranetDBString))
                {
                    using (SqlCommand cmd = new SqlCommand("stp_Conteudos_S_Anos", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        con.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read() == true)
                            {
                                Ano aux = new Ano();
                               
                                aux.Year = (int)reader["Year"];
                                aux.Count = (int)reader["Count"];


                                anos.Add(aux);
                            }

                        }
                        con.Close();

                    }
                }
            }
            catch (SqlException ex)
            {
            }
            return anos;
        }

        #endregion
    }

}