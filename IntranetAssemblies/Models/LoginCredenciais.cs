using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models
{
    public class LoginCredenciais
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public LoginCredenciais()
        {
            Username = "";
            Password = "";
        }
    }
}
