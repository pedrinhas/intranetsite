﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.SIGACAD
{
    public class Plano
    {
        public int CodPlano { get; set; }
        public string Legislacao { get; set; }
        public int CodCurso { get; set; }
        public string Etiqueta { get; set; }
        public DateTime DataCriacao { get; set; }
        public int CodEstadoActual { get; set; }
        public string EstadoActual { get; set; }
        public int CodEstado { get; set; }
        public string AnoLectivo { get; set; }
        public DateTime Data { get; set; }
        public string EstadoAnoLectivo { get; set; }
        public int IdRamo { get; set; }

        public Plano()
        {
            CodPlano = 0;
            Legislacao = "";
            CodCurso = 0;
            Etiqueta = "";
            DataCriacao = new DateTime();
            CodEstadoActual = 0;
            EstadoActual = "";
            CodEstado = 0;
            AnoLectivo = "";
            Data = new DateTime();
            EstadoAnoLectivo = "";
            IdRamo = 0;
        }
    }
}
