﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.SIGACAD
{
    public class UC
    {
        public int AA { get; set; }
        public int AC { get; set; }
        public int TipoModuloTematico { get; set; }
        public int IdAssDisciplinaModTematico { get; set; }
        public int CodDisciplinaCodMod { get; set; }
        public string NomeDisciplinaMod { get; set; }
        public int IdAssRamoDisciplina { get; set; }
        public int IdRamo { get; set; }
        public int CodDisciplinaCod { get; set; }
        public string NomeDisciplina { get; set; }
        public int TipoDisciplina { get; set; }
        public int AnoCurricular { get; set; }
        public int PeriodoAula { get; set; }
        public int Uc { get; set; }
        public int ECTS { get; set; }
        public int EstadoActual { get; set; }
        public double Teoricas { get; set; }
        public double TPraticas { get; set; }
        public double Praticas { get; set; }
        public double OT { get; set; }
        public double TC { get; set; }
        public double E { get; set; }
        public double S { get; set; }
        public double PL { get; set; }
        public double O { get; set; }
        public string Dep { get; set; }
        public int TipoDisciplinaAux { get; set; }
        public string SiglaPeriodo { get; set; }
        public int ECTS1Sem { get; set; }
        public int ECTS2Sem { get; set; }

        public UC()
        {
            AA = 0;
            AC = 0;
            TipoModuloTematico = 0;
            IdAssDisciplinaModTematico = 0;
            CodDisciplinaCodMod = 0;
            NomeDisciplinaMod = "";
            IdAssRamoDisciplina = 0;
            IdRamo = 0;
            CodDisciplinaCod = 0;
            NomeDisciplina = "";
            TipoDisciplina = 0;
            AnoCurricular = 0;
            PeriodoAula = 0;
            Uc = 0;
            ECTS = 0;
            EstadoActual = 0;
            Teoricas = 0.0;
            TPraticas = 0.0;
            Praticas = 0.0;
            OT = 0.0;
            TC = 0.0;
            E = 0.0;
            S = 0.0;
            PL = 0.0;
            O = 0.0;
            Dep = "";
            TipoDisciplinaAux = 0;
            SiglaPeriodo = "";
            ECTS1Sem = 0;
            ECTS2Sem = 0;
        }
    }
}
