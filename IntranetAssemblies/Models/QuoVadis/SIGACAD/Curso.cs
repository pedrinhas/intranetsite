﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.SIGACAD
{
    public class Curso
    {
        public string CodInterno { get; set; }
        public DateTime DataCriacao { get; set; }
        public string Sigla { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int IdTipoCurso { get; set; }
        public string TipoCurso { get; set; }
        public int EstadoAtual { get; set; }
        public string Estado { get; set; }
        public string CodEscola { get; set; }
        public string SiglaEscola { get; set; }
        public string NomeEscola { get; set; }
        public string CodNacionalEscola { get; set; }
        public string NomeDepartamento { get; set; }
        public string AbrevDepartamento { get; set; }
        public int CirculoDepartamento { get; set; }
        public string DescricaoDepartamento { get; set; }
        public int IdGrauCurso { get; set; }
        public string GrauCurso { get; set; }
        public string TituloTipoCurso { get; set; }
        public string TituloFTipoCurso { get; set; }
        public string SiglaTipoCurso { get; set; }
        public string NomeIng { get; set; }
        public int IdTipoLecionacao { get; set; }
        public string TipoLecionacao { get; set; }
        public string NomeAltCertidao { get; set; }
        public string NomeAltCertidaoIng { get; set; }
        public string NomeParaCartaoEstudante { get; set; }
        public string CodNacional { get; set; }
        public string DiarioRepublica { get; set; }
        public DateTime DataDiario { get; set; }
        public int MinNumAnos { get; set; }
        public int AreaCNAEF { get; set; }
        public string NVCCNAEFI { get; set; }
        public string NVCCNAEFII { get; set; }
        public string NVCCNAEFIII { get; set; }
        public bool Ativo { get; set; }
        public string SiglaArea { get; set; }
        public string NomeArea { get; set; }
        public string NomeAreaIng { get; set; }
        public int IdAreaFormacao { get; set; }
        public string NomeAreaFormacao { get; set; }
        public string NomeAreaFormacaoIng { get; set; }

        public Curso()
        {

        }
    }
}
