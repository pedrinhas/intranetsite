using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Pessoas
{
    public class Pessoa
    {
        public Guid Iupi { get; set; }
        public string Nome { get; set; }

        public List<PessoaPerfil> PessoaPerfisList { get; set; }

        public Pessoa()
        {
            Iupi = new Guid();
            Nome = "";

            PessoaPerfisList = new List<PessoaPerfil>();
        }
    }
}
