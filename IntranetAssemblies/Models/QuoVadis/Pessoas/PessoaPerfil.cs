using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Pessoas
{
    public class PessoaPerfil
    {
        public string Categoria { get; set; }
        public int VisibilidadeCategoria { get; set; }

        public string Uo { get; set; }
        public int VisibilidadeUo { get; set; }

        public PessoaPerfil()
        {
            Categoria = "";
            VisibilidadeCategoria = 0;

            Uo = "";
            VisibilidadeUo = 0;
        }
    }
}
