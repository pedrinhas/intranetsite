using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Models.QuoVadis.Cargos
{
    public class Cargo
    {
        public int Id { get; set; }
        public int IdTipoCargo { get; set; }
        public string TipoCargo { get; set; }
        public string CodigoTipoCargo { get; set; }
        public int IdOrgao { get; set; }
        public string Orgao { get; set; }
        public string CodigoOrgao { get; set; }
        public string UnidadeOrganica { get; set; }
        public string CodigoUnidadeOrganica { get; set; }
        public string Pessoa { get; set; }
        public string PessoaDelegacao { get; set; }
        public DateTime DataAdmissao { get; set; }
        public string Documento { get; set; }
        public Guid Iupi { get; set; }
        public bool Ativo { get; set; }
        public bool Delegado { get; set; }

        public Cargo()
        {
            Id = 0;
            IdTipoCargo = 0;
            TipoCargo = "";
            CodigoTipoCargo = "";
            IdOrgao = 0;
            Orgao = "";
            CodigoOrgao = "";
            UnidadeOrganica = "";
            CodigoUnidadeOrganica = "";
            Pessoa = "";
            PessoaDelegacao = "";
            DataAdmissao = new DateTime();
            Documento = "";
            Iupi = new Guid();
            Ativo = true;
            Delegado = false;
        }
    }
}