using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Cargos
{
    public class TipoCargo
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public string Codigo { get; set; }

        public TipoCargo()
        {
            Id = 0;
            Nome = "";
            Ativo = false;
            Codigo = "";
        }
    }
}
