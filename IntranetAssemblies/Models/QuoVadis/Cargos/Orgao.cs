﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntranetAssemblies.Extensions;

namespace IntranetAssemblies.Models.QuoVadis.Cargos
{
    public class Orgao : ITreeItem<Orgao>
    {
        public int Id { get; set; }
        public int IdTipoOrgao { get; set; }
        public string Nome { get; set; }
        public int IdOrgaoParent { get; set; }
        public string Contato { get; set; }
        public string Morada { get; set; }
        public bool Ativo { get; set; }
        public string Codigo { get; set; }
        public string Grupos { get; set; }
        public string GrupoSecretariado { get; set; }
        public string CodigoRH { get; set; }
        public string Extensao { get; set; }

        public string TipoOrgao { get; set; }
        public string CodigoTipoOrgao { get; set; }

        public string OrgaoParent { get; set; }

        public Orgao()
        {
            Id = 0;
            IdTipoOrgao = 0;
            Nome = "";
            IdOrgaoParent = 0;
            Contato = "";
            Morada = "";
            Ativo = true;
            Codigo = "";
            Grupos = "";
            GrupoSecretariado = "";
            CodigoRH = "";
            Extensao = "";

            TipoOrgao = "";
            CodigoTipoOrgao = "";

            OrgaoParent = "";
        }

        public IList<Orgao> Children { get; set; } = new List<Orgao>();

#if DEBUG //teste
        public override string ToString() => Nome;
#endif
    }
}
