﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Cargos
{
    public class TipoConteudo
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string IntranetDetailsConteudoForm { get; set; }
        public string IntranetCreateConteudoForm { get; set; }
        public string IntranetEditConteudoForm { get; set; }

        public TipoConteudo()
        {
            Id = 0;
            Nome = "";
            Descricao = "";
            IntranetDetailsConteudoForm = "";
            IntranetCreateConteudoForm = "";
            IntranetEditConteudoForm = "";
        }
    }
}
