using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Grupos
{
    public class Utilizador
    {
        public int Id { get; set; }
        public Guid Iupi { get; set; }
        public string Nome { get; set; }
        public string Username { get; set; }
        public string OriginalEmail { get; set; }
        public bool Ativo { get; set; }

        public Utilizador()
        {
            Id = 0;
            Iupi = new Guid();
            Nome = "";
            Username = "";
            OriginalEmail = "";
            Ativo = true;
        }
    }
}
