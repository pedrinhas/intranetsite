using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Grupos
{
    public class Grupo
    {
        public int IdGrupos { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Condicoes { get; set; }
        public int Tipo { get; set; }

        public Grupo()
        {
            IdGrupos = 0;
            Nome = "";
            Descricao = "";
            Condicoes = "";
            Tipo = 0;
        }
    }
}
