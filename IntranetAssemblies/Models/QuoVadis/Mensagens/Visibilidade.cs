using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Mensagens
{
    public class Visibilidade
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public Visibilidade()
        {
            Id = 0;
            Nome = "";
        }

        public Visibilidade(int id, string nome)
        {
            Id = id;
            Nome = nome;
        }
    }
}
