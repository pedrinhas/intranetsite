using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Mensagens
{
    public class Mensagem
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public DateTime? DataPublicacaoInicio { get; set; }
        public DateTime? DataPublicacaoFim { get; set; }
        public int Visibilidade { get; set; }
        public string PerfisPublicacao { get; set; }
        public string TiposCargosPublicacao { get; set; }
        public string OrgaosPublicacao { get; set; }
        public string PessoasPublicacao { get; set; }

        public bool IsFromQuoVadis { get; set; }

        public Mensagem()
        {
            Id = 0;
            Titulo = "";
            Texto = "";
            DataPublicacaoInicio = null;
            DataPublicacaoFim = null;
            Visibilidade = 0;
            PerfisPublicacao = "";
            TiposCargosPublicacao = "";
            OrgaosPublicacao = "";
            PessoasPublicacao = "";

            IsFromQuoVadis = false;
        }
    }
}
