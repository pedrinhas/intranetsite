﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.DSD
{
    public class CursoDiretor
    {
        public int CodCurso { get; set; }
        public string Curso { get; set; }
        public string IDCoordenador { get; set; }
        public string NMecDiretor { get; set; }
        public string Diretor { get; set; }
        public string CatDiretor { get; set; }
        public string DepDiretor { get; set; }
        public string IDViceCoordenador { get; set; }
        public string NMecViceDiretor { get; set; }
        public string ViceDiretor { get; set; }
        public string CatViceDiretor { get; set; }
        public string DepViceDiretor { get; set; }
        public string IDVogal { get; set; }
        public string NMecVogal { get; set; }
        public string Vogal { get; set; }
        public string CatVogal { get; set; }
        public string DepVogal { get; set; }

        public CursoDiretor()
        {
            CodCurso = 0;
            Curso = "";
            IDCoordenador = "";
            NMecDiretor = "";
            Diretor = "";
            CatDiretor = "";
            DepDiretor = "";
            IDViceCoordenador = "";
            NMecViceDiretor = "";
            ViceDiretor = "";
            CatViceDiretor = "";
            DepViceDiretor = "";
            IDVogal = "";
            NMecVogal = "";
            Vogal = "";
            CatVogal = "";
            DepVogal = "";
        }
    }
}
