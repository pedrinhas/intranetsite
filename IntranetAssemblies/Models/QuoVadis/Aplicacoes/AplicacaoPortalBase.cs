using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Aplicacoes
{
    public class AplicacaoPortalBase
    {
        public string IdentificadorPortalBase { get; set; }
        public string SiglaPortalBase { get; set; }
        public string CorPortalBase { get; set; }
        public string GruposMenuPortalBase { get; set; }
        public string TextoDashboard { get; set; }
        public string EndpointDashboard { get; set; }
        public DateTime? DataInicioDashboard { get; set; }
        public DateTime? DataFimDashboard { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Link { get; set; }
        public string LinkAutenticado { get; set; }

        public AplicacaoPortalBase()
        {
            IdentificadorPortalBase = "";
            SiglaPortalBase = "";
            CorPortalBase = "";
            GruposMenuPortalBase = "";
            TextoDashboard = "";
            EndpointDashboard = "";
            DataInicioDashboard = null;
            DataFimDashboard = null;
            Nome = "";
            Descricao = "";
            Link = "";
            LinkAutenticado = "";
        }
    }
}
