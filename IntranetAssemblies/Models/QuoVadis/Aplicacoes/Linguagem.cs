using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Models.QuoVadis.Aplicacoes
{
    public class Linguagem
    {
        public int Id { get; set; }
        public string Ref { get; set; }
        public string Symbol { get; set; }
        public string Titulo { get; set; }

        public Linguagem()
        {
            Id = 0;
            Ref = "";
            Symbol = "";
            Titulo = "";
        }
    }
}