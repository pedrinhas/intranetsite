using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class CredenciaisTemporarias
    {
        public int Id { get; set; }
        public string NMec { get; set; }
        public Guid IUPI { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime DataCriacao { get; set; }
        public string SistemaCriacao { get; set; }
        public DateTime? DataValidade { get; set; }
        public string CodigoTipoPerfil { get; set; }
        public string TipoPerfil { get; set; }
        public string DefaultLanguage { get; set; }

        public CredenciaisTemporarias()
        {
            Id = 0;
            NMec = "";
            IUPI = new Guid();
            Nome = "";
            Email = "";
            Username = "";
            Password = "";
            DataCriacao = new DateTime();
            SistemaCriacao = "";
            DataValidade = null;
            CodigoTipoPerfil = "";
            TipoPerfil = "";
            DefaultLanguage = "";
        }
    }
}
