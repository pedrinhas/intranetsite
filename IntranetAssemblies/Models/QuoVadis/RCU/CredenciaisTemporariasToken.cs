using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class CredenciaisTemporariasToken
    {
        public int Id { get; set; }
        public int IdCredenciaisTemporarias { get; set; }
        public Guid Token { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataExpiracao { get; set; }
        public bool EstadoUso { get; set; }

        public CredenciaisTemporariasToken()
        {
            Id = 0;
            IdCredenciaisTemporarias = 0;
            Token = new Guid();
            DataCriacao = new DateTime();
            DataExpiracao = new DateTime();
            EstadoUso = false;
        }
    }
}
