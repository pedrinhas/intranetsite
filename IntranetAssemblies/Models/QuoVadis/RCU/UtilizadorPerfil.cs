using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class UtilizadorPerfil
    {
        public int Id { get; set; }
        public int IdPerfil { get; set; }
        public Guid? IUPI { get; set; }
        public string Username { get; set; }
        public string Nome { get; set; }
        public string NomePersonalizado { get; set; }
        public string NMec { get; set; }
        public string Extensao { get; set; }
        public int Visibilidade { get; set; }
        public string NIF { get; set; }
        public bool ContaNoGIAF { get; set; }
        public string Email { get; set; }
        public string OriginalEmail { get; set; }
        public string AliasedEmail { get; set; }
        public int VisibilidadePerfil { get; set; }
        public DateTime DataEntrada { get; set; }
        public DateTime DataSaida { get; set; }

        public int IdPerfilTipo { get; set; }
        public string PerfilTipoCodigo { get; set; }
        public string PerfilTipo { get; set; }
        public string PerfilTipoDescricao { get; set; }

        public int IdCarreira { get; set; }
        public string CodCarreira { get; set; }
        public string Carreira { get; set; }
        public string CarreiraDescricao { get; set; }

        public int IdCategoria { get; set; }
        public string CodCategoria { get; set; }
        public string Categoria { get; set; }
        public string CategoriaDescricao { get; set; }

        public int IdRegime { get; set; }
        public string CodRegime { get; set; }
        public string Regime { get; set; }
        public string RegimeDescricao { get; set; }

        public int IdEntidade { get; set; }
        public string Entidade { get; set; }
        public string EntidadeDescricao { get; set; }
        public string Sigla { get; set; }
        public string CodEntidade { get; set; }

        public bool PerfilAtivo { get; set; }
        public bool UtilizadorAtivo { get; set; }

        public UtilizadorPerfil()
        {
            Id = 0;
            IdPerfil = 0;
            IUPI = new Guid();
            Username = "";
            Nome = "";
            NomePersonalizado = "";
            NMec = "";
            Extensao = "";
            Visibilidade = 0;
            NIF = "";
            ContaNoGIAF = true;
            Email = "";
            OriginalEmail = "";
            AliasedEmail = "";
            VisibilidadePerfil = 0;
            DataEntrada = new DateTime();
            DataSaida = new DateTime();

            IdPerfilTipo = 0;
            PerfilTipoCodigo = "";
            PerfilTipo = "";
            PerfilTipoDescricao = "";

            IdCarreira = 0;
            CodCarreira = "";
            Carreira = "";
            CarreiraDescricao = "";

            IdCategoria = 0;
            CodCategoria = "";
            Categoria = "";
            CategoriaDescricao = "";

            IdRegime = 0;
            CodRegime = "";
            Regime = "";
            RegimeDescricao = "";

            IdEntidade = 0;
            Entidade = "";
            EntidadeDescricao = "";
            Sigla = "";
            CodEntidade = "";

            PerfilAtivo = true;
            UtilizadorAtivo = true;
        }
    }
}