using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Drawing;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class EstudanteFoto
    {
        public int Numero { get; set; }
        public byte[] Foto { get; set; }
        public DateTime DataFoto { get; set; }
        public int Tipo { get; set; }

        public EstudanteFoto()
        {
            Numero = 0;
            Foto = null;
            DataFoto = new DateTime();
            Tipo = 0;
        }
    }
}