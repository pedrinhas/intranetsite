using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class CredenciaisTemporariasUpdate
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string DefaultLanguage { get; set; }

        public CredenciaisTemporariasUpdate()
        {
            Id = 0;
            Nome = "";
            Email = "";
            DefaultLanguage = "";
        }
    }
}
