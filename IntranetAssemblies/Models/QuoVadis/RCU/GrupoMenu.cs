using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class GrupoMenu
    {
        public int Id { get; set; }
        public string Grupo { get; set; }
        public string Codigo { get; set; }

        public GrupoMenu()
        {
            Id = 0;
            Grupo = "";
            Codigo = "";
        }
    }
}
