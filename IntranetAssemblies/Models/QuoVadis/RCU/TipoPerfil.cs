using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class TipoPerfil
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Sigla { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataCriado { get; set; }
        public string UtilizadorCriado { get; set; }
        public DateTime DataModificado { get; set; }
        public string UtilizadorModificado { get; set; }

        public TipoPerfil()
        {
            Id = 0;
            Codigo = "";
            Sigla = "";
            Nome = "";
            Descricao = "";
            Ativo = false;
            DataCriado = new DateTime();
            UtilizadorCriado = "";
            DataModificado = new DateTime();
            UtilizadorModificado = "";
        }
    }
}
