using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class NomeAndIUPI
    {
        public string Nome { get; set; }
        public Guid IUPI { get; set; }

        public NomeAndIUPI()
        {
            Nome = "";
            IUPI = new Guid();
        }
    }
}
