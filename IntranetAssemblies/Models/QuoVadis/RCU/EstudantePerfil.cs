using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class EstudantePerfil
    {
        public int Numero { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string AnoLectivo { get; set; }
        public DateTime DataMatricula { get; set; }
        public string IdEstado { get; set; }
        public string Estado { get; set; }
        public string CodCurso { get; set; }
        public string Curso { get; set; }
        public string CursoIngles { get; set; }
        public string SiglaCurso { get; set; }
        public string IdTipoCurso { get; set; }
        public string TipoCurso { get; set; }
        public string Titulo { get; set; }
        public DateTime DataNotaFimCurso { get; set; }
        public int AnoLectivoFimCurso { get; set; }
        public double Nota { get; set; }
        public int IdMatricula { get; set; }
        public string TipoMatricula { get; set; }
        public string IdTipoMatricula { get; set; }
        public string TipoAcesso { get; set; }
        public string NomeAcesso { get; set; }
        public int CodPlano { get; set; }
        public int IdRamo { get; set; }
        public DateTime DataInicioEstadoMatricula { get; set; }

        public EstudantePerfil()
        {
            Numero = 0;
            Nome = "";
            Email = "";
            AnoLectivo = "";
            DataMatricula = new DateTime();
            IdEstado = "";
            Estado = "";
            CodCurso = "";
            Curso = "";
            CursoIngles = "";
            SiglaCurso = "";
            IdTipoCurso = "";
            TipoCurso = "";
            Titulo = "";
            DataNotaFimCurso = new DateTime();
            AnoLectivoFimCurso = 0;
            Nota = 0.0;
            IdMatricula = 0;
            TipoMatricula = "";
            IdTipoMatricula = "";
            TipoAcesso = "";
            NomeAcesso = "";
            CodPlano = 0;
            IdRamo = 0;
            DataInicioEstadoMatricula = new DateTime();
        }
    }
}