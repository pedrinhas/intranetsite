using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class CredenciaisTemporariasPasswordUpdate
    {
        public int Id { get; set; }
        public string Password { get; set; }

        public CredenciaisTemporariasPasswordUpdate()
        {
            Id = 0;
            Password = "";
        }
    }
}
