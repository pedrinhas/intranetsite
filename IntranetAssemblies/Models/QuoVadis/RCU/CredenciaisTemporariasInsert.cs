using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class CredenciaisTemporariasInsert
    {
        public string NMec { get; set; }
        public Guid IUPI { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Dominio { get; set; }
        public string Password { get; set; }
        public string SistemaCriacao { get; set; }
        public int? DiasValidade { get; set; }
        public string DefaultLanguage { get; set; }

        public CredenciaisTemporariasInsert()
        {
            NMec = "";
            IUPI = new Guid();
            Nome = "";
            Email = "";
            Dominio = "";
            Password = "";
            SistemaCriacao = "";
            DiasValidade = null;
            DefaultLanguage = "";
        }
    }
}
