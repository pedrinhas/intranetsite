using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.RCU
{
    public class CredenciaisTemporariasTokenInsert
    {
        public int IdCredenciaisTemporarias { get; set; }
        public int HorasValidade { get; set; }

        public CredenciaisTemporariasTokenInsert()
        {
            IdCredenciaisTemporarias = 0;
            HorasValidade = 0;
        }
    }
}
