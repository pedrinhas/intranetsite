﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.Dashboard
{
    public class Favorito
    {
        public int Id { get; set; }
        public Guid IUPI { get; set; }
        public string Nome { get; set; }
        public string Link { get; set; }
        public string SiglaDashboard { get; set; }
        public string CorDashboard { get; set; }

        public Favorito()
        {
            Id = 0;
            IUPI = new Guid();
            Nome = "";
            Link = "";
            SiglaDashboard = "";
            CorDashboard = "";
        }
    }
}
