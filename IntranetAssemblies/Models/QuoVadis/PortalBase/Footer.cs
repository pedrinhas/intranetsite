using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.QuoVadis.PortalBase
{
    public class Footer
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string FooterImage { get; set; }
        public string LinkContatos { get; set; }
        public string LinkPolPriv { get; set; }

        public Footer()
        {
            Id = 0;
            Nome = "";
            FooterImage = "";
            LinkContatos = "";
            LinkPolPriv = "";
        }
    }
}
