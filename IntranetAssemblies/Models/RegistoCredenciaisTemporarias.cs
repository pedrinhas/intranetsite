using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models
{
    public class RegistoCredenciaisTemporarias
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string EmailRepeticao { get; set; }
        public string DefaultLanguage { get; set; }

        public RegistoCredenciaisTemporarias()
        {
            Nome = "";
            Email = "";
            EmailRepeticao = "";
            DefaultLanguage = "";
        }
    }
}
