using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Models
{
    public class Login
    {
        public string Username { get; set; }
        public string Nome { get; set; }
        public Guid IUPI { get; set; }

        public Login()
        {
            Username = "";
            Nome = "";
            IUPI = new Guid();
        }
    }
}