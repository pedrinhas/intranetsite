using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Models
{
    public class SessionPerfil
    {
        public int Id { get; set; }
        public int Ano { get; set; }
        public string Tipo { get; set; }
        public string Categoria { get; set; }
        public string CodigoCategoria { get; set; }
        public string UnidadeOrganica { get; set; }
        public string CodigoUnidadeOrganica { get; set; }
        public bool Ativo { get; set; }

        public SessionPerfil()
        {
            Id = 0;
            Ano = 0;
            Tipo = "";
            Categoria = "";
            CodigoCategoria = "";
            UnidadeOrganica = "";
            CodigoUnidadeOrganica = "";
            Ativo = false;
        }
    }
}