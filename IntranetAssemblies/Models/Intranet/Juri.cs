﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class Juri
    {
        public int Id { get; set; }
        public int IdConteudo { get; set; }
        public int Numero { get; set; }
        public string Username { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Instituicao { get; set; }
        public bool Externo { get; set; }
        public int Tipo { get; set; }
        public Guid Guid { get; set; }

        public Juri()
        {
            Id = 0;
            IdConteudo = 0;
            Username = "";
            Nome = "";
            Email = "";
            Instituicao = "";
            Numero = 0;
            Tipo = 0; //0-Presidente 1-Vogal
            Externo = false;
            Guid = new Guid();
        }
    }

    public class Vogais
    {
        public List<Juri> VogaisList { get; set; }
        public int MaxVogais { get; set; }
        public int MinVogais { get; set; }
        public int NVogais { get; set; }

        public Vogais()
        {
            MaxVogais = 0;
            NVogais = 0;
            MinVogais = 0;
            VogaisList = new List<Juri>();
        }
    }
}
