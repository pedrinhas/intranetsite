﻿using IntranetAssemblies.Models.QuoVadis.Cargos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class Contactos
    {
        public int Id { get; set; }
        public int IdOrgao { get; set; }
        public string NomeOrgao { get; set; }
        public string CodigoOrgao { get; set; }

        [Display(Name = "Título")]
        public string Titulo { get; set; }
        [Display(Name = "Descrição")]
        [DataType(DataType.MultilineText)]
        public string Descricao { get; set; }
        [Display(Name = "Site da Unidade")]
        public string Site { get; set; }

        [DataType(DataType.MultilineText)]
        [MaxLength(1000)]
        public string Morada { get; set; }
        public string Gabinete { get; set; }
        //[Phone]
        public string Telefone { get; set; }
        [Display(Name = "Extensão")]
        public string Extensao { get; set; }
        [Display(Name = "Correio eletrónico")]
        //[EmailAddress]
        public string Correioeletronico { get; set; }

        //public string NomeFoto { get; set; }
        public string Foto { get; set; }
        //public long SizeFoto { get; set; }
        public string FotoAnexo { get; set; }


        public Contactos()
        {
            Id = 0;
            IdOrgao = 0;
            NomeOrgao = "";
            CodigoOrgao = "";
            Titulo = "";
            Descricao = "";
            Site = "";
            Morada = "";
            Gabinete = "";
            Telefone = "";
            Extensao = "";
            Correioeletronico = "";
            //NomeFoto = "";
            Foto = "";
            FotoAnexo = "";
            //SizeFoto = 0;

        }
    }
}
