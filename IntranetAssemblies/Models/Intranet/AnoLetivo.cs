﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class AnoLetivo
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Ano Letivo")]
        public int Ano { get; set; }
        [Display(Name = "Data de Início")]
        public DateTime? DataInicio { get; set; }
        [Display(Name = "Data de Fim")]
        public DateTime? DataFim { get; set; }
        public bool Ativo { get; set; }


        public AnoLetivo(int id, int ano)
        {
            Id = id;
            Ano = ano;
        }
        public AnoLetivo()
        {
            Id = 0;
            Ano = DateTime.Today.Year;
            DataInicio = null;
            DataFim = null;
            Ativo = false;
        }

    }
}
