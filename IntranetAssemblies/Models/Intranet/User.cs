using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public Guid IUPI { get; set; }
        public string Nome { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public bool IsFromUTAD { get; set; }

        public int Index { get; set; }
        public bool Checked { get; set; }

        public User()
        {
            Id = 0;
            Username = "";
            IUPI = new Guid();
            Nome = "";
            LastLoginDate = null;
            IsFromUTAD = true;

            Index = 0;
            Checked = false;
        }
    }
}
