﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class ConteudoHistorico
    {
        public Conteudo Conteudo { get; set; }

        public string UsernameHistorico { get; set; }
        public DateTime DataHistorico { get; set; }

        public ConteudoHistorico()
        {
            Conteudo = new Conteudo();

            UsernameHistorico = "";
            DataHistorico = new DateTime();
        }
    }
}
