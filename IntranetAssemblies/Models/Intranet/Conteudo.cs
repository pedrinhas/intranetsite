﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class Conteudo
    {
        public int Id { get; set; }
        public Guid IUPI { get; set; }
        [Display(Name = "Título")]
        public string Titulo { get; set; }
        [DataType(DataType.MultilineText)]
        //[MaxLength(5000)]
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }
        public string Username { get; set; }
        public string Nome { get; set; }
        public bool IsForEstudante { get; set; }
        public string UsernameCriacao { get; set; }
        public DateTime DataCriacao { get; set; }
        public string UsernameAlteracao { get; set; }
        public DateTime? DataAlteracao { get; set; }
        public int Estado { get; set; }
        public DateTime DataEstado { get; set; }
        [Display(Name = "Data de Início")]
        public DateTime? DataInicio { get; set; }
        [Display(Name = "Hora de Início")]
        public TimeSpan HoraInicio { get; set; }
        [Display(Name = "Data de Fim")]
        public DateTime? DataFim { get; set; }
        [Display(Name = "Hora de Fim")]
        public TimeSpan HoraFim { get; set; }
        [Display(Name = "Data de Início de Publicação")]
        public DateTime? DataInicioPublicacao { get; set; }
        [Display(Name = "Data de Fim de Publicação")]
        public DateTime? DataFimPublicacao { get; set; }
        public bool PrimeiraPagina { get; set; }
        public bool AprovacaoPrimeiraPagina { get; set; }
        public string UsernameAprovacaoPrimeiraPagina { get; set; }
        public DateTime? DataAprovacaoPrimeiraPagina { get; set; }
        public string NomeFoto { get; set; }
        public string LinkFoto { get; set; }
        public int IdTipoConteudo { get; set; }
        [Display(Name = "Tipo de Conteúdo")]
        public string NomeTipoConteudo { get; set; }
        public int IdOrgao { get; set; }
        [Display(Name = "Unidade Orgânica")]
        public string NomeOrgao { get; set; }
        public string CodigoOrgao { get; set; }
        public string Acao { get; set; }
        public string LabelData1 { get; set; }
        public DateTime? Data1 { get; set; }
        public string LabelData2 { get; set; }
        public DateTime? Data2 { get; set; }
        public string LabelData3 { get; set; }
        public DateTime? Data3 { get; set; }
        public string LabelData4 { get; set; }
        public DateTime? Data4 { get; set; }
        public string LabelData5 { get; set; }
        public DateTime? Data5 { get; set; }
        public string LabelHora1 { get; set; }
        public TimeSpan Hora1 { get; set; }
        public string LabelHora2 { get; set; }
        public TimeSpan Hora2 { get; set; }
        public string LabelHora3 { get; set; }
        public TimeSpan Hora3 { get; set; }
        public string LabelHora4 { get; set; }
        public TimeSpan Hora4 { get; set; }
        public string LabelHora5 { get; set; }
        public TimeSpan Hora5 { get; set; }
        public string LabelTitulo1 { get; set; }
        public string Titulo1 { get; set; }
        public string LabelTitulo2 { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(1000)]
        public string Titulo2 { get; set; }
        public string LabelTitulo3 { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(1000)]
        public string Titulo3 { get; set; }
        public string LabelTitulo4 { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(1000)]
        public string Titulo4 { get; set; }
        public string LabelTitulo5 { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(1000)]
        public string Titulo5 { get; set; }
        public string LabelTitulo6 { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(1000)]
        public string Titulo6 { get; set; }
        public string LabelTitulo7 { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(1000)]
        public string Titulo7 { get; set; }
        public string LabelDescricao1 { get; set; }
        [DataType(DataType.MultilineText)]
        //[MaxLength(5000)]
        public string Descricao1 { get; set; }
        public string LabelDescricao2 { get; set; }
        [DataType(DataType.MultilineText)]
        //[MaxLength(5000)]
        public string Descricao2 { get; set; }
        public string LabelDescricao3 { get; set; }
        [DataType(DataType.MultilineText)]
        //[MaxLength(5000)]
        public string Descricao3 { get; set; }
        public string LabelDescricao4 { get; set; }
        [DataType(DataType.MultilineText)]
        //[MaxLength(5000)]
        public string Descricao4 { get; set; }
        public string LabelDescricao5 { get; set; }
        [DataType(DataType.MultilineText)]
        //[MaxLength(5000)]
        public string Descricao5 { get; set; }
        public string LabelDescricao6 { get; set; }
        [DataType(DataType.MultilineText)]
        //[MaxLength(5000)]
        public string Descricao6 { get; set; }
        public string LabelDescricao7 { get; set; }
        [DataType(DataType.MultilineText)]
        //[MaxLength(5000)]
        public string Descricao7 { get; set; }
        public string LabelLink1 { get; set; }
        public string Link1 { get; set; }
        public string LabelLink2 { get; set; }
        public string Link2 { get; set; }
        public string LabelLink3 { get; set; }
        public string Link3 { get; set; }
        public string LabelLink4 { get; set; }
        public string Link4 { get; set; }
        public string LabelLink5 { get; set; }
        public string Link5 { get; set; }
        public string LabelLink6 { get; set; }
        public string Link6 { get; set; }
        public string LabelLink7 { get; set; }
        public string Link7 { get; set; }
        public string LabelLink8 { get; set; }
        public string Link8 { get; set; }
        public string LabelLink9 { get; set; }
        public string Link9 { get; set; }
        public string LabelLink10 { get; set; }
        public string Link10 { get; set; }
        public string LabelValor1 { get; set; }
        public float Valor1 { get; set; }
        public string LabelValor2 { get; set; }
        public float Valor2 { get; set; }
        public string LabelValor3 { get; set; }
        public float Valor3 { get; set; }
        public string LabelValor4 { get; set; }
        public float Valor4 { get; set; }
        public string LabelValor5 { get; set; }
        public float Valor5 { get; set; }
        public string LabelCheck1 { get; set; }
        public bool? Check1 { get; set; }
        public bool VisibilidadePublica { get; set; }
        public string PerfisVisibilidade { get; set; }
        public string TiposCargosVisibilidade { get; set; }
        public string OrgaosVisibilidade { get; set; }
        public string PessoasVisibilidade { get; set; }
        public string GruposQuoVadisVisibilidade { get; set; }

        //Para as provas públicas
        public Juri Presidente { get; set; }
        public Vogais Vogais { get; set; }

        //Para as licenças
        public List<Pessoas> Pessoas { get; set; }

        //Para as licenças
        public Pessoas Representante1 { get; set; }
        //Para as licenças
        public Pessoas Representante2 { get; set; }


        public List<string> PerfisVisibilidadeList { get; set; }
        public List<string> TiposCargosVisibilidadeList { get; set; }
        public List<string> OrgaosVisibilidadeList { get; set; }
        public List<string> PessoasVisibilidadeList { get; set; }
        public List<string> GruposQuoVadisVisibilidadeList { get; set; }

        public List<ConteudoCategoria> ConteudoCategoriasList { get; set; }

        public Conteudo()
        {
            Id = 0;
            IUPI = new Guid();
            Titulo = "";
            Descricao = "";
            Username = "";
            Nome = "";
            IsForEstudante = false;
            UsernameCriacao = "";
            DataCriacao = new DateTime();
            UsernameAlteracao = "";
            DataAlteracao = null;
            Estado = 0;
            DataEstado = new DateTime();
            DataInicio = null;
            HoraInicio = new TimeSpan();
            DataFim = null;
            HoraFim = new TimeSpan();
            DataInicioPublicacao = null;
            DataFimPublicacao = null;
            PrimeiraPagina = false;
            AprovacaoPrimeiraPagina = false;
            UsernameAprovacaoPrimeiraPagina = "";
            DataAprovacaoPrimeiraPagina = null;
            NomeFoto = "";
            LinkFoto = "";
            IdTipoConteudo = 0;
            NomeTipoConteudo = "";
            IdOrgao = 0;
            NomeOrgao = "";
            CodigoOrgao = "";
            Acao = "";
            LabelData1 = "";
            Data1 = null;
            LabelData2 = "";
            Data2 = null;
            LabelData3 = "";
            Data3 = null;
            LabelData4 = "";
            Data4 = null;
            LabelData5 = "";
            Data5 = null;
            LabelHora1 = "";
            Hora1 = new TimeSpan();
            LabelHora2 = "";
            Hora2 = new TimeSpan();
            LabelHora3 = "";
            Hora3 = new TimeSpan();
            LabelHora4 = "";
            Hora4 = new TimeSpan();
            LabelHora5 = "";
            Hora5 = new TimeSpan();
            LabelTitulo1 = "";
            Titulo1 = "";
            LabelTitulo2 = "";
            Titulo2 = "";
            LabelTitulo3 = "";
            Titulo3 = "";
            LabelTitulo4 = "";
            Titulo4 = "";
            LabelTitulo5 = "";
            Titulo5 = "";
            LabelTitulo6 = "";
            Titulo6 = "";
            LabelTitulo7 = "";
            Titulo7 = "";
            LabelDescricao1 = "";
            Descricao1 = "";
            LabelDescricao2 = "";
            Descricao2 = "";
            LabelDescricao3 = "";
            Descricao3 = "";
            LabelDescricao4 = "";
            Descricao4 = "";
            LabelDescricao5 = "";
            Descricao5 = "";
            LabelDescricao6 = "";
            Descricao6 = "";
            LabelDescricao7 = "";
            Descricao7 = "";
            LabelLink1 = "";
            Link1 = "";
            LabelLink2 = "";
            Link2 = "";
            LabelLink3 = "";
            Link3 = "";
            LabelLink4 = "";
            Link4 = "";
            LabelLink5 = "";
            Link5 = "";
            LabelLink6 = "";
            Link6 = "";
            LabelLink7 = "";
            Link7 = "";
            LabelLink8 = "";
            Link8 = "";
            LabelLink9 = "";
            Link9 = "";
            LabelLink10 = "";
            Link10 = "";
            LabelValor1 = "";
            Valor1 = 0;
            LabelValor2 = "";
            Valor2 = 0;
            LabelValor3 = "";
            Valor3 = 0;
            LabelValor4 = "";
            Valor4 = 0;
            LabelValor5 = "";
            Valor5 = 0;
            LabelCheck1 = "";
            Check1 = null;
            VisibilidadePublica = true;
            PerfisVisibilidade = "";
            TiposCargosVisibilidade = "";
            OrgaosVisibilidade = "";
            PessoasVisibilidade = "";
            GruposQuoVadisVisibilidade = "";
            PerfisVisibilidadeList = new List<string>();
            TiposCargosVisibilidadeList = new List<string>();
            OrgaosVisibilidadeList = new List<string>(); ;
            PessoasVisibilidadeList = new List<string>();
            GruposQuoVadisVisibilidadeList = new List<string>();
            ConteudoCategoriasList = new List<ConteudoCategoria>();

        }
    }

    public class Ano
    {
        public int Year { get; set; }
        public int Count { get; set; }

        public Ano()
        {
            Year = 0;
            Count = 0;
        }
    }
}
