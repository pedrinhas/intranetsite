﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class Categoria
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        [Display(Name = "Descrição")]
        [DataType(DataType.MultilineText)]
        public string Descricao { get; set; }
        [Display(Name = "Código")]
        public string Codigo { get; set; }

        public Categoria()
        {
            Id = 0;
            Nome = "";
            Descricao = "";
            Codigo = "";
        }
    }
}
