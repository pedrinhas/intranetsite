﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class ConteudoAnexoFile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public byte[] Content { get; set; }
        public string ContentType { get; set; }

        public bool WasDeletedByUser { get; set; }

        public ConteudoAnexoFile()
        {
            Id = 0;
            Name = "";
            Size = 0;
            Content = null;
            ContentType = "";
            WasDeletedByUser = false;
        }
    }
}
