﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class ConteudoAnexo
    {
        public int Id { get; set; }
        public Guid IUPI { get; set; }
        public int IdConteudo { get; set; }
        public Guid IUPIConteudo { get; set; }
        public string Anexo { get; set; }
        public int Tipo { get; set; }

        public ConteudoAnexo()
        {
            Id = 0;
            IUPI = new Guid();
            IdConteudo = 0;
            IUPIConteudo = new Guid();
            Anexo = "";
            Tipo = 0;
        }
    }
}
