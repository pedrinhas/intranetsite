﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class Link
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Nome { get; set; }
        public string URL { get; set; }
        public DateTime Data { get; set; }

        public Link()
        {
            Id = 0;
            Tipo = "";
            Nome = "";
            URL = "";
            Data = new DateTime();
        }
        public Link(int id, string tipo, string nome, string url)
        {
            Id = id;
            Tipo = tipo;
            Nome = nome;
            URL = url;
        }
    }
}
