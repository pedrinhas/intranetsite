using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class UserGrupo
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdGrupo { get; set; }

        public UserGrupo()
        {
            Id = 0;
            IdUser = 0;
            IdGrupo = 0;
        }
    }
}
