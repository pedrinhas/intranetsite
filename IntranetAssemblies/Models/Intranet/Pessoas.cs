﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class Pessoas
    {
        public int Id { get; set; }
        public int IdConteudo { get; set; }
        public string Username { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
      
        public Pessoas()
        {
            Id = 0;
            IdConteudo = 0;
            Username = "";
            Nome = "";
            Email = "";

        }

        public Pessoas(string _Username, string _Nome, string _Email)
        {
            Id = 0;
            IdConteudo = 0;
            Username = _Username;
            Nome = _Nome;
            Email = _Email;

        }
    }
}
