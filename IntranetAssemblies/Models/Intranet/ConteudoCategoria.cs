﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class ConteudoCategoria
    {
        public int Id { get; set; }
        public int IdCategoria { get; set; }
        public string Nome { get; set; }
        public int IdConteudo{ get; set; }
        public bool Checked { get; set; }
        public DateTime DataAlteracao { get; set; }

        public ConteudoCategoria()
        {
            Id = 0;
            IdCategoria = 0;
            Nome = "";
            IdConteudo = 0;
            Checked = false;
            DataAlteracao = new DateTime();
        }

        public ConteudoCategoria(int idCategoria, int idConteudo, string nome, bool check)
        {
            Id = 0;
            IdCategoria = idCategoria;
            Nome = nome;
            IdConteudo = idConteudo;
            Checked = check;
            DataAlteracao = DateTime.Now;
        }
    }
}
