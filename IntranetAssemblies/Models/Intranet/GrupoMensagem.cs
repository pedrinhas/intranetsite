using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class GrupoMensagem
    {
        public int Id { get; set; }
        public int IdGrupo { get; set; }
        public int IdMensagem { get; set; }

        public GrupoMensagem()
        {
            Id = 0;
            IdGrupo = 0;
            IdMensagem = 0;
        }
    }
}
