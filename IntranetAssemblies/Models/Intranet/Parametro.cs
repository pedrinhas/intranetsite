using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Models.Intranet
{
    public class Parametro
    {
        public int Id { get; set; }
        public string Chave { get; set; }
        public string Valor { get; set; }
        public string Valor1 { get; set; }
        public string Valor2 { get; set; }

        public Parametro()
        {
            Id = 0;
            Chave = "";
            Valor = "";
            Valor1 = "";
            Valor2 = "";
        }
    }
}