﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class ConteudoPropriedade
    {
        public string Nome { get; set; }
        public string Valor { get; set; }

        public ConteudoPropriedade()
        {
            Nome = "";
            Valor = "";
        }

        public ConteudoPropriedade(string nome, string valor)
        {
            Nome = nome;
            Valor = valor;
        }
    }
}
