using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetAssemblies.Models.Intranet
{
    public class GrupoAutorizacao
    {
        public int Id { get; set; }
        public int IdGrupo { get; set; }
        public int IdAutorizacao { get; set; }

        public GrupoAutorizacao()
        {
            Id = 0;
            IdGrupo = 0;
            IdAutorizacao = 0;
        }
    }
}
