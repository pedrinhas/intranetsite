using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Models
{
    public class MenuItem
    {
        public int Id { get; set; }
        public int MenuLevel { get; set; }
        public string Name { get; set; }
        public string Toggle { get; set; }
        public string Descricao { get; set; }

        public string Url { get; set; }
        public bool UrlIsOpenedInNewTab { get; set; }

        public string Icon { get; set; }

        public List<MenuItem> MenuItemChildList { get; set; }
        public bool MenuItemChildListIsOpen { get; set; }

        public MenuItem()
        {
            Id = 0;
            MenuLevel = 0;
            Name = "";
            Toggle = "";
            Descricao = "";

            Url = "";
            UrlIsOpenedInNewTab = false;

            Icon = "";

            MenuItemChildList = new List<MenuItem>();
            MenuItemChildListIsOpen = true;
        }

        public MenuItem(int menuLevel, string name, string toggle, string descricao, string url, string icon)
        {
            MenuLevel = menuLevel;
            Name = name;
            Toggle = toggle;
            Descricao = descricao;

            Url = url;
            UrlIsOpenedInNewTab = false;

            Icon = icon;

            MenuItemChildList = new List<MenuItem>();
            MenuItemChildListIsOpen = true;
        }

        public MenuItem(int menuLevel, string name, string toggle, string descricao, string url, bool urlIsOpenedInNewTab, string icon)
        {
            MenuLevel = menuLevel;
            Name = name;
            Toggle = toggle;
            Descricao = descricao;

            Url = url;
            UrlIsOpenedInNewTab = urlIsOpenedInNewTab;

            Icon = icon;

            MenuItemChildList = new List<MenuItem>();
            MenuItemChildListIsOpen = true;
        }

        public MenuItem(int menuLevel, string name, string toggle, string descricao, string icon, List<MenuItem> menuItemChildList)
        {
            MenuLevel = menuLevel;
            Name = name;
            Toggle = toggle;
            Descricao = descricao;
            Url = "";
            Icon = icon;

            MenuItemChildList = menuItemChildList;
            MenuItemChildListIsOpen = true;
        }

        public MenuItem(int menuLevel, string name, string toggle, string descricao, string icon, List<MenuItem> menuItemChildList, bool menuItemChildListIsOpen)
        {
            MenuLevel = menuLevel;
            Name = name;
            Toggle = toggle;
            Descricao = descricao;
            Url = "";
            Icon = icon;

            MenuItemChildList = menuItemChildList;
            MenuItemChildListIsOpen = menuItemChildListIsOpen;
        }
    }
}