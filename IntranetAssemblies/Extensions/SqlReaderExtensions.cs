﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Extensions
{
    public static class SqlReaderExtensions
    {
        /// <summary>
        /// The GetSafeSqlValue.
        /// </summary>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="i">The i<see cref="int"/>.</param>
        /// <returns>The <see cref="object"/>.</returns>
        public static object GetSafeSqlValue(this SqlDataReader dbReader, int i)
        {
            return GetSafeSqlValue<object>(dbReader, i);
        }

        /// <summary>
        /// The GetSafeSqlValue.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="i">The i<see cref="int"/>.</param>
        /// <returns>The <see cref="T"/>.</returns>
        public static T GetSafeSqlValue<T>(this SqlDataReader dbReader, int i)
        {
            if (i < 0 || i >= dbReader.FieldCount)
            {
                throw new ArgumentException($"Index out of range: {i}", nameof(i));
            }

            if (!dbReader.IsDBNull(i))
            {
                var dbVal = dbReader.GetValue(i);

                if (dbVal is T)
                {
                    return (T)dbVal;
                }
                try
                {
                    return (T)Convert.ChangeType(dbVal, typeof(T));
                }
                catch (FormatException)
                {

                }
                catch (InvalidCastException)
                {

                }
            }

            return default;
        }

        /// <summary>
        /// The GetSafeSqlValue.
        /// </summary>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="columnName">The columnName<see cref="string"/>.</param>
        /// <returns>The <see cref="object"/>.</returns>
        public static object GetSafeSqlValue(this SqlDataReader dbReader, string columnName)
        {
            return GetSafeSqlValue<object>(dbReader, columnName);
        }

        /// <summary>
        /// The GetSafeSqlValue.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="columnName">The columnName<see cref="string"/>.</param>
        /// <returns>The <see cref="T"/>.</returns>
        public static T GetSafeSqlValue<T>(this SqlDataReader dbReader, string columnName)
        {
            if (string.IsNullOrWhiteSpace(columnName))
            {
                throw new ArgumentNullException(nameof(columnName));
            }

            if (!HasColumn(dbReader, columnName))
            {
                return default;
                //throw new ArgumentException($"Invalid column name: {columnName}");
            }

            return GetSafeSqlValue<T>(dbReader, dbReader.GetOrdinal(columnName));
        }

        /// <summary>
        /// The GetSafeSqlDateTime.
        /// </summary>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="columnName">The columnName<see cref="string"/>.</param>
        /// <returns>The <see cref="DateTime"/>.</returns>
        public static DateTime? GetSafeSqlDateTime(this SqlDataReader dbReader, string columnName) => GetSafeSqlDateTime(dbReader, columnName, null);

        /// <summary>
        /// The GetSafeSqlDateTime.
        /// </summary>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="columnName">The columnName<see cref="string"/>.</param>
        /// <param name="expectedFormats">The expectedFormats<see cref="string[]"/>.</param>
        /// <returns>The <see cref="DateTime"/>.</returns>
        public static DateTime? GetSafeSqlDateTime(this SqlDataReader dbReader, string columnName, string[] expectedFormats)
        {
            if (string.IsNullOrWhiteSpace(columnName))
            {
                throw new ArgumentNullException(nameof(columnName));
            }

            if (!HasColumn(dbReader, columnName))
            {
                return default;
                //throw new ArgumentException($"Invalid column name: {columnName}");
            }

            return GetSafeSqlDateTime(dbReader, dbReader.GetOrdinal(columnName), expectedFormats);
        }

        /// <summary>
        /// The GetSafeSqlDateTime.
        /// </summary>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="i">The i<see cref="int"/>.</param>
        /// <returns>The <see cref="DateTime"/>.</returns>
        public static DateTime GetSafeSqlDateTime(this SqlDataReader dbReader, int i) => dbReader.GetSafeSqlDateTime(i, null);

        /// <summary>
        /// The GetSafeSqlDateTime.
        /// </summary>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="i">The i<see cref="int"/>.</param>
        /// <param name="expectedFormats">The expectedFormats<see cref="string[]"/>.</param>
        /// <returns>The <see cref="DateTime"/>.</returns>
        public static DateTime GetSafeSqlDateTime(this SqlDataReader dbReader, int i, string[] expectedFormats)
        {
            var rawValue = GetSafeSqlValue<string>(dbReader, i);

            if (!string.IsNullOrWhiteSpace(rawValue))
            {
                DateTime res;

                if (expectedFormats == null || !expectedFormats.Any())
                {
                    if (DateTime.TryParse(rawValue, out res))
                    {
                        return res;
                    }
                }
                else
                {
                    if (DateTime.TryParseExact(rawValue, expectedFormats.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray(), null, DateTimeStyles.None, out res))
                    {
                        return res;
                    }
                }
            }

            return default;
        }

        /// <summary>
        /// The HasColumn.
        /// </summary>
        /// <param name="dbReader">The dr<see cref="SqlDataReader"/>.</param>
        /// <param name="columnName">The columnName<see cref="string"/>.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        public static bool HasColumn(this SqlDataReader dbReader, string columnName)
        {
            for (int i = 0; i < dbReader.FieldCount; i++)
            {
                if (dbReader.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// The IsDBNull.
        /// </summary>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="columnName">The columnName<see cref="string"/>.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        public static bool IsDBNull(this SqlDataReader dbReader, string columnName)
        {
            if (string.IsNullOrWhiteSpace(columnName))
            {
                throw new ArgumentNullException(nameof(columnName));
            }

            if (!HasColumn(dbReader, columnName))
            {
                return default;
                //throw new ArgumentException($"Invalid column name: {columnName}");
            }

            return dbReader.IsDBNull(dbReader.GetOrdinal(columnName));
        }
    }
}