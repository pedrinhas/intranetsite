﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetAssemblies.Extensions
{
    public static class TreeExtensions
    {
        /// <summary>
        /// Generates tree of items from an item list. Uses a hard tree item class, containing a reference to an object and it's children sepparately
        /// </summary>
        /// 
        /// <typeparam name="T">Type of item in collection</typeparam>
        /// <typeparam name="K">Type of parent_id</typeparam>
        /// 
        /// <param name="collection">Collection of items</param>
        /// <param name="id_selector">Function extracting item's id</param>
        /// <param name="parent_id_selector">Function extracting item's parent_id</param>
        /// <param name="root_id">Root element id</param>
        /// 
        /// <returns>Tree of items</returns>
        public static IEnumerable<ITreeItemHard<T>> GenerateHardTree<T, K>(
            this IEnumerable<T> collection,
            Func<T, K> id_selector,
            Func<T, K> parent_id_selector,
            K root_id = default)
        {
            var enumerable = collection as T[] ?? collection.ToArray();
            foreach (var c in enumerable.Where(c => EqualityComparer<K>.Default.Equals(parent_id_selector(c), root_id)))
            {
                yield return new TreeItemHard<T>
                {
                    Item = c,
                    Children = enumerable.GenerateHardTree(id_selector, parent_id_selector, id_selector(c)).ToList()
                };
            }
        }


        /// <summary>
        /// Generates tree of items from an item list of something that implements ITreeItem (that is, it has a list of children internally)
        /// </summary>
        /// 
        /// <typeparam name="T">Type of item in collection</typeparam>
        /// <typeparam name="K">Type of parent_id</typeparam>
        /// 
        /// <param name="collection">Collection of items</param>
        /// <param name="id_selector">Function extracting item's id</param>
        /// <param name="parent_id_selector">Function extracting item's parent_id</param>
        /// <param name="root_id">Root element id</param>
        /// 
        /// <returns>Tree of items</returns>
        public static IEnumerable<T> GenerateTree<T, K>(
            this IEnumerable<T> collection,
            Func<T, K> id_selector,
            Func<T, K> parent_id_selector,
            K root_id = default)
        where T : ITreeItem<T>
        {
            var enumerable = collection as T[] ?? collection.ToArray();
            foreach (var c in enumerable.Where(c => EqualityComparer<K>.Default.Equals(parent_id_selector(c), root_id)))
            {
                c.Children = enumerable.GenerateTree(id_selector, parent_id_selector, id_selector(c)).ToList();

                yield return c;
            }
        }
    }

    public class TreeItemHard<T> : ITreeItemHard<T>
    {
        public T Item { get; set; }
        public IList<ITreeItemHard<T>> Children { get; set; } = new List<ITreeItemHard<T>>();
    }

    public interface ITreeItemHard<T>
    {
        T Item { get; set; }
        IList<ITreeItemHard<T>> Children { get; set; }
    }

    public interface ITreeItem<T>
    {
        IList<T> Children { get; set; }
    }
}